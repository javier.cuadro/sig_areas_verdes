// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  backend: {
    host: 'http://207.244.229.255:5011/api',
    //host: 'http://207.244.229.255:5006/api',
    //host: 'http://spladit-server1:5006/api'
  }, 
 
  
  backend_gis: {
    host: 'http://207.244.229.255:5012',
    //host: 'http://spladit-server1:5005'
  },

  backend_geoserver: {
    //host: 'http://spladit-server1:8090',
    host: 'http://207.244.229.255:9080',
  },

  backend_gis_php: {
    host: 'http://207.244.229.255:8085/apirestphp_pg',
    //host: 'http://localhost/apirestphp_pg',
  },
  backend_gis_php_mapa: {
    host: 'http://207.244.229.255:8085/apirestphp_pg_mapa_completo',
    //host: 'http://localhost/apirestphp_pg',
  },

  backend_qr: {
    //host: 'http://190.186.159.75:7777',
    host:'http://207.244.229.255:5006/api'
  },

  backend_VisorCapas: {
    //host: 'http://190.186.159.75:7777',
    host:'http://207.244.229.255:7000'
  },

  //http://207.244.229.255:5008/api

  oauth: {
    host: 'https://demo0034835.mockable.io',
    client_id: '2',
    client_secret: 'tsN80QNwTawD3WZSX2uziOFI6HstTEs2bXBqsCyv',
    scope: '*',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
