import { Injectable } from '@angular/core';
import { HttpBackend, HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import { HttpApi } from '../http/http-api';
import { ParametroCambio, ParametroPersonaFotos, ParametroPersonaUbicacion, ParametroUsuario, RespuestaLogin, RespuestaPersona, RespuestaUsuario, User } from 'src/app/auth/interfaces/auth.interface';
import { RespuestaNormal } from 'src/app/dashboard/interfaces/dashboard.interfaces';
import { ParametroPersonaDatos } from '../../auth/interfaces/auth.interface';

const OAUTH_DATA =environment.oauth;

@Injectable({
  providedIn: 'root'
})
export class AuthService {
 

  constructor(
    private handler: HttpBackend,
    private http: HttpClient
  ) {
    this.http = new HttpClient(handler); /// to skip interceptors, becouse this service hits third backend provider
  }

  actualizaDatos(parametroPersonaDatos: ParametroPersonaDatos) : Observable<RespuestaNormal> {
    const httpOptions = {
      headers: new HttpHeaders({
                  'Content-Type': 'application/json',
                  'Access-Control-Allow-Origin': '*'
      })
    };
    return this.http.put(`${environment.backend.host}/adm_usuarios/actualizadatos`, parametroPersonaDatos, httpOptions)
      .pipe(
        map((response: any) => {
          console.log(response);
          return response;
        })
      );
  }

  actualizaUbicacion(parametroPersonaUbicacion: ParametroPersonaUbicacion) : Observable<RespuestaNormal> {
    const httpOptions = {
      headers: new HttpHeaders({
                  'Content-Type': 'application/json',
                  'Access-Control-Allow-Origin': '*'
      })
    };
    return this.http.put(`${environment.backend.host}/adm_usuarios/actualizaubicacion`, parametroPersonaUbicacion, httpOptions)
      .pipe(
        map((response: any) => {
          console.log(response);
          return response;
        })
      );
  }
  
  actualizaFoto(parametroPersonaFotos: ParametroPersonaFotos) : Observable<RespuestaNormal> {
    const httpOptions = {
      headers: new HttpHeaders({
                  'Content-Type': 'application/json',
                  'Access-Control-Allow-Origin': '*'
      })
    };
    return this.http.put(`${environment.backend.host}/adm_usuarios/actualizafoto`, parametroPersonaFotos, httpOptions)
      .pipe(
        map((response: any) => {
          console.log(response);
          return response;
        })
      );
  }

  Cambiar_Password(parametroCambio: ParametroCambio): Observable<RespuestaNormal>{
   
    const httpOptions = {
      headers: new HttpHeaders({
                  'Content-Type': 'application/json',
                  'Access-Control-Allow-Origin': '*'
      })
    };
    return this.http.post(`${environment.backend.host}/lcnpersona/cambiar_password`, parametroCambio, httpOptions)
      .pipe(
        map((response: any) => {
          console.log(response);
          return response;
        })
      );
  }  

 
  Existe_Correo(email): Observable<RespuestaNormal> {
    const httpOptions = {
      headers: new HttpHeaders({
                  'Content-Type': 'application/json',
                  'Access-Control-Allow-Origin': '*'
      })
    };
    return this.http.get<RespuestaLogin>( `${environment.backend.host}/lcnpersona/existe_correo?correo_electronico=${email}`, httpOptions)
      .pipe(
        map((response: any) => {
          console.log(response);
          return response;
        })
      );
  }

  loginWithUserCredentials(username: string, password: string): Observable<RespuestaLogin> {

    let user: User = {
      correo_electronico: username,
      clave: password  
    }
    const httpOptions = {
      headers: new HttpHeaders({
                  'Content-Type': 'application/json',
                  'Access-Control-Allow-Origin': '*'
      })
    };    

    return this.http.post<RespuestaLogin>( `${environment.backend.host}/adm_usuarios/login`, user, httpOptions)
      .pipe(
        map((response: RespuestaLogin) => {
          if (response.codigo == 0){
            localStorage.setItem('session', JSON.stringify(response));            
          }
          return response;
        })
      );
  } 

  recuperarPassword(correo: string): Observable<RespuestaNormal> {
    
    let user: User = {
      correo_electronico: correo,
      clave: ''
    }
    const httpOptions = {
      headers: new HttpHeaders({
                  'Content-Type': 'application/json',
                  'Access-Control-Allow-Origin': '*'
      })
    };
    return this.http.post(`${environment.backend.host}/lcnpersona/recuperar_password`, user, httpOptions)
      .pipe(
        map((response: any) => {
          console.log(response);
          return response;
        })
      );
  }


  register(parametroUsuario: ParametroUsuario): Observable<RespuestaUsuario> {
    
    const httpOptions = {
      headers: new HttpHeaders({
                  'Content-Type': 'application/json',
                  'Access-Control-Allow-Origin': '*'
      })
    };
    return this.http.post(`${environment.backend.host}/adm_usuarios/registro`, parametroUsuario, httpOptions)
      .pipe(
        map((response: any) => {
          return response;
        })
      );
  }

  loginWithRefreshToken(): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/x-www-form-urlencoded');

    const body = new URLSearchParams();
    body.set('grant_type', 'refresh_token');
    body.set('client_id', OAUTH_DATA.client_id);
    body.set('client_secret', OAUTH_DATA.client_secret);
    body.set('refresh_token', this.refreshToken);
    body.set('scope', OAUTH_DATA.scope);

    return this.http.post(HttpApi.oauthLogin, body.toString(), { headers })
      .pipe(
        map((response: any) => {
          localStorage.setItem('session', JSON.stringify(response));
          return response;
        })
      );
  }

  isLogged(): boolean {
    return localStorage.getItem('session') ? true : false;
  }

  logout(): void {
    localStorage.clear();
  }

  get accessToken() {
    return localStorage['session'] ? JSON.parse(localStorage['session']).access_token : null;
  }

  get refreshToken() {
    return localStorage['session'] ? JSON.parse(localStorage['session']).refresh_token : null;
  }

  getUsuario(): string {
    let respLogin: RespuestaLogin;
    respLogin= JSON.parse(localStorage.getItem("session")!);
    return respLogin.usuario.nombre + " " + respLogin.usuario.apellido_paterno + " " + respLogin.usuario.apellido_materno;

  }

  getidPersona(): number{
    let respLogin: RespuestaLogin;
    respLogin= JSON.parse(localStorage.getItem("session")!);
    return respLogin.usuario.idpersona;
  }

  getidUsuario(): number {
    let respLogin: RespuestaLogin;
    respLogin= JSON.parse(localStorage.getItem("session")!);
    return respLogin.usuario.idusuario;
  }

  getLogin():string {
    let respLogin: RespuestaLogin;
    respLogin= JSON.parse(localStorage.getItem("session")!);
    return respLogin.usuario.login;
  }

  getIdEmpresa():number {
    let respLogin: RespuestaLogin;
    respLogin= JSON.parse(localStorage.getItem("session")!);
    return respLogin.usuario.idempresa;
  }

  getIdRol():number {
    let idRol:number = 0;
    idRol= JSON.parse(localStorage.getItem("idrol")!);
    return idRol;
  }

  getPersona(idpersona: number, sw: boolean ): Observable<RespuestaPersona> {
    const httpOptions = {
      headers: new HttpHeaders({
                  'Content-Type': 'application/json',
                  'Access-Control-Allow-Origin': '*'
      })
    };
    return this.http.get(`${environment.backend.host}/lcnpersona/getpersona?idpersona=${idpersona}&sw=${sw}`, httpOptions)
      .pipe(
        map((response: any) => {
          return response;
        })
      );
  }

  getPersonaByCI(ci: string) : Observable<RespuestaPersona> {
    const httpOptions = {
      headers: new HttpHeaders({
                  'Content-Type': 'application/json',
                  'Access-Control-Allow-Origin': '*'
      })
    };
    return this.http.get(`${environment.backend.host}/lcnpersona/getpersonabyci?ci=${ci}`, httpOptions)
      .pipe(
        map((response: any) => {
          return response;
        })
      );
  }
}
