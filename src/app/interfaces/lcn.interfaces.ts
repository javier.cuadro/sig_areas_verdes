export interface Clasificacion {
    idclasificacion: number;
    nombre:      string;
}
export interface Categorias {
    idcategoria: number;
    nombre:      string;
}

export interface Subcategorias {
    idsubcategoria: number;
    nombre:         string;
    idcategoria:    number;
}

export interface Actividades {
    idactividad:    number;
    nombre:         string;
    idsubcategoria: number;
}


export interface GrupoClasificacion {
    idgrupolineamiento: number;
    idclasificacion:        number;
    nombreclasificacion:    string;
}

export interface GrupoCategoria {
    idgrupolineamiento: number;
    idcategoria:        number;
    nombrecategoria:    string;
}

export interface Clasifi_Activi_Regla {
    id_clasi_activi_regla: number;
    idactividad:           number;
    idgrupolineamiento:    number;
    idpdf:                 number;
    idregla:               number;
    idtipolineamiento:     number;
}


export interface resultadoLineamiento {
    codigo:        number;
    mensaje:       string;
    idlineamiento: number;
}
export interface actualizarEstadoPagoLineamiento {
    idlineamiento: number;
}


export interface Properties_lotes {
    id:         number;
    objectid:   number;
    dm:         string;
    uv:         string;
    mz:         string;
    lt:         string;
    codigomuni: string;
    usodesuelo: number;
    codigolote: string;
    shape_leng: string;
    shape_area: string;
}

export interface areaMunicipal{
     id: number;
     distrito: string,
     uv: string,
     sit_legal: string,
     nom: string,
     uso: string,
     uso_especi: string,
     ley_mcpal: string,
     instrument: string,
     matricula: string,
     sup_titulo: string,
     sup_men: string,
     sup_cc: string,
     n_plano: string,
     f_emision: string,
     obs: string, 
}

export interface RespuestaMenu {
    codigo:   number;
    mensaje:  string;
    idrol:    number;
    recursos: Recurso[];
}

export interface Recurso {
    idrecurso:      number;
    idrecursopadre: number;
    nombre:         string;
    descripcion:    string;
    tipo:           string;
    acceso:         string;
    url:            string;
    estado:         string;
    orden:          number;
    icono:          string;
    desplegable:    string;
    recursos:       Recurso[];
}

export interface RespuestaMenuRol {
  codigo:   number;
  mensaje:  null;
  idrol:    number;
  recursos: RecursoRol[];
}

export interface RespuestaRol {
  codigo:  number;
  mensaje: string;
  roles:   Rol[];
}

export interface Rol {
  idrol:       number;
  nombre:      string;
  descripcion: string;
  estado:      string;
  fechaAlta:   string;
  usuarioAlta: string;
  fechaBaja:   null | string;
  usuarioBaja: null | string;
  tipo:        null | string;
}


export interface RecursoRol {
  idrecurso:      number;
    idrecursopadre: number;
    nombre:         string;
    orden:          number;
    icono:          string;
    checked:         boolean;
    expand:         boolean;
    recursos:       RecursoRol[];
}


export interface interfazAreaMunicipio{
    ip_geoserver: string,
     xcentro: string,
     ycentro: string,
     xsouthwest: string,
     ysouthwest: string,
     xsoutheast: string,
     ysoutheast: string,    
}

export interface interfazCapas{
    idmapa: number,
    titulocapa: string,
    nombregeoserver: string,    
    nombretabla: string

}

export interface interfazDistritosGeo{
    jsontreeview: string,    

}