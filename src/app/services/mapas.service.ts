import { Injectable } from '@angular/core';
import { HttpBackend, HttpClient, HttpHeaders } from "@angular/common/http";
import { map, Observable } from "rxjs";
import { environment } from 'src/environments/environment';
import { interfazAreaMunicipio, interfazCapas, interfazDistritosGeo } from '../interfaces/lcn.interfaces';
import { AreaMunicipio } from '../classes/areaMunicipio';

@Injectable({
  providedIn: 'root'
})
export class MapasService {

  httpOptions = {
    headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*'
    })
  };    



  url = 'http://localhost/apirestphp_pg/mapas';
//  constructor(private http: HttpClient) { }
constructor(
  private handler: HttpBackend,
  private http: HttpClient
) {
  this.http = new HttpClient(handler); /// to skip interceptors, becouse this service hits third backend provider
}

  obtenerMapas1():Observable<any>{
    return this.http.get(`${environment.backend_gis.host}/mapas/listar_todos`,this.httpOptions);
    //console.log("OBTENERMAPAS "+'${environment.backend.host}/mapas/listar.php?');
  }
  obtenerMapas(lat: number,lng:number):Observable<any>{
    
    return this.http.get(`${environment.backend_gis_php.host}/mapas/listar.php?lat=`+lat +`&lng=`+lng);
    //return this.http.get(`${environment.backend_gis_php.host}/mapas/listar.php?lat=`+lat +`&lng=`+lng);
    //return this.http.get(`${environment.backend_gis.host}/mapas/listar?lat=`+lat +`&lng=`+lng);
  }
  obtenerMapasPredios(doc: String):Observable<any>{
    
    return this.http.get(`${environment.backend_gis_php_mapa.host}/mapas/listarpredios.php?doc=`+doc);
    //return this.http.get(`${environment.backend_gis_php.host}/mapas/listar.php?lat=`+lat +`&lng=`+lng);
    //return this.http.get(`${environment.backend_gis.host}/mapas/listar?lat=`+lat +`&lng=`+lng);
  }


  
  obtenerCalles(lat: number,lng:number):Observable<any>{
    return this.http.get(`${environment.backend_gis_php_mapa.host}/mapas/listarcalles.php?lat=`+lat +`&lng=`+lng);
    //return this.http.get(`${environment.backend_gis.host}/mapas/listarcalles?lat=`+lat +`&lng=`+lng);
  }

  obtenerCallesPredios(doc:String):Observable<any>{
    return this.http.get(`${environment.backend_gis_php_mapa.host}/mapas/listarcallespredios.php?doc=`+doc);
    //return this.http.get(`${environment.backend_gis.host}/mapas/listarcalles?lat=`+lat +`&lng=`+lng);
  }

  obtenerLote(lat: number,lng:number):Observable<any>{
    //return this.http.get(`${environment.backend}/mapas/obtenerLote.php?lat=`+lat +`&lng=`+lng);
    //console.log(`${environment.backend_gis_php.host}/mapas/obtenerlote.php?lat=`+lat +`&lng=`+lng);
    //return this.http.get(`${environment.backend_gis.host}/mapas/obtenerlote?lat=`+lat +`&lng=`+lng,this.httpOptions);
    return this.http.get(`${environment.backend_gis_php_mapa.host}/mapas/obtenerlote.php?lat=`+lat +`&lng=`+lng);
    
  }

  obtenerLotePredios(doc:String):Observable<any>{
    //return this.http.get(`${environment.backend}/mapas/obtenerLote.php?lat=`+lat +`&lng=`+lng);
    //console.log(`${environment.backend_gis_php.host}/mapas/obtenerlote.php?lat=`+lat +`&lng=`+lng);
    //return this.http.get(`${environment.backend_gis.host}/mapas/obtenerlote?lat=`+lat +`&lng=`+lng,this.httpOptions);
    return this.http.get(`${environment.backend_gis_php_mapa.host}/mapas/obtenerlotepredios.php?doc=`+doc );
    
  }

  
  obtenerMallaCoordenada(lat: number,lng:number):Observable<any>{
    
    //return this.http.get(`${environment.backend_gis.host}/mapas/listarmalla?lat=`+lat +`&lng=`+lng);
     console.log("lat="+lat +"lng="+lng);
    return this.http.get(`${environment.backend_gis_php_mapa.host}/mapas/listarmalla.php?lat=`+lat +`&lng=`+lng);
  }

  obtenerAceraPredio(doc:String):Observable<any>{
    
    //return this.http.get(`${environment.backend_gis.host}/mapas/listarmalla?lat=`+lat +`&lng=`+lng);
     
    return this.http.get(`${environment.backend_gis_php.host}/mapas/listaracerapredios.php?doc=`+doc);
  }


  obtenerLoteEtiqueta(lat: number,lng:number):Observable<any>{
    //return this.http.get(`${environment.backend_gis.host}/mapas/obtenerloteetiqueta?lat=`+lat +`&lng=`+lng);
    return this.http.get(`${environment.backend_gis_php.host}/mapas/obtenerloteetiqueta.php?lat=`+lat +`&lng=`+lng);
  }

  obtenerJurisdiccion(lat: number,lng:number):Observable<any>{
    //return this.http.get(`${environment.backend}/mapas/obtenerJurisdiccion.php?lat=`+lat +`&lng=`+lng);
    return this.http.get(`${environment.backend_gis.host}/mapas/obtenerjurisdiccion?lat=`+lat +`&lng=`+lng,this.httpOptions);
  }
  obtenerJurisdiccion_lote(lat: number,lng:number):Observable<any>{
    //return this.http.get(`${environment.backend}/mapas/obtenerJurisdiccion.php?lat=`+lat +`&lng=`+lng);
    return this.http.get(`${environment.backend_gis.host}/mapas/jurisdiccion_lote?lat=`+lat +`&lng=`+lng,this.httpOptions);
  }

  buscarGeografico(dm:string,uv:string,mz:string,lt:string,tipobus:string):Observable<any>{
    //return this.http.get(`${environment.backend}/mapas/obtenerJurisdiccion.php?lat=`+lat +`&lng=`+lng);

    //$dm,$uv,$mz,$lt,$tipobus
    console.log(`${environment.backend_gis.host}/mapas/buscargeografico?dm=`+dm +`&uv=`+uv +`&mz=`+mz+`&lt=`+lt+`&tipobus=`+tipobus);
    return this.http.get(`${environment.backend_gis.host}/mapas/buscargeografico?dm=`+dm +`&uv=`+uv +`&mz=`+mz+`&lt=`+lt+`&tipobus=`+tipobus,this.httpOptions);
  }

  obtenerDatosUrbanizacion(lat: number,lng:number):Observable<any>{
    //return this.http.get(`${environment.backend}/mapas/obtenerLote.php?lat=`+lat +`&lng=`+lng);
    return this.http.get(`${environment.backend_gis.host}/mapas/obtenerdatosurbanizacion?lat=`+lat +`&lng=`+lng,this.httpOptions);
  }
  obtenerDatosEdificio(lat: number,lng:number):Observable<any>{
    //return this.http.get(`${environment.backend}/mapas/obtenerLote.php?lat=`+lat +`&lng=`+lng);
    return this.http.get(`${environment.backend_gis.host}/mapas/obtenerDatosedificio?lat=`+lat +`&lng=`+lng,this.httpOptions);
  }

  obtenerDatosLote(lat: number,lng:number):Observable<any>{
    //return this.http.get(`${environment.backend}/mapas/obtenerLote.php?lat=`+lat +`&lng=`+lng);


    //backend_gis_php
    return this.http.get(`${environment.backend_gis_php.host}/mapas/obtenerdatoslote.php?lat=`+lat +`&lng=`+lng,this.httpOptions);
    //return this.http.get(`${environment.backend_gis.host}/mapas/obtenerdatoslote?lat=`+lat +`&lng=`+lng,this.httpOptions);
  }


  obtenerMedidasPredios(doc:String):Observable<any>{
    return this.http.get(`${environment.backend_gis_php.host}/mapas/listarmedidaspredios.php?doc=`+doc);
    //return this.http.get(`${environment.backend_gis.host}/mapas/listarcalles?lat=`+lat +`&lng=`+lng);
  }

  obtenerAnchosPredios(doc:String):Observable<any>{
    return this.http.get(`${environment.backend_gis_php.host}/mapas/listaranchospredios.php?doc=`+doc);
    //return this.http.get(`${environment.backend_gis.host}/mapas/listarcalles?lat=`+lat +`&lng=`+lng);
  }

  obtenerColindantesPredios(doc:String):Observable<any>{
    return this.http.get(`${environment.backend_gis_php.host}/mapas/listarcolindantespredios.php?doc=`+doc);
    //return this.http.get(`${environment.backend_gis.host}/mapas/listarcalles?lat=`+lat +`&lng=`+lng);
  }


  obtenerAreaMunicipal(lat: number,lng:number):Observable<any>{
    //return this.http.get(`${environment.backend}/mapas/obtenerLote.php?lat=`+lat +`&lng=`+lng);

    //console.log("coordenadas Municipales: " + lat);
    
    return this.http.get(`${environment.backend_gis.host}/areamunicipal/obtenerareamunicipal?lat=`+lat +`&lng=`+lng,this.httpOptions);
  }

  obtenerDatosAreaMunicipal(lat: number,lng:number):Observable<any>{
    //return this.http.get(`${environment.backend}/mapas/obtenerLote.php?lat=`+lat +`&lng=`+lng);
    return this.http.get(`${environment.backend_gis.host}/areamunicipal/obtenerdatosareamunicipal?lat=`+lat +`&lng=`+lng,this.httpOptions)    ;
      
  }

  obtenerDatosAreaMunicipio_php(idempresa: number):Observable<AreaMunicipio>{
    //return this.http.get(`${environment.backend}/mapas/obtenerLote.php?lat=`+lat +`&lng=`+lng);
    //return this.http.get<interfazAreaMunicipio[]>(`${environment.backend_local.host}/capas/obtenerDatosAreaMunicipio.php?idempresa=`+idempresa,this.httpOptions);
    //console.log(`${environment.backend_local.host}/obtenerdatosareamunicipio?idempresa=`+idempresa,this.httpOptions);
   
    return this.http.get(`${environment.backend.host}/capas/obtenerdatosareamunicipio?idempresa=${idempresa}`,this.httpOptions)
    .pipe(
      map((response: any) => {
        return response;
      })
    );
    
    
    

  }

  obtenerDatosCapas_php(idempresa: number,ruta: string,idrol:number):any{
    //return this.http.get(`${environment.backend}/mapas/obtenerLote.php?lat=`+lat +`&lng=`+lng);
    //console.log(`${environment.backend.host}/capas/obtenerdatoscapas?idempresa=`+idempresa+ `&ruta=`+ruta);

     //return this.http.get<interfazCapas[]>(`${environment.backend.host}/capas/obtenerdatoscapas?idempresa=`+idempresa+ `&ruta=`+ruta ,this.httpOptions)
    
    /* return this.http.get<interfazCapas[]>(`${environment.backend_gis_php.host}/capas/obtenerDatosCapas.php?idempresa=`+idempresa+ `&ruta=`+ruta+ `&idrol=`+idrol ,this.httpOptions)    
    .subscribe(resp=>{
      localStorage.setItem('capas',JSON.stringify(resp));          
    });*/

    //return this.http.get(`${environment.backend.host}/capas/obtenerdatosareamunicipio?idempresa=${idempresa}`,this.httpOptions)
    return this.http.get<interfazCapas[]>(`${environment.backend_gis_php.host}/capas/obtenerDatosCapas.php?idempresa=`+idempresa+ `&ruta=`+ruta+ `&idrol=`+idrol ,this.httpOptions)    
    .pipe(
      map((response: any) => {
        //localStorage.clear();
        
        return response;
      })
    );
  }

  obtenerDatosDistritosGeo(idempresa:number,idrol:number):any{
    //return this.http.get(`${environment.backend}/mapas/obtenerLote.php?lat=`+lat +`&lng=`+lng);
    //console.log(`${environment.backend.host}/capas/obtenerdatoscapas?idempresa=`+idempresa+ `&ruta=`+ruta);
    //return this.http.get<interfazCapas[]>(`${environment.backend_gis.host}/capas/obtenerDatosCapas.php?idempresa=`+idempresa+ `&ruta=`+ruta ,this.httpOptions);
     return this.http.get<interfazDistritosGeo>(`${environment.backend_gis_php.host}/capas/obtenerdatosdistritosgeo.php?idempresa=`+idempresa+ `&idrol=`+idrol,this.httpOptions)          
    .subscribe(resp=>{
      var myArray;
      myArray= JSON.parse(resp[0].jsontreeview)
      localStorage.setItem('jsondistritosgeo',JSON.stringify(myArray));    
      //console.log("Service  obtenerDatosDistritosGeo " + resp[0].jsontreeview);
    });
  }
}
