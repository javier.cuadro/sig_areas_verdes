import { Injectable } from '@angular/core';
import { HttpBackend, HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class QrbnbService {

  public token:string='eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJTRUNVUklUWSIsImlzcyI6IlZFQ09NIiwiY29tcGFueSI6IjEiLCJsb2dpbiI6ImFkbWluIiwiZXhwIjoxNjUyNDUxODg5fQ.j8zpU2PBpnU8pQO33HKvqzkkgu7sp3eFFzlQxLFh2Y0';
  headers = { 'Authorization': this.token};
  constructor(
    private handler: HttpBackend,
    private http: HttpClient
  ) {
    this.http = new HttpClient(handler); /// to skip interceptors, becouse this service hits third backend provider
  }

  qrCrearSesion(data:any){
    //let url="http://190.186.159.75:7777//vpay/api/auth/login";


    let url="/vpay/api/auth/login";
    return this.http.post(url,data);
  }

  /*qrGenerar(data:any){
    const headers = { 'Authorization': this.token};
    let url="/vpay/api/transactions/doPayment";
    return this.http.put(url,data,{ headers });
  }*/

  qrGenerar(data:any){
    //const headers = { 'Authorization': this.token};
    let url= `${environment.backend_qr.host }/qr/generar`;
    return this.http.post(url,data);
  }

  /*qrConsultar(data:any){
    const headers = { 'Authorization': this.token};
    let url="/vpay/api/operations/statusQr";
    return this.http.post(url,data,{ headers });
  }*/


  qrConsultar(data:any){
    //const headers = { 'Authorization': this.token};
    let url= `${environment.backend_qr.host }/qr/consultar`;
    return this.http.post(url,data);
  }

}
