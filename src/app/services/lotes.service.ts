import { Injectable } from '@angular/core';
import { HttpBackend, HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LotesService {

  url = 'http://localhost/apirestphp_pg/lotes';

  httpOptions = {
    headers: new HttpHeaders({
                 'Content-Type': 'application/json',
                 'Access-Control-Allow-Origin': '*'
    })
  };

  //constructor(private http: HttpClient) { }
  constructor(
    private handler: HttpBackend,
    private http: HttpClient
  ) {
    this.http = new HttpClient(handler); /// to skip interceptors, becouse this service hits third backend provider
  }

  
  obtenerDonde(lat: number,lng:number): Observable<any> {
    //return this.http.get(`${environment.backend_gis_php}/lotes/seleccionar.php?lat=`+lat +`&lng=`+lng);

    console.log("obtenerDonde - "+`${environment.backend_gis.host}/lotes/seleccionar?lat=`+lat +`&lng=`+lng);
    return this.http.get(`${environment.backend_gis.host}/lotes/seleccionar?lat=`+lat +`&lng=`+lng,this.httpOptions);
  }
}
