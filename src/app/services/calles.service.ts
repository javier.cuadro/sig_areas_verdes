import { Injectable } from '@angular/core';
import { HttpBackend, HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CallesService {

  url = 'http://localhost/apirestphp_pg/calles';


  httpOptions = {
    headers: new HttpHeaders({
                 'Content-Type': 'application/json',
                 'Access-Control-Allow-Origin': '*',
                 'Access-Control-Allow-Headers': 'Content-Type, Authorization',
                 'Access-Control-Allow-Methods': '*'
    })
  };


  headers: {
    "access-control-allow-origin": "*",
    'Access-Control-Allow-Headers': 'Content-Type, Authorization',
    'Access-Control-Allow-Methods': '*',
  };


  //constructor(private http: HttpClient) { }
  constructor(
    private handler: HttpBackend,
    private http: HttpClient
  ) {
    this.http = new HttpClient(handler); /// to skip interceptors, becouse this service hits third backend provider
  }

  
  obtenerDonde(lat: number,lng:number): Observable<any> {
    //return this.http.get(`${environment.backend_gis_php}/calles/seleccionar.php?lat=`+lat +`&lng=`+lng);
    //return this.http.get(`${environment.backend_gis_php.host}/calles/seleccionar?lat=`+lat +`&lng=`+lng,this.httpOptions);
    return this.http.get(`${environment.backend_gis.host}/calles/seleccionar?lat=`+lat +`&lng=`+lng);
  }
}
