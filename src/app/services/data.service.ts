import { HttpBackend, HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, delay } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { RecursoRol, RespuestaMenu, RespuestaMenuRol, RespuestaRol } from '../interfaces/lcn.interfaces';
import { RespuestaNormal } from '../dashboard/interfaces/dashboard.interfaces';


@Injectable({
  providedIn: 'root'
})
export class DataService {

  nombreTramite :string  ="";

  constructor(
    private handler: HttpBackend,
    private http: HttpClient
  ) {
    this.http = new HttpClient(handler); /// to skip interceptors, becouse this service hits third backend provider
  }

  guardarPermisos(idrol: number, recursos: RecursoRol[]): Observable<RespuestaNormal>{
    const httpOptions = {
      headers: new HttpHeaders({
                  'Content-Type': 'application/json',
                  'Access-Control-Allow-Origin': '*'
      })
    };

    return this.http.post(`${environment.backend.host}/menu/rolpermisos?idrol=${idrol}`, recursos, httpOptions )
      .pipe(
        map((data: any) => {         
          //console.log(data);
          return data;
        })
      );
  }

 
  getMenu(idusuario: number): Observable<RespuestaMenu>{
    let params = new HttpParams();
    params = params.append('idusuario', idusuario);

    return this.http.get(`${environment.backend.host}/menu/rolusuario`, { params })
      .pipe(
        map((data: any) => {         
          //console.log(data);
          return data;
        })
      );
  }

  getMenuRol(idrol: number): Observable<RespuestaMenuRol>{
    let params = new HttpParams();
    params = params.append('idrol', idrol);

    return this.http.get(`${environment.backend.host}/menu/rol`, { params })
      .pipe(
        map((data: any) => {         
          //console.log(data);
          return data;
        })
      );
  }

  getRoles(): Observable<RespuestaRol>{   

    return this.http.get(`${environment.backend.host}/menu/roles`)
      .pipe(
        map((data: any) => {         
          //console.log(data);
          return data;
        })
      );
  }


  getOpciones(idusuario: number, idrecurso: number): Observable<RespuestaMenu>{
    let params = new HttpParams();
    params = params.append('idusuario', idusuario);
    params = params.append('idrecurso', idrecurso);

    return this.http.get(`${environment.backend.host}/menu/rolusuario/opciones`, { params })
      .pipe(
        map((data: any) => {         
          return data;
        })
      );
  }

}
