import { Injectable } from '@angular/core';
import { HttpBackend, HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { RespuestaNormal } from '../dashboard/interfaces/dashboard.interfaces';
import { ParametroAreaMunicipal, ParametroAreaVerdeDocumento, RespuestaAreas } from '../dashboard/interfaces/areasverdes.interfaces';

const OAUTH_DATA =environment.oauth;

@Injectable({
  providedIn: 'root'
})
export class AreaVerdeService {  

  constructor(
    private handler: HttpBackend,
    private http: HttpClient
  ) {
    this.http = new HttpClient(handler); /// to skip interceptors, becouse this service hits third backend provider
  }

  getPerfil(idusuario: number): Observable<RespuestaNormal>{
    console.log(idusuario);
    return this.http.get(`${environment.backend.host}/adm_usuarios/getperfil?idusuario=${idusuario}`)
      .pipe(
        map((data: any) => {
          return data;
        })
      );
  }


  listadoArchivos(id: number): Observable<RespuestaAreas> {    
   
    const httpOptions = {
      headers: new HttpHeaders({
                  'Content-Type': 'application/json',
                  'Access-Control-Allow-Origin': '*'
      })
    };
    return this.http.get(`${environment.backend.host}/areaverde/listado/?idareaverde=${id}`, httpOptions)
      .pipe(
        map((response: any) => {
          return response;
        })
      );
  }

  actualizarAreaMunicipal(parametro: ParametroAreaMunicipal): Observable<RespuestaNormal> {    
   
    const httpOptions = {
      headers: new HttpHeaders({
                  'Content-Type': 'application/json',
                  'Access-Control-Allow-Origin': '*'
      })
    };
    return this.http.post(`${environment.backend_gis.host}/api/lcnareamunicipal/actualizar`, parametro, httpOptions)
      .pipe(
        map((response: any) => {
          return response;
        })
      );
  }

  eliminarDocumento(parametro: ParametroAreaVerdeDocumento): Observable<RespuestaNormal> {    
   
    const httpOptions = {
      headers: new HttpHeaders({
                  'Content-Type': 'application/json',
                  'Access-Control-Allow-Origin': '*'
      })
    };
    return this.http.post(`${environment.backend.host}/areaverde/documento`, parametro, httpOptions)
      .pipe(
        map((response: any) => {
          return response;
        })
      );
  }

  registroDocumento(parametro: ParametroAreaVerdeDocumento): Observable<RespuestaNormal> {    
   
    const httpOptions = {
      headers: new HttpHeaders({
                  'Content-Type': 'application/json',
                  'Access-Control-Allow-Origin': '*'
      })
    };
    return this.http.post(`${environment.backend.host}/areaverde/registrodocumento`, parametro, httpOptions)
      .pipe(
        map((response: any) => {
          return response;
        })
      );
  }

  obtenerDocumento(parametro: ParametroAreaVerdeDocumento): Observable<RespuestaNormal> {    
   
    const httpOptions = {
      headers: new HttpHeaders({
                  'Content-Type': 'application/json',
                  'Access-Control-Allow-Origin': '*'
      })
    };
    return this.http.post(`${environment.backend.host}/areaverde/obtenerdocumento`, parametro, httpOptions)
      .pipe(
        map((response: any) => {
          return response;
        })
      );
  }
}