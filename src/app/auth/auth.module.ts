import { NgModule } from '@angular/core';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { NgxDropzoneModule } from 'ngx-dropzone';

import { SharedModule } from '../shared/shared.module';
import { AuthRoutingModule } from './auth-routing.module';
import { RecoverComponent } from './recover/recover.component';

@NgModule({
  declarations: [
    ...AuthRoutingModule.components,
    RecoverComponent
  ],
  imports: [
    SharedModule,
    AuthRoutingModule,
    NgxDropzoneModule,
    LeafletModule
  ]
})
export class AuthModule { }
