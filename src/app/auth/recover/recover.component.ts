import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { AuthService } from 'src/app/core/services/auth.service';
import Swal from 'sweetalert2';
import { LoginComponent } from '../login/login.component';

@Component({
  selector: 'app-recover',
  templateUrl: './recover.component.html',
  styleUrls: ['./recover.component.scss']
})
export class RecoverComponent implements OnInit {

  form!: FormGroup;
  message!: string; 
  loginLoading = false;

  constructor(
    private authService: AuthService,
    public formBuilder: FormBuilder,
    private router: Router,
    public snackBar: MatSnackBar
  ) { 
    this.initFormBuilder();
  }

  ngOnInit(): void {
  }

  private initFormBuilder() {
    this.form = this.formBuilder.group({
      email: ['', [
        Validators.required,
        Validators.email
      ]],
    });
  }
  
  recuperarPassword(){
    this.loginLoading = true;
    this.authService
      .recuperarPassword(this.form.value.email)
      .pipe(finalize(() => this.loginLoading = false))      
      .subscribe(
        data => {
          console.log(data);
          
          if (data.codigo == 0) {
            Swal.fire({
              position:'center',
              icon:'success',
              title: "Se ha enviado el password al email solicitado",
              showConfirmButton: false,
              timer: 3000
            });
            this.loginLoading = false;
            this.router.navigate(['auth/login']);
            
          }
          else  {
            Swal.fire({
              position:'center',
              icon:'warning',
              title: data.mensaje,
              showConfirmButton: false,
              timer: 3000
            });
            this.loginLoading = false;
          }
        },
        error => {
          Swal.fire({
            position:'center',
            icon:'error',
            title: 'El Servicio se encuentra fuera de línea',
            showConfirmButton: false,
            timer: 2500
          });
          this.loginLoading = false;
        }
    );

  }
}
