import { Component, OnInit, ViewChild } from '@angular/core';
import {
  AbstractControl, FormBuilder, FormControl, FormGroupDirective, FormGroup,
  NgForm, Validators, ValidationErrors, ValidatorFn
} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ErrorStateMatcher } from '@angular/material/core';
import { Subscription, finalize, Observable, of, timer } from 'rxjs';
import { switchMap, map } from "rxjs/operators";

import { AuthService } from '../../core/services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { ParametroUsuario } from '../interfaces/auth.interface';
import Swal from 'sweetalert2';
import { LoadingBackdropService } from '../../core/services/loading-backdrop.service';
import "leaflet";
declare let L;
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { MatStepper } from '@angular/material/stepper';
import * as htmlToImage from 'html-to-image';
//import {"../leaflet_export.js"><
//import * as printExport from 'leaflet.export'
import "leaflet.export";



export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl, form: FormGroupDirective | NgForm | null): boolean {
    const password = control.parent?.get('password')?.value;
    const confirmation = control.parent?.get('passwordConfirmation')?.value;
    const match = password !== confirmation;

    return (control && control.dirty && match) || (control && control.touched && control.invalid);
  }
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',  
  styleUrls: ['./register.component.scss'],
  
})

export class RegisterComponent implements OnInit {

  @ViewChild('stepper') stepper: MatStepper;
  map;
  map1;
  lat1=0;
   lng1=0;
   marker;
   i=0;
   public base64:string='';
   swFechaNac: boolean = false;
   swFechaVen: boolean = false;


  public showPassword: boolean = false;
  public showPassword2: boolean =false;

  matcher = new MyErrorStateMatcher();
  registerSubscription!: Subscription;

  files: File[] = [];
  files2: File[] = [];
  files3: File[] = [];

  foto_64: string='';
  ci_anverso64: string='';
  ci_reverso64: string='';

  foto; ci_anverso; ci_reverso; mapaP;

  latitud: number;
  longitud: number;
  X: number;
  Y: number;

  form1 = this.fb.group({
    documento_identidad: ['', [ Validators.required, ]],
    complemento: [''],
    expedicion: ['', [ Validators.required, ]],
    expiracion_documento: ['', [ Validators.required, ]],
    nombre: ['', [Validators.required,]],
    apellido_paterno:['', [ Validators.required, ]],
    apellido_materno:['', [ Validators.required, ]],    
    fecha_nacimiento:['', [ Validators.required ]],
    genero: ['', [ Validators.required, ]],
    estado_civil: ['', [ Validators.required, ]],
    correo_electronico: ['', [
      Validators.required,
      Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$'),], 
      
  ],
    telefono_fijo: [''],
    telefono_movil:[''],
    nit: [],  
    password: ['', [
      Validators.required,
      this.regexValidator(new RegExp('(?=.*?[0-9])'), { 'at-least-one-digit': true }),
      //this.regexValidator(new RegExp('(?=.*[a-z])'), { 'at-least-one-lowercase': true }),
      //this.regexValidator(new RegExp('(?=.*[A-Z])'), { 'at-least-one-uppercase': true }),
      this.regexValidator(new RegExp('(?=.*[.!@#$%^&*])'), { 'at-least-one-special-character': true }),
      this.regexValidator(new RegExp('(^.{8,}$)'), { 'at-least-eight-characters': true }),
    ]],
    passwordConfirmation: ['', Validators.required]
  }, { validator: this.checkPasswords });

  form2 = this.fb.group({
    residencia: ['',],
    tipo_lugar: ['', [ Validators.required, ]],
    nombre_lugar: ['', [ Validators.required, ]],
    numero_casa: ['', [ Validators.required, ]],
    direccion_descriptiva: ['', [ Validators.required, ]],
  },
    { updateOn: "blur" }
  );

  form3 = this.fb.group({
    
  },
    { updateOn: "blur" }
  );

  form4 = this.fb.group({
    
  },
    { updateOn: "blur" }
  );




  streetMaps =L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
   detectRetina: true,
   attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
   maxZoom: 19,
 });
 
 esri = L.tileLayer(  
   'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
   maxZoom: 19,
   minZoom: 1,
   attribution: 'Tiles © Esri',
 });
 
 
 wmsLayers = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
     layers: "lotes_cartografia_yina",
     format: 'image/png',
     maxZoom: 19,
     transparent: true,
     version: '1.3.0',
     attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
   }
 );
 
 wms_lim_jur = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
     layers: "limite_jurisdiccion",
     format: 'image/png',
     maxZoom: 19,
     transparent: true,
     version: '1.3.0',
     attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
   }
 );
 
 wms_lim_mun = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
     layers: "limite_municipio",
     format: 'image/png',
     maxZoom: 19,
     transparent: true,
     version: '1.3.0',
     attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
   }
 );

 //wms_area_mun_uso = L.tileLayer.wms("http://localhost:8080/geoserver/gamsc/wms?",{
  wms_area_mun_uso = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
     layers: "areas_municipales_uso",
     format: 'image/png',
     maxZoom: 19,
     transparent: true,
     version: '1.3.0',
     attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
   }
 );

 
 
 wms_lotes = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
     layers: "lotes_cartografia_yina",
     format: 'image/png',
     maxZoom: 19,
     transparent: true,
     version: '1.3.0',
     attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
   }
 );
 wms_Vias = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
        layers: "vias",
        format: 'image/png',
        transparent: true,
        version: '1.3.0',
        attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
      }
    );
 
 
 wms_uv_yina = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
     layers: "unidades_vecinales_yina",
     format: 'image/png',
     maxZoom: 19,
     transparent: true,
     version: '1.3.0',
     attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
   }
 );
 wms_manzanas_yina = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
     layers: "manzanas_yina",
     format: 'image/png',
     maxZoom: 19,
     transparent: true,
     version: '1.3.0',
     attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
   }
 );

 
 
 
 options = {
  layers: [ this.streetMaps ],
  zoom: 15,
  center: L.latLng([ -17.7876515, -63.1825049 ]),
  //maxBounds: [[-17.622216426146586, -63.28947607997527],[-17.902058376130743, -63.00586013670787]],

  //lat = -17.7876515;
  //lng = -63.1825049;
};
options1 = {
  layers: [ this.esri],
  zoom: 15,
  center: L.latLng([ -17.7876515, -63.1825049 ]),
  //maxBounds: [[-17.622216426146586, -63.28947607997527],[-17.902058376130743, -63.00586013670787]],

  //lat = -17.7876515;
  //lng = -63.1825049;
};

overlayMaps1 = [
  //{name:"general" , layer: this.streetMaps},
  {name:"Satélite" , layer: this.esri},  
  {name:"Áreas verdes según Uso" , layer: this.wms_area_mun_uso},
  {name:"Límite Municipal" , layer: this.wms_lim_mun},   
  {name:"Límite Jurisdicción" , layer:this.wms_lim_jur},
  {name:"Uv" , layer:this.wms_uv_yina},
  {name:"Manzanas" , layer:this.wms_manzanas_yina},        
];

  constructor(
    private authService: AuthService,
    public fb: FormBuilder,
    public snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private router: Router, 
    private datePipe: DatePipe,
    private _snackBar: MatSnackBar,
    private loadingBackdropService: LoadingBackdropService
  ) {
  
  }

  ngOnInit() {
    this.base64="iVBORw0KGgoAAAANSUhEUgAAAJoAAACYCAIAAACTVWdLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAVPSURBVHhe7ZrtkesgDEW3rhSUelJNmkkx+zCIDyFh8HM2zty5589usBCCYzvYk59fAgR1QkGdUFAnFNQJBXVCQZ1QUCcU1AkFdUJBnVBQJxTUCQV1QkGdUFAnFNQJBXVCQZ1QUCcU1AkFdUJBnVBQJxTUCQV1QkGdUFAnFNQJBXVCQZ1QUCcU1AkFdUJBnVBQJxTUCQV1QkGdUFAnFNQJBXVCQZ1QUCcU1AkFdUJBnVBQJxTUCQV1QkGdUFAnFNQJBXVCQZ1QUCcU1AkFdUJBnVBQJxQjnc/7z4j7U2I2Xs/7Tdp/brfH8yXtgwy3272ETIcYBLQ5Cq9HLkOVV5PoZmmVRm+gMEo7G+H1fNxvzYRDJSamxYmXIxtHJrjGKZ11DSvl6DjDziIKKWInoCki0laiD9Ukbbu0Tiu5PZp19aYbUDEt0/gDE1xkonM3qZQr51K4TmOXXK3NEE7VNL8UMh3CDchr5Cz07Z7uFKqDJInUAzq1M1C565RxysDl0rExLXngnfj1Ca7yDp153Oka6cbpEIOA1NzOVmw+XlJR20OSJI6UWtY1Ne8WY5rX4ndjPq4zB4Wht1OwG91meOvV2eatEq3PnOSe/soi6dSDSiRZ7DIsNh0wa78U7wY5E1zn8HenLlyHqe/wYYZc6HSIcQZVhVJofEqSUJgsU+xaWnVM/FRJ7U0HPfmIGTCxFC/jejg9VzipMxA3b3JwIwd4GYLvul2cDuEGmA1nt5798kqS7XMWGv5vWnWMIrXHcpp/NW/X6e6oVzl1s1XUR5bUaZrhWEDeaTVbi0h21JPTqiQ5R771StCgEknd6DQh+YDxNolPB1TQaIKHOKHThrTzn2c4HFDMNYs3slm6dUnko+DHZFSzfOi85eH7rn3OEJf2FypNP643wWOcuTpLaelsKjsdNf9xhv8IMPOVBj191c8kkYaItDqVdNu2QO5XLx7z4KHQ8fXetSEd7LhnhR7+7mxHKoO35MPOGmmmQ3gZ9Hxdm7qjTdJULa3jSlRmd7p29MIgvhYzn+BRTukMbGddKXrb6ZRjXq2K6RCDDLlfuCv4NlVPL0lZMmn1KlG7tkLc96n57n/Pmfh4jeZu0wnu5vYY6SR/xmsz3Ct8E9QJBXVCQZ1QUCcU1AkFdULx1zpXHum8GMHu593gbePfP6PVh3j/oUA9MfuPkOapcfJTkuu5QmeiPiSf1xnRse0rGZNl9L6mfXAfxPzHs/0H+ZDOdkHNq04nZowNtq9XA0mG+3OTIsp5+1oid2K+WegFOgN5sVLzSZ0B0yg2935u0g0XApv77aCkQ5VewTU6s890oh9aJBvsXJ2NROPTEWwYVpQOfO/1eZFOtS4SY/GWbRg8MtbrWzEyjFk5F64EQme3Le3WvPvYq5LDmRjWxxSoc5u+mb8si9K5tkbz4M5PQbr0RsY6nUGGnr+Ei3Sq5rmhhmnwyGbpIxl6J15JXUxOvVbpFVyg02xdpoZaZsGy5FqE7pStNO8e4guD2Cgx0oMPKpq8LJa6KuMYZ+kmOl2bpldQEz8bgr0UUaV3fLPMa3T2L9TeqHNg0+tWL8iNtZd8NuTL+Gud5KNQJxTUCQV1QkGdUFAnFNQJBXVCQZ1QUCcU1AkFdUJBnVBQJxTUCQV1QkGdUFAnFNQJBXVCQZ1QUCcU1AkFdUJBnVBQJxTUCQV1QkGdUFAnFNQJBXVCQZ1QUCcU1AkFdUJBnVBQJxTUCQV1QkGdUFAnFNQJBXVCQZ1QUCcU1AkFdUJBnVBQJxTUCQV1QkGdUFAnFNQJBXVCQZ1QUCcU1AkFdUJBnUD8/v4DngF37uJ5l38AAAAASUVORK5CYII=";
    

    this.form1.get("correo_electronico").valueChanges
    .subscribe(email => {
      if (this.validMail(email)){
        this.verificarEmail(email)
      }
    });    
  }

  comprobarEdad() {
    let birthday: Date = this.form1.value.fecha_nacimiento;    
    var ageDifMs = Date.now() - new Date(birthday).getTime();
    var ageDate = new Date(ageDifMs); // miliseconds from epoch
    let age = Math.abs(ageDate.getUTCFullYear() - 1970);
    
    if (age<18) {
      this.swFechaNac = false;
      Swal.fire({
        position:'center',
        icon:'warning',
        title: 'Falta que cumpla la mayoría de edad',
      });
      return; 
    }

    this.swFechaNac = true;
    
  }

  comprobarExpiracion() {
    let fecha_expiracion: Date = this.form1.value.expiracion_documento;
    
    if (new Date(fecha_expiracion).getTime() <Date.now()) {
      this.swFechaVen=false;
      Swal.fire({
        position:'center',
        icon:'warning',
        title: 'Ha caducado la fecha de vencimiento del documento',
      });
      return;
    }
    this.swFechaVen=true;
  }

  comprobar() {
    let ci: string=this.form1.value.documento_identidad;

    this.loadingBackdropService.show();

    this.authService
      .getPersonaByCI(ci)
      .pipe(finalize(() => this.loadingBackdropService.hide()))
      .subscribe((data) => { 
        if (data.codigo == 0) {
          let mensaje='El ciudadano: '+ data.persona.nombre + ' ' + data.persona.apellido_paterno + ' ' + data.persona.apellido_materno;
          mensaje=mensaje+'tiene este número de documento: ' + data.persona.documento_identidad + ' con el correo:'+ data.persona.correo_electronico;
          this.form1.patchValue({
            documento_identidad: ''
          });
          Swal.fire({
            position:'center',
            icon:'warning',
            title:  mensaje,
            text: 'Por favor verifique',
          });
        } 
        else  {
          this.comprobarEdad();
          this.comprobarExpiracion();
          if (this.swFechaNac && this.swFechaVen) {
            this.stepper.next();
          }
        }
      });
  }
  


  validMail(email:string):boolean
  {
      return /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()\.,;\s@\"]+\.{0,1})+([^<>()\.,;:\s@\"]{2,}|[\d\.]+))$/.test(email);
  }

  public togglePasswordVisibility(): void {
    this.showPassword = !this.showPassword;
  }

  public togglePasswordVisibility2(): void {
    this.showPassword2 = !this.showPassword2;
  }

  tieneFotos(): boolean {
    if (this.foto_64.length> 0 && this.ci_anverso64.length>0 && this.ci_reverso64.length>0){
      return true;
    } else {
      return false;
    }
   
  }

  registerUser() {
    this.loadingBackdropService.show();
    //let fecha_nac =this.datepipe.transform(this.form.value.fecha_nacimiento, 'dd/mm/yyyy');
    let estado_civil: string;
    estado_civil =  this.form1.value.estado_civil;
    estado_civil = estado_civil.substring(0,1);

    let genero: string;
    genero = this.form1.value.genero;
    genero = genero.substring(0,1);

    const parametroUsuario : ParametroUsuario = {
      idusuario: 0,
      nombre: this.form1.value.nombre,      
      apellido_paterno: this.form1.value.apellido_paterno || '',
      apellido_materno: this.form1.value.apellido_materno || '',
      fecha_nacimiento: this.datePipe.transform(this.form1.value.fecha_nacimiento,"dd/MM/yyyy"),    
      correo_electronico: this.form1.value.correo_electronico,
      telefono_fijo: this.form1.value.telefono_fijo  || '',
      telefono_movil: this.form1.value.telefono_movil || '',
      estado: '',
      direccion: this.form1.value.direccion || '',
      fecha_alta: this.datePipe.transform(new Date(),"dd/MM/yyyy"),
      usuario_alta: '',
      fecha_baja: '31/12/2022',
      usuario_baja: '',
      documento_identidad: this.form1.value.documento_identidad,
      complemento: this.form1.value.complemento,
      expedicion: this.form1.value.expedicion,
      expiracion_documento: this.datePipe.transform(this.form1.value.expiracion_documento,"dd/MM/yyyy"),
      nit: this.form1.value.nit,
      genero: genero,
      estado_civil: estado_civil,
      residencia: this.form2.value.residencia,
      tipo_lugar: this.form2.value.tipo_lugar,
      nombre_lugar: this.form2.value.nombre_lugar,
      numero_casa: this.form2.value.numero_casa,
      latitud: this.latitud,
      longitud: this.longitud,
      x: this.X,
      y: this.Y,
      foto: this.foto_64 ,
      ci_anverso: this.ci_anverso64,
      ci_reverso: this.ci_reverso64,

      login: this.form1.value.correo_electronico,
      clave: this.form1.value.password,
      dias_vigencia_password: 180,
      es_cajero: false,
      es_vendedor: false,      
      fecha_caducidad: '31/12/2022',     
      fecha_vencimiento_password: '31/12/2022',

    };

   
    this.registerSubscription = this.authService
      .register(parametroUsuario)
      .pipe(finalize(() => this.loadingBackdropService.hide()))
      .subscribe(
        data => {
          if (data.codigo == 0) {
            this.router.navigate(['..'], { relativeTo: this.route });          
            Swal.fire({
              position:'center',
              icon:'success',
              title: 'Se ha registrado correctamente, por favor inicie sesión',
              showConfirmButton: false,
              timer: 3000
            })
          } else {
            Swal.fire({
              position:'center',
              icon:'warning',
              title: data.mensaje,
              showConfirmButton: false,
              timer: 3000
            })
          }
         
        },
        error => {
          console.log('error en el componente', error);
          Swal.fire({
            position:'center',
            icon:'error',
            title:'El servicio se encuentra fuera de línea',
            showConfirmButton: false,
            timer: 2000
          });
         
        }
      );
  }  

  private checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    const pass = group.controls['password'].value;
    const confirmPass = group.controls['passwordConfirmation'].value;
    return pass === confirmPass ? null : { notSame: true };
    return true;  
  }

  private regexValidator(regex: RegExp, error: ValidationErrors): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (!control.value) {
        return null;
      }
      const valid = regex.test(control.value);
      return valid ? null : error;
    };
  } 

  onSelect(event) {
    this.files = [];
		this.files.push(...event.addedFiles);
    if (this.files && this.files[0]) {
      for (let i = 0; i < this.files.length; i++) {
       this.fileToBase64(this.files[i])
        .then(result=>{
          this.foto = result;
          this.foto_64 = result.replace('data:', '')
          .replace(/^.+,/, ''); // To remove data url part
        });         
      }
    }
	}

  fileToBase64 = (file:File):Promise<string> => {
    return new Promise<string> ((resolve,reject)=> {
         const reader = new FileReader();
         reader.readAsDataURL(file);
         reader.onload = () => resolve(reader.result.toString());
         reader.onerror = error => reject(error);
     })
    }

	onRemove(event) {
		this.files.splice(this.files.indexOf(event), 1);
    this.foto_64='';
    this.foto='';
	}
 
  onSelect2(event) {
    this.files2 = [];
		this.files2.push(...event.addedFiles);
    if (this.files2 && this.files2[0]) {
      for (let i = 0; i < this.files2.length; i++) {
       this.fileToBase64(this.files2[i])
        .then(result=>{
          this.ci_anverso=result;
          this.ci_anverso64= result.replace('data:', '')
          .replace(/^.+,/, ''); // To remove data url part
        });         
      }
    }
	}

	onRemove2(event) {
		this.files2.splice(this.files.indexOf(event), 1);
    this.ci_anverso64 = '';
    this.ci_anverso='';
	}

  onSelect3(event) {
    this.files3 = [];
		this.files3.push(...event.addedFiles);
    if (this.files3 && this.files3[0]) {
      for (let i = 0; i < this.files3.length; i++) {
       this.fileToBase64(this.files3[i])
        .then(result=>{
          this.ci_reverso=result;
          this.ci_reverso64= result.replace('data:', '')          
          .replace(/^.+,/, ''); // To remove data url part
        });         
      }
    }
	}

	onRemove3(event) {
		this.files3.splice(this.files.indexOf(event), 1);
    this.ci_reverso64 = '';
    this.ci_reverso='';
	}




  onMapReady(map) {     


    var fitBoundsSouthWest = new L.LatLng(-17.606754259255016, -63.394996841180316);
    var fitBoundsNorthEast = new L.LatLng(-17.9500307832854, -62.98270820363417);
    var fitBoundsArea = new L.LatLngBounds(fitBoundsSouthWest, fitBoundsNorthEast);


    L.Icon.Default.imagePath = "assets/leaflet/";

    let geojson;
    
    this.map = map;
    this.map.setMaxBounds(fitBoundsArea);

     

  
    var baseMaps = {      
      
      "Satelital":this.esri,
      "General": this.streetMaps
    };       

      var overlayMaps = {
        "Limite Municipio": this.wms_lim_mun,
        "Limite Jurisdicción": this.wms_lim_jur,
        "Uv": this.wms_uv_yina,
        "Manzanas": this.wms_manzanas_yina,                 
        "Lotes": this.wms_lotes, 
                       
      };
      
      L.control.scale().addTo(this.map);

      var markersLayer = new L.LayerGroup();	//layer contain searched elements    
      map.addLayer(markersLayer);  



     
      
        


      setTimeout(() => {
        this.map = map;
        map.invalidateSize();        
      }, 0);

     



  } 

  onMapReady1(map1) {     


    var fitBoundsSouthWest = new L.LatLng(-17.606754259255016, -63.394996841180316);
    var fitBoundsNorthEast = new L.LatLng(-17.9500307832854, -62.98270820363417);
    var fitBoundsArea = new L.LatLngBounds(fitBoundsSouthWest, fitBoundsNorthEast);


    L.Icon.Default.imagePath = "assets/leaflet/";

    let geojson;
    
    this.map1 = map1;
    this.map1.setMaxBounds(fitBoundsArea);

     

  
    var baseMaps = {      
      
      "Satelital":this.esri,
      "General": this.streetMaps
    };       

      var overlayMaps = {
        "Limite Municipio": this.wms_lim_mun,
        "Limite Jurisdicción": this.wms_lim_jur,
        "Uv": this.wms_uv_yina,
        "Manzanas": this.wms_manzanas_yina,                 
        "Lotes": this.wms_lotes, 
                       
      };
      
      L.control.scale().addTo(this.map1);

      var markersLayer = new L.LayerGroup();	//layer contain searched elements    
      map1.addLayer(markersLayer);      


      setTimeout(() => {
        this.map1 = map1;
        map1.invalidateSize();        
      }, 0);

     



  } 

  agregarMarcadorJurisdiccion(evt){ 
    
    let i = 0;
    
    var coordenada =  evt.latlng;
    var latitud = coordenada.lat; // lat  es una propiedad de latlng
    var longitud = coordenada.lng; 

    this.GeogToUTM(latitud,longitud);

  console.log("Coordenadas en Grados " + latitud+"-" +longitud );
  console.log("Coordenadas en UTM (X Y) "+ this.lat1 +"-" +  this.lng1);
  
  this.latitud = latitud;
  this.longitud = longitud;

  this.X = this.lat1;
  this.Y = this.lng1;

  //this.map.removeLayer(this.marker);
      if (this.i>0){
        this.map.removeLayer(this.marker);
        
      }
    this.marker  = L.marker([coordenada.lat, coordenada.lng]).bindPopup('Punto');
        //this.marker.push(LamMarker);
        //LamMarker.addTo(this.map);
        //this.map.fitBounds(this.marker);
        this.map.addLayer(this.marker);
        this.i=1;
        

  }
  
     
 GeogToUTM(latd0: number,lngd0 : number){
  //Convert Latitude and Longitude to UTM
  //Declarations();
  let k0:number;
  let b:number;
  let a:number;
  let e:number;
  let f:number;
  let k:number;
  //let latd0:number;
  //let lngd0 :number;
  let lngd :number;
  let latd :number;
  let xd :number;
  let yd :number;
  let phi :number;
  let drad :number;
  let utmz :number;
  let lng :number;
  let latz :number;
  let zcm :number;
  let e0 :number;
  let esq :number;
  let e0sq :number;
  let N :number;
  let T :number;
  let C :number;
  let A :number;
  let M0 :number;
  let x :number;
  let M :number;
  let y :number;
  let yg :number;
  let g :number;
  let usft=1;
  let DatumEqRad:number[];
  let DatumFlat:number[];
  let Item;

  let lng0 :number;  

  DatumEqRad = [6378137.0, 6378137.0, 6378137.0, 6378135.0, 6378160.0, 6378245.0, 6378206.4,
    6378388.0, 6378388.0, 6378249.1, 6378206.4, 6377563.4, 6377397.2, 6377276.3, 6378137.0];	
    DatumFlat = [298.2572236, 298.2572236, 298.2572215,	298.2597208, 298.2497323, 298.2997381, 294.9786982,
    296.9993621, 296.9993621, 293.4660167, 294.9786982, 299.3247788, 299.1527052, 300.8021499, 298.2572236]; 
    Item = 1;//Default

  //drad = Math.PI/180;//Convert degrees to radians)
  k0 = 0.9996;//scale on central meridian
  a = DatumEqRad[Item];//equatorial radius, meters. 
        //alert(a);
  f = 1/DatumFlat[Item];//polar flattening.
  b = a*(1-f);//polar axis.
  e = Math.sqrt(1 - b*b/a*a);//eccentricity
  drad = Math.PI/180;//Convert degrees to radians)
  latd = 0;//latitude in degrees
  phi = 0;//latitude (north +, south -), but uses phi in reference
  e0 = e/Math.sqrt(1 - e*e);//e prime in reference
  N = a/Math.sqrt(1-Math.pow(e*Math.sin(phi),2));
  
  
  T = Math.pow(Math.tan(phi),2);
  C = Math.pow(e*Math.cos(phi),2);
  lng = 0;//Longitude (e = +, w = -) - can't use long - reserved word
  lng0 = 0;//longitude of central meridian
  lngd = 0;//longitude in degrees
  M = 0;//M requires calculation
  x = 0;//x coordinate
  y = 0;//y coordinate
  k = 1;//local scale
  utmz = 30;//utm zone
  zcm = 0;//zone central meridian

  k0 = 0.9996;//scale on central meridian
  b = a*(1-f);//polar axis.
  
  e = Math.sqrt(1 - (b/a)*(b/a));//eccentricity
  

  lngd=lngd0;
  latd=latd0;
  
  
  
  xd = lngd;
  yd = latd;


  phi = latd*drad;//Convert latitude to radians
  lng = lngd*drad;//Convert longitude to radians
  utmz = 1 + Math.floor((lngd+180)/6);//calculate utm zone
  latz = 0;//Latitude zone: A-B S of -80, C-W -80 to +72, X 72-84, Y,Z N of 84
  if (latd > -80 && latd < 72){latz = Math.floor((latd + 80)/8)+2;}
  if (latd > 72 && latd < 84){latz = 21;}
  if (latd > 84){latz = 23;}
    
  zcm = 3 + 6*(utmz-1) - 180;//Central meridian of zone
  //alert(utmz + "   " + zcm);
  //Calculate Intermediate Terms
  e0 = e/Math.sqrt(1 - e*e);//Called e prime in reference
  esq = (1 - (b/a)*(b/a));//e squared for use in expansions
  e0sq = e*e/(1-e*e);// e0 squared - always even powers
  //alert(esq+"   "+e0sq)
  N = a/Math.sqrt(1-Math.pow(e*Math.sin(phi),2));
  //alert(1-Math.pow(e*Math.sin(phi),2));
  //alert("N=  "+N);
  T = Math.pow(Math.tan(phi),2);
  //alert("T=  "+T);
  C = e0sq*Math.pow(Math.cos(phi),2);
  //alert("C=  "+C);
  A = (lngd-zcm)*drad*Math.cos(phi);
  //alert("A=  "+A);
  //Calculate M
  M = phi*(1 - esq*(1/4 + esq*(3/64 + 5*esq/256)));
  M = M - Math.sin(2*phi)*(esq*(3/8 + esq*(3/32 + 45*esq/1024)));
  M = M + Math.sin(4*phi)*(esq*esq*(15/256 + esq*45/1024));
  M = M - Math.sin(6*phi)*(esq*esq*esq*(35/3072));
  M = M*a;//Arc length along standard meridian
  
  M0 = 0;//M0 is M for some origin latitude other than zero. Not needed for standard UTM
  
  //Calculate UTM Values
  x = k0*N*A*(1 + A*A*((1-T+C)/6 + A*A*(5 - 18*T + T*T + 72*C -58*e0sq)/120));//Easting relative to CM
  x=x+500000;//Easting standard 
  y = k0*(M - M0 + N*Math.tan(phi)*(A*A*(1/2 + A*A*((5 - T + 9*C + 4*C*C)/24 + A*A*(61 - 58*T + T*T + 600*C - 330*e0sq)/720))));//Northing from equator
  yg = y + 10000000;//yg = y global, from S. Pole
  if (y < 0){y = 10000000+y;}
  //Output into UTM Boxes
        //alert(utmz);
  //console.log(utmz)  ;
  //console.log(Math.round(10*(x))/10 / usft)  ;
  //console.log(Math.round(10*y)/10 /usft)  ;
  this.lat1=Math.round(10*(x))/10 / usft  ;
  this.lng1=Math.round(10*y)/10 /usft;  
}//close Geog to UTM



irSiguiente(event){
  this.loadingBackdropService.show();
  var x = document.getElementById("map");
  this.getValueWithPromise(x);        
}

getValueWithPromise(mapElement) {
    this.resolveAfter2Seconds(2,mapElement).then(value => {
    console.log(`promise result: ${value}`);


  }).catch(function (error) {
    console.error('oops, something went wrong!', error);
  });
  ;
  console.log('I will not wait until promise is resolved');
}


resolveAfter2Seconds(image64,mapElement) {
  return new Promise(resolve => {
    setTimeout(() => {
      
      //resolve(x);
      //console.log("resolveAfter2Seconds: "+ mapElement);      
      htmlToImage.toPng(mapElement, { quality: 0.95 })
    .then((dataUrl) => {
      
      this.loadingBackdropService.hide();

      this.stepper.next();
      
       image64=dataUrl;// this prints only data:,

       this.mapaP=dataUrl;

       this.base64=dataUrl.substring(22, dataUrl.length-1);

       //this.base64=dataUrl;
       
       //console.log("#idMap"+this.ContadorMapa); // this prints only data:,
       //console.log("Imagen64: "+dataUrl);

       localStorage.setItem('imagenLcn', JSON.stringify(dataUrl) );

       //mapElement.hidden=true;


       /*var img = new Image();
        img.src = dataUrl;
        document.body.appendChild(img);*/
        return image64;
    });
    }, 0);
  });
}

  printMap(caption) {
    var printOptions = {
      container: this.map._container,
      exclude: ['.leaflet-control-zoom'],
      format: 'image/png',
      //afterRender: afterRender,
      //afterExport: afterExport
    };
    /*printOptions.caption = {
      text: caption,
      font: '30px Arial',
      fillStyle: 'black',
      position: [50, 50]
    };*/
    var promise = this.map.printExport(printOptions);
    var data = promise.then(function (result) {
      return result;
    });
  }

  verificarEmail(email:string){
 
    this.authService
      .Existe_Correo(email)     
      .subscribe(
        data => {         
          
          if (data.codigo == 0) {
            
          }
          else  {
            this.form1.patchValue({
              correo_electronico: ''
            });
            Swal.fire({
              position:'center',
              icon:'warning',
              title: 'Este Correo electrónico ya se encuentra registrado',
              showConfirmButton: false,
              timer: 2000
            })
             
           
          }
        },
        error => {
          Swal.fire({
            position:'center',
            icon:'error',
            title: 'El Servicio se encuentra fuera de línea',
            showConfirmButton: false,
            timer: 2500
          })
        }
    );
 
  }

}
