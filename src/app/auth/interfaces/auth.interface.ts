
export interface RespuestaLogin {
    codigo:      number;
    descripcion: string;
    usuario:     Usuario;    
}

export interface Usuario {
    idusuario:        number;
    login:            string;
    idpersona:        number;
    nombre:           string;
    segundo_nombre:   string;
    apellido_paterno: string;
    apellido_materno: string;
    apellido_casado:  string;
    correo:           string;
    idempresa:   number;
}

export class ParametroUsuario {
    idpersona?: number;
    nombre?: string;
    apellido_paterno?: string;
	apellido_materno?: string;
	fecha_nacimiento?: string;
    correo_electronico?: string;
    telefono_fijo?: string;
    telefono_movil?: string;
    estado?: string;
    direccion?: string; 
	fecha_alta?: string;
    usuario_alta?: string;
	fecha_baja?: string;
	usuario_baja?: string;
    documento_identidad?: string;
    complemento?: string;
    expedicion?: string;
    expiracion_documento?: string;
    nit?: string;
    genero?: string;
    estado_civil?: string;
    residencia?: string;
    tipo_lugar?: string;
    nombre_lugar?: string;
    numero_casa?: string;
    latitud?: number;
    longitud?: number;
    x?: number;
    y?: number;
    foto?: string;
    ci_anverso?: string;
    ci_reverso?: string;

	idusuario?:number;
	clave?: string;
	dias_vigencia_password?: number;
	es_cajero?: boolean;
	es_vendedor?: boolean;	
	fecha_caducidad?: string;
	fecha_vencimiento_password?: string;
	login?: string;
}

export class User {
    correo_electronico?: string;
    clave?: string;
}

export interface RespuestaUsuario {
    codigo: number;
    descripcion: string;
    mensaje: string;
    usuario: tblUsuario;
    persona: tblPersona;    
}

export interface tblUsuario {
    idusuario:        number;
    login:            string;
    idpersona:        number;
}

export interface tblPersona { 
    idpersona:        number;
    nombre:           string;
    apellido_paterno: string;
    apellido_materno: string;
    correo_electronico:           string;
}

export class ParametroCambio{
    idusuario: number;
    correo_electronico: string;
    clave: string;
    clavenueva: string;
}

export interface RespuestaPersona {
    codigo:  number;
    mensaje: string;
    persona: Persona;
}

export interface Persona {
    idpersona:            number;
    nombre:               string;
    apellido_paterno:     string;
    apellido_materno:     string;
    fecha_nacimiento:     string;
    correo_electronico:   string;
    telefono_fijo:        string;
    telefono_movil:       string;
    estado:               string;
    direccion:            string;
    documento_identidad:  string;
    complemento:          string;
    expedicion:           string;
    expiracion_documento: string;
    nit:                  string;
    genero:               string;
    estado_civil:         string;
    residencia:           string;
    tipo_lugar:           string;
    nombre_lugar:         string;
    numero_casa:          string;
    latitud:              number;
    longitud:             number;
    x:                    number;
    y:                    number;
    foto:                 string;
    ci_anverso:           string;
    ci_reverso:           string;
}

export class ParametroPersonaFotos{
    idpersona: number;
    foto: string;
    ci_anverso: string;
    ci_reverso: string;
}

export interface ParametroPersonaDatos {
    idpersona:            number;
    nombre:               string;
    apellido_paterno:     string;
    apellido_materno:     string;
    fecha_nacimiento:     string;
    telefono_fijo:        string;
    telefono_movil:       string;
    documento_identidad:  string;
    complemento:          string;
    expedicion:           string;
    expiracion_documento: string;
    nit:                  string;
    genero:               string;
    estado_civil:         string;
}

export interface ParametroPersonaUbicacion {
    idpersona:    number;
    direccion:    string;
    residencia:   string;
    tipo_lugar:   string;
    nombre_lugar: string;
    numero_casa:  string;
    latitud:      number;
    longitud:     number;
    x:            number;
    y:            number;
}