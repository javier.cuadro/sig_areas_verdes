import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { AuthService } from '../../core/services/auth.service';
import { DashboardComponent } from '../../dashboard/dashboard.component';
import Swal from 'sweetalert2';
import { MapasService } from 'src/app/services/mapas.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public showPassword: boolean = false;

  form!: FormGroup;
  message!: string;
  loginSubscription!: Subscription;
  loginLoading = false;
  static path = () => ['login'];

  constructor(
    private authService: AuthService,
    public formBuilder: FormBuilder,
    private router: Router,
    public snackBar: MatSnackBar,
    private mapaService:MapasService,
  ) {
    this.initFormBuilder();
  }

  ngOnInit() {
  }

  public togglePasswordVisibility(): void {
    this.showPassword = !this.showPassword;
  }


  loginUser() {
    this.loginLoading = true;

    console.log("inside login method");
    this.loginSubscription = this.authService
      .loginWithUserCredentials(this.form.value.email, this.form.value.password)
      .pipe(finalize(() => this.loginLoading = false))      
      .subscribe(
        data => {
          console.log("LOGIN: "+data);
          
          if (data.codigo == 0) {
            Swal.fire({
              position:'center',
              icon:'success',
              title: "Inicio de Sesión Correcto",
              showConfirmButton: false,
              timer: 2000
            })
            //this.distritosGeo(1,7);
            this.router.navigate(DashboardComponent.path());
            
          }
          else  {
            Swal.fire({
              position:'center',
              icon:'warning',
              title: data.descripcion,
              showConfirmButton: false,
              timer: 3000
            })
          }
        },
        error => {
          Swal.fire({
            position:'center',
            icon:'error',
            title: 'El Servicio se encuentra fuera de línea',
            showConfirmButton: false,
            timer: 2500
          })
        }
    );
  }

  

  public distritosGeo(idempresa:number,irdrol:number){

    this.mapaService.obtenerDatosDistritosGeo(idempresa,irdrol);
  }
  private initFormBuilder() {
    this.form = this.formBuilder.group({
      email: ['', [
        Validators.required,
        Validators.email
      ]],
      password: ['', Validators.required]
    });
  }

}
