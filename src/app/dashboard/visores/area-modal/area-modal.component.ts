import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AreaMunicipal } from '../../../classes/areaMunicipal';
import { DocumentoArea, ParametroAreaMunicipal, ParametroAreaVerdeDocumento } from '../../interfaces/areasverdes.interfaces';
import { AreaVerdeService } from '../../../services/areaverde.service';
import Swal from 'sweetalert2';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { MatTableDataSource } from '@angular/material/table';
import { finalize, Observable, ReplaySubject } from 'rxjs';
import { AuthService } from 'src/app/core/services/auth.service';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-area-modal',
  templateUrl: './area-modal.component.html',
  styles: [ `
    .mat-header-row {
      height: 70%;
      width: 100%;
      background: #005173;
    }
    .mat-header-cell {
      color: white;
    }

    .mat-icon-button {
      vertical-align: middle;
    }
    
    input:-moz-read-only { /* For Firefox */
      color: #003153;
      cursor: not-allowed;
    }

    input:read-only { 
      color: #003153;
      cursor: not-allowed;
    }
  ` ]
})
export class AreaModalComponent implements OnInit {
  
  login: string;
  cargando  : boolean = false;

  swDescarga: boolean=false;
  swAdjuntar: boolean=false;
  swActualizar: boolean=false;
  swEliminar: boolean=false;

  data: AreaMunicipal;
  form: FormGroup;
  base64_pdf: string[] = [];
  es_admin: boolean;

  public displayedColumns = ['nombre', 'idareaverde', 'seleccionar', 'idtipodocumento', 'eliminar'];
  public dataSource = new MatTableDataSource<DocumentoArea>();

  constructor(   
    private fb: FormBuilder,
    private authService: AuthService,
    private areaVerdeService: AreaVerdeService,
    private activeModal: NgbActiveModal,
    private dataService: DataService
  ) {}

  ngOnInit(): void {
    this.login= this.authService.getLogin();
    this.data= JSON.parse(localStorage.getItem('areamunicipal'));
    this.form = this.fb.group({
      idareaverde: [ this.data.idareaverde ],
      distrito:[ this.data.distrito,Validators.required],
      uv: [ this.data.uv,Validators.required],
      sit_legal: [ this.data.sit_legal ],
      nom: [ this.data.nom ],
      uso: [ this.data.uso ],
      uso_especi: [ this.data.uso_especi ],
      ley_mcpal: [ this.data.ley_mcpal ],
      instrument: [ this.data.instrument ],
      matricula: [ this.data.matricula ],
      sup_titulo: [ this.data.sup_titulo, [Validators.required, Validators.pattern(/^(0|[1-9]\d*)(\.\d+)?$/)]],
      sup_men: [ this.data.sup_men, [Validators.required, Validators.pattern(/^(0|[1-9]\d*)(\.\d+)?$/)]],
      sup_cc: [ this.data.sup_cc, [Validators.required, Validators.pattern(/^(0|[1-9]\d*)(\.\d+)?$/)]],
      n_plano: [ this.data.n_plano ],
      f_emision: [ this.data.f_emision ],
      obs: [ this.data.obs ],
    });

   this.cargarDataTable();
   this.cargarAdmin();
   this.cargarPermisos();
  }

  cargarPermisos(){
    const idusuario= this.authService.getidUsuario();
    
    this.dataService
    .getOpciones(idusuario, 2)
    .subscribe((data) => {
      if (data.codigo==0){        
        if (data.recursos.length>0){          
          for (let recurso of data.recursos){            
            if (recurso.descripcion=='descarga_area_verde'){
              this.swDescarga=true;
            }
            if (recurso.descripcion=='adjuntar_area_verde'){
              this.swAdjuntar=true;
            }
            if (recurso.descripcion=='actualizar_area_verde'){
              this.swActualizar=true;
            }
            if (recurso.descripcion=='eliminar_area_verde'){
              this.swEliminar=true;
            }
          }
        } else{

        }
      }      
    });
  }

 

  cargarAdmin(){
    const idusuario= this.authService.getidUsuario();
    
    this.areaVerdeService
    .getPerfil(idusuario)     
    .subscribe((data) => {
      if (data.codigo==0){
        
        if (data.mensaje=="Si"){
          this.es_admin=true;
        } else{
          this.es_admin=false;
        }
      } else {
        this.es_admin=false;
      }      
    });
  }

  cargarDataTable(){
    this.areaVerdeService
    .listadoArchivos(this.data.idareaverde)      
    .subscribe((data) => {
      console.log(data.mensaje);
      console.log(data.lista);     
      this.dataSource.data = data.lista;
    });
  }

  cerrar(){
    this.activeModal.close();
  }

  guardarAreaMunicipal(){
    Swal.fire({
      title: 'Está seguro?',
      text: "De Actualizar esta Area Verde!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, quiero actualizar!',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.isConfirmed) {
        let parametro : ParametroAreaMunicipal = {
          idareaverde: this.data.idareaverde,
          distrito: this.form.value.distrito|| '',
          uv: this.form.value.uv|| '',
          sit_legal: this.form.value.sit_legal|| '',
          nom: this.form.value.nom|| '',
          uso: this.form.value.uso|| '',
          uso_especi: this.form.value.uso_especi|| '',
          ley_mcpal: this.form.value.ley_mcpal|| '',
          instrument: this.form.value.instrument|| '',
          matricula: this.form.value.matricula|| '',
          sup_titulo: this.form.value.sup_titulo|| 0,
          sup_men: this.form.value.sup_men|| 0,
          sup_cc: this.form.value.sup_cc|| 0,
          n_plano: this.form.value.n_plano|| '',
          f_emision: this.form.value.f_emision|| '',
          obs: this.form.value.obs|| '',
        }
        
        this.areaVerdeService
          .actualizarAreaMunicipal(parametro)      
          .subscribe((data) => {       
            if (data.codigo==0){
              Swal.fire({
                position:'center',
                icon:'success',
                title: data.mensaje,
                showConfirmButton: false,
                timer: 3000
              })
              this.activeModal.close();
            } else {
              Swal.fire({
                position:'center',
                icon:'error',
                title: data.mensaje,
                showConfirmButton: false,
                timer: 3000
              })
              this.activeModal.close();
            }
          });
      }
    });
    return;

    
  }

  descargarPdf(idtipodocumento: number, nombre: string, motivo: string){
    let parametro : ParametroAreaVerdeDocumento = {
      idareaverde: this.data.idareaverde,
      idtipodocumento: idtipodocumento,
      base64:'',
      login: this.login,
      motivo: motivo
    }
    this.cargando = true;
    this.areaVerdeService
      .obtenerDocumento(parametro)
      .pipe(finalize(() => this.cargando = false))  
      .subscribe((data) => {       
        if (data.codigo==0 && data.mensaje.length>10){
          const source = `data:application/pdf;base64,${data.mensaje}`;
          const link = document.createElement("a");
          link.href = source;
          link.download = nombre+ "_" + this.data.idareaverde + ".pdf";
          link.click();
        } else {
          Swal.fire({
            position:'center',
            icon:'error',
            title: data.mensaje,
            showConfirmButton: false,
            timer: 3000
          })
        }
      });
  }
  
  public onFileChange(idtipodocumento : number, event) {   
    
    if (event.target.files[0].size>=25*1000*1024){
      Swal.fire({
        position:'center',
        icon:'warning',
        title: 'El Archivo PDF debe ser inferior o igual a 25 Megas',
        showConfirmButton: false,
        timer: 3000
      });

      this.base64_pdf[idtipodocumento]="";      

      return;
    }
   
    this.convertFile(event.target.files[0]).subscribe(base64 => {      
      this.base64_pdf[idtipodocumento]= base64;  
    });    
  }

  convertFile(file : File) : Observable<string> {
    const result = new ReplaySubject<string>(1);
    const reader = new FileReader();
    reader.readAsBinaryString(file);
    reader.onload = (event) => result.next(btoa(event.target.result.toString()));
    return result;
  } 

  cargarPdf(idtipodocumento:number){
    
    if (this.base64_pdf[idtipodocumento]==undefined || this.base64_pdf[idtipodocumento]==''){
      Swal.fire({
        position:'center',
        icon:'warning',
        title: 'Debe Seleccionar Archivo PDF para Cargar',
        showConfirmButton: false,
        timer: 3000
      })
      return;
    }

    let parametro : ParametroAreaVerdeDocumento = {
      idareaverde: this.data.idareaverde,
      idtipodocumento: idtipodocumento,
      base64:this.base64_pdf[idtipodocumento],
      login: this.login,
      motivo:''
    }
    this.cargando = true;
    this.areaVerdeService
      .registroDocumento(parametro)
      .pipe(finalize(() => this.cargando = false))
      .subscribe((data) => {       
        if (data.codigo==0){
          Swal.fire({
            position:'center',
            icon:'success',
            title: 'Se adjuntó correctamente',
            showConfirmButton: false,
            timer: 3000
          });
          this.base64_pdf[idtipodocumento]=undefined;
          this.cargarDataTable();
        } else {
          Swal.fire({
            position:'center',
            icon:'error',
            title: data.mensaje,
            showConfirmButton: false,
            timer: 3000
          });
          this.base64_pdf[idtipodocumento]=undefined;
        }
      });
 
    //console.log(this.base64_documento);
  }

  eliminar_documento(idtipodocumento: number){
    Swal.fire({
      title: 'Está seguro?',
      text: "De Eliminar el documento de esta Area Verde!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, quiero eliminar!',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.isConfirmed) {
        let parametro : ParametroAreaVerdeDocumento = {
          idareaverde: this.data.idareaverde,
          idtipodocumento: idtipodocumento,
          base64: '',
          login: this.login,
          motivo: ''
        }
        this.cargando = true;
        this.areaVerdeService
          .eliminarDocumento(parametro)
          .pipe(finalize(() => this.cargando = false))
          .subscribe((data) => {       
            if (data.codigo==0){
              Swal.fire({
                position:'center',
                icon:'success',
                title: 'Se eliminó correctamente',
                showConfirmButton: false,
                timer: 3000
              });
              this.cargarDataTable();
            } else {
              Swal.fire({
                position:'center',
                icon:'error',
                title: data.mensaje,
                showConfirmButton: false,
                timer: 3000
              });
            }
          });
      }
    });
    return;
    
  }

  abrirDialogoDownload(idtipodocumento: number, nombre: string){
   
    Swal    
    .fire({
        allowOutsideClick: false,
        title: "Debe introducir Motivo para proceder con la descarga",
        input: "text",
        showCancelButton: true,
        confirmButtonText: "Descargar",
        cancelButtonText: "Cancelar",
        inputValidator: motivo => {
            // Si el valor es válido, debes regresar undefined. Si no, una cadena
            if (!motivo) {
              return "Por favor escriba el motivo";
            } else if (motivo.length<20){
              return "El motivo debe ser un poco más detallado, mínimo 20 caracteres"
            } else {
               return undefined;
            }
        }
    })
    .then(resultado => {
        if (resultado.value) {
            this.descargarPdf(idtipodocumento, nombre, resultado.value)
        }
    });
  }
    
}

