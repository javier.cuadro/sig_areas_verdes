import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisorCapasComponent } from './visor-capas.component';

describe('VisorCapasComponent', () => {
  let component: VisorCapasComponent;
  let fixture: ComponentFixture<VisorCapasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisorCapasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisorCapasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
