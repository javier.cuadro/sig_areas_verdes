import { Component,Inject,OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import * as htmlToImage from 'html-to-image';


import { geoJSON, GeoJSONOptions,icon,control ,latLng, Layer, layerGroup, Map, marker, point, polyline, tileLayer } from 'leaflet';
import { MapasService } from 'src/app/services/mapas.service';
import { Marcador } from 'src/app/classes/marcador.class';
import { UploadFilesService } from 'src/app/services/upload-files.service';


//import * as L from 'leaflet';
//import * as shp from "shpjs"

import "leaflet";

declare let L;

//import * as shp from "shpjs";
import * as shp from "shpjs";

//import { environment } from './../environments/environment'
import "Leaflet.UTM";
//import "leaflet.easy-button";
import "leaflet-easybutton";

import { LeafletModule } from '@asymmetrik/ngx-leaflet';

import { LotesService } from 'src/app/services/lotes.service';

import { Lote } from 'src/app/classes/lote';

import { Router } from '@angular/router';
import { async } from '@angular/core/testing';
import { MarcadorLote } from '../../../classes/marcadorlote.class';
import { fromEvent, Observable, ReplaySubject, Subscription, map } from 'rxjs';
import { ResizeService } from 'src/app/services/resize.service';

import "leaflet-multicontrol";
import { CallesService } from 'src/app/services/calles.service';
import { Properties_Lotes } from '../../../classes/properties_lotes';
import { defaultMaxListeners } from 'events';
import { DataService } from '../../../services/data.service';
import { MatStepper } from '@angular/material/stepper';
import { URLReporte } from 'src/app/classes/urlReporte';
import { QrbnbService } from 'src/app/services/qrbnb.service';
import { TramiteJurisdiccion } from '../../../classes/tramiteJurisdiccion';
import { LcnService } from 'src/app/services/lcn.service';
import { respuestaQR } from 'src/app/classes/respuestaQr';
import { AreaMunicipal } from '../../../classes/areaMunicipal';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { areaMunicipal } from 'src/app/interfaces/lcn.interfaces';
import { ParametroAreaVerdeDocumento } from '../../interfaces/areasverdes.interfaces';
import Swal from 'sweetalert2';
import { AreaVerdeService } from 'src/app/services/areaverde.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AreaModalComponent } from '../area-modal/area-modal.component';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
import { environment } from 'src/environments/environment';

import { IAxiomTreeSearch } from 'axiom-ngx-tree';
import { persons } from '../sample-data';
import {CdkAccordionModule} from '@angular/cdk/accordion';
import { coordenada } from '../../jurisdiccion/jurisdiccion-ubicacion/jurisdiccion-ubicacion.component';
import { DistritoGeo } from 'src/app/classes/distritoGeo';
import { AuthService } from 'src/app/core/services/auth.service';
import { AreaMunicipio } from 'src/app/classes/areaMunicipio';
import { Capas } from 'src/app/classes/capas';
import {finalize} from 'rxjs/operators';


@Component({
  selector: 'app-visor-capas',
  templateUrl: './visor-capas.component.html',
  styleUrls: ['./visor-capas.component.scss']
})
export class VisorCapasComponent implements OnInit {

  public vecImageOverlays:DistritoGeo[] =[];
         
  clsDistritosGeo: DistritoGeo =new DistritoGeo(1);
  jsonDistritosGeo:any;
  items = ['REESTRUCTURACIONES', 'BUSCADOR'];
  expandedIndex = 0;
  secondPanelOpenState: boolean = false;

  model: any;
  //data =  persons;
  //data=localStorage.getItem("")
  data=JSON.parse(localStorage.getItem('jsondistritosgeo')!);
  
  //data;

  
  selectedItems: any[];
  selectedItem: any;
  imageOverlay1:any;
  imageOverlay2:any;
  imageOverlayBus:any;
  checked=0;
  search: IAxiomTreeSearch = (item: any, search: string): boolean => {
    return item.first_name.toLowerCase().indexOf(search) > -1;
  };

  selectedFiles?: FileList;
  currentFile?: File;
  progress = 0;
  message = '';
  fileInfos?: Observable<any>;


  @ViewChild('stepper') stepper: MatStepper;
  miFormulario_Coordenada: FormGroup = this.fbCoordenada.group({

    valorx:[''],
    valory:[''],    
  });

  groupBuscador;
  resBusquedaCoordenada;
  resBuscarGeografico;

  displayStyle = "none";
 
   private resizeSubscription: Subscription;
   private resizeObservable$: Observable<Event>;
   private resizeSubscription$: Subscription;

   areamunicipal= new AreaMunicipal(0,"","","","","","","","","","","","","","","");

   public areamunicipio= new AreaMunicipio("","","","","","","");

   //data: AreaMunicipio;
   public areamunicipioXcentro;
   public capas= new Capas(0,"","","");
   properties_lote: Properties_Lotes;

   form: FormGroup = this.fb.group({
 
    documento:['',Validators.required],   
  })
 
   miFormulario_Jurisdiccion: FormGroup = this.fb.group({
 
     superficiemensura:['',Validators.required],
     uv:['',Validators.required],
     manzana:['',Validators.required],
     lote:['',Validators.required],
     coordenada_latitud:['',Validators.required],
     coordenada_longitud:['',Validators.required],
     poligono:['',Validators.required],
 
     
     precio: [ , [ Validators.required, Validators.min(0)] ],
     existencias: [ , [ Validators.required, Validators.min(0)] ],
   })
 
 
   firstFormGroup = this._formBuilder.group({
     firstCtrl: ['', Validators.required],
   });
   secondFormGroup = this._formBuilder.group({
     secondCtrl: ['', Validators.required],
   });
 
   /*FormGroupBusqueda = this._formBuilder.group({
     secondCtrl: ['', Validators.required],
   });*/
 
 
 
   FormGroupBusqueda: FormGroup = this.fbBusqueda.group({
 
     busdm:[''],
     busuv:[''],
     busmz:[''],
     buslt:[''],    
     busx:[''],    
     busy:[''],    
   });
 
 
   isLinear = false;
 
 
   ContadorMapa:number=0;  
 
  public geo_json_data;
   title = 'Sig alcaldia';
   json;
   layerFarmacias;
   map;
   //streetLabelsRenderer;
   marcadores : MarcadorLote[]=[];
   marker;
   i=0;
   
 
   lat = -17.7876515;
   lng = -63.1825049;
   lat1=0;
   lng1=0;
   lote= new Lote(0,'');
   //areamunicipal= new AreaMunicipal(0,"","","","","","","","","","","","","","","");

   bandera=0;
 
 
   MAPELEMENT:any;
   geoJSon;
 
 
   public jurisdiccion_uv;
   public jurisdiccion_manzana;
   public jurisdiccion_lote;
   public jurisdiccion_latitud;
   public jurisdiccion_longitud;     
   public jurisdiccion_poligono;     
   ourCustomControl: any;
 
 
 ////////////////////////Pago QR
 estadoqr = new FormControl('');  
 imagenqr = new FormControl(''); 
 objtoken:any;
 objqr:any;
 token:any;
 public base64:string='';
 public idqr:string='';
 identificadorDeTemporizador;
 public identificadorDeTemporizador_tiempo:number=0;
 
 
 urlReporte:any;
   ProcesoCompleto: number;
 tramiteJurisdicion:TramiteJurisdiccion;
 respuestaTramite:any;
 respuestaQr:any= new respuestaQR(0,'','','');
 respuestaConsultaQr:any;
  filename: string;
 
 
 
 
   getColor(d) {
     return d > 100000000 ? '#800026' :
     d > 50000000 ? '#BD0026' :
     d > 4 ? '#EFEFEF' :
     d > 3 ? '#000287' :
     d > 2 ? '#ccff00' :
     d > 1 ? '#d9880f' :
     d > 0 ? '#0b5e05' :
     '#FFEDA0'}
     
 
 
 
 
   style = feature => {
     return {
       weight: 3,
       opacity: 1,
       //color: 'white',
       dashArray: '1',
       fillOpacity: 0.8,
       color: this.getColor(feature.properties.tiposvia)
     };
   }
   styleManzana = feature => {
     return {
       weight: 3,
       opacity: 1,
       color: 'white',
       dashArray: '1',
       fillOpacity: 0.8,
       //color: this.getColor(feature.properties.tiposvia)
     };
   }
 
    popup(feature, layer) {
     if (feature.properties && feature.properties.nombrevia) {
       layer.bindPopup(feature.properties.nombrevia);
     }
 
     /*if (feature.properties && feature.properties.lt) {
       this.jurisdiccion_lote=feature.properties.lt
       console.log("LOTE_"+this.jurisdiccion_lote);
 
       //this.miFormulario_Jurisdiccion.controls['lote'].setValue("555");
       //localStorage.setItem("jurisdiccion_lote",feature.properties.lt);
 
       //this.miFormulario_Jurisdiccion.get("lote").setValue(feature.properties.lt.toString());
       
       alert("lote");
     }
     if (feature.properties && feature.properties.mz) {
       this.jurisdiccion_manzana=feature.properties.mz
       localStorage.setItem("jurisdiccion_manzana",feature.properties.mz);
     }
     if (feature.properties && feature.properties.uv) {
       this.jurisdiccion_uv=feature.properties.uv
       localStorage.setItem("jurisdiccion_uv",feature.properties.uv);
     }*/
 
     
 
 
 
 
 
     /*for(let i in layer.features)
     layer.features[i].properties.color = this.getColor( layer.features[i].properties.jerarquia );*/
   }
 
  
   
   ngOnInit() {
    

    
    
     //console.log("IDROL: "+idrol);

     //this.distritosGeo(idempresa,idrol);
     this.data=JSON.parse(localStorage.getItem('jsondistritosgeo')!);

     //console.log("Empresa: " + idrol);     
      //this.AreaMunicipioLCN(idempresa) ;                  
     
     //console.log(this.router.url);
     
     setTimeout(() => {
      
    }, 3000);
    setTimeout(() => {
     
    }, 3000);
 
     this.urlReporte=new URLReporte(0,'','');
     
     this.resizeSubscription = this.resizeService.onResize$
       .subscribe(size => console.log(size));         
 
       this.resizeObservable$ = fromEvent(window, 'resize')
       this.resizeSubscription$ = this.resizeObservable$.subscribe( evt => {
         console.log('event: ', evt)
       })

       //this.capas =JSON.parse(localStorage.getItem('capas')!);
       

       

       
   }
   ngOnDestroy() {
     if (this.resizeSubscription) {
       this.resizeSubscription.unsubscribe();
     }
     this.resizeSubscription$.unsubscribe()
   }
 
 
   //constructor(private _formBuilder: FormBuilder);
   //constructor(private resizeService: ResizeService,private _formBuilder: FormBuilder, private mapaService:MapasService,private lotesService:LotesService,private router:Router ) { 
     constructor(private dataService:DataService ,
      private authService: AuthService,
       private resizeService: ResizeService,
       private fb: FormBuilder,
       private fbCoordenada: FormBuilder,
       private fbBusqueda: FormBuilder, 
       private _formBuilder: FormBuilder,
       private mapaService:MapasService,
       private calleService:CallesService,
       private router:Router,
       private qr:QrbnbService,
       private lcnService:LcnService,
       public dialog: MatDialog,
       private modalService: NgbModal,
       private uploadService: UploadFilesService,
       @Inject(DOCUMENT) private document: Document
       ) { 
     //console.log('ss');
     //alert("Constructor");

     




     
   }
 

   public distritosGeo(idempresa:number,idrol:number){

    this.mapaService.obtenerDatosDistritosGeo(idempresa,idrol);
  }

  public llamada(idempresa){

    return new Promise((resolve,reject)=>{
      this.mapaService
     .obtenerDatosAreaMunicipio_php(idempresa) 
     //.pipe(finalize(() => console.log("terminado")))    
     .subscribe(
      (data)  => {                                               
              this.areamunicipio= (data) ;
              resolve(data);
              localStorage.setItem('areaMunicipio', JSON.stringify(data));
                
            
    });
    setTimeout(()=>{
      console.log(".");                        
        ;} , 5000
        );                        
    });
  
  }



   AreaMunicipioLCN(idempresa: number){

    this.mapaService
     .obtenerDatosAreaMunicipio_php(idempresa) 
     //.pipe(finalize(() => console.log("terminado")))    
     .subscribe(
      async(data)  => {                               
            //console.log( this.areamunicipio[0].xcentro ); 

            
              //Promise.resolve(data);
              this.areamunicipio=  (data) ;
              
                localStorage.setItem('areaMunicipio', JSON.stringify(await this.areamunicipio));
                //alert("entro");
            
    });

   }


async AreaMunicipioP(idempresa: number){    

await this.llamada(idempresa) ;                

}

/*public capasPhp(idempresa: number,ruta:string,idrol:number){

  this.mapaService.obtenerDatosCapas_php(idempresa,ruta,idrol);
}*/

 capasPhp(idempresa: number,ruta:string,idrol:number) {

  this.mapaService
   .obtenerDatosCapas_php(idempresa,ruta,idrol)
   //.pipe(finalize(() => console.log("terminado")))    
   .subscribe(
    async(data)  => {                               
          //console.log( this.areamunicipio[0].xcentro ); 

          sessionStorage.setItem('capas', JSON.stringify(data));
            //Promise.resolve(data);
            this.capas=  (data) ;
            
              //localStorage.setItem('capas', JSON.stringify(await this.capas));
              //localStorage.setItem('capas',JSON.stringify(resp));
              //alert("entro capasPhp");
          
  });

 }



      
 
 
 
 // Define our base layers so we can reference them multiple times
 streetMaps =L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
   detectRetina: true,
   attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
   maxZoom: 22,
 });
 
 esri = L.tileLayer(  
   'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
   maxZoom: 25,
   minZoom: 1,
   attribution: 'Tiles © Esri',
 });


 
 
 
 /*wmsLayers = L.tileLayer.wms(environment.backend_geoserver.host+"/geoserver/gamsc/wms?",{
     layers: "lotes_cartografia_yina",
     format: 'image/png',
     maxZoom: 19,
     transparent: true,
     version: '1.3.0',
     attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
   }
 );
 
 wms_lim_jur = L.tileLayer.wms(environment.backend_geoserver.host+"/geoserver/gamsc/wms?",{
     layers: "limite_jurisdiccion",
     format: 'image/png',
     maxZoom: 19,
     transparent: true,
     version: '1.3.0',
     attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
   }
 );
 
 wms_lim_mun = L.tileLayer.wms(environment.backend_geoserver.host+"/geoserver/gamsc/wms?",{
     layers: "limite_municipio",
     format: 'image/png',
     maxZoom: 19,
     transparent: true,
     version: '1.3.0',
     attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
   }
 );

 //wms_area_mun_uso = L.tileLayer.wms("http://localhost:8080/geoserver/gamsc/wms?",{
  wms_area_mun_uso = L.tileLayer.wms(environment.backend_geoserver.host+"/geoserver/gamsc/wms?",{
     layers: "areas_municipales_uso",
     format: 'image/png',
     maxZoom: 19,
     transparent: true,
     version: '1.3.0',
     attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
   }
 );

 
 
 wms_lotes = L.tileLayer.wms(environment.backend_geoserver.host+"/geoserver/gamsc/wms?",{
     layers: "predios_2",
     format: 'image/png',
     maxZoom: 19,
     transparent: true,
     version: '1.3.0',
     attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
   }
 );
 
 
 wms_uv_yina = L.tileLayer.wms(environment.backend_geoserver.host+"/geoserver/gamsc/wms?",{
     layers: "unidades_vecinales_yina",
     format: 'image/png',
     maxZoom: 19,
     transparent: true,
     version: '1.3.0',
     attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
   }
 );
 wms_manzanas_yina = L.tileLayer.wms(environment.backend_geoserver.host+"/geoserver/gamsc/wms?",{
     layers: "manzanas_yina",
     format: 'image/png',
     maxZoom: 19,
     transparent: true,
     version: '1.3.0',
     attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
   }
 );
 */
 
 
 
 
 
 
 
 
 
 options = {
   layers: [ this.esri],
   zoom: 11,
   center: L.latLng([ -17.7876515, -63.1825049 ]),
   //maxBounds: [[-17.622216426146586, -63.28947607997527],[-17.902058376130743, -63.00586013670787]],
 
   //lat = -17.7876515;
   //lng = -63.1825049;
 };
 
 /*overlayMaps1 = [
   //{name:"general" , layer: this.streetMaps},
   {name:"Satélite" , layer: this.esri},  
   {name:"Áreas verdes según Uso" , layer: this.wms_area_mun_uso},
   {name:"Límite Municipal" , layer: this.wms_lim_mun},   
   {name:"Límite Jurisdicción" , layer:this.wms_lim_jur},
   {name:"Uv" , layer:this.wms_uv_yina},
   {name:"Manzanas" , layer:this.wms_manzanas_yina},  
   {name:"lotes" , layer:this.wms_lotes},  
         
 ];*/

  
 
 
 
 
   onMapReady(map) {
    //window.location.reload();
    //alert("Mapa");
    
   // this.capas =JSON.parse(localStorage.getItem('capas')!);
    //console.log('this.jsonDistritosGeo: '+this.jsonDistritosGeo);

    /*
    setTimeout(() => {
      this.map = map;
      map.invalidateSize();        
    }, 0);

    setTimeout(() => {
      console.log(" tre segundo");
    }, 3000);*/


    var idempresa,idrol;
     idempresa = this.authService.getIdEmpresa();     
     idrol = this.authService.getIdRol() ;
     //console.log("Datos capa:" + idempresa+" - "+this.router.url+"-"+idrol);
     //this.capasPhp(idempresa,this.router.url,idrol);

     this.areamunicipio =JSON.parse(localStorage.getItem('areaMunicipio')!);        
     
     
     
     //this.capas =JSON.parse(sessionStorage.getItem('capas')!);

     this.mapaService
            .obtenerDatosCapas_php(idempresa,this.router.url,idrol)
            .pipe(finalize(() => console.log("terminado")))    
            .subscribe(
            async(data)  => {                               
                  //console.log( this.areamunicipio[0].xcentro ); 

                  //sessionStorage.setItem('capas', JSON.stringify(data));
                    //Promise.resolve(data);
                    this.capas=  (data) ;                                                                             
                      this.dataService.nombreTramite="VISOR DE CARTOGRAFIA";
                  
                      //document.getElementById('map').style.cursor = 'crosshair'
                  
                      setTimeout(()=>{
                        
                      //this.toLatLngBtn();

                      this.map = map;
                        //var wms_Capas:Capas1[]=[];
                        var wms_Capas_final:L.TileLayer.WMS[]=[];                        
                        var overlaymap=[];
                        var tabla_uso_suelo:string ;
                        
                        





                        
                        map.setView([this.areamunicipio[0].xcentro,this.areamunicipio[0].ycentro], 15);
                                      
                          var esri = L.tileLayer(  
                            'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
                            maxZoom: 19,
                            minZoom: 1,
                            attribution: 'Tiles © Esri',
                          });                      
                        
                          var i=0;
                        for (let clave in this.capas){
                          console.log(this.capas[i].nombregeoserver);
                            if (this.capas[i].titulocapa==="Áreas verdes según uso"){
                              console.log("Áreas verdes según uso: "+this.capas[i].nombregeoserver);
                            }

                          wms_Capas_final.push(       
                            L.tileLayer.wms(this.areamunicipio[0].ip_geoserver,{          
                              layers: this.capas[i].nombregeoserver,                    
                              format: 'image/png',
                              maxZoom: 19,
                              transparent: true,
                              version: '1.3.0',
                              attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'             
                            })
                          );      
                        i=i+1;
                        }
                        i=0;
                        
                        overlaymap.push({name:"Satélite" , layer: esri});

                        for (let clave in wms_Capas_final){
                          overlaymap.push(
                            { name:this.capas[i].titulocapa , layer:wms_Capas_final[i]}
                            );
                            i=i+1;
                        } 
                  
                  
                        var fitBoundsSouthWest = new L.LatLng(this.areamunicipio[0].xsouthwest, this.areamunicipio[0].ysouthwest);
                        var fitBoundsNorthEast = new L.LatLng(this.areamunicipio[0].xsoutheast, -this.areamunicipio[0].ysoutheast);
                        var fitBoundsArea = new L.LatLngBounds(fitBoundsSouthWest, fitBoundsNorthEast);
                        this.map.setMaxBounds(fitBoundsArea); 
                      
                      
                          L.Icon.Default.imagePath = "assets/leaflet/";
                      
                          let geojson;
                          
                          this.map = map;
                          this.map.setMaxBounds(fitBoundsArea);
                          
                          
                      
                            
                      
                        
                              
                            L.control.scale().addTo(this.map);
                      
                            var markersLayer = new L.LayerGroup();	//layer contain searched elements    
                            map.addLayer(markersLayer);  
                      
                      
                      
                            
                            
                            let mc=L.multiControl(overlaymap, {position:'topright', label: 'Mapas'}).addTo(map);   

                            let ebSearch=L.easyButton('fa-search', function(btn, map){
                              //helloPopup.setLatLng(map.getCenter()).openOn(map);        
                              let b = document.getElementById("sidebar");
                                  b.click();            
                            }).addTo(map );

                            /*let ebDistritos=L.easyButton('fa-braille', function(btn, map){
                              //helloPopup.setLatLng(map.getCenter()).openOn(map);        
                              let b = document.getElementById("distritos");
                                  b.click();            
                            }).addTo(map );*/

                            let ebSearch_capa=L.easyButton('fa-map', function(btn, map){
                              //helloPopup.setLatLng(map.getCenter()).openOn(map);        
                              let b = document.getElementById("cargar_capa");
                                  b.click();  
                                  //this.openPopup();
                                  //this.displayStyle = "block";
                            }).addTo(map );
                      
                            let ebmc=L.easyButton('fa-globe', function(btn, map){        
                              //helloPopup.setLatLng(map.getCenter()).openOn(map);                            
                                  mc.toggle();
                            }).addTo(map );                            
                            

                            //L.imageOverlay(imageUrl, imageBounds).bringToFront();
                      
                      
                            

                        
                  
                          ;} , 500
                          );

                          
                        });

                        setTimeout(() => {
                          this.map = map;
                          map.invalidateSize();        
                        }, 0);

                        

                         // Opacity slider
          var slider = L.DomUtil.get('range-overlay-map');
          L.DomEvent.addListener(slider, 'change', function() {

              //source.setOpacity(this.value);
              //alert(this.value);
              //this.esri.setOpacity(this.value);

              /*for (let id of this.vecImageOverlays){
                //item.Opacity=this.value;
                this.map(id).Opacity=this.value;
              }*/

              //for (let id of this.vecImageOverlays){
                //console.log("RemoveMarkers-"+id.id);
                //this.map.removeLayer(id);

              //}   
              
              //for (let id of this.vecImageOverlays){
                for (let item of Object.keys(this.vecImageOverlays)) {
                //console.log("RemoveMarkers-"+id.id);
                this.map.removeLayer(item);
              }
            });
                  
    }  
                  
                  
                  
                    /*overlayMaps1 = [{name: 'Marker', layer: this.marker3},
                    {name: 'OpenStreepMap', layer: this.streetMaps},
                    {name: 'Esri', layer: this.esri},
                    {name: 'Marker3', layer: this.wMaps}];*/
                  
                  
                    
                  
                  
                      
                  GeogToUTM(latd0: number,lngd0 : number){
                    //Convert Latitude and Longitude to UTM
                    //Declarations();
                    let k0:number;
                    let b:number;
                    let a:number;
                    let e:number;
                    let f:number;
                    let k:number;
                    //let latd0:number;
                    //let lngd0 :number;
                    let lngd :number;
                    let latd :number;
                    let xd :number;
                    let yd :number;
                    let phi :number;
                    let drad :number;
                    let utmz :number;
                    let lng :number;
                    let latz :number;
                    let zcm :number;
                    let e0 :number;
                    let esq :number;
                    let e0sq :number;
                    let N :number;
                    let T :number;
                    let C :number;
                    let A :number;
                    let M0 :number;
                    let x :number;
                    let M :number;
                    let y :number;
                    let yg :number;
                    let g :number;
                    let usft=1;
                    let DatumEqRad:number[];
                    let DatumFlat:number[];
                    let Item;
                  
                    let lng0 :number;  
                  
                    DatumEqRad = [6378137.0, 6378137.0, 6378137.0, 6378135.0, 6378160.0, 6378245.0, 6378206.4,
                      6378388.0, 6378388.0, 6378249.1, 6378206.4, 6377563.4, 6377397.2, 6377276.3, 6378137.0];	
                      DatumFlat = [298.2572236, 298.2572236, 298.2572215,	298.2597208, 298.2497323, 298.2997381, 294.9786982,
                      296.9993621, 296.9993621, 293.4660167, 294.9786982, 299.3247788, 299.1527052, 300.8021499, 298.2572236]; 
                      Item = 1;//Default
                  
                    //drad = Math.PI/180;//Convert degrees to radians)
                    k0 = 0.9996;//scale on central meridian
                    a = DatumEqRad[Item];//equatorial radius, meters. 
                          //alert(a);
                    f = 1/DatumFlat[Item];//polar flattening.
                    b = a*(1-f);//polar axis.
                    e = Math.sqrt(1 - b*b/a*a);//eccentricity
                    drad = Math.PI/180;//Convert degrees to radians)
                    latd = 0;//latitude in degrees
                    phi = 0;//latitude (north +, south -), but uses phi in reference
                    e0 = e/Math.sqrt(1 - e*e);//e prime in reference
                    N = a/Math.sqrt(1-Math.pow(e*Math.sin(phi),2));
                    
                    
                    T = Math.pow(Math.tan(phi),2);
                    C = Math.pow(e*Math.cos(phi),2);
                    lng = 0;//Longitude (e = +, w = -) - can't use long - reserved word
                    lng0 = 0;//longitude of central meridian
                    lngd = 0;//longitude in degrees
                    M = 0;//M requires calculation
                    x = 0;//x coordinate
                    y = 0;//y coordinate
                    k = 1;//local scale
                    utmz = 30;//utm zone
                    zcm = 0;//zone central meridian
                  
                    k0 = 0.9996;//scale on central meridian
                    b = a*(1-f);//polar axis.
                    
                    e = Math.sqrt(1 - (b/a)*(b/a));//eccentricity
                    
                  
                    lngd=lngd0;
                    latd=latd0;
                    
                    
                    
                    xd = lngd;
                    yd = latd;
                  
                  
                    phi = latd*drad;//Convert latitude to radians
                    lng = lngd*drad;//Convert longitude to radians
                    utmz = 1 + Math.floor((lngd+180)/6);//calculate utm zone
                    latz = 0;//Latitude zone: A-B S of -80, C-W -80 to +72, X 72-84, Y,Z N of 84
                    if (latd > -80 && latd < 72){latz = Math.floor((latd + 80)/8)+2;}
                    if (latd > 72 && latd < 84){latz = 21;}
                    if (latd > 84){latz = 23;}
                      
                    zcm = 3 + 6*(utmz-1) - 180;//Central meridian of zone
                    //alert(utmz + "   " + zcm);
                    //Calculate Intermediate Terms
                    e0 = e/Math.sqrt(1 - e*e);//Called e prime in reference
                    esq = (1 - (b/a)*(b/a));//e squared for use in expansions
                    e0sq = e*e/(1-e*e);// e0 squared - always even powers
                    //alert(esq+"   "+e0sq)
                    N = a/Math.sqrt(1-Math.pow(e*Math.sin(phi),2));
                    //alert(1-Math.pow(e*Math.sin(phi),2));
                    //alert("N=  "+N);
                    T = Math.pow(Math.tan(phi),2);
                    //alert("T=  "+T);
                    C = e0sq*Math.pow(Math.cos(phi),2);
                    //alert("C=  "+C);
                    A = (lngd-zcm)*drad*Math.cos(phi);
                    //alert("A=  "+A);
                    //Calculate M
                    M = phi*(1 - esq*(1/4 + esq*(3/64 + 5*esq/256)));
                    M = M - Math.sin(2*phi)*(esq*(3/8 + esq*(3/32 + 45*esq/1024)));
                    M = M + Math.sin(4*phi)*(esq*esq*(15/256 + esq*45/1024));
                    M = M - Math.sin(6*phi)*(esq*esq*esq*(35/3072));
                    M = M*a;//Arc length along standard meridian
                    
                    M0 = 0;//M0 is M for some origin latitude other than zero. Not needed for standard UTM
                    
                    //Calculate UTM Values
                    x = k0*N*A*(1 + A*A*((1-T+C)/6 + A*A*(5 - 18*T + T*T + 72*C -58*e0sq)/120));//Easting relative to CM
                    x=x+500000;//Easting standard 
                    y = k0*(M - M0 + N*Math.tan(phi)*(A*A*(1/2 + A*A*((5 - T + 9*C + 4*C*C)/24 + A*A*(61 - 58*T + T*T + 600*C - 330*e0sq)/720))));//Northing from equator
                    yg = y + 10000000;//yg = y global, from S. Pole
                    if (y < 0){y = 10000000+y;}
                    //Output into UTM Boxes
                          //alert(utmz);
                    //console.log(utmz)  ;
                    //console.log(Math.round(10*(x))/10 / usft)  ;
                    //console.log(Math.round(10*y)/10 /usft)  ;
                    this.lat1=Math.round(10*(x))/10 / usft  ;
                    this.lng1=Math.round(10*y)/10 /usft;  
                  }//close Geog to UTM
                  
                  
                  
                  agregarMarcadorJurisdiccion(evt){
                  
                    this.miFormulario_Jurisdiccion.controls['coordenada_latitud'].setValue('');
                    this.miFormulario_Jurisdiccion.controls['coordenada_longitud'].setValue('');  
                    this.miFormulario_Jurisdiccion.controls['superficiemensura'].setValue('');        
                    this.miFormulario_Jurisdiccion.controls['uv'].setValue('');
                    this.miFormulario_Jurisdiccion.controls['manzana'].setValue('');
                    this.miFormulario_Jurisdiccion.controls['lote'].setValue(''); 
                    this.miFormulario_Jurisdiccion.controls['poligono'].setValue(''); 
                  
                    let i = 0;
                    
                    var coordenada =  evt.latlng;
                    var latitud = coordenada.lat; // lat  es una propiedad de latlng
                    var longitud = coordenada.lng; // lng también es una propiedad de latlng
                    this.i=this.i+1;
                  
                    this.GeogToUTM(latitud,longitud);
                    
                    
                    
                      if (this.geoJSon!=null){
                        if(this.map.hasLayer(this.geoJSon) ) {
                  
                        //console.log(this.geoJSon);  
                          this.map.removeLayer(this.geoJSon);
                        //this.map.removeLayer(marker);
                        } 
                      }
                  
                      this.jurisdiccion_latitud=this.lat1;
                      this.jurisdiccion_longitud=this.lng1;
                  
                      this.miFormulario_Jurisdiccion.controls['coordenada_latitud'].setValue(this.jurisdiccion_latitud);
                      this.miFormulario_Jurisdiccion.controls['coordenada_longitud'].setValue(this.jurisdiccion_longitud);
                  
                      
                  
                    this.mapaService.obtenerAreaMunicipal(this.lat1,this.lng1).subscribe((data)=>{
                      this.geo_json_data = data;
                      //console.log(this.geo_json_data);
                      //this.initStatesLayer();
                      
                  
                      this.geoJSon  = L.geoJSON(this.geo_json_data, {
                        style: this.style,
                        //onEachFeature: this.onEachFeature,
                        onEachFeature: this.popup,                
                      }).addTo(this.map);
                      /*console.log(this.jurisdiccion_uv);        
                      console.log(this.jurisdiccion_manzana);        
                      console.log(this.jurisdiccion_lote);        */
                      
                  
                      /*this.geoJSon  = L.geoJSON(this.geo_json_data, {
                        style: this.style,
                        //onEachFeature: this.onEachFeature,
                        onEachFeature: {
                          if (feature.properties && feature.properties.popupContent);
                        }*/
                  
                        //console.log("GEOSON _"+data[0]);
                        
                          
                      
                      
                      //this.properties_lote= JSON.stringify(this.geoJSon);
                  
                      
                      //let json_Object = JSON.stringify(this.geo_json_data);
                      //console.log("json_Object"+json_Object);  
                  
                    });
                  
                    //console.log("Lote: "+this.geoJSon);
                  
                    //localStorage.setItem("jurisdiccion_lote",feature.properties.mz);
                    //localStorage.getItem("jurisdiccion_lote");
                  
                  
                  
                    
                    /*console.log(localStorage.getItem("jurisdiccion_lote"));
                    console.log(localStorage.getItem("jurisdiccion_manzana"));
                    console.log(localStorage.getItem("jurisdiccion_uv"));*/
                    
                  
                    /*onEachFeature: function (feature, layer) {
                      let label = String(feature.properties.medida);          
                      //layer.bindTooltip(feature.properties.mz);
                      //layer.bindTooltip(label, {permanent: true, opacity: 0.7}).openTooltip();
                      layer.bindTooltip(label, {permanent: true, opacity: 0.7}).openTooltip();
                  }*/
                    
                  
                    
                  
                    
                  
                    this.mapaService.obtenerDatosAreaMunicipal (this.lat1,this.lng1)
                    .subscribe(areamunicipal =>{ this.areamunicipal = areamunicipal;              
                          
                          /*this.miFormulario_Jurisdiccion.controls['superficiemensura'].setValue(lote[0].shape_area);        
                          this.miFormulario_Jurisdiccion.controls['uv'].setValue(lote[0].uv);
                          this.miFormulario_Jurisdiccion.controls['manzana'].setValue(lote[0].mz);
                          this.miFormulario_Jurisdiccion.controls['lote'].setValue(lote[0].lt);                */
                          //alert(areamunicipal[0].uv);
                          //console.log(this.areamunicipal);
                          this.openDialog();

                      }
                    ); 
                  
                    
                        
                    }
                  
                  
                    
                    
                    buscarGeografico(){

                      //console.log("seleccionados: "+this.selectedItems[0].idmapreestructuracion); 

                      
                      /*latLngBounds = L.latLngBounds([[-17.82985458776867,-63.205451591115356], [-17.832013712275735,-63.20281686958617]]);
                      //this.imageOverlay1 = L.imageOverlay("./assets/mapas/DM10_UV117.jpg", latLngBounds).addTo(this.map).bringToBack();
                      this.imageOverlay1 = L.imageOverlay("./assets/mapas/DM10_UV117.jpg", latLngBounds, {
                        opacity: 1,
                        errorOverlayUrl: "https://cdn-icons-png.flaticon.com/512/110/110686.png",
                        alt: "Imagen de GAMSC DiT",
                        interactive: false
                      }).addTo(this.map);//.bringToBack();*/
                      
                      


                      

                      /*if (this.checked==0){
                          var latLngBounds;        
                          
                          latLngBounds = L.latLngBounds([[-17.822851145453775,-63.20880829291573], [-17.82646277604453,-63.20391471356259]]);
                          
                          this.imageOverlay2 = L.imageOverlay("./assets/mapas/DM10_UV-118_B_23MARZOMARAYAU.jpg", latLngBounds, {
                          opacity: 1,
                          errorOverlayUrl: "https://cdn-icons-png.flaticon.com/512/110/110686.png",
                          alt: "Imagen de GAMSC DiT",
                          interactive: true
                          }).addTo(this.map);
                          this.checked=1;
                        }else{
                          //alert("checked" + this.checked);
                          //this.map.removeLayer(this.imageOverlay1);
                          //this.map.removeLayer(this.imageOverlay2);
                          this.checked=0;
                        }*/











                  
                      //this.map.removeLayer( this.groupBuscador );
                      //buscarGeografico
                      let dm;
                      let uv;
                      let mz;
                      let lt;
                      let tipobus;
                  
                      if (this.FormGroupBusqueda.get('busdm')?.value!=0){
                        dm=this.FormGroupBusqueda.get('busdm')?.value;
                      }else{      
                        dm=0;
                      }  
                  
                      if (this.FormGroupBusqueda.get('busuv')?.value!=0){
                        uv=this.FormGroupBusqueda.get('busuv')?.value;
                      }else{      
                        uv=0;
                      }  
                  
                      if (this.FormGroupBusqueda.get('busmz')?.value!=0){
                        mz=this.FormGroupBusqueda.get('busmz')?.value;
                      }else{      
                        mz=0;
                      }  
                  
                      if (this.FormGroupBusqueda.get('buslt')?.value!=0){
                        lt=this.FormGroupBusqueda.get('buslt')?.value;
                      }else{      
                        lt=0;
                      }  
                  
                  
                      tipobus=0;
                      if (dm!=0 && uv===0 && mz===0 && lt===0){
                        tipobus='1'
                      }
                  
                      if (dm===0 && uv!=0 && mz===0 && lt===0){
                        tipobus='2'
                      }
                  
                      if (dm===0 && uv!=0 && mz!=0 && lt===0){
                        tipobus='3'
                      }
                  
                      if (dm===0 && uv!=0 && mz!=0 && lt!=0){
                        tipobus='4'
                      }
                  
                      if (tipobus!=0){                
                        //tipobus='4';//this.FormGroupBusqueda.get('tipobus').value;
                        //alert(dm);  
                        this.mapaService.buscarGeografico(dm,uv,mz,lt,tipobus).subscribe((data)=>{
                          this.geo_json_data = data;          
                          
                          if (this.geoJSon!=null){
                            if(this.map.hasLayer(this.geoJSon) ) {
                      
                            //console.log(this.geoJSon);  
                              this.map.removeLayer(this.geoJSon);
                            //this.map.removeLayer(marker);
                            } 
                          }
                      
                          this.geoJSon  = L.geoJSON(this.geo_json_data, {
                            style: this.style,        
                            onEachFeature: this.popup,                
                          }).addTo(this.map);
                    
                          this.map.flyToBounds(this.geoJSon.getBounds());
                          this.groupBuscador = L.layerGroup().addTo(this.map);
                      
                        });
                      }else{
                        alert('Su búsqueda podría generar muchos resultados, debe ser más específico');
                      }      
                    }
                  
                    buscarCoordenada(){
                  
                      if (this.resBusquedaCoordenada !=null){
                        if(this.map.hasLayer(this.resBusquedaCoordenada) ) {
                  
                        //console.log(this.geoJSon);  
                          this.map.removeLayer(this.resBusquedaCoordenada);
                        //this.map.removeLayer(marker);
                        } 
                      }
                  
                    let busx;
                    let busy;
                      if (this.FormGroupBusqueda.get('busx')?.value!=0){
                        busx=this.FormGroupBusqueda.get('busx')?.value;
                      }else{      
                        busx=0;
                      }  
                  
                      if (this.FormGroupBusqueda.get('busy')?.value!=0){
                        busy=this.FormGroupBusqueda.get('busy')?.value;
                      }else{      
                        busy=0;
                      }  
                  
                      //var utm = L.utm(479304.6, 8034569.2, 20, 'K', true);
                      //https://jjimenezshaw.github.io/Leaflet.UTM/examples/input.html
                      var utm = L.utm(busx, busy, 20, 'K', true);
                      var ll = utm.latLng();
                      if (ll) {
                          //marker.setLatLng(ll).bindPopup(utm + '<br>' + ll).openPopup()
                          //el1.lat.value = ll.lat.toFixed(6);
                          //el1.lng.value = ll.lng.toFixed(6);
                          //document.getElementById('result2').innerHTML = '' + ll;
                  
                          //alert(ll.lat.toFixed(6));
                          //alert(ll.lng.toFixed(6));
                  
                  
                            this.resBusquedaCoordenada = L.marker([ll.lat.toFixed(6),ll.lng.toFixed(6)], {
                            title: "Resultado de búsqueda",
                            draggable:false,
                            opacity: 1
                            }).bindPopup("<b>Coordenada</b>")
                            .addTo(this.map);
                            //this.map.flyToBounds(this.resBusquedaCoordenada.getBounds());
                      
                      
                            
                            /*let marker;
                            marker = L.marker([-17.795595,-63.196819], {
                                //icon: customIcon,
                            })
                            .bindPopup("<p>Coordenada: </p><p>Latitud: " +"-17.795595" + "</p><p>Longitud: " +"-63.196819"+ "</p>")
                            .addTo(this.map);
                      
                            var coordenada = marker.latlng;*/
                      
                            /*let animatedMarker = L.animatedMarker(marker.getLatLngs(), {
                              autoStart: false,
                              icon
                            });
                        
                            this.map.addLayer(animatedMarker);*/
                      }
                    }
                  
                    limpiarCoordenada(){
                  
                      
                    this.FormGroupBusqueda.controls['busx'].setValue(''); 
                    this.FormGroupBusqueda.controls['buxy'].setValue(''); 
                  
                      if (this.resBusquedaCoordenada !=null){
                        if(this.map.hasLayer(this.resBusquedaCoordenada) ) {
                  
                        //console.log(this.geoJSon);  
                          this.map.removeLayer(this.resBusquedaCoordenada);
                        //this.map.removeLayer(marker);
                        } 
                      }
                  
                    }
                  
                    limpiarGeografico(){


                      
                      
                      //this.map.removeLayer(this.imageOverlay2);
                      //this.imageOverlay2.removeFrom(this.map)
                      
                    this.FormGroupBusqueda.controls['busdm'].setValue('');
                    this.FormGroupBusqueda.controls['busuv'].setValue('');  
                    this.FormGroupBusqueda.controls['busmz'].setValue('');        
                    this.FormGroupBusqueda.controls['buslt'].setValue('');  
                    if (this.geoJSon!=null){
                      if(this.map.hasLayer(this.geoJSon) ) {
                  
                      //console.log(this.geoJSon);  
                        this.map.removeLayer(this.geoJSon);
                      //this.map.removeLayer(marker);
                      } 
                    }
                    //
                    //alert("");
                    
                    }
                  
                  
                    
                  
                    /* toUTMBtn() {
                      document.getElementById('result1').innerHTML = '---';
                      var ll = L.latLng(el1.lat.value, el1.lng.value);
                      var utm = ll.utm();
                      marker.setLatLng(ll).bindPopup(utm + '<br>' + ll).openPopup()
                      el2.x.value = utm.x.toFixed(1);
                      el2.y.value = utm.y.toFixed(1);
                      el2.zone.value = utm.zone;
                      el2.band.value = utm.band;
                      el2.southHemi.value = utm.southHemi;
                      document.getElementById('result1').innerHTML = '' + utm;
                  }*/
                  
                    toLatLngBtn() {
                  
                    //https://jjimenezshaw.github.io/Leaflet.UTM/examples/input.html
                      //document.getElementById('result2').innerHTML = '---';
                      //var sh = el2.southHemi.value.toLowerCase();
                      //var southHemi = ['true', 'y', 'yes', '1'].indexOf(sh) > -1 ? true : false;
                      var utm = L.utm(479304.6, 8034569.2, 20, 'K', true);
                      var ll = utm.latLng();
                      if (ll) {
                          //marker.setLatLng(ll).bindPopup(utm + '<br>' + ll).openPopup()
                          //el1.lat.value = ll.lat.toFixed(6);
                          //el1.lng.value = ll.lng.toFixed(6);
                          //document.getElementById('result2').innerHTML = '' + ll;
                  
                          //alert(ll.lat.toFixed(6));
                          //alert(ll.lng.toFixed(6));
                      }
                  }
                  
                  
                  
                  irSiguiente(event){
                    console.log("Proceso completo: "+this.ProcesoCompleto);
                  
                    //if (this.ProcesoCompleto===1){   
                        //alert("Click");
                  
                        this.tramiteJurisdicion.superficiemensura=this.miFormulario_Jurisdiccion.get('superficiemensura')?.value;
                        this.tramiteJurisdicion.uv=this.miFormulario_Jurisdiccion.get('uv')?.value;;
                        this.tramiteJurisdicion.manzana=this.miFormulario_Jurisdiccion.get('manzana')?.value;;
                        this.tramiteJurisdicion.lote=this.miFormulario_Jurisdiccion.get('lote')?.value;;
                        this.tramiteJurisdicion.coordlat=this.miFormulario_Jurisdiccion.get('coordenada_latitud')?.value;;
                        this.tramiteJurisdicion.coordlng=this.miFormulario_Jurisdiccion.get('coordenada_longitud')?.value;;
                        this.tramiteJurisdicion.estadopago=0;
                        this.tramiteJurisdicion.idpersona=1;
                        this.tramiteJurisdicion.poligono=0;
                        
                        /*
                        miFormulario_Jurisdiccion
                        superficiemensura:['',Validators.required],
                          uv:['',Validators.required],
                          manzana:['',Validators.required],
                          lote:['',Validators.required],
                          coordenada_latitud:['',Validators.required],
                          coordenada_longitud:['',Validators.required],
                          poligono:['',Validators.required],*/
                  
                  
                  
                    
                                
                        
                      
                      this.lcnService.registrarTramiteJurisdiccion(this.tramiteJurisdicion).subscribe(resp=>{
                          this.respuestaTramite=resp;              
                            
                            localStorage.setItem('resultadoTramiteJurisdiccion', JSON.stringify(resp));                                                   
                      });                   
                        //alert('LINEAMIENTO GENERADO CORRECTAMENTE, SE PROCEDE A REALIZAR EL PAGO');
                        // this.router.navigate(['pages/pago'])
                        
                        //this.router.navigate(['pages/pago']);;;
                    
                        //this.miFormulario_Catastro.markAllAsTouched;
                  
                  
                        this.GenerarQR();
                    
                    this.stepper.next();
                  /* }else{
                    alert("Faltan datos");
                    }*/
                  
                    
                  }
                  
                  
                    /*GenerarQR()
                      {
                          //alert("Generando QR");
                        this.qr.qrCrearSesion(
                          {
                          "user":"admin",
                          "password":"admin123",
                          "companyId":"1"
                          }     
                        ).subscribe((result)=>{
                          console.warn(result);
                          this.objtoken=result;
                          console.warn(this.objtoken['transactionIdentifier']);
                          this.token=this.objtoken['transactionIdentifier'];
                        })       
                    
                        const header = [{
                        attribute: "currency",
                        value: "BOB"
                        },
                        {
                          attribute: "gloss",
                          value: "PRUEBA GMSCZ"
                        },
                        {
                          attribute: "amount",
                          value: "0.5"
                        },
                        {
                          attribute: "singleUse",
                          value: "true"
                        },
                        {
                          attribute: "expirationDate",
                          value: "2022-12-31"
                        },
                        {
                          attribute: "additionalData",
                          value: "Datos adicionales"
                        },
                        {
                          attribute: "destinationAccountId",
                          value: "1"
                        },
                        {
                          attribute: "bank",
                          value: "BNB"
                        },
                        {
                          attribute: "user",
                          value: "admin"
                        },
                        {
                          attribute: "company",
                          value: "1"
                        }                 
                    ];
                    
                    const body = { 
                      'operation': 'VTO041',
                      'header': header
                    
                    };
                    
                    console.warn("Datos enviados:" + body);
                    
                        this.qr.qrGenerar(body).subscribe((result)=>{
                        console.warn(result);
                        this.objqr=result;
                        console.warn(this.objqr['message']);
                        console.warn(this.objqr['status']);
                        console.warn(this.objqr['responseList']);
                        console.warn(this.objqr['responseList'][0]);
                        console.warn(this.objqr['responseList'][0]['response']);
                        console.warn(this.objqr['responseList'][0]['response'][0]);
                        console.warn(this.objqr['responseList'][0]['response'][0]['identificator']);
                        console.warn(this.objqr['responseList'][0]['response'][1]['identificator']);
                    
                        //this.idqr.setValue("100");
                        this.idqr=this.objqr['responseList'][0]['response'][0]['identificator'];
                        this.base64=this.objqr['responseList'][0]['response'][1]['identificator'];
                    
                        this.temporizadorDeRetraso()
                    
                    
                      })   
                      }*/
                      GenerarQR()
                      {
                    
                        this.qr.qrGenerar({
                          "importe": 1}).subscribe((resp)=>{
                        //this.urlReporte=resp;
                        //let respuesta=resp;
                        //console.log(resp);//
                        this.respuestaQr=resp;
                        //console.log("RutaReporte"+this.urlReporte.urldocumento);
                    
                        this.base64=this.respuestaQr.imagen64;
                        this.idqr=this.respuestaQr.idqr
                    
                        });
                        this.temporizadorDeRetraso();
                      }
                    
                      temporizadorDeRetraso() {
                        //alert("Three seconds have elapsed." +this.identificadorDeTemporizador_tiempo) ;
                        this.identificadorDeTemporizador_tiempo=0;
                        //this.identificadorDeTemporizador = setInterval(this.funcionConRetraso,this.identificadorDeTemporizador_tiempo, 3000);
                        this.identificadorDeTemporizador = setInterval(()=>{
                          this.identificadorDeTemporizador_tiempo=this.identificadorDeTemporizador_tiempo+1;
                          //alert(this.identificadorDeTemporizador_tiempo);
                    
                          this.ConsultarQR()
                          if (this.identificadorDeTemporizador_tiempo===100 ){
                            
                    
                    
                            //this.FormGroupBusqueda.get('busx')?.value
                                  //alert("Three seconds have elapsed.");
                    
                    
                            this.borrarAlerta();
                          }
                          
                        }, 5000);
                      }        
                        ConsultarQR()
                      {
                    
                        console.warn("Consultar QR:" + this.idqr);
                        const body = { 'operation': this.idqr};
                        this.qr.qrConsultar(body).subscribe((result)=>{
                          console.warn(result);
                          this.objqr=result;
                          console.warn(this.objqr['responseList']);
                          console.warn(this.objqr['responseList'][0]);
                          console.warn(this.objqr['responseList'][0]['response']);
                          console.warn(this.objqr['responseList'][0]['response'][0]);
                          console.warn(this.objqr['responseList'][0]['response'][0]['identificator']);
                          //this.estadoqr=this.objqr['responseList'][0]['response'][0]['identificator'];
                          this.estadoqr.setValue(this.objqr['responseList'][0]['response'][0]['identificator']);
                    
                          //alert(this.estadoqr.value);
                          
                          if (this.estadoqr.value=="PAG"){
                    
                          this.borrarAlerta();
                          this.Pagar();  
                          }                  
                        });            
                      }  
                      
                      
                      cargar() {
                        //console.log(this.document.location.href);

                        //const m = L.map("map").setView([-17.7429318,-63.0625584], 15);
                    
                        const geo = L.geoJson(
                          { features: [] },
                          {
                            onEachFeature: function popUp(f, l) {
                              const out = [];
                              if (f.properties) {
                                for (const key of Object.keys(f.properties)) {
                                  out.push(key + " : " + f.properties[key]);
                                }
                                l.bindPopup(out.join("<br />"));
                              }
                            }
                          }
                        ).addTo(this.map);  
                        //src/app/services
                        //http://localhost:4200/ngx-admin/src/assets
                        
                        
                        
                        //const base1 = "."+this.router.url +"/ejes_vias.zip";
                        //const base1 = this.document.location.href+"/ejes_vias.zip";
                        console.log( this.document.baseURI);
                        const base1 = this.document.baseURI+"assets/mapas/"+ this.filename;
                        console.log(base1);
                        shp(base1).then(function(data) {
                          geo.addData(data);
                        });
                        
                    
                        
                    
                      }
                      
                    
                      borrarAlerta() {
                        clearInterval(this.identificadorDeTemporizador);
                      }
                    
                      Pagar(){
                  
                        this.lcnService.ObtenerReportePdfJurisdiccion({
                          "idlineamiento": 1
                          //"idlineamiento": this.actualizarEstado.idlineamiento
                          }).subscribe((resp)=>{
                        this.urlReporte=resp;
                        console.log(resp);
                        //this.urlReporte=resp;
                        console.log("RutaReporte"+this.urlReporte.urldocumento);
                        this.goToLink(this.urlReporte.urldocumento);
                      });
                    
                    
                        
                    
                      /*
                        this.actualizarEstado= new ActualizarEstado(0,'',0);
                        this.ActualizarEstadoPagoLineamiento= new ClsActualizarEstadoPago(0);    
                    
                        if(localStorage.getItem('resultadoLineamiento')){
                          this.actualizarEstado =JSON.parse(localStorage.getItem('resultadoLineamiento')!);
                          console.log("actualizarEstado.idlineamiento: "+this.actualizarEstado.idlineamiento);      
                        }
                    
                        this.lcnService.ActualizaEstadoLineamiento(
                          {
                            "idlineamiento": this.actualizarEstado.idlineamiento        
                          }     
                        ).subscribe((result)=>{
                          console.warn(result);
                        });
                    
                        let cadena,cadena1:string;        
                        cadena=JSON.parse(localStorage.getItem('imagenLcn'));
                        
                        
                        cadena1=cadena.substring(22, cadena.length-1);
                        console.log("Cadena: "+cadena1);
                    
                        //cadena1=""
                    
                    
                        this.lcnService.ObtenerReportePdfLineamiento({
                            "idlineamiento": this.actualizarEstado.idlineamiento,
                            "imagen64": cadena1}).subscribe((resp)=>{
                          this.urlReporte=resp;
                          console.log(resp);
                          //this.urlReporte=resp;
                          console.log("RutaReporte"+this.urlReporte.urldocumento);
                          this.goToLink(this.urlReporte.urldocumento);
                        });*/
                    
                        
                        
                    
                        
                        alert("El pago ha sido realizado");
                    
                      }
                      goToLink(url: string){ window.open(url, "_blank"); }
                    
                    reset(){
                      //alert("aaa");
                        //this.router.navigate(['dashboard/jurisdiccion/jurisdiccion-ubicacion']);
                        location.reload();
                    };


                    openDialog() {
                      
                      if (this.areamunicipal[0]!=undefined){
                        localStorage.setItem('areamunicipal', JSON.stringify(this.areamunicipal[0]));
                        this.modalService.open(AreaModalComponent, {      
                          centered: true,
                          backdrop: 'static',
                          windowClass: 'my-class'
                        });
                      }
                    }


                    openModal(targetModal) {
                      this.modalService.open(targetModal, {
                      centered: true,
                      backdrop: 'static'
                      });
                    
                      this.form.patchValue({
                        documento: [''],
                      });
                    }

                    onSubmit() {
                      this.modalService.dismissAll();
                      console.log("res:", this.form.getRawValue());
                    }

                    
                    openPopup() {
                      this.displayStyle = "block";
                    }
                    closePopup() {
                      this.cargar();
                      this.displayStyle = "none";
                    }


                    selectFile(event: any): void {
                      this.selectedFiles = event.target.files;
                    }

                    upload(): void {
                      this.progress = 0;

                      if (this.selectedFiles) {
                        const file: File | null = this.selectedFiles.item(0);
                        this.filename=file.name;

                        if (file) {
                          this.currentFile = file;

                          this.uploadService.upload(this.currentFile).subscribe(
                            (event: any) => {
                              if (event.type === HttpEventType.UploadProgress) {
                                this.progress = Math.round(100 * event.loaded / event.total);
                              } else if (event instanceof HttpResponse) {
                                this.message = event.body.message;
                                
                                //console.log("Upload");
                                //this.fileInfos = this.uploadService.getFiles();
                              }
                            },
                            (err: any) => {
                              console.log(err);
                              this.progress = 0;

                              if (err.error && err.error.message) {
                                this.message = err.error.message;
                              } else {
                                this.message = 'Could not upload the file!';
                                //this.cargar();
                              }

                              this.currentFile = undefined;
                            });

                        }

                        this.selectedFiles = undefined;
                      }
                    }

                    


                    selected(e) {
                      this.selectedItems = e;
                      
                      this.checked=this.checked+1;
                      console.log(this.checked);
                      
                      
                      if (this.checked==4){

                        this.checked=0;
                        
                        //console.log(this.checked);
                        var elementos=this.selectedItems.length
                        //console.log(this.selectedItems.length);      
                        //console.log("Archivo: "+this.selectedItems[0].distrito);

                        
                        
                        console.log("Elementos"+this.selectedItems.length);
                        this.removeMarkers();
                        this.vecImageOverlays=[];
                        if (elementos>0){                
                          
                        for(let item of this.selectedItems){
                          var latLngBounds;          
                          console.log(item.coordenadanox);
                            //this.marker  = L.marker([marcador.lat, marcador.lng]).bindPopup('Punto');  
                            //latLngBounds = L.latLngBounds([[-17.75949509579157,-63.1284841735086], [-17.762471556464998,-63.12432606477618]]);
                            //this.resBusquedaCoordenada = L.marker([ll.lat.toFixed(6),ll.lng.toFixed(6)], {
                            var utm = L.utm(item.coordenadanox,item.coordenadanoy, 20, 'K', true);
                            var utm1 = L.utm(item.coordenadasex,item.coordenadasey, 20, 'K', true);
                            var ll = utm.latLng();
                            var ll1 = utm1.latLng();
                            //latLngBounds = L.latLngBounds([[item.coordenadanox,item.coordenadanoy], [item.coordenadasex,item.coordenadasey]]);
                            latLngBounds = L.latLngBounds([[ll.lat ,ll.lng], [ll1.lat ,ll1.lng]]);

                            
                            console.log(latLngBounds);
                        //this.imageOverlay1 = L.imageOverlay("./assets/mapas/DM10_UV117.jpg", latLngBounds).addTo(this.map).bringToBack();
                          //this.imageOverlay1 = L.imageOverlay("./assets/mapas/DM6_ET32_MZA_BARRIO-CUTARE.jpg", latLngBounds, {
                            //this.imageOverlay1 = L.imageOverlay("./assets/mapas/"+item.nombrearchivo, latLngBounds, {
                              this.imageOverlay1 = L.imageOverlay("https://190.186.159.75:8443/aforo/report/cartografia/mapas/"+item.nombrearchivo, latLngBounds, {
                          opacity: 0.7,
                          errorOverlayUrl: "https://cdn-icons-png.flaticon.com/512/110/110686.png",
                          alt: "Imagen de GAMSC DiT",
                          interactive: false
                          }).addTo(this.map);//.bringToBack();*/
                          //console.log("Elemento For"+this.imageOverlay1._leaflet_id);
                          //this.clsDistritosGeo.id=this.imageOverlay1._leaflet_id
                          //this.clsDistritosGeo.id=L.Util.stamp(this.imageOverlay1);
                          
                          //this.vecImageOverlays.push(this.clsDistritosGeo);
                          this.vecImageOverlays.push(this.imageOverlay1 );
                        
                        //this.map.addLayer(this.imageOverlay1,);
                        //this.map.fitBounds(latLngBounds);
                        }
                      }

                        
                      

                      //this.map.fitBounds(latLngBounds);
                      //this.map.flyToBounds(latLngBounds);          

                        //alert(this.checked);
                        

                      }

                      /*var center = [-17.82985458776867,-63.205451591115356];       
                        var imageUrl = './assets/mapas/DM10_UV117.jpg',      
                        imageBounds = [center, [-17.832013712275735,-63.20281686958617]];                      
                        L.imageOverlay(imageUrl, imageBounds).addTo(this.map);*/
                        //console.log(this.checked);  

                        


                        /*var center1 = [-17.822851145453775,-63.20880829291573];                               
                        var imageUrl1 = './assets/mapas/DM10_UV-118_B_ 23MARZOMARAYAU.jpg',      
                        imageBounds1 = [center1, [-17.82646277604453,-63.20391471356259]];                    
                        L.imageOverlay(imageUrl1, imageBounds1).addTo(this.map);

                        this.map.removeLayer(this.geoJSon);*/
                    }
                    select(e) {
                      this.selectedItem = e;

                      var utm = L.utm(this.selectedItem.coordenadanox,this.selectedItem.coordenadanoy, 20, 'K', true);
                      var utm1 = L.utm(this.selectedItem.coordenadasex,this.selectedItem.coordenadasey, 20, 'K', true);
                      var ll = utm.latLng();
                      var ll1 = utm1.latLng();
                      //latLngBounds = L.latLngBounds([[item.coordenadanox,item.coordenadanoy], [item.coordenadasex,item.coordenadasey]]);
                      var latLngBounds = L.latLngBounds([[ll.lat ,ll.lng], [ll1.lat ,ll1.lng]]);
                      //this.map.fitBounds(latLngBounds);
                      this.map.flyToBounds(latLngBounds);          
                    }

                    removeMarkers() {
                      var cantidadElementos=0;
                      //console.log("RemoveMarkers- Funcion "+this.vecImageOverlays.length);
                      
                      /*this.map.eachLayer(function (layer) {
                        //this.map.removeLayer(layer)
                        cantidadElementos++;
                        console.log("Mapas_Cargados_ID - "+ cantidadElementos+" "  );
                        //.removeLayer(layer);
                        //layer.remove

                      }); */
                      //setTimeout(()=>{

                        for (let id of this.vecImageOverlays){
                          //console.log("RemoveMarkers-"+id.id);
                          this.map.removeLayer(id);
                        }
                      //;} , 500
                        //  );

                      //for (i=)

                      /*for (let id of this.vecImageOverlays){
                        console.log("vecImageOverlays - "+id.id);
                        cantidadElementos++;

                        this.map.eachLayer(function (layer) {
                        
                          if (layer._leaflet_id == id.id){
                              console.log("IF "+layer._leaflet_id); 
                              console.log("vecImageOverlays Cantidad - "+ cantidadElementos+" "  );
                              //this.vecImageOverlays.splice(cantidadElementos, 1);
                          }
                        });*/

                        
                        //if (layer._leaflet_id == id){
                            //this.map.removeLayer(id.id);
                      //  }

                    // }
                      //this.map.clearLayers();
                      
                      //this.map.eachLayer( function(layer) {
                        //cantidadElementos++;
                        //console.log("Mapas_Cargados_ID - "+ layer._leaflet_id+" "  );
                        /*this.vecImageOverlays.each(){
                          this.map.removeLayer(400)
                        }*/

                        /*for (let id of this.vecImageOverlays){
                          console.log(id.id);
                          if (layer._leaflet_id == id.id){
                            this.map.removeLayer(layer._leaflet_id);
                          }

                        }*/
                            
                            

                        
                      // console.log(layer.alt);
                        /*if ( layer.myTag &&  layer.myTag === "myGeoJSON") {
                          this.map.removeLayer(layer)
                            }

                          });*/

                          /*if (layer instanceof L.imageOverlay){
                            alert("layer" + layer)
                            this.map.removeLayer(layer);
                          }*/

                        // switch (layer.options.pane) {
                          // case 'overlayPane':
                              //alert("Marker [" + layer.options.title + "]");

                              //var layerObj = layers[val];
                              //alert("overlayPane - Javier")
                            // this.map.removeLayer(400);

                            // break;
                          //}
                        //});

                  }



  

}


/*
export interface DialogData {
  animal: 'panda' | 'unicorn' | 'lion';
}

@Component({
  selector: 'dialog-areamunicipal',
  templateUrl: 'dialog-areamunicipal.html',
})
export class DialogDataExampleDialog {
 
  constructor(@Inject(MAT_DIALOG_DATA) public data: areaMunicipal,
     public areaVerdeService: AreaVerdeService,
     private fb: FormBuilder) {}

}*/
