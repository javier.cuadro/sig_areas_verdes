import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AreasMunicipalesComponent } from './areas-municipales.component';

describe('AreasMunicipalesComponent', () => {
  let component: AreasMunicipalesComponent;
  let fixture: ComponentFixture<AreasMunicipalesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AreasMunicipalesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AreasMunicipalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
