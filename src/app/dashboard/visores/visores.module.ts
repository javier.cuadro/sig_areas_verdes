import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VisoresRoutingModule } from './visores-routing.module';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { SharedModule } from 'src/app/shared/shared.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AreaModalComponent } from './area-modal/area-modal.component';
import { VisorCapasComponent } from './visor-capas/visor-capas.component';
import { AxiomNgxTreeModule } from 'axiom-ngx-tree';
import {CdkAccordionModule} from '@angular/cdk/accordion';





@NgModule({
  declarations: [
    ...VisoresRoutingModule.components,
    AreaModalComponent,
    VisorCapasComponent,
  ],
  imports: [
    CommonModule,
    VisoresRoutingModule,
    SharedModule,
    LeafletModule,
    FormsModule,
    ReactiveFormsModule,
    AxiomNgxTreeModule,
    CdkAccordionModule,
    
  ], 
})
export class VisoresModule { }
