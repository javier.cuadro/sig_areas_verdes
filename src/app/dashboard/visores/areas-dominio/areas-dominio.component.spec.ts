import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AreasDominioComponent } from './areas-dominio.component';

describe('AreasDominioComponent', () => {
  let component: AreasDominioComponent;
  let fixture: ComponentFixture<AreasDominioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AreasDominioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AreasDominioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
