import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AreasDominioComponent } from './areas-dominio/areas-dominio.component';
import { AreasMunicipalesComponent } from './areas-municipales/areas-municipales.component';
import { VisorCapasComponent } from './visor-capas/visor-capas.component';

const routes: Routes = [
  
  
  {
    path: '',
    component: AreasMunicipalesComponent
  }, 
  {
    path: 'areas-municipales',
    component: AreasMunicipalesComponent
  },   
  {
    path: 'areas-dominio',
    component: AreasDominioComponent
  }, 

  {
    path: 'visor-capas',
    component: VisorCapasComponent
  }, 
]; 

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VisoresRoutingModule {
  
  static components = [AreasMunicipalesComponent,AreasDominioComponent ];
}
