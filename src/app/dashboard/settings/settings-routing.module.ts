import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MenuRolComponent } from './menu-rol/menu-rol.component';

const routes: Routes = [
  {
    path: '',
    component: MenuRolComponent,
  },
  {
    path: 'menu-rol',
    component: MenuRolComponent
  }, 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule {
  static components = [MenuRolComponent];
 }
