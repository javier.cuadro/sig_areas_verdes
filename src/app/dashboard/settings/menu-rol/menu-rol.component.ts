
import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs';
import { DataService } from '../../../services/data.service';
import { RecursoRol, Rol } from '../../../interfaces/lcn.interfaces';
import Swal from 'sweetalert2';
import { stringify } from 'querystring';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoadingBackdropService } from '../../../core/services/loading-backdrop.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu-rol',
  templateUrl: './menu-rol.component.html',
  styleUrls: ['./menu-rol.component.scss']
})
export class MenuRolComponent implements OnInit {

  recursos : RecursoRol [] = [];
  roles: Rol[] = [];
  form: FormGroup;
  idRol: number = 0;

  constructor(    
    private dataService : DataService,
    private fb: FormBuilder,
    private loadingBackdropService: LoadingBackdropService,
    private router: Router,
  ) {
    this.form = this.fb.group({
      rol: [this.idRol, [Validators.required ] ],
    });
  }

  ngOnInit(): void {
    this.cargarRoles(); 
  }

  cargarRoles(): void {
    this.loadingBackdropService.show();
    this.dataService
    .getRoles()
    .pipe(finalize(() => {
      this.loadingBackdropService.hide();
    }))
    .subscribe((data) => {
      if (data.codigo == 0) {
        this.roles = data.roles;         
      } else {
        Swal.fire({
          position: 'center',
          icon: 'error',
          title: data.mensaje,
          showConfirmButton: false,
          timer: 3000
        })
      }
    });
  }

  cargarMenuRol() {
    this.recursos = [];
    this.loadingBackdropService.show();
    this.dataService
      .getMenuRol(this.idRol)
      .pipe(finalize(() => {
        this.loadingBackdropService.hide();
      }))
      .subscribe((data) => {
        if (data.codigo == 0) {
          this.recursos = data.recursos;         
        } else {
          Swal.fire({
            position: 'center',
            icon: 'error',
            title: data.mensaje,
            showConfirmButton: false,
            timer: 3000
          })
        }
      });
  }
  

  checkMinusSquare(item: RecursoRol) : boolean{
    const count = item.recursos.filter(x => x.checked == true).length;
    if (count > 0 && count < item.recursos.length) {
      return true;
    } else if (count == 0) {
      return false;
    } else {
      return false;
    }
  }

  checkParent(i, j) {
    
    this.recursos[i].recursos[j].checked = !this.recursos[i].recursos[j].checked;
    if (this.recursos[i].recursos[j].recursos.length==0) {

    } else {
      if (this.recursos[i].recursos[j].checked) {
        this.recursos[i].recursos[j].recursos.map(x => (x.checked = true));
      } else {
        this.recursos[i].recursos[j].recursos.map(x => (x.checked = false));
      }
    }
    if (this.recursos[i].recursos[j].checked) {
      this.recursos[i].checked=true;
    } else {
      this.recursos[i].checked=false;
    }
  }

  checkChild(i, j, k) {
    this.recursos[i].recursos[j].recursos[k].checked = !this.recursos[i]
      .recursos[j].recursos[k].checked;
    const count = this.recursos[i].recursos[j].recursos.filter(
      el => el.checked == true
    ).length;
   
    //console.log("cantidad:" + this.recursos[i].recursos[j].recursos.length);
    
    if (count > 0) {
      this.recursos[i].recursos[j].checked = true;
    } else {
      this.recursos[i].recursos[j].checked = false;
    }   
  }

  onChanged() {
    this.idRol=this.form.value.rol;    
    if (this.idRol==0) {
      this.recursos = [];
    } else {
      this.cargarMenuRol();
    }    
  }

  guardarPermisos() {
    let cantidad=0;
    this.recursos.forEach((menuPadre) => {
      menuPadre.recursos.forEach((menuHijo) => {
        if (menuHijo.checked) cantidad++;
        menuHijo.recursos.forEach((nieto) => {
          if (nieto.checked) cantidad++;
        });
      });
    });
    
    if (cantidad==0) {
      Swal.fire({
        position: 'center',
        icon: 'warning',
        title: 'Debe seleccionar algún permiso',
        showConfirmButton: false,
        timer: 3000
      })
    }

    console.log(this.recursos);    

    this.loadingBackdropService.show();
    this.dataService
      .guardarPermisos(this.idRol, this.recursos)
      .pipe(finalize(() => {
        this.loadingBackdropService.hide();
      }))
      .subscribe((data) => {
        if (data.codigo == 0) {
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: data.mensaje,
          }) .then((e) => {
            this.router.navigate(['dashboard/home']);  
          });          
          
        } else {
          Swal.fire({
            position: 'center',
            icon: 'error',
            title: data.mensaje,
            showConfirmButton: false,
            timer: 3000
          })
        }
      });
  }
  
}
