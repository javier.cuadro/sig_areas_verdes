import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SettingsRoutingModule } from './settings-routing.module';
import { MenuRolComponent } from './menu-rol/menu-rol.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [
    ...SettingsRoutingModule.components
  ],
  imports: [
    CommonModule,
    SettingsRoutingModule,
    SharedModule,
  ]
})
export class SettingsModule { }
