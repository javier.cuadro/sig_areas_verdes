import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { finalize } from 'rxjs/operators';


import { ActivatedRoute, Router } from '@angular/router';
import { DashboardService } from '../../dashboard.service';
import { LoadingBackdropService } from '../../../core/services/loading-backdrop.service';
import { ProrrateoUrb } from '../../interfaces/excel.interfaces';


@Component({
  selector: 'app-listado-prorrateo-cerr',
  templateUrl: './listado-prorrateo-cerr.component.html',
  styles: [
  ],  
})
export class ListadoProrrateoCerrComponent implements OnInit {

  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator!: MatPaginator;
 

  dataSource = new MatTableDataSource<ProrrateoUrb>();
  displayedColumns: string[] = [
    'idprorrateo_urb_cerr',
    'nombre',
    'uv',
    'superficie_total',
    'detalle'
  ];

  constructor(   
    private loadingBackdropService: LoadingBackdropService,
    private dashboardService: DashboardService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.loadProrrateos();
  }

  loadProrrateos(){
  
    this.loadingBackdropService.show();

    this.dashboardService
      .getProrrateosCerr()
      .pipe(finalize(() => this.loadingBackdropService.hide()))
      .subscribe((data) => {       
        this.dataSource.data = data.lista;
      });
  }

  onLineamientoDetailNavigate(p: ProrrateoUrb) {
    //this.router.navigate([lineamiento.idlineamiento], { relativeTo: this.route });
    console.log(p.idprorrateo_urb_cerr);
  }

}
