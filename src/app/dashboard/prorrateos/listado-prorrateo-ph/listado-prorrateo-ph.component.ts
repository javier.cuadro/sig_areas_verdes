import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { finalize } from 'rxjs/operators';


import { ActivatedRoute, Router } from '@angular/router';
import { DashboardService } from '../../dashboard.service';
import { LoadingBackdropService } from '../../../core/services/loading-backdrop.service';
import { ProrrateoEdificio } from '../../interfaces/excel.interfaces';

@Component({
  selector: 'app-listado-prorrateo-ph',
  templateUrl: './listado-prorrateo-ph.component.html',
  styleUrls: []
})
export class ListadoProrrateoPhComponent implements OnInit {

  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator!: MatPaginator;
 

  dataSource = new MatTableDataSource<ProrrateoEdificio>();
  displayedColumns: string[] = [
    'idprorrateo_urb_ph',
    'nombre',
    'superficie_terreno',
    'detalle'
  ];

  constructor(   
    private loadingBackdropService: LoadingBackdropService,
    private dashboardService: DashboardService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.loadProrrateos();
  }

  loadProrrateos(){
  
    this.loadingBackdropService.show();

    this.dashboardService
      .getProrrateosPh()
      .pipe(finalize(() => this.loadingBackdropService.hide()))
      .subscribe((data) => {       
        this.dataSource.data = data.lista;
      });
  }

  onLineamientoDetailNavigate(prorrateo: ProrrateoEdificio) { 
    //this.router.navigate(['/prorrateo-edificio-detalle', prorrateo.idprorrateo_urb_ph]) 
  }

}
