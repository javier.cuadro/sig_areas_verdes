import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProrrateosRoutingModule } from './prorrateos-routing.module';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { SharedModule } from 'src/app/shared/shared.module';
import { ProrrateosUrbanizacionComponent } from './prorrateos-urbanizacion/prorrateos-urbanizacion.component';
import { ProrrateosEdificioComponent } from './prorrateos-edificio/prorrateos-edificio.component';
import { ListadoProrrateoPhComponent } from './listado-prorrateo-ph/listado-prorrateo-ph.component';
import { ListadoProrrateoCerrComponent } from './listado-prorrateo-cerr/listado-prorrateo-cerr.component';
import { ProrrateoEdificioDetalleComponent } from './prorrateo-edificio-detalle/prorrateo-edificio-detalle.component';
import { ProrrateoUrbanizacionDetalleComponent } from './prorrateo-urbanizacion-detalle/prorrateo-urbanizacion-detalle.component';

@NgModule({
  declarations: [
    ...ProrrateosRoutingModule.components,
    ProrrateosUrbanizacionComponent,
    ProrrateosEdificioComponent,
    ListadoProrrateoPhComponent,
    ListadoProrrateoCerrComponent,
    ProrrateoEdificioDetalleComponent,
    ProrrateoUrbanizacionDetalleComponent,
  ],
  imports: [
    CommonModule,
    ProrrateosRoutingModule,
    SharedModule,
    LeafletModule
  ]
})
export class ProrrateosModule { }
