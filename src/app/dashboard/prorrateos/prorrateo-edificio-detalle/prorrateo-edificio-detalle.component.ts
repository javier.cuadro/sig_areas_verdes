import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { finalize } from 'rxjs/operators';

import { LoadingBackdropService } from '../../../core/services/loading-backdrop.service';
import { ProrrateoEdificio, ProrrateoEdificioDet } from '../../interfaces/excel.interfaces';
import { DashboardService } from '../../dashboard.service';

@Component({
  selector: 'app-prorrateo-edificio-detalle',
  templateUrl: './prorrateo-edificio-detalle.component.html',
  styles: [
  ]
})
export class ProrrateoEdificioDetalleComponent implements OnInit, OnDestroy {
  id: number;
  private sub: any;

  encabezado: ProrrateoEdificio = {
    idprorrateo_urb_ph: 0,
    nombre: 'Prueba',
    superficie_terreno: '',
    superficie_exclusiva_c_a_c: '',
    superficie_exclusiva_s_a_c: '',
    superficie_exclusiva: '',
    superficie_comun_cubiertas: '',
    superficie_construida_total: '',
    uv: '',
    manzana: '',
    lote: '',
  }

  detalles: ProrrateoEdificioDet [] = [];
 
  constructor(
    private dialog: MatDialog,
    private loadingBackdropService: LoadingBackdropService,
    private snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private router: Router,
    private dashboardService: DashboardService
  ) { 
    
  }

  ngOnInit(): void {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number

      // In a real app: dispatch action to load the details here.
   });

    this.id = this.route.snapshot.params['id'];
    console.log(this.id);
    this.loadProrrateoDetalle();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  loadProrrateoDetalle(){
  
    this.loadingBackdropService.show();

    this.dashboardService
      .getProrrateoPhDetalle(this.id)
      .pipe(finalize(() => this.loadingBackdropService.hide()))
      .subscribe((data) => {       
        this.encabezado = data.encabezado;
        this.detalles = data.lista;
      });
  }

}
