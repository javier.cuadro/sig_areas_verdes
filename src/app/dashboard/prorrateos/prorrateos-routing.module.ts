import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProrrateosEdificioComponent } from './prorrateos-edificio/prorrateos-edificio.component';
import { ProrrateosUrbanizacionComponent } from './prorrateos-urbanizacion/prorrateos-urbanizacion.component';
import { ListadoProrrateoPhComponent } from './listado-prorrateo-ph/listado-prorrateo-ph.component';
import { ListadoProrrateoCerrComponent } from './listado-prorrateo-cerr/listado-prorrateo-cerr.component';
import { ProrrateoEdificioDetalleComponent } from './prorrateo-edificio-detalle/prorrateo-edificio-detalle.component';
import { ProrrateoUrbanizacionDetalleComponent } from './prorrateo-urbanizacion-detalle/prorrateo-urbanizacion-detalle.component';

const routes: Routes = [
  {
    path: '',
    component: ProrrateosEdificioComponent
  },   
  
  {
    path: 'prorrateos-edificio',
    component: ProrrateosEdificioComponent
  },   

  {
    path: 'prorrateos-urbanizacion',
    component: ProrrateosUrbanizacionComponent
  },
  {
    path: 'prorrateos-listado-edificio',
    component: ListadoProrrateoPhComponent
  },
  {
    path: 'prorrateos-listado-urbanizaciones',
    component: ListadoProrrateoCerrComponent
  },
  {
    path: 'edificio-detalle/:id',
    component: ProrrateoEdificioDetalleComponent
  },
  {
    path: 'urbanizacion-detalle/:id',
    component: ProrrateoUrbanizacionDetalleComponent
  }
 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProrrateosRoutingModule {
  static components = [ListadoProrrateoPhComponent,ProrrateosEdificioComponent, ProrrateosUrbanizacionComponent, ProrrateoEdificioDetalleComponent ];
 }
