import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { finalize } from 'rxjs/operators';

import { LoadingBackdropService } from '../../../core/services/loading-backdrop.service';
import { ProrrateoUrb, ProrrateoUrbDet } from '../../interfaces/excel.interfaces';
import { DashboardService } from '../../dashboard.service';

@Component({
  selector: 'app-prorrateo-urbanizacion-detalle',
  templateUrl: './prorrateo-urbanizacion-detalle.component.html',
  styles: [
  ]
})
export class ProrrateoUrbanizacionDetalleComponent implements OnInit {

  id: number;
  private sub: any;

  encabezado: ProrrateoUrb = {
    idprorrateo_urb_cerr: 0,
    nombre: '',
    uv: '',
    superficie_total: '',
    superficie_util: '',
    superficie_vias_internas: '',
    superficie_areas_comunes: '',
    superficie_areas_verdes:'',
    nota: '',
  }

  detalles: ProrrateoUrbDet [] = [];
 
  constructor(
    private dialog: MatDialog,
    private loadingBackdropService: LoadingBackdropService,
    private snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private router: Router,
    private dashboardService: DashboardService
  ) { 
    
  }

  ngOnInit(): void {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number

      // In a real app: dispatch action to load the details here.
   });

    this.id = this.route.snapshot.params['id'];
    console.log(this.id);
    this.loadProrrateoDetalle();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  loadProrrateoDetalle(){
  
    this.loadingBackdropService.show();

    this.dashboardService
      .getProrrateoCerrDetalle(this.id)
      .pipe(finalize(() => this.loadingBackdropService.hide()))
      .subscribe((data) => {       
        this.encabezado = data.encabezado;
        this.detalles = data.lista;
      });
  }
}
