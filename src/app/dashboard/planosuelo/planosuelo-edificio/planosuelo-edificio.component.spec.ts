import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanosueloEdificioComponent } from './planosuelo-edificio.component';

describe('PlanosueloEdificioComponent', () => {
  let component: PlanosueloEdificioComponent;
  let fixture: ComponentFixture<PlanosueloEdificioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlanosueloEdificioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanosueloEdificioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
