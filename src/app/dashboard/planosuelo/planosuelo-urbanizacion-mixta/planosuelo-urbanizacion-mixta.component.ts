import { Component,OnInit} from '@angular/core';

import * as htmlToImage from 'html-to-image';
import { geoJSON, GeoJSONOptions,icon,control ,latLng, Layer, layerGroup, Map, marker, point, polyline, tileLayer } from 'leaflet';
//import {StreetLabels} from '@leaflet.streetlabels';
//import * as StreetLabel from 'streetlabels';





import { MapasService } from 'src/app/services/mapas.service';
import { Marcador } from 'src/app/classes/marcador.class';



//import * as L from 'leaflet';

import "leaflet";
declare let L;
import "leaflet-easybutton";
//import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
//import * as M from 'leaflet-streetlabels';
//import * as SM from 'leaflet-search';
//import * as LeafletSearch from 'leaflet-search';


import { LotesService } from 'src/app/services/lotes.service';
//import { Calle } from '../../classes/calle';
//import { Calle } from '../../classes/calle.class';
import { Lote } from 'src/app/classes/lote';




//import { ThisReceiver } from '@angular/compiler';
import { Router } from '@angular/router';
import { async } from '@angular/core/testing';
import { MarcadorLote } from '../../../classes/marcadorlote.class';


import { ResizeService } from 'src/app/services/resize.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { fromEvent, Observable, Subscription } from 'rxjs';
import "leaflet-multicontrol";
import { CallesService } from 'src/app/services/calles.service';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-planosuelo-urbanizacion-mixta',
  templateUrl: './planosuelo-urbanizacion-mixta.component.html',
  styleUrls: []
})
export class PlanosueloUrbanizacionMixtaComponent {

  idurb;
  Nombreurb;
  urbanizacion;

  

  ourCustomControl;
  groupBuscador;
 resBusquedaCoordenada;
 resBuscarGeografico;

  
  private resizeSubscription: Subscription;
  private resizeObservable$: Observable<Event>;
  private resizeSubscription$: Subscription;
  //@ViewChild('idMap') private IDMAP: ElementRef;

  miFormulario_Construccion: FormGroup = this.fb.group({

    categoria:['',Validators.required],
    subcategoria:['',Validators.required],
    actividad:['',Validators.required],
    nombre: [ , [ Validators.required, Validators.minLength(3) ]   ],
    precio: [ , [ Validators.required, Validators.min(0)] ],
    existencias: [ , [ Validators.required, Validators.min(0)] ],
  })

  firstFormGroup = this._formBuilder.group({
    firstCtrl: ['', Validators.required],
  });
  secondFormGroup = this._formBuilder.group({
    secondCtrl: ['', Validators.required],
  });

  
  FormGroupBusqueda: FormGroup = this.fbBusqueda.group({

    busdm:[''],
    busuv:[''],
    busmz:[''],
    buslt:[''],    
    busx:[''],    
    busy:[''],    
  })

  isLinear = false;
  

  ContadorMapa:number=0;  

   public geo_json_data;
    title = 'Sig alcaldia';
    json;
    layerFarmacias;
    map;
    //streetLabelsRenderer;
    marcadores : MarcadorLote[]=[];
    marker;
    i=0;
    
  
    lat = -17.7876515;
    lng = -63.1825049;
    lat1=0;
    lng1=0;
    lote= new Lote(0,'');
    bandera=0;


    MAPELEMENT:any;
    //cliente = new Cliente(0,'','','','','');
    //marcadores : Marcador[]=[];
    
  
    /*public layersControl = {
      baseLayers: { }
    };*/
    //public map: new L.Map('map');
     //map = L.map('map');
    //.setView([51.505, -0.09], 13);
  
    //fieldsVias = ["id", "nombre", "jerarquia", "categoria", "anchovia"];
     
  
    
    //constructor( private mapaService:MapasService,private lotesService:LotesService,private router:Router ) { 
      //constructor(private resizeService: ResizeService,private _formBuilder: FormBuilder, private mapaService:MapasService,private lotesService:LotesService,private router:Router ) { 
        constructor(private dataService:DataService,private resizeService: ResizeService,private fbBusqueda: FormBuilder,private fb: FormBuilder, private _formBuilder: FormBuilder,private mapaService:MapasService,private calleService:CallesService,private router:Router) { 
      //console.log('ss');
    }

    ngOnInit() {
      this.resizeSubscription = this.resizeService.onResize$
        .subscribe(size => console.log(size));
  
  
        this.resizeObservable$ = fromEvent(window, 'resize')
        this.resizeSubscription$ = this.resizeObservable$.subscribe( evt => {
          console.log('event: ', evt)
        })
    }
    ngOnDestroy() {
      if (this.resizeSubscription) {
        this.resizeSubscription.unsubscribe();
      }
      this.resizeSubscription$.unsubscribe()
    }
    
    geoJSon;//:GeoJSON.GeoJSON;
    
    
  
  
    // Define our base layers so we can reference them multiple times
    streetMaps =L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      detectRetina: true,
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
      maxZoom: 19,
    });
  
    esri = L.tileLayer(  
      'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
      maxZoom: 19,
      minZoom: 1,
      attribution: 'Tiles © Esri',
    });
  
    wMaps = L.tileLayer('http://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png', {
      detectRetina: true,
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    });
    etiquetasEsri= L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/toner-labels/{z}/{x}/{y}{r}.{ext}',{
      attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
      subdomains: 'abcd',
      minZoom: 0,
      maxZoom: 19,
      //ext: 'png'
      }
    );

    wmsLayers = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
        layers: "lotes_cartografia_yina",
        format: 'image/png',
        maxZoom: 19,
        transparent: true,
        version: '1.3.0',
        attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
      }
    );

    wms_taz_popurb20 = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
        layers: "taz_popurb20",
        format: 'image/png',
        maxZoom: 19,
        transparent: true,
        version: '1.3.0',
        attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
      }
    );

    wms_taz_popurb25 = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
        layers: "taz_popurb25",
        format: 'image/png',
        maxZoom: 19,
        transparent: true,
        version: '1.3.0',
        attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
      }
    );

    wms_taz_popurb30 = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
        layers: "taz_popurb30",
        format: 'image/png',
        maxZoom: 19,
        transparent: true,
        version: '1.3.0',
        attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
      }
    );

    wms_taz_popurb35 = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
        layers: "taz_popurb35",
        format: 'image/png',
        maxZoom: 19,
        transparent: true,
        version: '1.3.0',
        attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
      }
    );

    wmsLayers_uv = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
        layers: "uv",
        format: 'image/png',
        transparent: true,
        version: '1.3.0',
        attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
      }
    );

    wms_lotes = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
    layers: "lotes_cartografia_yina",
    format: 'image/png',
    maxZoom: 19,
    transparent: true,
    version: '1.3.0',
    attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
  }
);


wms_uv_yina = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
    layers: "unidades_vecinales_yina",
    format: 'image/png',
    maxZoom: 19,
    transparent: true,
    version: '1.3.0',
    attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
  }
);
wms_manzanas_yina = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
    layers: "manzanas_yina",
    format: 'image/png',
    maxZoom: 19,
    transparent: true,
    version: '1.3.0',
    attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
  }
);
   
wms_mapas_urbanizacion = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
    layers: "mapas_urb",
    format: 'image/png',
    maxZoom: 19,
    transparent: true,
    version: '1.3.0',
    attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
  }
);
  
    
  
    // Layers control object with our two base layers and the three overlay layers
    /*layersControl = {
      baseLayers: {
        'Street Maps': this.streetMaps,
        'Wikimedia Maps': this.wMaps
      },
      overlays: {
        'Mt. Rainier Summit': this.summit,
        'Mt. Rainier Paradise Start': this.paradise,
        'Mt. Rainier Climb Route': this.route
      }
    };*/
  

    options = {
      layers: [ this.streetMaps],
      zoom: 13,
      center: L.latLng([ -17.7876515, -63.1825049 ])
    
      //lat = -17.7876515;
      //lng = -63.1825049;
    };
  
  
    
  
    
    /*geoJSON = {
      id: 'geoJSON',
      name: 'Geo JSON Polygon',
      enabled: true,
      layer: L.geoJSON(this.geo_json_data)
    };*/
     getColor(d) {
      return d > 100000000 ? '#800026' :
      d > 50000000 ? '#BD0026' :
      d > 4 ? '#EFEFEF' :
      d > 3 ? '#000287' :
      d > 2 ? '#ccff00' :
      d > 1 ? '#d9880f' :
      d > 0 ? '#0b5e05' :
      '#FFEDA0'}
      
      
  
    style = feature => {
      return {
        weight: 3,
        opacity: 1,
        //color: 'white',
        dashArray: '1',
        fillOpacity: 0.8,
        color: this.getColor(feature.properties.tiposvia)
      };
    }
    styleManzana = feature => {
      return {
        weight: 3,
        opacity: 1,
        color: 'white',
        dashArray: '1',
        fillOpacity: 0.8,
        //color: this.getColor(feature.properties.tiposvia)
      };
    }

     popup(feature, layer) {
      if (feature.properties && feature.properties.nombrevia) {
        layer.bindPopup(feature.properties.nombrevia);
      }
  
      /*for(let i in layer.features)
      layer.features[i].properties.color = this.getColor( layer.features[i].properties.jerarquia );*/
    }
    
  
  
    /*resetHighlight = e => {
      geojson.resetStyle(e.target);
      info.update();
    }*/
  
    //zoomToFeature = e => {
  //    this.map.fitBounds(e.target.getBounds());
  //  }
  
  
    /*onEachFeature = (feature, layer) => {
      layer.on({
        //mouseover: this.highlightFeature,
        //mouseout: this.resetFeature,
        //click: this.zoomToFeature
        html = "",
        for (prop in feature.properties) {
            html += prop + ": " + feature.properties[prop] + "<br>";
        };
        layer.bindPopup(html);         
      });
    }*/
  
     onEachFeature(feature, layer) {
      //if (feature.properties && feature.properties.popupContent) {
          //layer.bindPopup(feature.properties.popupContent);
          for(let i in layer.features){
            layer.features[i].properties.color = this.getColor( layer.features[i].properties.tiposvia );
            //console.log(layer.features[i].properties.jerarquia);
          }
  
          /*let html = "";
        for (let prop in feature.properties) {
            html += prop + ": " + feature.properties[prop] + "<br>";
        };
        layer.bindPopup(html);         */
      //}
      }
      overlayMaps1 = [
        //{name:"general" , layer: this.streetMaps},
        {name:"Satélite" , layer: this.esri},          
        {name:"Uv" , layer:this.wms_uv_yina},
        {name:"Manzanas" , layer:this.wms_manzanas_yina},                 
        {name:"Lotes" , layer:this.wms_lotes}, 
        {name:"Urbanizaciones" , layer:this.wms_mapas_urbanizacion} 
                           
      ];

      
  
       
  
  
  
    onMapReady(map) {

      //document.getElementById('map').style.cursor = 'crosshair'

      
    //this.IDMAP.nativeElement.remove();

    this.dataService.nombreTramite="TRÁMITE: PRORRATEO DE PLANO DE USO EN URBANIZACIÓN";

     



      //alert('aaa');
      L.Icon.Default.imagePath = "assets/leaflet/";
      //L.Icon.Default.imagePath=
      let geojson;
      //let info =  L.control;
      this.map = map;
      var fitBoundsSouthWest = new L.LatLng(-17.606754259255016, -63.394996841180316);
    var fitBoundsNorthEast = new L.LatLng(-17.9500307832854, -62.98270820363417);
    var fitBoundsArea = new L.LatLngBounds(fitBoundsSouthWest, fitBoundsNorthEast);                
    this.map.setMaxBounds(fitBoundsArea);



      
    
      //console.log(SL);
      //console.log('ss');
    
      //////////////////////////////
      /*if(localStorage.getItem('marcadoresLote')){
        this.marcadores = JSON.parse(localStorage.getItem('marcadoresLote'));                
      }

      for(let marcador of this.marcadores){          
        this.marker  = L.marker([marcador.lat, marcador.lng]).bindPopup('Punto');        
        this.map.addLayer(this.marker);          
        this.i=1;
      }*/
      ////////////////////////////////////////
  
  
      
  
  
      /*info.onAdd = function (map) {
        this._div = L.DomUtil.create('div', 'info');
        this.update();
        return this._div;
      };
  
      info.update = function (props) {
        this._div.innerHTML = '<h4>US Population Density</h4>' + (props ?
          '<b>' + props.name + '</b><br />' + props.density + ' people / mi<sup>2</sup>'
          : 'Hover over a state');
      };
  
      info.addTo(map);*/
  
  
      /*L.control.zoom({
        position: 'bottomright'
      }).addTo(map);*/
  
  
      /*let legend = L.control.({position: 'bottomright'});
      legend.onAdd = function (map) {
       let div = L.DomUtil.create('div', 'info legend');
      
      div.innerHTML +=
      '<img alt="legend" src="../../../assets/leyenda.png " width="127" height="120" />';    
      
      
      };
      legend.addTo(this.map);*/
  
       
  
      /*const legend = new (L.Control.extend({
        options: { position: 'bottomright' }
      }));
  
  
      legend.onAdd = function (map) {
        const div = L.DomUtil.create('div', 'Leyenda');
        
        div.innerHTML = '<div><b>Leyenda</b></div>';
        
          div.innerHTML += '<img alt="legend" src="../../../assets/leyenda.png " width="127" height="80" />';    
          //div.innerHTML += '<img alt="legend" src="../.. " width="127" height="80" />';    
        
          
        
        return div;
      };
      legend.addTo(this.map);
      */
  
  
    
      var baseMaps = {
        
       
        "Global": this.streetMaps
        //"UV": this.wmsLayers_uv,        
        //"Predeterminado": this.streetMaps
    };
   
    
      
        var overlayMaps = {          
          
          //"Global": this.streetMaps
        };

        
        





          //this.esri,this.wMaps
        /*L.control.layers(baseMaps,overlayMaps,{
          position: 'topright', // 'topleft', 'bottomleft', 'bottomright'
          collapsed: false // true
        }).addTo(this.map);  */


        //L.multiControl(this.overlayMaps1, {position:'topright', label: 'Mapas'}).addTo(map);     

        
  
  
        L.control.scale().addTo(this.map);



        var markersLayer = new L.LayerGroup();	//layer contain searched elements
      
        map.addLayer(markersLayer);


        let mc=L.multiControl(this.overlayMaps1, {position:'topright', label: 'Mapas'}).addTo(map);   

        let ebSearch=L.easyButton('fa-search', function(btn, map){
          //helloPopup.setLatLng(map.getCenter()).openOn(map);        
          let b = document.getElementById("sidebar");
              b.click();            
        }).addTo(map );
  
        let ebmc=L.easyButton('fa-globe', function(btn, map){        
          //helloPopup.setLatLng(map.getCenter()).openOn(map);                            
              mc.toggle();
        }).addTo(map );

        
      setTimeout(() => {
        this.map = map;
        map.invalidateSize();        
      }, 0);


      


 
       
        
        
        

  
      
  
    }
  
    
    
  
  
   
  
  
  
  
  
  agregarMarcadorCalle(evt){

    //this.map.removeLayer(this.map );

    //console.log(this.map.getLayers().length); ;

    let i = 0;
    

    
    /*this.map.eachLayer(function(){ 
      i += 1; 

    });
    console.log('Map has', i, 'layers.');

    let j=1;
          this.map.eachLayer(function(map1) {

            if (j===i){
              this.map.removeLayer(map1)
            }
            j=j+1;
        }); 
      */
     
    
     //this.map=evt;
    var coordenada =  evt.latlng;
    var latitud = coordenada.lat; // lat  es una propiedad de latlng
    var longitud = coordenada.lng; // lng también es una propiedad de latlng
    this.i=this.i+1;
  
    this.GeogToUTM(latitud,longitud);
    //this.imagen(latitud , longitud);    

    

    //if (!!Object.keys(this.geoJSon).length){
      if (this.geoJSon!=null){
        if(this.map.hasLayer(this.geoJSon) ) {
  
        //console.log(this.geoJSon);  
          this.map.removeLayer(this.geoJSon);
        //this.map.removeLayer(marker);
        } 
      }

    
    //this.map.removeLayer(this.geoJSon);

    

    

    
    
  
  
      /*var punto = L.marker([this.lat1, this.lng1]).bindPopup('Soy un puntazo');
      punto.addTo();*/
        
    }

    
  
  
  GeogToUTM(latd0: number,lngd0 : number){
    //Convert Latitude and Longitude to UTM
    //Declarations();
    let k0:number;
    let b:number;
    let a:number;
    let e:number;
    let f:number;
    let k:number;
    //let latd0:number;
    //let lngd0 :number;
    let lngd :number;
    let latd :number;
    let xd :number;
    let yd :number;
    let phi :number;
    let drad :number;
    let utmz :number;
    let lng :number;
    let latz :number;
    let zcm :number;
    let e0 :number;
    let esq :number;
    let e0sq :number;
    let N :number;
    let T :number;
    let C :number;
    let A :number;
    let M0 :number;
    let x :number;
    let M :number;
    let y :number;
    let yg :number;
    let g :number;
    let usft=1;
    let DatumEqRad:number[];
    let DatumFlat:number[];
    let Item;
  
    let lng0 :number;
    
  
  
  
    DatumEqRad = [6378137.0, 6378137.0, 6378137.0, 6378135.0, 6378160.0, 6378245.0, 6378206.4,
      6378388.0, 6378388.0, 6378249.1, 6378206.4, 6377563.4, 6377397.2, 6377276.3, 6378137.0];	
      DatumFlat = [298.2572236, 298.2572236, 298.2572215,	298.2597208, 298.2497323, 298.2997381, 294.9786982,
      296.9993621, 296.9993621, 293.4660167, 294.9786982, 299.3247788, 299.1527052, 300.8021499, 298.2572236]; 
      Item = 1;//Default
  
  
  
    //drad = Math.PI/180;//Convert degrees to radians)
    k0 = 0.9996;//scale on central meridian
    a = DatumEqRad[Item];//equatorial radius, meters. 
          //alert(a);
    f = 1/DatumFlat[Item];//polar flattening.
    b = a*(1-f);//polar axis.
    e = Math.sqrt(1 - b*b/a*a);//eccentricity
    drad = Math.PI/180;//Convert degrees to radians)
    latd = 0;//latitude in degrees
    phi = 0;//latitude (north +, south -), but uses phi in reference
    e0 = e/Math.sqrt(1 - e*e);//e prime in reference
    N = a/Math.sqrt(1-Math.pow(e*Math.sin(phi),2));
    
    
    T = Math.pow(Math.tan(phi),2);
    C = Math.pow(e*Math.cos(phi),2);
    lng = 0;//Longitude (e = +, w = -) - can't use long - reserved word
    lng0 = 0;//longitude of central meridian
    lngd = 0;//longitude in degrees
    M = 0;//M requires calculation
    x = 0;//x coordinate
    y = 0;//y coordinate
    k = 1;//local scale
    utmz = 30;//utm zone
    zcm = 0;//zone central meridian
  
    k0 = 0.9996;//scale on central meridian
    b = a*(1-f);//polar axis.
    //alert(a+"   "+b);
    //alert(1-(b/a)*(b/a));
    e = Math.sqrt(1 - (b/a)*(b/a));//eccentricity
    //alert(e);
    //Input Geographic Coordinates
    //Decimal Degree Option
    
    //latd0 = parseFloat(document.getElementById("DDLatBox0").value);
    //lngd0 = parseFloat(document.getElementById("DDLonBox0").value);
    
    //latd1 = Math.abs(parseFloat(document.getElementById("DLatBox0").value));
    //latd1 = latd1 + parseFloat(document.getElementById("MLatBox0").value)/60;
    //latd1 = latd1 + parseFloat(document.getElementById("SLatBox0").value)/3600;    
  
    lngd=lngd0;
    latd=latd0;
    
    
    
    xd = lngd;
    yd = latd;
  //        alert(Item);
  //        alert(usft);
  
    phi = latd*drad;//Convert latitude to radians
    lng = lngd*drad;//Convert longitude to radians
    utmz = 1 + Math.floor((lngd+180)/6);//calculate utm zone
    latz = 0;//Latitude zone: A-B S of -80, C-W -80 to +72, X 72-84, Y,Z N of 84
    if (latd > -80 && latd < 72){latz = Math.floor((latd + 80)/8)+2;}
    if (latd > 72 && latd < 84){latz = 21;}
    if (latd > 84){latz = 23;}
      
    zcm = 3 + 6*(utmz-1) - 180;//Central meridian of zone
    //alert(utmz + "   " + zcm);
    //Calculate Intermediate Terms
    e0 = e/Math.sqrt(1 - e*e);//Called e prime in reference
    esq = (1 - (b/a)*(b/a));//e squared for use in expansions
    e0sq = e*e/(1-e*e);// e0 squared - always even powers
    //alert(esq+"   "+e0sq)
    N = a/Math.sqrt(1-Math.pow(e*Math.sin(phi),2));
    //alert(1-Math.pow(e*Math.sin(phi),2));
    //alert("N=  "+N);
    T = Math.pow(Math.tan(phi),2);
    //alert("T=  "+T);
    C = e0sq*Math.pow(Math.cos(phi),2);
    //alert("C=  "+C);
    A = (lngd-zcm)*drad*Math.cos(phi);
    //alert("A=  "+A);
    //Calculate M
    M = phi*(1 - esq*(1/4 + esq*(3/64 + 5*esq/256)));
    M = M - Math.sin(2*phi)*(esq*(3/8 + esq*(3/32 + 45*esq/1024)));
    M = M + Math.sin(4*phi)*(esq*esq*(15/256 + esq*45/1024));
    M = M - Math.sin(6*phi)*(esq*esq*esq*(35/3072));
    M = M*a;//Arc length along standard meridian
    //alert(a*(1 - esq*(1/4 + esq*(3/64 + 5*esq/256))));
    //alert(a*(esq*(3/8 + esq*(3/32 + 45*esq/1024))));
    //alert(a*(esq*esq*(15/256 + esq*45/1024)));
    //alert(a*esq*esq*esq*(35/3072));
    //alert(M);
    M0 = 0;//M0 is M for some origin latitude other than zero. Not needed for standard UTM
    //alert("M    ="+M);
    //Calculate UTM Values
    x = k0*N*A*(1 + A*A*((1-T+C)/6 + A*A*(5 - 18*T + T*T + 72*C -58*e0sq)/120));//Easting relative to CM
    x=x+500000;//Easting standard 
    y = k0*(M - M0 + N*Math.tan(phi)*(A*A*(1/2 + A*A*((5 - T + 9*C + 4*C*C)/24 + A*A*(61 - 58*T + T*T + 600*C - 330*e0sq)/720))));//Northing from equator
    yg = y + 10000000;//yg = y global, from S. Pole
    if (y < 0){y = 10000000+y;}
    //Output into UTM Boxes
          //alert(utmz);
    //console.log(utmz)  ;
    //console.log(Math.round(10*(x))/10 / usft)  ;
    //console.log(Math.round(10*y)/10 /usft)  ;
    this.lat1=Math.round(10*(x))/10 / usft  ;
    this.lng1=Math.round(10*y)/10 /usft;
  
    //document.getElementById("UTMeBox1").value = Math.round(10*(x))/10 / usft;
    //document.getElementById("UTMnBox1").value = Math.round(10*y)/10 /usft;
    //if (phi<0){document.getElementById("SHemBox").checked=true;}
    
    //ajaxmagic();			
  }//close Geog to UTM
  
  /*refresh(map){
    //alert('entro');
    
  }*/

  
  guardarStorage(){

    //console.log('Datos Lote: ' +this.marcadores);
    localStorage.setItem('marcadoresLote', JSON.stringify(this.marcadores) );
  }

  borrarMarcador(i : number){
    //console.log(i);
    this.marcadores.splice(i,1);
    this.guardarStorage();
    //this.snackBar.open('Marcador borrado', 'Cerrar',{duration : 3000});
  }
  


  toDataURL = async (url) => {
    console.log("Downloading image...");
    var res = await fetch(url);
    var blob = await res.blob();

    const result = await new Promise((resolve, reject) => {
      var reader = new FileReader();
      reader.addEventListener("load", function () {
        resolve(reader.result);
      }, false);

      reader.onerror = () => {
        return reject(this);
      };
      reader.readAsDataURL(blob);
    })

    return result
  };

 

    CargarMap1(lat:number,lng:number) {
    const width = 700;
    const height = 700;
    this.ContadorMapa++;
    let mapElement = document.createElement("div");
    
    mapElement.setAttribute("id", "Map1");
    //mapElement.setAttribute("class", "map");
    mapElement.style.width = `${width}px`;
    mapElement.style.height = `${height}px`;
    //mapElement.style.background="white";    
    document.body.appendChild(mapElement);

    let map = L.map(mapElement, {
      attributionControl: false,
      zoomControl: false,
      fadeAnimation: false,
      zoomAnimation: false
    }).setView([lat, lng], 20);
    //etiquetasEsri= L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/toner-labels/{z}/{x}/{y}{r}.{ext}',{
    
    /*const tileLayer = L.tileLayer(
      'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
      maxZoom: 20,
      minZoom: 1,
      attribution: 'Tiles © Esri',
    }).addTo(map);*/

    /*const streetMaps =L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      detectRetina: true,
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
      maxZoom: 19,
    }).addTo(map);*/

   /* const wmsLayers = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
        layers: "manzanas_yina",
        format: 'image/png',        
        //maxZoom: 20,
        transparent: true,
        version: '1.3.0',
        attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
      }
    ).addTo(map);*/
    
    
    this.mapaService.obtenerMapas(this.lat1,this.lng1).subscribe((data)=>{
      this.geo_json_data = data;
      //console.log(this.geo_json_data);
      //this.initStatesLayer();

      this.geoJSon = L.geoJSON(this.geo_json_data, {
        style: this.styleManzana,
        //onEachFeature: this.onEachFeature,
        //onEachFeature: this.popup,        
        
      }).addTo(map);
    
      //console.log(this.geoJSon); 

      /*var pointLayer = L.geoJSON(null, {
        pointToLayer: function(feature,latlng){
          let label = String(feature.properties.mz) // Must convert to string, .bindTooltip can't use straight 'feature.properties.attribute'
          return new L.CircleMarker(latlng, {
            radius: 1,
          }).bindTooltip(label, {permanent: true, opacity: 0.7}).openTooltip();
        }
      });
      pointLayer.addData(this.geo_json_data).addTo(map) ;*/

      var parcels = new L.GeoJSON(this.geo_json_data, {
        style: function (feature) {
            return {
                color: "black",
                weight: 2,
                fill: false,
                opacity: 1,
                clickable: false
                //fontSize: 12
            };
        },
        onEachFeature: function (feature, layer) {
            let label = String("UV-"+ feature.properties.UV + "\r\n"+"M-"+ feature.properties.Mz)
            //layer.bindTooltip(feature.properties.mz);
            layer.bindTooltip(label, {permanent: true, opacity: 0.7}).openTooltip();
        }
    }).addTo(map);





    });



    this.mapaService.obtenerCalles(this.lat1,this.lng1).subscribe((data)=>{
      this.geo_json_data = data;
      //console.log(this.geo_json_data);
      //this.initStatesLayer();

      this.geoJSon = L.geoJSON(this.geo_json_data, {
        style: this.style,
        //renderer: this.streetLabelsRenderer
        //renderer 
        //onEachFeature: this.onEachFeature,
        //onEachFeature: this.popup,   
        
        
      }).addTo(map);

      

      /*let streetLabelsRenderer = L.control.streeLabels({
        collisionFlg : true,
        propertyName : 'nombrevia',
        showLabelIf: function(layer) {
          return true; //layer.properties.type == "primary";
        },
        fontStyle: {
          dynamicFontSize: false,
          fontSize: 10,
          fontSizeUnit: "px",
          lineWidth: 8.0,
          fillStyle: "white",
          strokeStyle: "white",
        },
      }).addTo(map);
      */

      
      //map.getRenderer(this.streetLabelsRenderer);

      //L.control.layers

      //var map = new L.Map('map', {
        //renderer : this.streetLabelsRenderer, //Custom Canvas Renderer
        //renderer:this.streetLabelsRenderer

    //});




      var parcels = new L.GeoJSON(this.geo_json_data, {
        style: function (feature) {
            return {
                color: "black",
                weight: 2,
                fill: false,
                opacity: 1,
                clickable: false
            };
        },
        onEachFeature: function (feature, layer) {
            let label = String(feature.properties.nombrevia)
            //layer.bindTooltip(feature.properties.mz);
            //layer.bindTooltip(label, {permanent: true, opacity: 0.7}).openTooltip();
        }
      }).addTo(map);
    });
    ////////////////////////////////////////////////////////

    this.mapaService.obtenerMallaCoordenada(this.lat1,this.lng1).subscribe((data)=>{
      this.geo_json_data = data;
      //alert("obtenerMallaCoordenada");
      //console.log("DATOS MALLA"+this.geo_json_data);
      //this.initStatesLayer();

      this.geoJSon = L.geoJSON(this.geo_json_data, {
        style: this.style,
        //onEachFeature: this.onEachFeature,
        //onEachFeature: this.popup,        
        
      }).addTo(map);


      var parcels = new L.GeoJSON(this.geo_json_data, {
        style: function (feature) {
            return {
                color: "black",
                weight: 2,
                fill: false,
                opacity: 1,
                clickable: false
            };
        },
        onEachFeature: function (feature, layer) {
            let label = String(feature.properties.et_coord)
            //layer.bindTooltip(feature.properties.mz);
            layer.bindTooltip(label, {permanent: true, opacity: 0.7}).openTooltip();
        }
      }).addTo(map);
    });

    

    this.mapaService.obtenerLoteEtiqueta(this.lat1,this.lng1).subscribe((data)=>{
      this.geo_json_data = data ;
      
      
      //this.initStatesLayer();

      this.geoJSon  = L.geoJSON(this.geo_json_data, {        
        //style: this.style,        
        //onEachFeature: this.popup,                
      }).addTo(map);      

      //console.log("obtenerLoteEtiqueta" + this.geoJSon);


      var parcels = new L.GeoJSON(this.geo_json_data, {
        style: function (feature) {
            return {
                color: "black",
                weight: 2,
                fill: false,
                opacity: 1,
                clickable: false
            };
        },
        onEachFeature: function (feature, layer) {
            let label = String(feature.properties.medida);          
            //layer.bindTooltip(feature.properties.mz);
            //layer.bindTooltip(label, {permanent: true, opacity: 0.7}).openTooltip();
            layer.bindTooltip(label, {permanent: true, opacity: 0.7}).openTooltip();
        }
      }).addTo(map);
    });                                   
    

    return mapElement;                
  }                


  agregarMarcadorUrbanizacion(evt){

    /*this.miFormulario_Jurisdiccion.controls['coordenada_latitud'].setValue('');
    this.miFormulario_Jurisdiccion.controls['coordenada_longitud'].setValue('');  
    this.miFormulario_Jurisdiccion.controls['superficiemensura'].setValue('');        
    this.miFormulario_Jurisdiccion.controls['uv'].setValue('');
    this.miFormulario_Jurisdiccion.controls['manzana'].setValue('');
    this.miFormulario_Jurisdiccion.controls['lote'].setValue(''); 
    this.miFormulario_Jurisdiccion.controls['poligono'].setValue(''); */
  
    let i = 0;
    
    var coordenada =  evt.latlng;
    var latitud = coordenada.lat; // lat  es una propiedad de latlng
    var longitud = coordenada.lng; // lng también es una propiedad de latlng
    this.i=this.i+1;
  
    this.GeogToUTM(latitud,longitud);
    
    
    
      if (this.geoJSon!=null){
        if(this.map.hasLayer(this.geoJSon) ) {
  
        //console.log(this.geoJSon);  
          this.map.removeLayer(this.geoJSon);
        //this.map.removeLayer(marker);
        } 
      }
  
      /*this.jurisdiccion_latitud=this.lat1;
      this.jurisdiccion_longitud=this.lng1;
  
      this.miFormulario_Jurisdiccion.controls['coordenada_latitud'].setValue(this.jurisdiccion_latitud);
      this.miFormulario_Jurisdiccion.controls['coordenada_longitud'].setValue(this.jurisdiccion_longitud);*/


      this.mapaService.obtenerDatosUrbanizacion (this.lat1,this.lng1)
    .subscribe(urbanizacion =>{ this.urbanizacion = urbanizacion            

      if (urbanizacion.length>0) { 

        this.idurb= urbanizacion[0].idurb;
        this.Nombreurb= urbanizacion[0].nombreurb;
        alert("Urbanización seleccionado: " +this.Nombreurb);

        this.mapaService.obtenerLote(this.lat1,this.lng1).subscribe((data)=>{
          this.geo_json_data = data;             
          this.geoJSon  = L.geoJSON(this.geo_json_data, {
            style: this.style,
            //onEachFeature: this.onEachFeature,
            onEachFeature: this.popup,                
          }).addTo(this.map);        
        });
      }else{

        alert("Urbanización no seleccionado");
        this.idurb= "";
        this.Nombreurb= "";

        if (this.geoJSon!=null){
          if(this.map.hasLayer(this.geoJSon) ) {
    
          //console.log(this.geoJSon);  
            this.map.removeLayer(this.geoJSon);
          //this.map.removeLayer(marker);
          } 
        }
      }

      console.log(this.Nombreurb);
    });
  }



  buscarGeografico(){

    //this.map.removeLayer( this.groupBuscador );
    //buscarGeografico
    let dm;
    let uv;
    let mz;
    let lt;
    let tipobus;
  
    if (this.FormGroupBusqueda.get('busdm')?.value!=0){
      dm=this.FormGroupBusqueda.get('busdm')?.value;
    }else{      
      dm=0;
    }  
  
    if (this.FormGroupBusqueda.get('busuv')?.value!=0){
      uv=this.FormGroupBusqueda.get('busuv')?.value;
    }else{      
      uv=0;
    }  
  
    if (this.FormGroupBusqueda.get('busmz')?.value!=0){
      mz=this.FormGroupBusqueda.get('busmz')?.value;
    }else{      
      mz=0;
    }  
  
    if (this.FormGroupBusqueda.get('buslt')?.value!=0){
      lt=this.FormGroupBusqueda.get('buslt')?.value;
    }else{      
      lt=0;
    }  
  
  
    tipobus=0;
    if (dm!=0 && uv===0 && mz===0 && lt===0){
      tipobus='1'
    }
  
    if (dm===0 && uv!=0 && mz===0 && lt===0){
      tipobus='2'
    }
  
    if (dm===0 && uv!=0 && mz!=0 && lt===0){
      tipobus='3'
    }
  
    if (dm===0 && uv!=0 && mz!=0 && lt!=0){
      tipobus='4'
    }
  
    if (tipobus!=0){                
      //tipobus='4';//this.FormGroupBusqueda.get('tipobus').value;
      //alert(dm);  
      this.mapaService.buscarGeografico(dm,uv,mz,lt,tipobus).subscribe((data)=>{
        this.geo_json_data = data;          
        
        if (this.geoJSon!=null){
          if(this.map.hasLayer(this.geoJSon) ) {
    
          //console.log(this.geoJSon);  
            this.map.removeLayer(this.geoJSon);
          //this.map.removeLayer(marker);
          } 
        }
    
        this.geoJSon  = L.geoJSON(this.geo_json_data, {
          style: this.style,        
          onEachFeature: this.popup,                
        }).addTo(this.map);
  
        this.map.flyToBounds(this.geoJSon.getBounds());
        this.groupBuscador = L.layerGroup().addTo(this.map);
    
      });
    }else{
      alert('Su búsqueda podría generar muchos resultados, debe ser más específico');
    }      
  }
  
  buscarCoordenada(){
  
    if (this.resBusquedaCoordenada !=null){
      if(this.map.hasLayer(this.resBusquedaCoordenada) ) {
  
      //console.log(this.geoJSon);  
        this.map.removeLayer(this.resBusquedaCoordenada);
      //this.map.removeLayer(marker);
      } 
    }
  
  let busx;
  let busy;
    if (this.FormGroupBusqueda.get('busx')?.value!=0){
      busx=this.FormGroupBusqueda.get('busx')?.value;
    }else{      
      busx=0;
    }  
  
    if (this.FormGroupBusqueda.get('busy')?.value!=0){
      busy=this.FormGroupBusqueda.get('busy')?.value;
    }else{      
      busy=0;
    }  
  
    //var utm = L.utm(479304.6, 8034569.2, 20, 'K', true);
    //https://jjimenezshaw.github.io/Leaflet.UTM/examples/input.html
    var utm = L.utm(busx, busy, 20, 'K', true);
    var ll = utm.latLng();
    if (ll) {
        //marker.setLatLng(ll).bindPopup(utm + '<br>' + ll).openPopup()
        //el1.lat.value = ll.lat.toFixed(6);
        //el1.lng.value = ll.lng.toFixed(6);
        //document.getElementById('result2').innerHTML = '' + ll;
  
        //alert(ll.lat.toFixed(6));
        //alert(ll.lng.toFixed(6));
  
  
          this.resBusquedaCoordenada = L.marker([ll.lat.toFixed(6),ll.lng.toFixed(6)], {
          title: "Resultado de búsqueda",
          draggable:false,
          opacity: 1
          }).bindPopup("<b>Coordenada</b>")
          .addTo(this.map);
          //this.map.flyToBounds(this.resBusquedaCoordenada.getBounds());
    
    
          
          /*let marker;
          marker = L.marker([-17.795595,-63.196819], {
              //icon: customIcon,
          })
          .bindPopup("<p>Coordenada: </p><p>Latitud: " +"-17.795595" + "</p><p>Longitud: " +"-63.196819"+ "</p>")
          .addTo(this.map);
    
          var coordenada = marker.latlng;*/
    
          /*let animatedMarker = L.animatedMarker(marker.getLatLngs(), {
            autoStart: false,
            icon
          });
      
          this.map.addLayer(animatedMarker);*/
    }
  }
  limpiarCoordenada(){
  
      
    this.FormGroupBusqueda.controls['busx'].setValue(''); 
    this.FormGroupBusqueda.controls['buxy'].setValue(''); 
  
      if (this.resBusquedaCoordenada !=null){
        if(this.map.hasLayer(this.resBusquedaCoordenada) ) {
  
        //console.log(this.geoJSon);  
          this.map.removeLayer(this.resBusquedaCoordenada);
        //this.map.removeLayer(marker);
        } 
      }
  
    }
  
    limpiarGeografico(){
    this.FormGroupBusqueda.controls['busdm'].setValue('');
    this.FormGroupBusqueda.controls['busuv'].setValue('');  
    this.FormGroupBusqueda.controls['busmz'].setValue('');        
    this.FormGroupBusqueda.controls['buslt'].setValue('');  
    if (this.geoJSon!=null){
      if(this.map.hasLayer(this.geoJSon) ) {
  
      //console.log(this.geoJSon);  
        this.map.removeLayer(this.geoJSon);
      //this.map.removeLayer(marker);
      } 
    }
  }

}
