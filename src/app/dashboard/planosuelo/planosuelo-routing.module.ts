import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PlanosueloEdificioComponent } from './planosuelo-edificio/planosuelo-edificio.component';
import { PlanosueloTerrenoComponent } from './planosuelo-terreno/planosuelo-terreno.component';
import { PlanosueloUrbanizacionMixtaComponent } from './planosuelo-urbanizacion-mixta/planosuelo-urbanizacion-mixta.component';
import { IniciarTramiteComponent } from './iniciar-tramite/iniciar-tramite.component';


const routes: Routes = [
  
    {
      path: '',
      component: PlanosueloEdificioComponent
    },   
    
    {
      path: 'planosuelo-edificio',
      component: PlanosueloEdificioComponent
    },   

    {
      path: 'planosuelo-terreno',
      component: PlanosueloTerrenoComponent
    },
    {
      path: 'planosuelo-urbanizacion-mixta',
      component: PlanosueloUrbanizacionMixtaComponent
    },
    {
      path: 'iniciar-tramite',
      component: IniciarTramiteComponent
    },    
  ];  


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlanosueloRoutingModule { 
  static components = [IniciarTramiteComponent,
      PlanosueloEdificioComponent,PlanosueloTerrenoComponent,PlanosueloUrbanizacionMixtaComponent ];
}
