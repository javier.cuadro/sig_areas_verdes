import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanosueloTerrenoComponent } from './planosuelo-terreno.component';

describe('PlanosueloTerrenoComponent', () => {
  let component: PlanosueloTerrenoComponent;
  let fixture: ComponentFixture<PlanosueloTerrenoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlanosueloTerrenoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanosueloTerrenoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
