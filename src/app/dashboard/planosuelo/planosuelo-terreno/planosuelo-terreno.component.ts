import { Component,OnInit, ViewChild} from '@angular/core';

import * as htmlToImage from 'html-to-image';
import { geoJSON, GeoJSONOptions,icon,control ,latLng, Layer, layerGroup,  marker, point, polyline, tileLayer } from 'leaflet';
//import {StreetLabels} from '@leaflet.streetlabels';
//import * as StreetLabel from 'streetlabels';





import { MapasService } from 'src/app/services/mapas.service';
import { Marcador } from 'src/app/classes/marcador.class';



//import * as L from 'leaflet';

import "leaflet";
declare let L;
import "leaflet-easybutton";
import * as Map from 'leaflet';
import "leaflet-textpath";


//import '../../../../assets'
import '../../../../assets/js/ctxtextpath.js';
import '../../../../assets/js/L.LabelTextCollision.js';
import '../../../../assets/js/Leaflet.streetlabels.js';

//import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
//import * as M from 'leaflet-streetlabels';
//import * as SM from 'leaflet-search';
//import * as LeafletSearch from 'leaflet-search';


import { LotesService } from 'src/app/services/lotes.service';
//import { Calle } from '../../classes/calle';
//import { Calle } from '../../classes/calle.class';
import { Lote } from 'src/app/classes/lote';




//import { ThisReceiver } from '@angular/compiler';
import { Router } from '@angular/router';
import { async } from '@angular/core/testing';
import { MarcadorLote } from '../../../classes/marcadorlote.class';


import { ResizeService } from 'src/app/services/resize.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { fromEvent, Observable, Subscription } from 'rxjs';
import "leaflet-multicontrol";
import { CallesService } from 'src/app/services/calles.service';
import { DataService } from 'src/app/services/data.service';
import { LcnService } from 'src/app/services/lcn.service';
import { QrbnbService } from 'src/app/services/qrbnb.service';
import { MatStepper } from '@angular/material/stepper';
import { PlanoSuelo } from 'src/app/classes/planosuelo';
import { respuestaQR } from 'src/app/classes/respuestaQr';
import Swal from 'sweetalert2';
//import "leaflet-streetLabels";


@Component({
  selector: 'app-planosuelo-terreno',
  templateUrl: './planosuelo-terreno.component.html',
  styleUrls: ['./planosuelo-terreno.component.css']
})
export class PlanosueloTerrenoComponent implements OnInit {

    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
  //Add 'implements OnInit' to the class.
  map_export;//: Map.Map;
  marker_export;
  cargando=false;


  @ViewChild('stepper') stepper: MatStepper;


  ProcesoCompleto=0;
  planoSuelo:PlanoSuelo;
  idplanoSuelo:number;
  respuestaPlanoSuelo:any;
  urlReporte:any;
  respuestaQr:any= new respuestaQR(0,'','','');
  respuestaConsultaQr:any;
  



  loteid;
  lotedm;
  loteuv;
  lotemz;
  lotelt;
  lotearea;
  loteDato;


 groupBuscador;
 resBusquedaCoordenada;
 resBuscarGeografico;





  private resizeSubscription: Subscription;
  private resizeObservable$: Observable<Event>;
  private resizeSubscription$: Subscription;
  //@ViewChild('idMap') private IDMAP: ElementRef;

  miFormulario_PlanoTerreno: FormGroup = this.fb.group({

    superficietitulo:['',Validators.required],
    superficiemensura:['',Validators.required],
    zona:['',Validators.required],
    distrito:['',Validators.required],
    uv:['',Validators.required],
    manzana:['',Validators.required],
    lote:['',Validators.required]
    /*loteuv:['',Validators.required],
    lotemz:['',Validators.required],
    lotelt: [ , [ Validators.required, Validators.minLength(3) ] ]*/

  })

  firstFormGroup = this._formBuilder.group({
    firstCtrl: ['', Validators.required],
  });
  secondFormGroup = this._formBuilder.group({
    secondCtrl: ['', Validators.required],
  });


  FormGroupBusqueda: FormGroup = this.fbBusqueda.group({

    busdm:[''],
    busuv:[''],
    busmz:[''],
    buslt:[''],
    busx:[''],
    busy:[''],
  });

  isLinear = false;


  ContadorMapa:number=0;

   public geo_json_data;
    title = 'Sig alcaldia';
    json;
    layerFarmacias;
    map;
    //streetLabelsRenderer;
    marcadores : MarcadorLote[]=[];
    marker;
    i=0;


    lat = -17.7505725;
    lng = -63.1047556;

    
    lat1=0;
    lng1=0;
    lote= new Lote(0,'');
    bandera=0;


    MAPELEMENT:any;



////////////////////////Pago QR
estadoqr = new FormControl('');
imagenqr = new FormControl('');
objtoken:any;
objqr:any;
token:any;
public base64:string='';
public idqr:string='';
identificadorDeTemporizador;
public identificadorDeTemporizador_tiempo:number=0;

    //cliente = new Cliente(0,'','','','','');
    //marcadores : Marcador[]=[];


    /*public layersControl = {
      baseLayers: { }
    };*/
    //public map: new L.Map('map');
     //map = L.map('map');
    //.setView([51.505, -0.09], 13);

    //fieldsVias = ["id", "nombre", "jerarquia", "categoria", "anchovia"];



    //constructor( private mapaService:MapasService,private lotesService:LotesService,private router:Router ) {
      //constructor(private resizeService: ResizeService,private _formBuilder: FormBuilder, private mapaService:MapasService,private lotesService:LotesService,private router:Router ) {
        constructor(private qr:QrbnbService,private lcnService:LcnService, private dataService:DataService,private resizeService: ResizeService,private fbBusqueda: FormBuilder,private fb: FormBuilder, private _formBuilder: FormBuilder,private mapaService:MapasService,private calleService:CallesService,private router:Router) {
      //console.log('ss');
    }

    ngOnInit() {
      this.base64="iVBORw0KGgoAAAANSUhEUgAAAJoAAACYCAIAAACTVWdLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAVPSURBVHhe7ZrtkesgDEW3rhSUelJNmkkx+zCIDyFh8HM2zty5589usBCCYzvYk59fAgR1QkGdUFAnFNQJBXVCQZ1QUCcU1AkFdUJBnVBQJxTUCQV1QkGdUFAnFNQJBXVCQZ1QUCcU1AkFdUJBnVBQJxTUCQV1QkGdUFAnFNQJBXVCQZ1QUCcU1AkFdUJBnVBQJxTUCQV1QkGdUFAnFNQJBXVCQZ1QUCcU1AkFdUJBnVBQJxTUCQV1QkGdUFAnFNQJBXVCQZ1QUCcU1AkFdUJBnVBQJxQjnc/7z4j7U2I2Xs/7Tdp/brfH8yXtgwy3272ETIcYBLQ5Cq9HLkOVV5PoZmmVRm+gMEo7G+H1fNxvzYRDJSamxYmXIxtHJrjGKZ11DSvl6DjDziIKKWInoCki0laiD9Ukbbu0Tiu5PZp19aYbUDEt0/gDE1xkonM3qZQr51K4TmOXXK3NEE7VNL8UMh3CDchr5Cz07Z7uFKqDJInUAzq1M1C565RxysDl0rExLXngnfj1Ca7yDp153Oka6cbpEIOA1NzOVmw+XlJR20OSJI6UWtY1Ne8WY5rX4ndjPq4zB4Wht1OwG91meOvV2eatEq3PnOSe/soi6dSDSiRZ7DIsNh0wa78U7wY5E1zn8HenLlyHqe/wYYZc6HSIcQZVhVJofEqSUJgsU+xaWnVM/FRJ7U0HPfmIGTCxFC/jejg9VzipMxA3b3JwIwd4GYLvul2cDuEGmA1nt5798kqS7XMWGv5vWnWMIrXHcpp/NW/X6e6oVzl1s1XUR5bUaZrhWEDeaTVbi0h21JPTqiQ5R771StCgEknd6DQh+YDxNolPB1TQaIKHOKHThrTzn2c4HFDMNYs3slm6dUnko+DHZFSzfOi85eH7rn3OEJf2FypNP643wWOcuTpLaelsKjsdNf9xhv8IMPOVBj191c8kkYaItDqVdNu2QO5XLx7z4KHQ8fXetSEd7LhnhR7+7mxHKoO35MPOGmmmQ3gZ9Hxdm7qjTdJULa3jSlRmd7p29MIgvhYzn+BRTukMbGddKXrb6ZRjXq2K6RCDDLlfuCv4NlVPL0lZMmn1KlG7tkLc96n57n/Pmfh4jeZu0wnu5vYY6SR/xmsz3Ct8E9QJBXVCQZ1QUCcU1AkFdULx1zpXHum8GMHu593gbePfP6PVh3j/oUA9MfuPkOapcfJTkuu5QmeiPiSf1xnRse0rGZNl9L6mfXAfxPzHs/0H+ZDOdkHNq04nZowNtq9XA0mG+3OTIsp5+1oid2K+WegFOgN5sVLzSZ0B0yg2935u0g0XApv77aCkQ5VewTU6s890oh9aJBvsXJ2NROPTEWwYVpQOfO/1eZFOtS4SY/GWbRg8MtbrWzEyjFk5F64EQme3Le3WvPvYq5LDmRjWxxSoc5u+mb8si9K5tkbz4M5PQbr0RsY6nUGGnr+Ei3Sq5rmhhmnwyGbpIxl6J15JXUxOvVbpFVyg02xdpoZaZsGy5FqE7pStNO8e4guD2Cgx0oMPKpq8LJa6KuMYZ+kmOl2bpldQEz8bgr0UUaV3fLPMa3T2L9TeqHNg0+tWL8iNtZd8NuTL+Gud5KNQJxTUCQV1QkGdUFAnFNQJBXVCQZ1QUCcU1AkFdUJBnVBQJxTUCQV1QkGdUFAnFNQJBXVCQZ1QUCcU1AkFdUJBnVBQJxTUCQV1QkGdUFAnFNQJBXVCQZ1QUCcU1AkFdUJBnVBQJxTUCQV1QkGdUFAnFNQJBXVCQZ1QUCcU1AkFdUJBnVBQJxTUCQV1QkGdUFAnFNQJBXVCQZ1QUCcU1AkFdUJBnUD8/v4DngF37uJ5l38AAAAASUVORK5CYII=";

      this.planoSuelo= new PlanoSuelo('','','','','','','','','','',0,"",0);
      this.resizeSubscription = this.resizeService.onResize$
        .subscribe(size => console.log(size));


        this.resizeObservable$ = fromEvent(window, 'resize')
        this.resizeSubscription$ = this.resizeObservable$.subscribe( evt => {
          console.log('event: ', evt)
        })
    }
    ngOnDestroy() {
      if (this.resizeSubscription) {
        this.resizeSubscription.unsubscribe();
      }
      this.resizeSubscription$.unsubscribe()
    }

    geoJSon;//:GeoJSON.GeoJSON;




    // Define our base layers so we can reference them multiple times
    streetMaps =L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      detectRetina: true,
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
      maxZoom: 19,
    });

    esri = L.tileLayer(
      'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
      maxZoom: 19,
      minZoom: 1,
      attribution: 'Tiles © Esri',
    });

    wMaps = L.tileLayer('http://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png', {
      detectRetina: true,
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    });
    etiquetasEsri= L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/toner-labels/{z}/{x}/{y}{r}.{ext}',{
      attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
      subdomains: 'abcd',
      minZoom: 0,
      maxZoom: 19,
      //ext: 'png'
      }
    );

    wmsLayers = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
        layers: "lotes_cartografia_yina",
        format: 'image/png',
        maxZoom: 19,
        transparent: true,
        version: '1.3.0',
        attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
      }
    );

    wms_taz_popurb20 = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
        layers: "taz_popurb20",
        format: 'image/png',
        maxZoom: 19,
        transparent: true,
        version: '1.3.0',
        attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
      }
    );

    wms_taz_popurb25 = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
        layers: "taz_popurb25",
        format: 'image/png',
        maxZoom: 19,
        transparent: true,
        version: '1.3.0',
        attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
      }
    );

    wms_taz_popurb30 = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
        layers: "taz_popurb30",
        format: 'image/png',
        maxZoom: 19,
        transparent: true,
        version: '1.3.0',
        attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
      }
    );

    wms_taz_popurb35 = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
        layers: "taz_popurb35",
        format: 'image/png',
        maxZoom: 19,
        transparent: true,
        version: '1.3.0',
        attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
      }
    );

    wmsLayers_uv = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
        layers: "uv",
        format: 'image/png',
        transparent: true,
        version: '1.3.0',
        attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
      }
    );

    wms_lotes = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
    layers: "predios_2",
    format: 'image/png',
    maxZoom: 19,
    transparent: true,
    version: '1.3.0',
    attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
  }
);


wms_uv_yina = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
    layers: "unidades_vecinales_yina",
    format: 'image/png',
    maxZoom: 19,
    transparent: true,
    version: '1.3.0',
    attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
  }
);
wms_manzanas_yina = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
    layers: "manzanas_yina",
    format: 'image/png',
    maxZoom: 19,
    transparent: true,
    version: '1.3.0',
    attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
  }
);




    // Layers control object with our two base layers and the three overlay layers
    /*layersControl = {
      baseLayers: {
        'Street Maps': this.streetMaps,
        'Wikimedia Maps': this.wMaps
      },
      overlays: {
        'Mt. Rainier Summit': this.summit,
        'Mt. Rainier Paradise Start': this.paradise,
        'Mt. Rainier Climb Route': this.route
      }
    };*/

    streetLabelsRenderer = new L.StreetLabels({
      collisionFlg: true,
      propertyName: 'nombrevia',
      showLabelIf: function (layer) {
        return true; //layer.properties.type == "primary";
      },
      fontStyle: {
        dynamicFontSize: false,
        fontSize: 10,
        fontSizeUnit: "px",
        lineWidth: 4.0,
        fillStyle: "black",
        strokeStyle: "white",
      },
    });


    options = {
      layers: [ this.streetMaps],
      zoom: 13,
      center: L.latLng([ -17.7876515, -63.1825049 ])

      //lat = -17.7876515;
      //lng = -63.1825049;
    };

    options1 = {
      //layers: [ this.streetMaps],
      zoom: 18,
      center: L.latLng([ -17.7876515, -63.1825049 ]),
      attributionControl: false,
      zoomControl: false,
      fadeAnimation: false,
      zoomAnimation: false,
      transparent: true,
      //renderer: this.streetLabelsRenderer



      //lat = -17.7876515;
      //lng = -63.1825049;
    };

    





    /*geoJSON = {
      id: 'geoJSON',
      name: 'Geo JSON Polygon',
      enabled: true,
      layer: L.geoJSON(this.geo_json_data)
    };*/
     getColor(d) {
      return d > 100000000 ? '#800026' :
      d > 50000000 ? '#BD0026' :
      d > 4 ? '#EFEFEF' :
      d > 3 ? '#000287' :
      d > 2 ? '#ccff00' :
      d > 1 ? '#d9880f' :
      d > 0 ? '#0b5e05' :
      '#FFEDA0'}



    style = feature => {
      return {
        weight: 1,
        opacity: 1,
        //color: 'white',
        dashArray: '1',
        fillOpacity: 0.5,
        color: 'black'//this.getColor(feature.properties.tiposvia)
      };
    }

    styleMalla = feature => {
      return {
        weight: 1,
        opacity: 0.1,
        //color: 'white',
        dashArray: '1',
        fillOpacity: 0.5,
        color: 'black'//this.getColor(feature.properties.tiposvia)
      };
    }

    styleLote = feature => {
      return {
        weight: 3,
        opacity: 0.2,
        //color: 'black',
        dashArray: '1',
        fillOpacity: 0.8,
        color: 'white'//this.getColor(feature.properties.tiposvia)
      };
    }

    styleManzana = feature => {
      return {
        weight: 3,
        opacity: 1,
        color: 'white',
        dashArray: '1',
        fillOpacity: 0.8,
        colorFont:'black'
        //color: this.getColor(feature.properties.tiposvia)
      };
    }

     popup(feature, layer) {
      if (feature.properties && feature.properties.nombrevia) {
        layer.bindPopup(feature.properties.nombrevia);
      }

      /*for(let i in layer.features)
      layer.features[i].properties.color = this.getColor( layer.features[i].properties.jerarquia );*/
    }



    /*resetHighlight = e => {
      geojson.resetStyle(e.target);
      info.update();
    }*/

    //zoomToFeature = e => {
  //    this.map.fitBounds(e.target.getBounds());
  //  }


    /*onEachFeature = (feature, layer) => {
      layer.on({
        //mouseover: this.highlightFeature,
        //mouseout: this.resetFeature,
        //click: this.zoomToFeature
        html = "",
        for (prop in feature.properties) {
            html += prop + ": " + feature.properties[prop] + "<br>";
        };
        layer.bindPopup(html);
      });
    }*/

     /*onEachFeature(feature, layer) {
      //if (feature.properties && feature.properties.popupContent) {
          //layer.bindPopup(feature.properties.popupContent);
          for(let i in layer.features){
            layer.features[i].properties.color = this.getColor( layer.features[i].properties.tiposvia );
            //console.log(layer.features[i].properties.jerarquia);
          }

      }*/
      overlayMaps1 = [
        //{name:"general" , layer: this.streetMaps},
        {name:"Satélite" , layer: this.esri},
        {name:"Uv" , layer:this.wms_uv_yina},
        {name:"Manzanas" , layer:this.wms_manzanas_yina},
        {name:"Lotes" , layer:this.wms_lotes},
      ];







    onMapReady(map) {

      this.dataService.nombreTramite="TRÁMITE: PLANO DE USO EN DOMICILIO";

      //document.getElementById('map').style.cursor = 'crosshair'


    //this.IDMAP.nativeElement.remove();





      //alert('aaa');
      L.Icon.Default.imagePath = "assets/leaflet/";
      //L.Icon.Default.imagePath=
      let geojson;
      //let info =  L.control;
      this.map = map;
      var fitBoundsSouthWest = new L.LatLng(-17.606754259255016, -63.394996841180316);
    var fitBoundsNorthEast = new L.LatLng(-17.9500307832854, -62.98270820363417);
    var fitBoundsArea = new L.LatLngBounds(fitBoundsSouthWest, fitBoundsNorthEast);
    this.map.setMaxBounds(fitBoundsArea);





      //console.log(SL);
      //console.log('ss');

      //////////////////////////////
      /*if(localStorage.getItem('marcadoresLote')){
        this.marcadores = JSON.parse(localStorage.getItem('marcadoresLote'));
      }

      for(let marcador of this.marcadores){
        this.marker  = L.marker([marcador.lat, marcador.lng]).bindPopup('Punto');
        this.map.addLayer(this.marker);
        this.i=1;
      }*/
      ////////////////////////////////////////





      /*info.onAdd = function (map) {
        this._div = L.DomUtil.create('div', 'info');
        this.update();
        return this._div;
      };

      info.update = function (props) {
        this._div.innerHTML = '<h4>US Population Density</h4>' + (props ?
          '<b>' + props.name + '</b><br />' + props.density + ' people / mi<sup>2</sup>'
          : 'Hover over a state');
      };

      info.addTo(map);*/


      /*L.control.zoom({
        position: 'bottomright'
      }).addTo(map);*/


      /*let legend = L.control.({position: 'bottomright'});
      legend.onAdd = function (map) {
       let div = L.DomUtil.create('div', 'info legend');

      div.innerHTML +=
      '<img alt="legend" src="../../../assets/leyenda.png " width="127" height="120" />';


      };
      legend.addTo(this.map);*/



      /*const legend = new (L.Control.extend({
        options: { position: 'bottomright' }
      }));


      legend.onAdd = function (map) {
        const div = L.DomUtil.create('div', 'Leyenda');

        div.innerHTML = '<div><b>Leyenda</b></div>';

          div.innerHTML += '<img alt="legend" src="../../../assets/leyenda.png " width="127" height="80" />';
          //div.innerHTML += '<img alt="legend" src="../.. " width="127" height="80" />';



        return div;
      };
      legend.addTo(this.map);
      */



      var baseMaps = {


        "Global": this.streetMaps
        //"UV": this.wmsLayers_uv,
        //"Predeterminado": this.streetMaps
    };



        var overlayMaps = {

          //"Global": this.streetMaps
        };








          //this.esri,this.wMaps
        /*L.control.layers(baseMaps,overlayMaps,{
          position: 'topright', // 'topleft', 'bottomleft', 'bottomright'
          collapsed: false // true
        }).addTo(this.map);  */


        //L.multiControl(this.overlayMaps1, {position:'topright', label: 'Mapas'}).addTo(map);




        L.control.scale().addTo(this.map);



        var markersLayer = new L.LayerGroup();	//layer contain searched elements

        map.addLayer(markersLayer);

        let mc=L.multiControl(this.overlayMaps1, {position:'topright', label: 'Mapas'}).addTo(map);

        let ebSearch=L.easyButton('fa-search', function(btn, map){
          //helloPopup.setLatLng(map.getCenter()).openOn(map);
          let b = document.getElementById("sidebar");
              b.click();
        }).addTo(map );

        let ebmc=L.easyButton('fa-globe', function(btn, map){
          //helloPopup.setLatLng(map.getCenter()).openOn(map);
              mc.toggle();
        }).addTo(map );


      setTimeout(() => {
        this.map = map;
        map.invalidateSize();
      }, 0);
    }

  agregarMarcadorCalle(evt){

    //this.map.removeLayer(this.map );

    //console.log(this.map.getLayers().length); ;

    let i = 0;



    /*this.map.eachLayer(function(){
      i += 1;

    });
    console.log('Map has', i, 'layers.');

    let j=1;
          this.map.eachLayer(function(map1) {

            if (j===i){
              this.map.removeLayer(map1)
            }
            j=j+1;
        });
      */


     //this.map=evt;
    var coordenada =  evt.latlng;
    var latitud = coordenada.lat; // lat  es una propiedad de latlng
    var longitud = coordenada.lng; // lng también es una propiedad de latlng
    this.i=this.i+1;

    this.GeogToUTM(latitud,longitud);
    //this.imagen(latitud , longitud);



    //if (!!Object.keys(this.geoJSon).length){
      if (this.geoJSon!=null){
        if(this.map.hasLayer(this.geoJSon) ) {

        //console.log(this.geoJSon);
          this.map.removeLayer(this.geoJSon);
        //this.map.removeLayer(marker);
        }
      }


    //this.map.removeLayer(this.geoJSon);









      /*var punto = L.marker([this.lat1, this.lng1]).bindPopup('Soy un puntazo');
      punto.addTo();*/

    }




  GeogToUTM(latd0: number,lngd0 : number){
    //Convert Latitude and Longitude to UTM
    //Declarations();
    let k0:number;
    let b:number;
    let a:number;
    let e:number;
    let f:number;
    let k:number;
    //let latd0:number;
    //let lngd0 :number;
    let lngd :number;
    let latd :number;
    let xd :number;
    let yd :number;
    let phi :number;
    let drad :number;
    let utmz :number;
    let lng :number;
    let latz :number;
    let zcm :number;
    let e0 :number;
    let esq :number;
    let e0sq :number;
    let N :number;
    let T :number;
    let C :number;
    let A :number;
    let M0 :number;
    let x :number;
    let M :number;
    let y :number;
    let yg :number;
    let g :number;
    let usft=1;
    let DatumEqRad:number[];
    let DatumFlat:number[];
    let Item;

    let lng0 :number;




    DatumEqRad = [6378137.0, 6378137.0, 6378137.0, 6378135.0, 6378160.0, 6378245.0, 6378206.4,
      6378388.0, 6378388.0, 6378249.1, 6378206.4, 6377563.4, 6377397.2, 6377276.3, 6378137.0];
      DatumFlat = [298.2572236, 298.2572236, 298.2572215,	298.2597208, 298.2497323, 298.2997381, 294.9786982,
      296.9993621, 296.9993621, 293.4660167, 294.9786982, 299.3247788, 299.1527052, 300.8021499, 298.2572236];
      Item = 1;//Default



    //drad = Math.PI/180;//Convert degrees to radians)
    k0 = 0.9996;//scale on central meridian
    a = DatumEqRad[Item];//equatorial radius, meters.
          //alert(a);
    f = 1/DatumFlat[Item];//polar flattening.
    b = a*(1-f);//polar axis.
    e = Math.sqrt(1 - b*b/a*a);//eccentricity
    drad = Math.PI/180;//Convert degrees to radians)
    latd = 0;//latitude in degrees
    phi = 0;//latitude (north +, south -), but uses phi in reference
    e0 = e/Math.sqrt(1 - e*e);//e prime in reference
    N = a/Math.sqrt(1-Math.pow(e*Math.sin(phi),2));


    T = Math.pow(Math.tan(phi),2);
    C = Math.pow(e*Math.cos(phi),2);
    lng = 0;//Longitude (e = +, w = -) - can't use long - reserved word
    lng0 = 0;//longitude of central meridian
    lngd = 0;//longitude in degrees
    M = 0;//M requires calculation
    x = 0;//x coordinate
    y = 0;//y coordinate
    k = 1;//local scale
    utmz = 30;//utm zone
    zcm = 0;//zone central meridian

    k0 = 0.9996;//scale on central meridian
    b = a*(1-f);//polar axis.
    //alert(a+"   "+b);
    //alert(1-(b/a)*(b/a));
    e = Math.sqrt(1 - (b/a)*(b/a));//eccentricity
    //alert(e);
    //Input Geographic Coordinates
    //Decimal Degree Option

    //latd0 = parseFloat(document.getElementById("DDLatBox0").value);
    //lngd0 = parseFloat(document.getElementById("DDLonBox0").value);

    //latd1 = Math.abs(parseFloat(document.getElementById("DLatBox0").value));
    //latd1 = latd1 + parseFloat(document.getElementById("MLatBox0").value)/60;
    //latd1 = latd1 + parseFloat(document.getElementById("SLatBox0").value)/3600;

    lngd=lngd0;
    latd=latd0;



    xd = lngd;
    yd = latd;
  //        alert(Item);
  //        alert(usft);

    phi = latd*drad;//Convert latitude to radians
    lng = lngd*drad;//Convert longitude to radians
    utmz = 1 + Math.floor((lngd+180)/6);//calculate utm zone
    latz = 0;//Latitude zone: A-B S of -80, C-W -80 to +72, X 72-84, Y,Z N of 84
    if (latd > -80 && latd < 72){latz = Math.floor((latd + 80)/8)+2;}
    if (latd > 72 && latd < 84){latz = 21;}
    if (latd > 84){latz = 23;}

    zcm = 3 + 6*(utmz-1) - 180;//Central meridian of zone
    //alert(utmz + "   " + zcm);
    //Calculate Intermediate Terms
    e0 = e/Math.sqrt(1 - e*e);//Called e prime in reference
    esq = (1 - (b/a)*(b/a));//e squared for use in expansions
    e0sq = e*e/(1-e*e);// e0 squared - always even powers
    //alert(esq+"   "+e0sq)
    N = a/Math.sqrt(1-Math.pow(e*Math.sin(phi),2));
    //alert(1-Math.pow(e*Math.sin(phi),2));
    //alert("N=  "+N);
    T = Math.pow(Math.tan(phi),2);
    //alert("T=  "+T);
    C = e0sq*Math.pow(Math.cos(phi),2);
    //alert("C=  "+C);
    A = (lngd-zcm)*drad*Math.cos(phi);
    //alert("A=  "+A);
    //Calculate M
    M = phi*(1 - esq*(1/4 + esq*(3/64 + 5*esq/256)));
    M = M - Math.sin(2*phi)*(esq*(3/8 + esq*(3/32 + 45*esq/1024)));
    M = M + Math.sin(4*phi)*(esq*esq*(15/256 + esq*45/1024));
    M = M - Math.sin(6*phi)*(esq*esq*esq*(35/3072));
    M = M*a;//Arc length along standard meridian
    //alert(a*(1 - esq*(1/4 + esq*(3/64 + 5*esq/256))));
    //alert(a*(esq*(3/8 + esq*(3/32 + 45*esq/1024))));
    //alert(a*(esq*esq*(15/256 + esq*45/1024)));
    //alert(a*esq*esq*esq*(35/3072));
    //alert(M);
    M0 = 0;//M0 is M for some origin latitude other than zero. Not needed for standard UTM
    //alert("M    ="+M);
    //Calculate UTM Values
    x = k0*N*A*(1 + A*A*((1-T+C)/6 + A*A*(5 - 18*T + T*T + 72*C -58*e0sq)/120));//Easting relative to CM
    x=x+500000;//Easting standard
    y = k0*(M - M0 + N*Math.tan(phi)*(A*A*(1/2 + A*A*((5 - T + 9*C + 4*C*C)/24 + A*A*(61 - 58*T + T*T + 600*C - 330*e0sq)/720))));//Northing from equator
    yg = y + 10000000;//yg = y global, from S. Pole
    if (y < 0){y = 10000000+y;}
    //Output into UTM Boxes
          //alert(utmz);
    //console.log(utmz)  ;
    //console.log(Math.round(10*(x))/10 / usft)  ;
    //console.log(Math.round(10*y)/10 /usft)  ;
    this.lat1=Math.round(10*(x))/10 / usft  ;
    this.lng1=Math.round(10*y)/10 /usft;

    //document.getElementById("UTMeBox1").value = Math.round(10*(x))/10 / usft;
    //document.getElementById("UTMnBox1").value = Math.round(10*y)/10 /usft;
    //if (phi<0){document.getElementById("SHemBox").checked=true;}

    //ajaxmagic();
  }//close Geog to UTM

  /*refresh(map){
    //alert('entro');

  }*/


  guardarStorage(){

    //console.log('Datos Lote: ' +this.marcadores);
    localStorage.setItem('marcadoresLote', JSON.stringify(this.marcadores) );
  }

  borrarMarcador(i : number){
    //console.log(i);
    this.marcadores.splice(i,1);
    this.guardarStorage();
    //this.snackBar.open('Marcador borrado', 'Cerrar',{duration : 3000});
  }



  toDataURL = async (url) => {
    console.log("Downloading image...");
    var res = await fetch(url);
    var blob = await res.blob();

    const result = await new Promise((resolve, reject) => {
      var reader = new FileReader();
      reader.addEventListener("load", function () {
        resolve(reader.result);
      }, false);

      reader.onerror = () => {
        return reject(this);
      };
      reader.readAsDataURL(blob);
    })

    return result
  };

  onEachFeature(feature, layer) {
    //Do something with the features here (bind popups, etc)
  };

  



  //CargarMap1(lat:number,lng:number) {
  CargarMap1(doc:String,lat:number,lng:number) {
    const width = 700;
    const height = 700;

    this.cargando=true;

    this.map_export.setView([lat, lng], 19);
    //this.map_export.fill= false;


    this.mapaService.obtenerAceraPredio(doc).subscribe((data)=>{
      this.geo_json_data = data;
      //alert("obtenerMallaCoordenada");
      //console.log("DATOS MALLA"+lat);
      //this.initStatesLayer();

      this.geoJSon = L.geoJSON(this.geo_json_data, {
        style: this.style,
        //onEachFeature: this.onEachFeature,
        //onEachFeature: this.popup,

      }).addTo(this.map_export);

      L.geoJson(this.geo_json_data, {
        onEachFeature: function(feature, layer) {
          layer.setText(feature.properties.etancho, {
            offset: -7,
            center :true
          });
        },
        style: {
          weight: 1,
          color: "black",
          //dashArray: "4, 4"
        }
      }).addTo(this.map_export);


    });


    

    //this.mapaService.obtenerMapas(this.lat1,this.lng1).subscribe((data)=>{
    this.mapaService.obtenerMapasPredios(doc).subscribe((data)=>{
      this.geo_json_data = data;
      //console.log(this.geo_json_data);
      //this.initStatesLayer();

      this.geoJSon = L.geoJSON(this.geo_json_data, {
        style: this.styleManzana,
        //onEachFeature: this.onEachFeature,
        //onEachFeature: this.popup,

      }).addTo(this.map_export);



      var parcels = new L.GeoJSON(this.geo_json_data, {
        style: function (feature) {
            return {
                color: "black",
                weight: 1,
                fill: false,
                opacity: 1,
                clickable: false
                //fontSize: 12
            };
        },
        onEachFeature: function (feature, layer) {
            let label = String("UV-"+ feature.properties.uv + "\r\n"+"M-"+ feature.properties.manzana)

            /*layer.setText("UV-"+ feature.properties.uv + "\r\n"+"M-"+ feature.properties.mz, {
              offset: -5,
              orientation:180 ,
              center :true
  
            });*/
            //layer.bindTooltip(feature.properties.manzana);
            layer.bindTooltip(label, {permanent: true, opacity: 1,direction:'right',className: "my-labels-predioManzana"}).openTooltip();
            
        }
      }).addTo(this.map_export);
    });



   //this.mapaService.obtenerCalles(this.lat1,this.lng1).subscribe((data)=>{
    this.mapaService.obtenerCallesPredios(doc).subscribe((data)=>{
      this.geo_json_data = data;

      /*onEachFeature: function(feature, layer) {
        layer.setText(feature.properties.etvia, {
          offset: -5,
          orientation:360 ,
          center :true

        });
      },*/
      /*onEachFeature: function (feature, layer) {
        let label = String(feature.properties.predio)
        
        layer.bindTooltip(label, {permanent: true, direction: "center", className: "my-labels"}).openTooltip();
      },*/
      


      L.geoJson(this.geo_json_data, {
        onEachFeature: function(feature, layer) {
          layer.setText(feature.properties.etvia, {
            offset: -5,
            orientation:360 ,
            center :true
  
          });
        },
        
        style: {
          weight: 3,
          color: "black",
          //dashArray: "",
          opacity: 0,
        }
      }).addTo(this.map_export);


    });
  


    

    

    /*this.mapaService.obtenerCalles(this.lat1,this.lng1).subscribe((data)=>{
      this.geo_json_data = data;
       L.geoJson(this.geo_json_data, {        
        onEachFeature: function(feature, layer) {          
        },
        style: {
          weight: 3,
          color: "black",
          //dashArray: "",
          opacity: 1,
        }
      }).addTo(this.map_export);            
    });
    */

    

    ////////////////////////////////////////////////////////

    



    /*this.mapaService.obtenerLoteEtiqueta(this.lat1,this.lng1).subscribe((data)=>{
      this.geo_json_data = data ;


      //this.initStatesLayer();

      this.geoJSon  = L.geoJSON(this.geo_json_data, {
        //style: this.style,
        //onEachFeature: this.popup,
      }).addTo(this.map_export);

      //console.log("obtenerLoteEtiqueta" + this.geoJSon);

      L.geoJson(this.geo_json_data, {
        onEachFeature: function(feature, layer) {
          layer.setText(feature.properties.medida, {
            offset: -5,
            center :true,
            //below :true
          });
        },
        style: {
          weight: 3,
          color: "black",
          //dashArray: "2, 2",
          opacity: 1
        }
      }).addTo(this.map_export);
    });*/




    //this.mapaService.obtenerLote(this.lat1,this.lng1).subscribe((data)=>{
    this.mapaService.obtenerLotePredios(doc).subscribe((data)=>{
      this.geo_json_data = data;
      //console.log("obtenerLote "+this.geo_json_data);
      //this.initStatesLayer();

      this.geoJSon  = L.geoJSON(this.geo_json_data, {
        onEachFeature: function (feature, layer) {
          let label = String(feature.properties.predio)
          
          layer.bindTooltip(label, {permanent: true, direction: "center", className: "my-labels_predio"}).openTooltip();
      },
        style: this.styleLote,
        //onEachFeature: this.popup,
      }).addTo(this.map_export);

      /*let label = new L.bindLabel();
        label.setContent("MultiPolygon static label");
        label.setLatLng(this.geo_json_data.getBounds().getCenter());
        this.map_export.showLabel(label);*/
    });



    

    this.mapaService.obtenerAnchosPredios(doc).subscribe((data)=>{
      this.geo_json_data = data;


      L.geoJson(this.geo_json_data, {
        onEachFeature: function (feature, layer) {
          let label = String(feature.properties.avia)
          
          layer.bindTooltip(label, {permanent: true, direction: "right", className: "my-labels"}).openTooltip();
      },
        style: {
          weight: 1,
          color: "black",
          //fill: false,
          //dashArray: "",
          //dashArray: "2, 2",
          opacity: 0.5,
        }
      }).addTo(this.map_export);


    });

    this.mapaService.obtenerAnchosPredios(doc).subscribe((data)=>{
      this.geo_json_data = data;


      L.geoJson(this.geo_json_data, {
        onEachFeature: function (feature, layer) {
          let label = String(feature.properties.acvia)
          
          layer.bindTooltip(label, {permanent: true, direction: "left", className: "my-labels"}).openTooltip();
      },
        style: {
          weight: 1,
          color: "black",
          //fill: false,
          //dashArray: "",
          //dashArray: "2, 2",
          opacity: 0.8,
        }
      }).addTo(this.map_export);


    });

    this.mapaService.obtenerMedidasPredios(doc).subscribe((data)=>{
      this.geo_json_data = data;


      L.geoJson(this.geo_json_data, {
        onEachFeature: function(feature, layer) {
          layer.setText(feature.properties.etmedidas, {
            offset: -5,
            orientation:360 ,
            center :true

          });
        },
        style: {
          weight: 1,
          color: "black",
          //fill: false,
          //dashArray: "",
          //dashArray: "2, 2",
          opacity: 1,
        }
      }).addTo(this.map_export);


    });

    this.mapaService.obtenerColindantesPredios(doc).subscribe((data)=>{
      this.geo_json_data = data;


      var pointLayer = L.geoJSON(null, {
        pointToLayer: function(feature,latlng){
          let label = String(feature.properties.et) // .bindTooltip can't use straight 'feature.properties.attribute'
          return new L.CircleMarker(latlng, {
            radius: 9,
            color: "black",
            fill: false,
            weight: 1,
          }).bindTooltip(label, {permanent: true, direction: "center", className: "my-labels-predioColindantes"}).openTooltip();
          }
        });
        pointLayer.addData(this.geo_json_data);
      this.map_export.addLayer(pointLayer);


    });


    //getCenter()
    //console.log("Coordenadas Centro: "+ this.map_export.getCenter());
    this.GeogToUTM(this.map_export.getCenter().lat,this.map_export.getCenter().lng);
    this.mapaService.obtenerMallaCoordenada(this.lat1,this.lng1).subscribe((data)=>{
      this.geo_json_data = data;
      //alert("obtenerMallaCoordenada");
      //console.log("DATOS MALLA"+this.lat1);
      //this.initStatesLayer();

      this.geoJSon = L.geoJSON(this.geo_json_data, {
        style: this.styleMalla,
        //onEachFeature: this.onEachFeature,
        //onEachFeature: this.popup,

      }).addTo(this.map_export);

      L.geoJson(this.geo_json_data, {
        onEachFeature: function(feature, layer) {
          layer.setText(feature.properties.et_coord, {
            offset: -7,
           // center :true
          });
        },
        style: {
          weight: 1,
          color: "black",
          opacity: 0.2
          //dashArray: "4, 4"
        }
      }).addTo(this.map_export);



    });


    //return mapElement;


    var x = document.getElementById("map_export");
    this.getValueWithPromise(x);


      /*htmlToImage.toPng(x  , { quality: 0.95 })
      .then((dataUrl) => {
         localStorage.removeItem('imagenPlano');
          localStorage.setItem('imagenPlano', JSON.stringify(dataUrl) );
          console.log("ImagenFinal:"+dataUrl);
      });*/
  }

  agregarMarcadorLote(evt){

    /*this.miFormulario_Jurisdiccion.controls['coordenada_latitud'].setValue('');
    this.miFormulario_Jurisdiccion.controls['coordenada_longitud'].setValue('');
    this.miFormulario_Jurisdiccion.controls['superficiemensura'].setValue('');
    this.miFormulario_Jurisdiccion.controls['uv'].setValue('');
    this.miFormulario_Jurisdiccion.controls['manzana'].setValue('');
    this.miFormulario_Jurisdiccion.controls['lote'].setValue('');
    this.miFormulario_Jurisdiccion.controls['poligono'].setValue(''); */

    let i = 0;

    var coordenada =  evt.latlng;
    var latitud = coordenada.lat; // lat  es una propiedad de latlng
    var longitud = coordenada.lng; // lng también es una propiedad de latlng
    this.i=this.i+1;

    this.GeogToUTM(latitud,longitud);

    
    this.imagen(latitud , longitud);



      if (this.geoJSon!=null){
        if(this.map.hasLayer(this.geoJSon) ) {

        //console.log(this.geoJSon);
          this.map.removeLayer(this.geoJSon);
        //this.map.removeLayer(marker);
        }
      }

      /*this.jurisdiccion_latitud=this.lat1;
      this.jurisdiccion_longitud=this.lng1;

      this.miFormulario_Jurisdiccion.controls['coordenada_latitud'].setValue(this.jurisdiccion_latitud);
      this.miFormulario_Jurisdiccion.controls['coordenada_longitud'].setValue(this.jurisdiccion_longitud);*/


      this.mapaService.obtenerDatosLote(this.lat1,this.lng1)
    .subscribe(lote =>{ this.loteDato = lote

      if (lote.length>0) {

        this.lotedm= lote[0].dm;
        this.loteuv= lote[0].uv;
        this.lotemz= lote[0].mz;
        this.lotelt= lote[0].lt;
        this.lotearea= lote[0].shape_area;

        this.miFormulario_PlanoTerreno.controls['distrito'].setValue(this.lotedm);
        this.miFormulario_PlanoTerreno.controls['superficietitulo'].setValue(this.lotearea);
        this.miFormulario_PlanoTerreno.controls['superficiemensura'].setValue(this.lotearea);
        this.miFormulario_PlanoTerreno.controls['zona'].setValue("NORTE");
        this.miFormulario_PlanoTerreno.controls['distrito'].setValue(this.lotedm);
        this.miFormulario_PlanoTerreno.controls['manzana'].setValue(this.lotemz);
        this.miFormulario_PlanoTerreno.controls['lote'].setValue(this.lotelt);


        //alert("Lote seleccionado: " +this.lotelt);

       

        this.mapaService.obtenerLote(this.lat1,this.lng1).subscribe((data)=>{
          this.geo_json_data = data;
          this.geoJSon  = L.geoJSON(this.geo_json_data, {
            style: this.style,
            //onEachFeature: this.onEachFeature,
            onEachFeature: this.popup,
          }).addTo(this.map);
        });
      }else{

        //alert("Lote no seleccionado");

        Swal.fire({
          position:'center',
          icon:'warning',
          title: 'Lote no seleccionado' ,
          showConfirmButton: false,
          timer: 2000
        });
        this.lotedm="";
        this.loteuv="";
        this.lotemz= "";
        this.lotelt= "";
        this.lotearea= "";

        this.miFormulario_PlanoTerreno.controls['distrito'].setValue("");
        this.miFormulario_PlanoTerreno.controls['superficietitulo'].setValue("");
        this.miFormulario_PlanoTerreno.controls['superficiemensura'].setValue("");
        this.miFormulario_PlanoTerreno.controls['zona'].setValue("");
        this.miFormulario_PlanoTerreno.controls['distrito'].setValue("");
        this.miFormulario_PlanoTerreno.controls['manzana'].setValue("");
        this.miFormulario_PlanoTerreno.controls['lote'].setValue("");

        if (this.geoJSon!=null){
          if(this.map.hasLayer(this.geoJSon) ) {

          //console.log(this.geoJSon);
            this.map.removeLayer(this.geoJSon);
          //this.map.removeLayer(marker);
          }
        }
      }

      Swal.fire({
        position:'center',
        icon:'warning',
        title: 'Lote seleccionado: ' +this.lotelt,
        showConfirmButton: false,
        timer: 2000
      });

      this.cargando=false;

      //console.log(this.Nombreurb);
    });
  }



  buscarGeografico(){

    //this.map.removeLayer( this.groupBuscador );
    //buscarGeografico
    let dm;
    let uv;
    let mz;
    let lt;
    let tipobus;

    if (this.FormGroupBusqueda.get('busdm')?.value!=0){
      dm=this.FormGroupBusqueda.get('busdm')?.value;
    }else{
      dm=0;
    }

    if (this.FormGroupBusqueda.get('busuv')?.value!=0){
      uv=this.FormGroupBusqueda.get('busuv')?.value;
    }else{
      uv=0;
    }

    if (this.FormGroupBusqueda.get('busmz')?.value!=0){
      mz=this.FormGroupBusqueda.get('busmz')?.value;
    }else{
      mz=0;
    }

    if (this.FormGroupBusqueda.get('buslt')?.value!=0){
      lt=this.FormGroupBusqueda.get('buslt')?.value;
    }else{
      lt=0;
    }


    tipobus=0;
    if (dm!=0 && uv===0 && mz===0 && lt===0){
      tipobus='1'
    }

    if (dm===0 && uv!=0 && mz===0 && lt===0){
      tipobus='2'
    }

    if (dm===0 && uv!=0 && mz!=0 && lt===0){
      tipobus='3'
    }

    if (dm===0 && uv!=0 && mz!=0 && lt!=0){
      tipobus='4'
    }

    if (tipobus!=0){
      //tipobus='4';//this.FormGroupBusqueda.get('tipobus').value;
      //alert(dm);
      this.mapaService.buscarGeografico(dm,uv,mz,lt,tipobus).subscribe((data)=>{
        this.geo_json_data = data;

        if (this.geoJSon!=null){
          if(this.map.hasLayer(this.geoJSon) ) {

          //console.log(this.geoJSon);
            this.map.removeLayer(this.geoJSon);
          //this.map.removeLayer(marker);
          }
        }

        this.geoJSon  = L.geoJSON(this.geo_json_data, {
          style: this.style,
          onEachFeature: this.popup,
        }).addTo(this.map);

        this.map.flyToBounds(this.geoJSon.getBounds());
        this.groupBuscador = L.layerGroup().addTo(this.map);

      });
    }else{      

      Swal.fire({
        position:'center',
        icon:'warning',
        title: 'Su búsqueda podría generar muchos resultados, debe ser más específico',
        showConfirmButton: false,
        timer: 2000
      })
    }
  }

  buscarCoordenada(){

    if (this.resBusquedaCoordenada !=null){
      if(this.map.hasLayer(this.resBusquedaCoordenada) ) {

      //console.log(this.geoJSon);
        this.map.removeLayer(this.resBusquedaCoordenada);
      //this.map.removeLayer(marker);
      }
    }

  let busx;
  let busy;
    if (this.FormGroupBusqueda.get('busx')?.value!=0){
      busx=this.FormGroupBusqueda.get('busx')?.value;
    }else{
      busx=0;
    }

    if (this.FormGroupBusqueda.get('busy')?.value!=0){
      busy=this.FormGroupBusqueda.get('busy')?.value;
    }else{
      busy=0;
    }

    //var utm = L.utm(479304.6, 8034569.2, 20, 'K', true);
    //https://jjimenezshaw.github.io/Leaflet.UTM/examples/input.html
    var utm = L.utm(busx, busy, 20, 'K', true);
    var ll = utm.latLng();
    if (ll) {
        //marker.setLatLng(ll).bindPopup(utm + '<br>' + ll).openPopup()
        //el1.lat.value = ll.lat.toFixed(6);
        //el1.lng.value = ll.lng.toFixed(6);
        //document.getElementById('result2').innerHTML = '' + ll;

        //alert(ll.lat.toFixed(6));
        //alert(ll.lng.toFixed(6));


          this.resBusquedaCoordenada = L.marker([ll.lat.toFixed(6),ll.lng.toFixed(6)], {
          title: "Resultado de búsqueda",
          draggable:false,
          opacity: 1
          }).bindPopup("<b>Coordenada</b>")
          .addTo(this.map);
          //this.map.flyToBounds(this.resBusquedaCoordenada.getBounds());



          /*let marker;
          marker = L.marker([-17.795595,-63.196819], {
              //icon: customIcon,
          })
          .bindPopup("<p>Coordenada: </p><p>Latitud: " +"-17.795595" + "</p><p>Longitud: " +"-63.196819"+ "</p>")
          .addTo(this.map);

          var coordenada = marker.latlng;*/

          /*let animatedMarker = L.animatedMarker(marker.getLatLngs(), {
            autoStart: false,
            icon
          });

          this.map.addLayer(animatedMarker);*/
    }
  }
  limpiarCoordenada(){


    this.FormGroupBusqueda.controls['busx'].setValue('');
    this.FormGroupBusqueda.controls['buxy'].setValue('');

      if (this.resBusquedaCoordenada !=null){
        if(this.map.hasLayer(this.resBusquedaCoordenada) ) {

        //console.log(this.geoJSon);
          this.map.removeLayer(this.resBusquedaCoordenada);
        //this.map.removeLayer(marker);
        }
      }

    }

    limpiarGeografico(){
    this.FormGroupBusqueda.controls['busdm'].setValue('');
    this.FormGroupBusqueda.controls['busuv'].setValue('');
    this.FormGroupBusqueda.controls['busmz'].setValue('');
    this.FormGroupBusqueda.controls['buslt'].setValue('');
    if (this.geoJSon!=null){
      if(this.map.hasLayer(this.geoJSon) ) {

      //console.log(this.geoJSon);
        this.map.removeLayer(this.geoJSon);
      //this.map.removeLayer(marker);
      }
    }
    //
    //alert("");

    }

    irSiguienteImagen(event){

      this.stepper.next();

    }


    irSiguiente(event){




      this.ProcesoCompleto=1;
      console.log("Proceso completo: "+this.ProcesoCompleto);




      if (this.ProcesoCompleto===1){


        //alert("Click");
        this.planoSuelo.numerplano='2';
        this.planoSuelo.superficietitulo=this.miFormulario_PlanoTerreno.get('superficietitulo')?.value;
        //console.log(object);
        this.planoSuelo.superficiemensura=this.miFormulario_PlanoTerreno.get('superficiemensura')?.value;
        this.planoSuelo.zona=this.miFormulario_PlanoTerreno.get('zona')?.value;
        this.planoSuelo.manzana=this.miFormulario_PlanoTerreno.get('manzana')?.value;
        this.planoSuelo.distrito=this.miFormulario_PlanoTerreno.get('distrito')?.value;
        this.planoSuelo.lote=this.miFormulario_PlanoTerreno.get('lote')?.value;
        this.planoSuelo.tipousosuelo="0";
        //this.idtipoLineamiento= parseInt( this.miFormulario_Catastro.get('ubicacionlote')?.value);
        this.planoSuelo.uv=this.miFormulario_PlanoTerreno.get('uv')?.value;            ;
        this.planoSuelo.estadopago =0;
        this.planoSuelo.tipovia="COLECTORA";//this.miFormulario.get('tipovia')?.value;

        if(Math.floor(Math.random() * (10 + 1)) >5){
          this.planoSuelo.tipovia="COLECTORA";//this.miFormulario.get('tipovia')?.value;
        }else{
          this.planoSuelo.tipovia="NORMAL"
        }

        this.planoSuelo.idpersona=1;//this.miFormulario.get('idpersona')?.value

        this.lcnService.createPlanoSuelo(this.planoSuelo).subscribe(resp=>{
          this.respuestaPlanoSuelo=resp;
          this.idplanoSuelo=parseInt(this.respuestaPlanoSuelo.idplanosuelo);
            console.log("IDPLANOSUELO"+this.idplanoSuelo);
        });

      }
         this.GenerarQR();

      this.stepper.next();



    }

    GenerarQR()
  {

    this.qr.qrGenerar({
      "importe": 1}).subscribe((resp)=>{
    //this.urlReporte=resp;
    //let respuesta=resp;
    //console.log(resp);//
    this.respuestaQr=resp;
    //console.log("RutaReporte"+this.urlReporte.urldocumento);

    this.base64=this.respuestaQr.imagen64;
    this.idqr=this.respuestaQr.idqr

    });
    this.temporizadorDeRetraso();
  }

    /*GenerarQR()
  {
     //alert("Generando QR");
    this.qr.qrCrearSesion(
     {
      "user":"admin",
      "password":"admin123",
      "companyId":"1"
     }
   ).subscribe((result)=>{
     console.warn(result);
     this.objtoken=result;
     console.warn(this.objtoken['transactionIdentifier']);
     this.token=this.objtoken['transactionIdentifier'];
   })

   const header = [{
    attribute: "currency",
    value: "BOB"
    },
    {
      attribute: "gloss",
      value: "PRUEBA GMSCZ"
    },
    {
      attribute: "amount",
      value: "0.5"
    },
    {
      attribute: "singleUse",
      value: "true"
    },
    {
      attribute: "expirationDate",
      value: "2022-12-31"
    },
    {
      attribute: "additionalData",
      value: "Datos adicionales"
    },
    {
      attribute: "destinationAccountId",
      value: "1"
    },
    {
      attribute: "bank",
      value: "BNB"
    },
    {
      attribute: "user",
      value: "admin"
    },
    {
      attribute: "company",
      value: "1"
    }
];

const body = {
  'operation': 'VTO041',
  'header': header

};

console.warn("Datos enviados:" + body);

   this.qr.qrGenerar(body).subscribe((result)=>{
    console.warn(result);
    this.objqr=result;
    console.warn(this.objqr['message']);
    console.warn(this.objqr['status']);
    console.warn(this.objqr['responseList']);
    console.warn(this.objqr['responseList'][0]);
    console.warn(this.objqr['responseList'][0]['response']);
    console.warn(this.objqr['responseList'][0]['response'][0]);
    console.warn(this.objqr['responseList'][0]['response'][0]['identificator']);
    console.warn(this.objqr['responseList'][0]['response'][1]['identificator']);

    //this.idqr.setValue("100");
    this.idqr=this.objqr['responseList'][0]['response'][0]['identificator'];
    this.base64=this.objqr['responseList'][0]['response'][1]['identificator'];

    this.temporizadorDeRetraso()


  })
  }*/


  ConsultarQR()
  {


    this.qr.qrConsultar({
      "operation": this.idqr}).subscribe((resp)=>{
    //this.urlReporte=resp;
    //let respuesta=resp;
    console.log(resp);//
    this.respuestaConsultaQr =resp;
    //console.log("RutaReporte"+this.urlReporte.urldocumento);
    this.estadoqr.setValue(this.respuestaConsultaQr.estado);

    if (this.estadoqr.value=="PAG"){

      this.borrarAlerta();
      this.Pagar();

     }

    });
  }

  /*ConsultarQR()
  {

    console.warn("Consultar QR:" + this.idqr);
    const body = { 'operation': this.idqr};
    this.qr.qrConsultar(body).subscribe((result)=>{
     console.warn(result);
     this.objqr=result;
     console.warn(this.objqr['responseList']);
     console.warn(this.objqr['responseList'][0]);
     console.warn(this.objqr['responseList'][0]['response']);
     console.warn(this.objqr['responseList'][0]['response'][0]);
     console.warn(this.objqr['responseList'][0]['response'][0]['identificator']);
     //this.estadoqr=this.objqr['responseList'][0]['response'][0]['identificator'];
     this.estadoqr.setValue(this.objqr['responseList'][0]['response'][0]['identificator']);

     //alert(this.estadoqr.value);

     if (this.estadoqr.value=="PAG"){

      this.borrarAlerta();
      this.Pagar();

     }

   })


  }*/
  temporizadorDeRetraso() {
    //alert("Three seconds have elapsed." +this.identificadorDeTemporizador_tiempo) ;
    this.identificadorDeTemporizador_tiempo=0;
    //this.identificadorDeTemporizador = setInterval(this.funcionConRetraso,this.identificadorDeTemporizador_tiempo, 3000);
    this.identificadorDeTemporizador = setInterval(()=>{
      this.identificadorDeTemporizador_tiempo=this.identificadorDeTemporizador_tiempo+1;
      //alert(this.identificadorDeTemporizador_tiempo);

      this.ConsultarQR()
      if (this.identificadorDeTemporizador_tiempo===100 ){

        this.borrarAlerta();
      }

    }, 5000);

  }

   funcionConRetraso() {
    ////let valor=0;


    this.identificadorDeTemporizador_tiempo=this.identificadorDeTemporizador_tiempo+1;
    //alert("Three seconds have elapsed." +this.identificadorDeTemporizador_tiempo) ;
    if (this.identificadorDeTemporizador_tiempo===5 ){      //alert("Three seconds have elapsed.");
      this.borrarAlerta();
    }
  }

   borrarAlerta() {
    clearInterval(this.identificadorDeTemporizador);
  }

  Pagar(){



    let cadena,cadena1:string;

    //data:image/png;base64,


    //cadena="/9j/4AAQSkZJRgABAQAAAQABAAD/4gIoSUNDX1BST0ZJTEUAAQEAAAIYAAAAAAQwAABtbnRyUkdCIFhZWiAAAAAAAAAAAAAAAABhY3NwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAA9tYAAQAAAADTLQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAlkZXNjAAAA8AAAAHRyWFlaAAABZAAAABRnWFlaAAABeAAAABRiWFlaAAABjAAAABRyVFJDAAABoAAAAChnVFJDAAABoAAAAChiVFJDAAABoAAAACh3dHB0AAAByAAAABRjcHJ0AAAB3AAAADxtbHVjAAAAAAAAAAEAAAAMZW5VUwAAAFgAAAAcAHMAUgBHAEIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFhZWiAAAAAAAABvogAAOPUAAAOQWFlaIAAAAAAAAGKZAAC3hQAAGNpYWVogAAAAAAAAJKAAAA+EAAC2z3BhcmEAAAAAAAQAAAACZmYAAPKnAAANWQAAE9AAAApbAAAAAAAAAABYWVogAAAAAAAA9tYAAQAAAADTLW1sdWMAAAAAAAAAAQAAAAxlblVTAAAAIAAAABwARwBvAG8AZwBsAGUAIABJAG4AYwAuACAAMgAwADEANv/bAEMAAwICAgICAwICAgMDAwMEBgQEBAQECAYGBQYJCAoKCQgJCQoMDwwKCw4LCQkNEQ0ODxAQERAKDBITEhATDxAQEP/bAEMBAwMDBAMECAQECBALCQsQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEP/AABEIANsAxwMBIgACEQEDEQH/xAAdAAACAgMBAQEAAAAAAAAAAAAGBwUIAAMECQIB/8QARBAAAgEDAwMDAgQEAwYEBQUBAQIDBAURBhIhAAcxEyJBUWEIFDJxFSOBkUJSoRYzYrHB8CRy0fE0Q1OC4QkXJSZj0v/EABsBAAIDAQEBAAAAAAAAAAAAAAQFAgMGBwEA/8QAQxEAAgEDAgIHAwgIAwkAAAAAAQIDAAQRBSESMQYTIkFRYYFxkdEHFCNCUpKhwTIzQ2JygqKxFVPSFhckNUVUsuHx/9oADAMBAAIRAxEAPwCtPeLvJd9X3OorU1RfnmSPb+cr7jNUVM2MAbmOMfQADjHPQDS6t1PSLFDU6juYZhyZKuR2Bz9c4PXzULTXGkkpppC5bKoxLbt2frn9uo6NGpkMMtOXkhOG93j9sdUR6pe26hI5mAHdxH41jjDBc5doxnv2FGdu1XfppF23esYITu9Sd8MeMcZ+erX9kb3PcbW9LcdB2W6Rq4/8XUUETEMQMrllLED9/nqounTa45oqyvjf2kOoLH3HPxz5GfHjq6PauutuptIWW108VXE9MqwmaCPPrFnLKX2kHI3hT+3Un1W9Zc9e/wB4/GjNOsbVjgRL7hRNcrRYiFNwtWm6PdxspLbC7/sVCgj989Q1RBoVHSlorHbaqqVMmJyiFyWYDChASeDxuHjprUfa+3l0kuG/3EkHB3eSASCTjx1G6l7A6Pu0hqzDNFVtEI0qIHYSABmP1wcZOQfjJGMEEU6nqH1bh/vt8a1SaZZEgPCv3R8KEKHQV6vFUltXQdooZmQylWpKfKxjAJxlm4JGPrz1O3HsbPpq2LU3S2WucyNh9lGuUOfGWUYz9uPIzkgHXpaC79qbnUUZuFfd1kEaxyVUoZkhTO0ITwV9xyPJIJ8bQHTaNWUV+sjTyiJ1Ee5oY0J3EDIx9cYxz9z568/xbUlXtTv95vjRMukaZEwMcS/dHwpd6d7L2CrhWqu2nrTSh19sJpkaTOPnI4/byPnBz193fsVoB4PSFhCyckMpEXPz+nGQPoOmJbZpquCKqqYJaVZl9UKy5bB8BgPBHz0P6qvuj6eaVK+8QQTUiB5czktEpJw2P8Pg8nPXw1fUT+3f7zfGqZbLT0/ZJj+EfClZX9urLa7ha7LbKGwxeu2+SouEY3OC6RosZEUhLFm8MEHBJkXrXdu8+pKNa+22jX2qKM28pQxFbo1LT0hJUBjT7FEewOMBpGXIAKsPMjTXq51XcairNJpFqE11pq6CKOGaEb4ZADI675I13Kq85xncv0I6X98r5dO6JvHc7VNQlTUrWVFNUWeHAamnhnkiKtIG/wALI3uOFPBzjBJBu5rpB18jNjxJOPeaDjSCIkwIq+wAf2p6as7p0vaHT9dURd3r7c7PQj06CvrbxLXTV5dDKNswYiVt0jKDgALGBgAZFY9J/iR72DWdVrrUWutUfk3bZRWie7V0VCFdcLOzbvSyp9wSTajNlWZAAOp7TFX/ALd0VNijcUctOld7UDOCH/UDwSeAoKnwePGejXRlsslykrXp7dTVNTHI9MvoVAZoVL4LMg4B3KFA+pH16D61ojkUdHGXGTyo9ou4131JW/wLRevNW+pHRUq3MVl0qlqaeUgM8hSV8qso2lXiJTG4q3uVepKifutR3K2XGfujqBzQUslKYamrqJIahWRUEkwDRKZAE3bhk72z/iI6jLXu0pQVdbHaZLg52xilpIVEk8ruVVAXIGGZudxA3Z589bv4h3hqZWlpu0UwpBAi0y1F2oFkDDglmWZ8JgLtRF4xzngCsfOLo8SbVahitD2yM1D1lx1jo63Xeqpu5mua1mQysK3UlZMYmUe1U3SYXI5IX5Pk/Hn7r/8AER+ISw6julNZu/8A3Qqqb1mMZbVVcmxfkACXAGTgft16D1v/AO5EbOa7QOjVVoWEUdy1ZOgSTIO+QxUMg454zg56rBrj8KustTaiqdSXHuJ2rglrpDOaOG9MkajcDgZpw2Avt8DLY+vR1tbXCg8e9K9RuY5WzGQKr9bPxP8A4mEjJl79dzJGGP16orm/c/73pjWX8Rv4idlt1JN3f7gy0UddCkgfUFayOM52lfVwxIB4JxgHqGpOxl1qr1NDDftKtFSVDU5nmklSMHk7gwjO9M58KWOCBg46tr2B7Vacmsb2SKy1Tx2+X1Dc3iiWKpn27d0aLIzhWw5UOgYLjIViw6+uI5LYB376DsYjcy5zsKsXB+MXQYp6RaOHUFxeqDCNlakhRmTG8CSeoRSVyAUyGz4621H4uYKlquCw6KuFfV0m4PQQ18VTVuyoG2rHRioC/qTl2Vfdy3BIX4tF7o7/AE1FAtGtpgZnk/kjJJBw30VskHPHjnz0uNRfiUtmktdJZLa1EtrjdEnnkKkoWOTtQsMnO3k55HS8yuBkGnc5ihxk03Lv+MnXFPbp6hu31ttUlNSS1c6V9xr98AXJ2SAUKoJMchRI24fpL+eq+ax76dwO69NQa/pO4mr9OxVNMCtDZL9XUdOuGYZKpKMtyQc/QeCODPv7PV3bS1VLWVdDFQ6ls1NW26GQZniSVOPWXAO4FccZwWbnk9VV7d3+e0aLNju9M8k0c80e31sKVC4PPzzzn5/v0PLcS8GUNPdHtrWaZetO1Fvdjvn3701oiKayd5daLGssSyt/tBVfmHcZGfUL7wuG5AOCQMg4HWdLvV2rbdrWzR2RbYiUrBSrGZt3B3Dkft1nUY3k4e0aJvhZrORGNqRVTXVtLbJauWaJmimC7U8ecZP26+VrmlC3KQlHlG19pG04+3/Xos7j9v62kq2g0vNDW26R1C+idzrk+0uCOB4/oc9dPbvstrO/XWCljt38Seo3KKWkjeWTx5CgHPGTgHxnjpqxRedcz6pQOFedDlruNJW1y0j1qhdv6S2BjPPu+PvjBwM54wbidgu9mkdJWI2IGCqFMCXqIxh3ZmzlG8lfGD9jwPHUVpz8EesrRC18pe3tZuqgDKGnTeMeQFYqwXI4DDJ+c8dZafw0XHTl+SOa1y29J2MjIcA5LcjOSDyc4HPVMhHIVfbpJCwKrVmqfu9U3K3LXUFFEscnuVnB3H98dEdl7g0V1hhoqlESpdc7cex/cR8/PHSvvVoi0bpimq6md4pJE9lOQRtAH/fnpa2bu9DLqOipY4VV9wMau3D/AMxh+r48dU53xT17tYiFkO9We1xZ7fV0SCYqKwESoh/UCM5x/fz0DaT1ILDcJIq2o9OFjiQlipUlsjA8f+56Op70KoR3GuihiYwKgL7WxkcqD/bqmf4ktWX5tWRWnTu8Rwv6s2ZgFPkAHndwM458sD8de5QKWdgAOeTiiEtrrUJlgsI2kkbYKoLE+wDJo47v/i/lkqJ6DSjz0lPGoUylgDIfO4sOQDkdVjvXdjUV2rpalbrVO87h5CQSOfkHPI64xZqVJN93uJmYAZiGP9fr/p1orLvQWgf/AMdZ4w3xIy/9n/XoH5+JRi1jL+f6K+8/kDWj/wB3Mtn2uk1/FafuZM0w9sceeH+Z1PjT2/DH3QGmu41k1BqW5vQ08VQfXq5GZNsbKUY+CRkP8c8dZ3KveiLter7Zqaue42a4XGtraeVUytNI9TLKjRkqGGd+fnz46RkGqHYCWrlEaYz/AC1BOR8YOeuKk1u1W808Zb2sVWNgFwo+f36+D6i3IIvvPwqaW3QGzXqy95OR3gQxA+wHrTj271Yvtf3A0ro6nFNX1dbJ6qeixlMkiRLnJK5ycnHkAdSnbDWmhtGapvM9FfBRUFdO1RB+YM8+TjBy4jSQEk7gCWCsoO7nhL6P7n0lippKu8UsEtOgYmNoxIHJAxuP0yOuCbXi3W4RXKegp1R3D+nHCERk5xgAAY8dVP8A4gpzxKfQ/Gmaah0EKBGt7pR4iWIn3GIUztW94tXv3tjFi1PcmsP8QgEDQPKICuFDEMpBQ53HJI5P1HVirdHT3SBP4i71jAAq9S5kbwOcsSf69UV1Hqr1NdRw2pEhtlQ8BMEajgEYcDPI56sFb9D3K0gVGjNcV9umHuWnqATG2TxuK8H+qHp7ZXGoW0eXtw48Vbf3MBn30svujnQ/WGzY6nJbv3CeLKk/xxM2B5lKc1Tpyxy7itroW3Da/wD4dCSPvx1H1GkqKV1lhgSJozuHpR44HPJHnkDz9B0F2ruvr3RUn/8Ad9HQ3ahHElZQHY+P8xAyv9Cqfv0c6b7w6N1G5a0zESNE26nlwkoP/l+eD5GR9+nNrrNpct1OSkn2WHC348/TNZHW/k71zRIPn5VZ7YftoWEsfqV3T+cKaFdV6RpIYpamnLowDMm0Mu34OMfUZz1YHsXRLNpW33uWsqo2WiSljpDKFp1KkqWWPIDFsfqOTz0r7lcBVxyJJRsyFGICs3IKnI4B/wAw6qn3hortLeKymt99uyLRw+r6JgkmiSUAbTkA+krbmwSqrlDznPV+qR/OIAW7qz2kXJWVlWnb37/FNerXfrpYNMPQCjppnp3kEQLtGTtYEOD/AIty8fAHVUdT6tWevFwg2xyM5YpgEY8c/Xj69C0t0lqqSKSJZJipG5pCuSM5JPtGfnxkffOehy7VFZJVuVcsFbHjHOOekFxahEHDU79XlfnTNoO5dbU1Y3VchV02hs4wcYP/AL9dNHdq2BXpYK4BJWO31FLKufphgR/y6UdNMyTpECF4GRnkffpq6VsdwrkWqpKepkwAqlEJ3fuc9ArCAcGj9OnaIhia/ZbddWyjXucJgYWONEUD7Yz/AKnrOpLUSLpyaGC811LDUSpuWBpQrgc+Rz/frOrjCRtTYnrDxZprdj+3dl79094uFFc5rRc6ZUegduUIB2usg/xqTtHjPHtZPPVqewtbojQMUXby8adptM6nqNxNYX9SK7upyrRy4ywC5/8ADkhlw2N/uJqF2z0P3C7Z0z3iys9VQK6zU8tDMyzoHXcSI1GcruwyKzYOdu79PTqTu7bNeWKSxajFLPUtH6kc0o2xyyquU3OMmLDhcsoOAPAwWDFrdXwRWfhuF4s99XUn7i6ctrvQ188VLUbd6RF8yTg/p2452nIGSTx1k2lbBqa2y3aogLVdSoWFi/qLGoJ2lR4XknnG76ngYoR2sk1lqiOquevLjfKWs0/E0kbG2y1E0u3LRIxjLs6cO3qJu3ooPLZ3Ww7Ydx6LT+lKZ57ymomngBpmohEv5iVmwUUBsMzSMqheMM4QgbXcCdT26cxyhl3qv34pblU0F8msMklUZqVChkJ4Jx8DwR9+qu2uWOnutrE1SEBxl3yoT+c+SR84BJ4+mPkdXU/F3HZ75oSLWBeKCqjqvRjAZd0yEHB3fOcHAHODyAc9UdrqtKVoTKrSMqhFZ/JQucn6Z5Pj6dKtQuhBKIoxxOeQ/M+Ao3Quh02uO+qajMLexiIDykZy3dHGu3HIfAbKO0xAxl3t3O1hqCmNg0WpWONQKivmPsTj/CCOP35JxwB0ou5LVOkL+lJU1k1bNNE0lTUSoC5kznKhslhgEZPyR0ztES23U1bRaN0/MHrqkrBHTQTKpeQuuE9zKoYttG4k8E56me8vbSlksddLLEI7zb5VjVWRGkZcSbkJHHkHx8qD19a6aqv194eNu7wHsH5netzddMha2jab0ZjNrb8iQfppPOSQb7/YXCDOMGqo0ZZ5JJYgxVB75G4Eh8Aj+mOue5JUVKKEmw3Ht+MdTN+pnM0WynalhEce9GbPxnGF8ec/161elRVEyCFXGzIwfpjpjI68W1cyeYLJg99CdzrRRhEhnR9rgNj5PW1YoZJWlEQy4DZB6gLosZvckCo8arIcbvA+/wB+pClrJopCmx5HzgOq8H746t4ABkb17KoBylTE1Z6lI8MfCp7TEvBb746+7dWzPGsGDmLAVnfCj/0/5dRlS4mb1G/lT59uVPu/sR102xZ5auJgVdRhXURHaTnweeePr0M2eeKoKOd6/b0Zqe9UFc8oQB1XcrbvDA8Y8j3deg2haSy3TStvvIq6tZJVQyBqbblim72BuCv6QDvzljxx1576yqkkuNNEY0X0ASoQDYMkYGMYHgdXh7b3eSt7T2KthikSMQxRepEoBUxopIznOMfA6O+cyQICKcWFqLtMSUxks9trbrtpY6uSEjdwEhLLnG7DOfv446GNW9jdNX6Waot9DUWqsD7oqqBkA3A+WXdhv3GD9+ibRVTFJWFmkeRZN231JBtyX3H4yOM8Ho4kTfE7oxxg5G/I+vHVU0sWoR9XcKGHn+Xh6U30y6v+jN2LvS52ifxU7EeDDkw8mBFV7or7qbthWfkO6dkN4tEymCnvFNIWaEnhS+P1eP0thvJBbGOpTXPa3uL3O0+tRoC822bScyKEoaKpkcVBzy7AL7iCRuDnIGRgEdOB7XQVtNLRXCiiqoJ1MUkUq7ldfkEHgjoDOi9f9gppe53aYzXPTAm3XvTsrl1EYHLqOSVAP6wCyeTuXdhbLdXWmrwljJF57sv+ofiPOtXBpWidOZC0caWmoHlw9mCY+GOUUh7sdhj3KTVOu5nbO6aduz2eWnInpF9N4zEYgj/qYANyEG4j6Zz0qb3bp2rZKeGnkkkZz/MDADHnOP69ep1m0B2m/ELWTd1qCseWKZQlRbwm2WCpIPE+DxjPxw3nkHoP7mfg6srNUXjSa0u2pSQyCdeKYbPaUIxlcrjactk58dMotQjmQE7g+FcuvtEu7C4ktrpCkiEhlbIII7jXmnQWqWWZ546KpkWAH3rGWAP7jj+/HTM0hqKupaWJYJZYomRXxwfIBHTz7M9oqu9dq9U1lrsc0k1PKwmf1UCiMxq+0KzhiB/MxhWbAGcnpC0dtloYlWOMiNAI4wCcBV4HB5HA+RnqD8Jag7aDtcLVO6meLVtOlLcaeGaRcF5fQRHOPA3KAf8AXrOiHTtHZZqUtdKsQMcEu5AUf/nOB/XrOvjIe4UyKpH2c1y6c7vdxu3F0YyNDe7LPNiSEKygeSGGQecHAbOCQ2fI6bVJZNPd1KdtS6QP8JvDESPA6Eeq/wDlkjGOf/8ARcH5GeB0iKCmoNYVclkhoXl/OVeymf8AlsrRqMurZ/SAOduMZA6NJu1HcrRhivehbnM70srSCAS4EgUsCg/wrtAA58gDq2K6YMFIrMCElsJTi7e93tYdpmn0nfaAxLLGy/lpoxsC/pMtPIfa4GOcY5/UFY8TloZjbL93X0ffYtOXGxSLU1NNUyN6NVvVkJkjPPqlWYLKhEgMhXLjHS30j3z0d3BiTRHdu3fwq8x4VKqfMMcknAJWQ8o/uABBCngEjgHTT2T/AGh10vbyl1FNV2OmqcVVT/u3nEalirEHYWARlDYC+3ODgZ+1W5W0QdWMu2yjxPwHM+VbboZ0efX79kun6u1hXjmk+yg8PF2PZQbkseWAai+5HczUncqpS4X6aeOkgDT0lGSWDu3DyDACnlccAABQMZ3EqrUlzpqCmpGrpE4i5O4nj1W5GfPkD+p6uTr+k07Q6V/L01ppgyQCGjRYMsR+kBeAQo+P2yfnNN9a6Fu99ip6p7dLFIKcmPejKgJkY4HgZA+PPS6G0S2HHI3E53Y+J+A7hR/S3Xm124jgt4+qtIRwxRDki55nxdubsd2PkBU/ovVUlk1Q18sElPTEMrGopuHZhklQxARQeeSxJABxkA9NPvp3pstkoKe5UEs9ylukCzq1QgjeT4MpC8HLqeBjyfjpTaP7R19LYR6tWonqsSRvGQHAU54wSF+nwQPsetn4kNNpTUOnq6IVksjU4iqJKidZ3M4zufegC4bllAAwrLn3Ano9XjdgtKYUKoSKVdNqiuvEktVVqY3lcy4VcAZPjjnjkY6m4HYiOWNYxu9xbJBz0I22RYkWmgaVyAWkViF+fP1PGOpyirdsIBhYjJP+YjoS6jXi2pJdJ28miVNNWS5V9JW3SjkqI1kjaVKchHkQMCyBiDjIyM44JB58HVU9q7xXXGprbektJbJ9wt7VC/zJADjDAHhseT+nOccdEmnJK1dM3LUNthLfkad3Evpe0NtyAT9iASPHRtoi61uoNDUlyr23TCZldo1C5G4jGFAAABxjqUEzhcGmlhAHTLVVi/Wqvob/AD22qkY/lZTEjFc7iBnGf9enP267XzUppr1elUQVCetEqsN2DjYxA+x6YujO3dmuF9vOq7pZLc9ttUqQVLsRJM0zQqy7YmcZU8KWCkKSM4z1MVV0s1DWyK9J6cYCijRKdEaOMLjB2synBBHz+nq+R+IcON6YCJFXNV/772qkoNVxyUAxDLTofkjILAk55GCB4+vVvezEdJP2L05XflkhIbCyPCpxtiKlhuByCwccfTqvHfnS1wvlTb7tYaCtrA8LQNHSUErOgBJJJAZSDu/f29DVg7sd0dP22n0pVXKejoLW5VaRo2V425zw4z5YnI45+3XuFlj4TXol+bbjlV5tNVaLd2lNQpjMjMiBowEzHz7QoJ5OeTno2a5QBGWSRVIJA+Dx9s+OvOyXufrusqf5OppKfd53RKcj78Zz11U+oO79dBVyyXmpjhgQyLL6yRxYPJYtjOcbjt/4fv1KKCNRjNQe8689mr8pf7XBIxqblTQBP8TtwAP+x0xu3d6tdfaJhbrrS1y+rudo+FQED4JyeVP189eTlDr6rudJHLebndZpnznFSUUD9gRk5Geenf2x7utVNZ7JQhoI7dMh3FyXZnIySTznjqu5RQwCneoW98YpOA1Y7uJpC+di9Xt3r7S0Qe0SkHUNjjXbC0JOTJGBnCDzkDKNyBsLAOTT9xvHd+jodc9u9UUMlhq6P0JKKqpneSmmB/mbwoP8wDCYzxnIyGBOnSVwhulhhaokjcTIwl9Qq2/LEHOTzn79Ju0XOq/Cl3ULBJD211rKFljcFkttR5B+2z6+THnyyDpA4OnS8S/qyd/3Se8eR7/fXVuFen+n/NZP+YQr9G3fPGoyY28ZFG6HmwBU9xqQ7T3Gk0hYu7enpKmktklEBTQQVNQFEwSOaEOASCTJ6SSHjP8AN6pVTxwVNmmrYQsiRTzZ2ceXbjx8Dn+nVufxo2nt1WaKuGp7HT2QahmrqWUz00kTTzgsFJfaQSAmcEg/p6pFqt7tZtNtHY7vWRiWpdo6dKuJtrMxLbwYgyjwcZxzgdOpQWwBXKLW2jOWc8qB9eavrYp3sdPLGI2AMq+TkHOM44wfp1nQJcTd6mvYXQSCrOS/qKVOM+eeecdZ0ySI8I2pJO3G5Ir2q0Z2p7XWWgp5LRoWwxN6QdJxQpI+DzkO4Lf1znr77gQ2Om09XAWmFl9Pc6rCqbhz+kKACef7kdbLv3U7a6SoxSXLV1LF6cSqilHyqjgfH26UWqe//bq9YtFov35qasYU0TiNtgdmAGTnK+fIBAx9x0vmc81o4GADPfVF7/SX3UPcSvrblUs1FHVTNMkowSqOREm0cYwBg+cA/U5sf2pp7XLpuShNH6xlO6q3ReqjFhwHIztXG0Dggnzjz0KU/wDB67XFbcNRmnW326V0lenXfHNMSwXBwNwLBmGR4UDpmWKt0/V6JutpsKztW0dY9VSO8SCkp6U+kgAAOfULtISWGCrbdwChelVrnUZmuJDsvZX8z6nb2Cum6rGOiui2+gR/rZgs9x45YZhjPkiHiI+0+e6pK4adt18SCKauenni90QjqSrvuTaSdpy+QxGT8EcDrXQ9sbdVQz3H+G/n4rVCZ6qpDeutLH/Mb1Hc5Kqdp5HJ5+nUfcNHxR3nUqCjvl/raW3JJQXeiAanocq7SSytvjSJCo4KxuoZT+ryZ3tpV6s0Tp+4JBaqOnob3YZoq4Vt3jIp4lf/AH8WyNlnlOWK0x9MrvYMfGGHzRO9qxckiucha2Wyz2H0ARKGWXMagIrKfOCxDAkAfb4HznoU7rdsjftPhrpR1MKxI7w5VkZH55CtjLefIIzn6ddtrvlgNsrKe4VFSJljl9COkiYF58n0xuZdjKXxubIBBIznpqd2aiprLLa4a+ezO8VJHC5tVY1TCdoYZMjIjbzjJBUEFsZPk/RRBDxA71NXJXBWvMCWmalr6iKSJDJll5c4wpwu0+c/UeM9dtPdJkqVCoUGNpxjzjop7lWGa36xuAgoXZJpCUYwDbyoPtAxwMgHHJyT0K00T002Km3TSSR5LhFySftjhvPx9Prnq64Gd81nruNy+QKdXb7VivZKjTtdSt/DamB0nSPjcSMbj/38dT2kbtZqjRTmzUb0dHSVMvt3mQyKrckYycnjjHz0V9nOzlHc9Ff7R3I1ETVtPIsURjP8rjAZlznnnjr67d6QpdPWI22oho3gilBAp1ZVmkCqCx3EkH6gY+OidF06bVbgW8PtJ7gPGq7y/XRLI3Fzy7h3k+FBtj1Tqyr1TX2K10FXT6cuao89RJSvGrSLFwdzoDjKoCBwdq+T0Vaz7I3G4XGPWOn74amKKijhdIva84AILKfjDMeDycdNK3W3UFdSTVNotVfPS0gzO9HRSyRQDGf5jRoVTj/MR1FyVdUsUooKhoJZBu/lFds3zyCCpP0bHW0vOhjdWWs5eKReYPf7uVZqw6cjrQl7Dwxt3ju89+fpSiqGj0J2+vF1krpK2VmJi/OKY2UuOB4zwcL5/wAXVbaj8/WH847v6k8nvkZ8DcTgEnyRwBx09daWnuprGCutc97NRbTN/K9aojVnC4w5VIgM/b7dcujuzlNZ6OZrnUw1Ek8QjcyoZfRHyYzuRVYgYyynHkc9YUKqfrNjW+uI1nQdWdqTFMtSm3bNJuMmDhiT8c/XHzz9em/pLSms71p+qp7dZBVQzKYWmkqkjHuGD4YHPI5xx/Xrg1t23s1LdILnT3OG20dHAsAjhhcyzFSSzM7ykM/I5xt5GBx1s/iFhuMlLS1dzrIfyiCnKwTIo2jJBdU9pJOCSfPA8DqlmUHI3oWFUt2w3OhOm7WVtuM0N7sF1ow07RxVT07RJO5PAjLqBIxzkYyWUp9emVpfsnqGqoZbjTOtCLZB6lTOZmRYlHBkZsEKMYYZBOQRk4PT60nruWjtNra23yasitLRR0QkhgYQMqiJH3Pg71jYqGGWAI+gxzPp2eO4VV2tGoLhbBcJlmrYBPH6M4XAAK7vcMDHI55yOefnmU4Kiio7JGfj51Gdv+1PcCW70K1epfWjjnjleKneZ12Z5G5j9z5A8dNn8RNjqNTadeS8Vtup7Hb1apeRVKz00ixMFY5Y+oOTwqjwcn6R1HraC2fnKo122Sqb/wARJJIpMrfLeTnzz1zVOr7Heaae11c8NTDUApJGwLRyKfK4PGP/AM9CE9YGWUbHatLYNcWMqXVuSroQykcwQcg1XLRS0mpKmnsFQWkMqMkcYpDJGynncSFwrZIGXI8+c9Sl+7AwUBrtWX3UtZLRemxkhjhjUIWJwpywVvceCCcY6INSwaV0FryyalsFrpYrSG/L1dNHCojjOCC6qBgHacjHyh+vTSuGurRgQU2z5TBkO05PgKOg7C4l3gO5Q49o7j7v7UZ8oWnRLPFrlmvDFeqXwOSyg4lQeQftD91hiqia0/DzTX/TdLrXS90mrK9ykNZalLNNABlTIZDw2SF4GQN3WdWynviRRJNNbq1UQ4ZoqBuDj6s2COfp1nWgW/IGK5qkGBvWqO01kpWqp9F0MczYKzNaUjmK+32g7VIHDff3+fGFp3+g1bFZbEauOWnSG5BxJJIrOvsbAUjLKPnHAyo+nVsI9A6sqJxGaBwOdx/zHcuTzxyRn9uk7+KLt/X2LTOnbZJRFa653oRUytICXOx14/rIo/r1i3v7yJWZu4f/ACujdFuiNjq2tWlk5yruvF/CN3/pBpC6etl9FFHcaChjq3E6zrFIUcO2QeYw4ZuATzjxxnJHVne3umHqNNUlfdrTTU9dVQD85FHCVG4kkgoQCuQR7SP9Om7287Q0Fl0lZqKpiolq6ahigmdEyDIqgNz9SR/Xx0WLpq3QYWeo244C+kQf22+cdPLZBBAsQ7gK+1m9k1rV7nU5T+sdiPIZ7I9FwPSlFS6Uipa+WupVngaWL0ZUilaNJkznbIoIEi55wwI616p0k1405UWqMinadG95GVGSc5X5HJz+5z02Limm7YoM1TI5LKhUSwFuSBuIyMADk/boJ15ftCR0MQpNd0lAPVCyejcKXcYy+1wC4bBAzkDyRg56vXtHApe6KBSboO28bJ+RnuUE0bZcSxZw/OQcngjx/YfTrsvFKIrU6vUyTuuMtIST5P1/6dM7RVLpC92CC4WR2rKFyyQyTurMEDEYZY8KpyCRgDKlSeSSRrVdqYwVIgiwqnbhseAfjHxjrxgeLLd1UBewc0mW0Vp/Vt4o7ZfaFZImqI9pB2yAFgDhhhhkE/PX13C/DdovTNyob7bqw26gaVT/AD5dsVOwTIACgkl2OM/Xon0bQXR9R3EyWiWrio5KRhVrNEi03L5VkYb5d5AxsPtIBb2kgsPu9rGn0JpcTq9E1VUn0oFqY/VUsFLA7AwL4CnGCPPUznah2WLhLnurRoqgFTpCmqKWGSSF4y0cklNsLRgYyOcnP16SdBMKaEwPG0Rgq545FYYO4Ock/wBMdOztbr+n1fp8Crak/O0qn1hEvpAjOP0EttAyo854PS71tpkQ6Zs/cGlVlh1HU10M8W4MsNRDUyomCPh0TPPyB1sugd0lvqJjfk4x691Y35QLRr3Slnh+oc48uR91W5/D73j7RWPtLQWq66ts1krbYsorqWtqUhllcuzeoqsQZQwIwVz/AJfjHVQ9cXu03jWl2uVghMFvq7hPPSxldpETOSDtP6c8tj43Y6B0uc6LtSZwB4Abx1Aa41S+m9K111iRpKl0MNOgPLSNxn9uRz8Z66VDo0OiNNfu5IIJ37u/1rls+rTa2kFgqAEEAY7+6lncNV3q6ayuVPYrjVQ0yVTxJiEOo5xxyPHP+vU9dK7WtqsVTdZaqqnEUe8RNQxDA55OX4GMc4PQX2Nu1FcNdWuy3eDe9YJpKlahB+vaxAGfOT89Xbt3a7SN0k9SpsVJggbVUYC/tt8H9uuKXH08zSH6xJ95rs9kksMKxLuAAPcKoRFLrXXtsrbmaG61kUMyItRBTsyLuyUUlAQTlWO3glQx8KSNtLp/VompZP8AZ2uUsgSfcuFKAcOpByec/Tj4HXo1bu2GnLQGNr07RwPKu2V46RQzr9GYruI+xOOsrO2NLMplFkZznhkp9xPB4/QSf6ZPHVHAvLFFNbdawLVQ+NdSU9bbLNT3Gb8tWyLHWqgxsjJwCS2cjOfHPHT3vOrtBWDS1Ta7fGsV0paEtApmlcbwAq4yxGckHH0B6FO/unKnQ9bLcqm0PbI5KlY6OF5Yy7KwGJFRSdo5PDAMPoOOlRBX1VwphU19Qss9T72aM5UnwT+/HVDwsTmnlsBGoFS9u1XqG725JrlVkSuzFmc4AO74wp4xjHPjHU9QXY08iSTaigDFduEaQ84J8cY8fT56G4Kf1KIsGHBwB8+B18RtDSKJtoLL+rPBI8n/AEB/v1XMnGeEU5S4Ea70zdaaotE2j7XZqFoKg1cTSSuJA0kUqSkDIHIyB8/foHhqbjVSwzG5VpendZEAlZgWB8YPGOOlvomsqrhfK6Zap6SmSp9R0U8yZBwp+cA548dMK3bjIqNI4yy5C8Z6oSAWl7Gx+uCp9o3H50fcTnWuiN7b/WtZI5l8eFz1Ug9mTGfTNMSp7s6znCQSNEVRQGBpsnP3YED48Y6zoVmh2xFigc5BOeCfPz1nTLqlzXKUuCBXrPKxklKA8sf0g5/0+n9eqq/imvlBV97+1VkqKkVENpqpLpUxwkMf97G20qD5xTnz9T46+u4X4iNUW+UWO5t/DaSINHUNC2+d8AjczIFBb2lyFAHOMdJuC+WHUnejRV7tUpmoaq2TVJbB9Qt/4lSG5zuyg/06SajEOoUeLKPxFdZ+Ts9Xqs10DvFb3DD2iFh+dWDuPcLUl0rBJZjLa4I8iOb1Vd+fghgYz9uCR8H6arbpe63Ctju9fdrqtTTtueV6uY+QRyN/IO7z48dTqW4PbIZLUo9aVeGkALqPv8Z/bqQoK+PSdEaq/VaBVyr44L5/1OCMgfb7YJ0eTtisyzYG1Q09ss9SkljmWmkil2h4jJlsjH1yR+kZ5+o8dC2tO3Ogqi2vp+ot0NHJUxCJJ6VAXiBJy2Tx/wC/TMittBJVmtjhCzli+5Vx555+v1z85z9uvq4UVFXMpraaJ2Hgn/v7dXqCDmqixIxSl7QWuLtVU3jREVe1xp4nirIJps72jkTCqT9vSI/p0TS3W0XnStRdqGppmqZZ5IZ6ZJAXpyryoBID43+m7Jjyoz1BVkcsPcq/mIM+aGiYDIC//P4XP7+MdBGnNS/mbre9H09PQw2+gqZD6Ykj/Mhx7y5ZVDuu2SQZckLkBcE9QfIY5qAcquKmYoqJauOrVYWlwdsyqCwLYBKMeVyOMjGRx0GfivueoZq63U9NaGNniQPHWPKNrTFWYoE8cKq/Hzjr61FforDbqi41ckcS0iNIQW4Hzgf6YH3HVcNbd9NQ9y6sUl1RYqahRTRxJjMW1VBwR+oHAPuz+rqcMZYcR5UDqRVYSAd6fn4Y6uWht12jq5wHJCsxYBVHuyPceccdfmr9Q6nt2kK/S9TXQ1lptlTLV2+IQBfSLTvIzCQcniQn3EgfA6Avw23OnuV+MFfR745ISsnq+/DEnj6eSem/etP0r3W4aeqaVViniKvCjYAjwB98HwBnA5POeD7bzPbyB05g7VVbKtzZ9XJuOR9lLK6U9VQ3B6KpjKTrIY5E+kgOCB9vkfYjoE7kX1KO2S1i05rIaZTGkakZB/z+7g88/Xx0ZV1F6dvnfTcP5qNZ5aQvDhlWoXG8DHg4cZB+WH7Droe31ken/KaoWOcqqs0WDkHAycfHOR/TrovSTpUNQ02C3jOC27+m2PU71gejnRJrDUpppBlVOE9d8+g29tKX8OsP8d7s6Xramkp5mFxigMoZSqM4YhShOSDg+OOOvV3TtHb6OBRFSQIF5GFjX+vHXlwLDftCdyKKXR9NUPS1FfHNHJTxMjw7doJDL48lvPjPV+6Gu1fcbNQC33qrmrxTpPWxQTyMzDGXBVGUryThh9s+esL1ik4FdHit5IF3G1OaSvgBO5o1OMeRyOuOqraYReqzR7RxvJyAfgD7k4H9R1VTXvc2ssNFUV1beblvt0cEdbJDf6uCFZJSAgjiaT1CSSMgnjI56BKT8TmmKCsjrUrbhLWw5WKasq6qdVfaRnZJIw884OR4PX3CQc1ONxIcAZrh/HhWpUVscTK6SUlSiYJO9cqCQfoc8f06rJZD6ttglAclF2gZJ+T0edwdW6i7n3kUjxBY5qr/AOJqJgQVJIEjEjIAPOTnPz0L2qSntizUSFJVhnli9VSGV9jlCQfkEqT1TI7YwKujV0ftDArfQyypUBXQgEHAfz9evqrjVkcSMDkMMD+3XxUTfzhOS2VfjAzxxjrAXZHZDlyMAN/f/p1WpJwWouQsRkUC6BcRXi7xFv8AGSAecHe3/r0zLfIvroyzFMYw3nkY+vSz0qyx6su0fsGWkOOc59T/AND0cmtFFRmrELSmMg7BwWPHVepAfQMOYkX8cj86ddEj1seqQHk1pN/RwuPcVo5qJdgWXcCMDGQOs6D4tZxVKLDNbqmIjjLDKj5+Oes6MIwa5OKs5rSjo9e6Pj1PbrcstcxH5sJIBtVfleP1E4Gfn2n/ABdKnsUKfTveGxUlzhkpqSkqKlIxI3/yXhl2D7fqxj+/Oenx3TsI7Ma7/OU9PPDp69iWrSFpV2IoHMQAGF2q2ec8BCMeiekx3Egi0pqq0aijl/PYqPWkb1NrzphGBYHAjZlY8A4Bz9D0m1TsW4b7LKf6hXVPkzPWa69n/mwzp6mF8f2q1U+vJaxVpbLSqiMu2KQoVY/QkjOB9to/fqDv+iNSalpKBG1IstQK2nqqlmhIgWOKQPsUkljnH2B+nU1pFrfcGt1xttGtbQVcMdUtQQMPG6hlYcDGQRx8HI+OjW4TR8SxyRUkKMG5wqADxnwMfYZ/botGYClDCNuVcEt2o7HBAtwnZ5HHnLHcfk8fU56647rRtCJ8oARkblb/AJ9QN9vtFHOryQmqyoZWVdoAzjcMjBH3+3XFVa7tcNFGWp9zVBIEXqplF+pGf3/t1cqyEZoZmReVQmqlePX4rqVwTcLQWOBt2tFIFByD9Js8DPt6X1rgs1s1XeqemucdVcapJJZjsBljUxqv+8ADFeVOwn9QU/Trst2r6fWfdmvpo1YQ2m3R05Crui3tKrEeovggNCWAOcHrtsfb661WvdRaiFPWyUKrBiRPQMC1DrszK24SqSjnaFUrkgtgopEZOMnevARjJoO1bZ4r/Yaq2zxM5nAycZIIGVxn4yM4+vVR9UaFrNIXJo9QpNSOp3IkQDesmWC5IYbScYHGPb+3V2ayn90mGXETZBDE/HJY+H/px1r7kHt9a9D2286m07RV9dUK9FSrJBuMzFnOT/wrtBJbOBnGCevIZyv0bUNe2vGOLupWfhUsE1Rd/wCJU7g00UbZdo2j9UD9J2tyPpn5Kk9WBNfbpKvXlnud3qFqWpoGtdCJ321UjRSGQFAQpI2pgtnHxjnpB9nu7Gn9KVdTQUGmlgFVMoAD5ZVXgMw+AcHHzxz0fa5uEP8AHpbhgI7Q7gzcEAjnJH068DFW2qu16vgwnrX73U1VY7rpTTVssVHNFNYLRFQVavCFaWeONASvkMfa+WPyR0h9X6g19W6RmutlWelnglQVcA2sY4vSd2Zsglsbfefjb1E0Gv7hDZIqQ1amVlfa+0ZQEk5IxzznqOtN+SirZZbzqkmee3VdOiJDlJfUTYFbLAZIZgcKT7s9Es4ZcsKtMkMUyiM0U2e7vU2+2TOJVZoAGyMEMq4yfvkefp0y9K9zNcdtqw3DStthrFmppoZTNC0u0uytkopGR7fH/oOk/o+4SVVro6OU7pYIzG5LAk88c/sfnB6dFptUVVQwyTxoxkRHXwxAIznjkeRz0FESr8QrRalOrW4RRzoJ7f6noLr3Dutb3VqoI7fdlnllpqqmllatmYlo1UZLIVcqwLH/AA4+3URPQWUzSx0lIkELs22NlGVByMeB/px1Ldy9APPdrFebXQtV1K14iqDSRB5Y4ApYs74BChvA55P9vy3aFulQQTS1MefA9F8DnyOPjA48cjqV7PI4xGKhoAsoTwTHeomuNBDEqmWCEEEKWKqAfgjJznpY06CSsrlhl3oayQAhhglju8jj56aerdO3Ckt6plvUikGInfbhxxsOVHOQQf26/dF2mk/2Tq4dV223T22oqvVR3mZJHKgA5ZCGAU7QApHkk56qtX7HbO9F64g60CIdmgWKmgjpWmlq48MQAxYbRj4B+vWz1Lbu/wDj6dnzlV9QbjxjkYGc5+Ombc9K9p5qSd7XoSmmdtn5eSsiLMuc4Db3yQcg5Vh/TobodK2q3MKmngtUMpkDQyJbFkEOGyCoM/OORznx46k3Ee0DWfacr2U3pQabFng7mTwXe4CioZpCk1SWwqLgEkDBOdwxg/Xo61nPpingMmiLlFcaIIg9aZ5MNJu9w4XORwOBjj9+mPYbVFY665utmsldUSuspqpxCpw55YJK6HdyDtQP4HUEKe7VWpK8fxhYqiD2NPHBEgYZHAXbtHj4APHnz0Jf3e8K/vqfdk1uehumlbfU7pv+1kX1kKoP/KlRFcb7FDG1NQ0xkkAAxDUSHGMknhecg8fTP26zp22zVOpdC3Spq1rFvdPJTgRpVxGRFk3jOwKu0Ng/POAes6PN1xnIrnS6OsYwy716Q93e3dm7g6HrdLLsFSGFVRVk0eQlQuSp/wCGM8qwXGUZx89UQ7i0i3TtzNa6+nkpbxpOoWNqaXmVYAxjMbY4JTcORhRhm59Qdej9fOzhW2ExFd0WPAyf/wAdU0/Fxpy1ae1FR6lpCT/HoJaW40xTbGwVAvq7vAYDbwOfYreEYGu4g+dQND3kEetEaBqv+z2s2mrDlFIrN5rntD1XI9ag/wAP2tdQU/bmkscdtrK2qpZpIKeFIiz+icOrk+MEu2OfH08dOaz2bVFzjje6zR2pI8kiJszkn43Yyv7A9KX8J2t4amwVvb+4yBaqzzNLACMloHbLYHklZM/0cfTqwG+RwshJJxnGcgft9upWU3XwJKOZG/tGx/GtB0m0caJrFzZKeyrHh80btIR7VINbLVZ7Nbacq9to6ipLbnqZYUeZz93I3f69SEt+q6ZCKX0kzk42/P2x46iQ7eQAPjr8mkcqBuxx8cHz0XxsKRdWpO9JWrtsene7kNElTWmmulBXXGRJX3bqlpoMt9cKF4HxuPRLp9aWlqdSU0l9MazvDWwW5KLK1s8ZYRkTH9DRuqkjjOR5yeh/uk/5LuLpOtVmQvSXGnLL85RDj/Tpc6n7q1un9c2yzNTQtTXWbFRNMDkICPA8D7EDO7HPJ6oeRwc18sSlc5o0ujrLNOAAQjBR9fkAk/XHz/fnqU1Z23s3cPthFQXpZS1HvlhkilaN1cZPlCCQd3IPB+Rx1EyxiOeb1D/u2wTnIz9B/wB+c9MjS5jm0lNHtBwHQBjhRwDnJ6FBZnya9c8ULA1S/Q2ltNad7i0V2vNTNBSiRIXWZiUmds+mMBSzHec+cc89d/4j9Z3KO/SaW03FJLVPS5nWIF3CgnK4AUDK593UXeb9YU1lTvNRS1K0NYtQsSyYcOhGCCPj2+PnoQ7wanuV51xU6lpI0SlrqQxwxRPtckYzu+rAnOBwQPp0fbgO+KzcEvArIDvS8muyyhIXqH9SMKrsgAG7HwcePH9c9Rl1NQ0rVAcmMDa2fPWqmq4FiERqNjxDYAx3qwUkbl+gJB4+Dn46+K2sggdXarjeN1yTk53fTHjq90KncVWkbh+Km12vjr6emhuUmyeBnDMrkk7fJwOr+dmNGWW76Jtt3rbbBJLh4yUXAwkjIBj7BV589eVtn1TWGqp6WCvniiB9yiQrhs8Bfjn78dev/ZqOnh0JbpaaX1I5IVqdxG3a0qrIy4+zOwx/fnPVXUcB3760IuutQI3dXDZuyF+uGp5btdNW1q2+mqlqEtsFQqQPSg7ijqYy5yQAcN4Jxg46a9gtXa/UsNT/AAGw2iT0VUSOtvVCN4OMMVB3cZI8+Pp0he6/fvXGhNYTaT0/RW54ZaSPE0lNJJKvqsQWQiVQT7Rj24GOc8dNnsXos6Pa/U8l4nrTXPDMY6gg+m4LlyOSeSwOfv0V1IRc+NDqVJyBiqmd+LDVQahkRfykFuQgtHLAwDlSdxMiupycfA+pJ6GdK6a09cYaOpqa6nqILc0plVJ0G5/btDncAqn3Zx9OgL8dV5p6nu9U0YLCOji2HawL59aYgIefgjIPx56Tds1RW2TQNro6GV91xr6nJRmJCoI+PBxlnXkEeT0sksxE3Gxp9DqDXirbY35VdOm0tpqgNMZ6G2TpISsbxx+AefcQcY4ADccn7dRurNFW2+1SC0CGkgihzmOIrvz4+Qc4+nPPPUaLhVzUMdQ7lmVVAO48ggZyPnrh13ZbjqHT9LJbLbNW3FKuJUEMMckixkn1CGkBA4J/rjpet51jlafXXRtNPt+uHPnW+5W6026BnFTtNPGu6eI5EpCgEnI84Hx0u9JQ/mXqbrMzA1EznlsZ+f8AmT0Q69krrPppLdcLa9HVyN+XEQKFU+WX2nA+eFGOehmkrqC3UEdMVmdoVBYKmRuPJGQT856XXKtNdhPsjPqeX4ZrY2k0Wm9E3mOzXUiqP4Iu0x++VHpU25CxCONSGU8tnOes646a6x1cRkpqWVyuB6IG1sn/ABHI/ThSOD5I6zo5YWxWKluLfi516PaV7lWW/VIsUqTUxmd4aaUggNMrEMuGGfCjB8HOB7ioMX3k7ZR9xtIVthn9KKVR+YpamYeyGoUZTA8sDkjj4O0/q6rVfu6tx0tdhQyLbkekkSWb0KlJtjk5KmRWYBgy8gEHxx46mbr+LLUWo2zTvToikRqVhBA+vDA/TyOr4p3Rh1grESWi3IITlVYbLf752t7jCsRZoa22VBhnil9pZQdkkZIxnGCM+DhX8MOr4aXv1t1PZKS/WuoL0tbEJYiRhgPGCPrxz1UnvNpeq1rp6Hu9a4zNVx1Ho3pioBUMUWFwB4GHSNuOf5bHiPog/Cb3Famucug7jOxhrA1RQ+pn2SgZdOfG4Dx9R9SczjdLe5MY/Qk7S+TfWHrzHrW/mz0h6OxXw3uLICKUd5iyepk/l3jbmf0SdqtWMY5Xr8lA2A4+P+vX0pwDvRl+eef9R1h/nIFjXe5GAqHcQST5HHTHntWQBBpK991liumj62GURMlwlhLsONrwPx/dR/bpNa/ls51NYq6tjjrKhZW9CQSFUUBScY5DEkL8ecdPXvOlJWTaWo3nhScXiGb05gVUxFGy3POCG9v9Oh+m0LYL1qOlirq1bNFRI9TR1zSCIRzBlATcyspyjuArAFsLg56gpCydqhHkI2Xxrlqqt4jUOxTadzMF5A92c58H55+nSl7r98Z/9l6rQen7h+XeWQfmKhSck4GUGOV8efv11fiR1LctNW+msFNVRUpq3EM8sRKE4XBKA87PIz9fnz1VCsr7hXVUkklSCQ3DKx/mLnySPP3z8g9Qkj4Tmhb25MS8K0TNUTGqLidGfChnLDJKjA/9x56LtGU1l1ZeLfZ9VUQqqCaceqqlo2GThtrKQVyD5BBzjpZwTbtymMMwGOBgf389Emmby9HX0oRm3xuroM5HB5XJ6r4mB7NZmOQq+TTO1x+HDSlo1Tdqamq61YoJ5BDHmNAsQLbF4GSQgUEnkkEnJyelfrft1ZrBo6evpI2klnmjjTMm5ixyTwR9ATx9OrL/AJ6t1FRVeoxQTVUiJG9X6EUzmFZWIR2ZYmjUeThnVjj2g89Kfu/HG/buZYm3RwzBwQf14JUDyceSfrxz89GRceQWNaReFkGKrZRU7QVIE0LCX9QVwoLA4xgZz4x4Hnr2M/D5XUd17cWU00/qGGjgjnO1h/M9Nd36gCcjac/Oc5znrxrp2b1fWV8AqRjOD9uf2A/v16ofh01/YrJ27tq3G6U0J/I0ki7iSzFYVTG0eeEXo2XLkACvY1w2Safd/wC0WiO4bwSX2hIrlbdHW0u2OoRUyQm/bnbk+M4P06nbD2e07YqiSonr7tdlMe307nVLNHkEYIAAweq86s7z61g1RHJobULG2ERsY5YhGpdSGcAFST4HO7nd446b9L+JDSdJbYTc4bh6kMKrLMojw7gYzgPnBPUSGzg1evC2ymqGf/qCaPtFg7lU9VQ05U1wlWRQxdVZWTaVB4XAkPjH36HPw5mjqO1tTSVlKk35e7TyKJBnbmKLx9Oiv8dmvrfq662fU9rjdYpamoWKOVAGyFhzuIJ/ynn/ANOlX2G1nQ27S1wpq6hu1W9ZWvMgttE1QFJVQVO3xnA+Pj79L9VyYuzWj6NGJLwNLyFP22GOWlEaxqqY4AHx1Da5uNfbrOHoiPZKrMPf4APPtOT+2MdcVHrujpYUo00XrN3XCl/4FUf/APP3z1xa3vVPf9K+jNp660X5ipWOmjrqf0ZJnGCcRMN+0Bh7vBJA6yikWv00nKuoSwN0gddPsRl3IA8s958gNye4Ur9cdwqsUNFLVuRL6jx0yIu07mYn1Np5zgLkH5wOmrFTpVxQz08PppUKJAPT2ef+f79JXv3o6o0ZetP2i5KBXrF6lVGScRM5U4GODtVlBx9Sfp1d7td2qtd90TY7r/EKKQVNDGd4qkRSyqFJC4zwQRz9Om9rYmaHrWOHY5Pl4D0G1YPpR0mtf8RFhZ5a2tVEUZ+1jd5PbI5LezFJmO3VVNG3+9ZPJA/SOR8f9/HWdWil7PWWhhNRLUUkyLhXSCqDyc4wduQcZ+3WdEpYFRgvWXn16NnysdVK19ZW0x3I1PpqNzKkVX6yHGVYEFgecj/GvP26jrRbbjNSyYb2OSof1+QfkAf1+Pr08e+mg3p9caf1w9JHLar+tNb6tQXDb8kHJB4JOzbj/KeojvPoOxWO2Wm62eh/KQbDGqxk7mK4OW584z/2B1YY1cZpZa3TQN1Z76muy88NTSXrRl7SOSjvFE8M/qOF3IylCOfghtpA5Oft1WW7RX3tp3HqLM0sq1tsqzJTz4IZip3K5+zrtf8A+4j46Y1rr62WkpapW3cAI/yGBIy30wG/59MjvPpq2ay7NLqwWuI6gtcaVJrUQbmgUh3BI9xxtG36EkeHPQjQJOvUOSPA+B7jWg0nV5+jV+upWwDjBV0P6MkbbOjeTD3HB5iuaz98+6mtKGKpt01rt8SnZI1NGokLD/AvqHDSEY2xrzjPwM9FlLT6guC0r3q+1l04zIk8jek5LEn+UTgeMeMjOMnya49rNY6g0tXtqnTkclTRGEtdaWI8+mjqJTgeNpZTn4EmORu6ud28tmktc2Sl1PaNQm4U864fYoQrLncyOCS6MCeRn6EZBySbW4JJguNpV5+BH2h5H8ORpj0h0OKKNNY0UmSxl/RP1o274pPBl7jydcMCd6X960TQ3aldfy0UHqMZlSnQIUOc5VRj9OR9sDA8dcUWpaTQk/8AF9YWimrpaFAscdZTh43cuArhWBG8cMjYyDkDHViaWwWegJamooxIP8TZZh54BbJA5PSO/FLpc3fT0VbDOsSs0dJM7MTsYuDBJj6LLsDHIwrMfjo08JINZVIuAcRqk/4itQ0d/wBapWmr9aOWL1SG8oTkj7lvcCc/9OlHR1MU0LSwExtu9oP+XzyPGejrvTpCpsl8gaYyzTzRJJU5YENIR7gm0AAYx7c5A5JOelzDUQyNLTyo0QbBjCDc2fucgH+3VjoGGRSu6Uuc0Qh3IVkhIJbOQx56mbXGZqhN8LqAwJYcY+vQtTzzU5Cb9yR//LkBVv8An0T6WrGqqqOPaY3eRVUF8+Tg/wDMdBNldwKThBxgGrPdpr7X6asF7tklvjmjv1LTK1ST/MjFOr4/uX6UP+0M2uNK6voJKBIY7apkjyxYuPdgefPj+/TmtCxPQU/qlFLRbTyFOcnIB+fJ4x8dJmroLHpWpuWn6VqqpN3jdpFWblsKpVEKr7WYMRz/AOnR9nC9zsTWhHBGgIpOaO7e601nJN/slpyruaUrJ6skaLhWbJXILL5Ct48Y6uboTQmsrHpS10FfY5Ypo6aNZEeeM+mQoyp92ODxn7dA/wCDCjv0CX+qqqOY2qZVEbO38sTKRvGCMggMMnx9On9ee83bKhD2ybUm6qpJXgkp6ekmkZGDEY4Xb5BPH16Ille2OMVYsJuB2RQDf5bjpiP8xc9kKuSFLSBucHgEE9R0mor5XxO9utdTVo2SGipWfH7Mo48eOpHUstu7wxLbdKy1vrwyGRVqIPy+5cFSRvU5G4oCceWX79TvZy0i109+s8kUn/hbmYgZ8M+Ai/2ycnjHnoZb1pmq9rLqFBO1Ux72V1+uOo1mu9kr7fTr7acVaMjMobllVvAJY+OrB/gc3S6R1NTnD+nWQkF8tgFG8DwPA6DPxj0oW+2hhGVAhmYDz/iT6/8ALos/A/erTadI62rrzVx0dFRvSSSSyHAGRKPjkk4AAHJPA6o1UAwFs4pn0bhZ7wRqCxJwB3kmntqautek7fPcbjIkUcaZZsYJ+AAPknwOgvtXYazuBqo90tTqsdLTSbLTTyH2rsJIf7hT4+rknjHQ/Wvde8mpFr3iqINM0spFNFKdrVJHknB/fJB4HAOST05aGaGhoaOioxD6UMexFijCBABjH/TrBNx3JDZ7A5eZ8fhXbL64h6JWkljER89lGJCP2SH6gP22+v8AZG3PJrZU9udK6q1lXV9507T3d4oo3Rp5CgjLqApA2kncsTeMfpHnrqGnNXWpPyWmLjabZblyYKeOn3sqsc4LNtyfuAOu2yXCpo7rXUtbNSgVdJTNApx+ZbaZM5f5VfZgHwWb6nr6qbukyOKSty5YMcn3Mfkk/J+M/brV2r/Rjh51wG/tz84yOVao7RriUr+Z123jmP8AKRMgP7Z6zrUbjVRys7b8H9PuO3/T56zowCQ1QyxA4JqX7t0C3rszWVMdQ6VGnq5KuLJICEH/AHhA/UBvc4zxg9Aet6ujv3bOG4XSKQKAlRFuUxrITldi4OSSTtPPGCeiy9alks9/oLVe6ikGmbzT1NHcIap40ilJQlSxdlzgjbtBPtkc7cgHpVa+ulv1VFQ6feFKK0W12ejpaFjHEpIbkEHPO8nzjkfQdVmVYFDVbFp0moPwxUtaDUP8poYI1WND6gTAYhjhWVsfsR/T69OfQevIZbFPQXenikpMGOMgbU2nIYHHIHIyfoCBychdw2Sz0Ct6FBGruOT6algfr1y6UsEel1mipa+qkilkeRBIf92WznB8/J/v0LJeowyBitBD0UuBtK21BCLW9uu501u0eJq6iM6PAppi0StgBqedCcbNjtFklQyvE5YZwHPfJ6TsxdKXW3aLWlJWNW0sUt4sT07xwzgj/fIoG1FblgoYFN2FJU7Qpe7VHUPYaya1sWKyQ1tR6ZYYSLcrE7fAxKGJ+dgz46gK/VT19DaJKmPKJT78Bjuibe2UB/4TlTj5B6Plt49QhVjsw5EcwfL8xS+w1u96EXzpBiSGQYeNxlHHgw8RzVhhlPI1au2fi0pNRejS2qwCCtdMypVVOFjI8kMFIKjn/iPGASQvR9qbtnqvuRoiOoqdXl6G7xRutNS0AWEhwMBwWZnyG8KwGOD1R6BKC7wxz0UNTHUb2Y1S7jlvbhT+3Jz556tF2q/Eb3C7eaPpIu5ui7hetKRZpYNQW9N0kAGBslzhTgFQNxQ4/wA3HQYupbM8F6Oz9sDb1H1f7Vqxomm9Kx1/RmTEh520hAkB8InOFlXwGz45qTSgm0vdL0Lno3Vluhqau3sIRUxQgrOrA7JViOSjHB3KfDK2MDA6Sfdb8O95s1bGdK2mpqkMY9YBN8glx+o5IIBwT54OB1bKu13pG496IdaaF1Ba1t1WKaqqZKzMMULmR1b1lO11VWaNmI+Cccdd+pTSV2qKiVqyjqYaiRs1EbD8vIXAP8r5cZYgfPt8dHm8jUAxnIPeNxWRu9LuLWRoLqMo45qwII9CM15uxSNQVEkFfGX9D2bcYw44If5BB4PPTD7fadq79cEqaOnQwx7WmlwNoGCQF5+oHVkO4P4fNKahmDw0v5KoXHp7FCj6srj5Y7skEA/Ofgi2oO3dFpetsWnbXRyRxuzFyscir6gKry+CoP6vaxwM8/atbuK4PCtKJdLCN1hovsVrs1NrSI3m21M609mb8mEhZ0jqmlXk4YbcgnliR9ugy8WejtlgvVTPp2Oe52wVk8FSSWaRWdWQsuOdqqSrZxmQg446b89LFR0UtRIlOiUpWIgzwM+CpO9olkMioOPeUCkg4PGOlRqfW2lb3TXejpEneelRnInUGGbaMYVgTxxkD6ryBgjotGeLsg1N0Thop/BpldE3WnwfUSvbeCpRl3RoMcgEjH9/I8g9RfempvNJc6impwViFTvLLwx5Jxnz89aPwd6jo0tuphca+khxVRvuZ9mQfO0eMDGPsAB0Ud2EivV2drUY6uKoQyCVGypA/wApHx/r1Tfy4TJO9O9BjVnoY0a+odK2emv1trqo1FYjK59QYRC2HTPkEggjPBIX6DqH7f8AdTuE9wv1Vpun0/P+brC7S3OplErsqImSka4LDYc4OMk8cddUN1oNPW2SC4hpKqQFRGzezbt4PHyCf9OhPTDVtvP8N03H6clTMzB2O0bmHJH38n7/AE6QDUkt+Zy3gNzXRrXoVca0BKF4YRzkbsoP5jt6DJ8qDu6+p9Z9z9ai13mjokNoLRSvR7xAd2D+phn6eOeOjr8OWitLX65XOy1V3lntlnSOuucUcwjSokBKou4nAA3NnGSAWAIJJCY7sakv1sv9fpmOZEjiIE7pkSSlkBOT8Dn+vz9Ol/ar5cLfDLDSV09KtQfckbMu/wA+QOCOfHTMWdzrEebk8Kdyj+7H8ht45rP3XSbSegszW/R7MtydmuGGOEHYiFTy85G7XPhC869Ftcd2e1mlbTNFBX2OKppYdtPTxgsFZVG1MqfaP246TVq/FmtDSCsq7G8kk0bZgjZPTV8+3aMbiP69VVq7jPJG0btJuY87xgnrYtVP+QhUH2bicYPTC20OBFCuc4rn970hmncyA7n31dPtl+IFu4utp6y/0kNBRUltkijSJCCSzx5zyeeP+H9+jOXvJoS0TNHbIqm6VQYKSzOVJ/xe2mScj9m2/wBOqr9h6ijprhWVEtHawrUgBNeZEp5JFlRvcFwXx/lXn6jo5re6VntM1d+Y1fFSrOQiQWKnjgEm0gFV3kSqAT9BxnoxLSGHas5NqdzO23KnRVd4dbz0K3O26ZNvoWOFqKqKKmHnja8ssm7PjBjU/YdZ1WO96mut0Z7XZdF3arrIHZXq6n1WPDHONxwfBHAx1nVwji8apLTNuauP+JO0PW9uK6rhUmWikiqgB8qjZYf1UsP69KXTF1TUFgoK5CrhFMXLhQpQkfP2A6YdFpmr0fpq3WrujryumnuUgWdK28ioyxb3RSEyMIzygVGAY7zxwenNpDtNo6z0woV0pboqVJMJmkT9Q9pIbbg52+R1nrqLEYVga2uiauYZjJGtVyMNfNE88O70lOJJREdqj/zH6f8AXqHq6v05PT9bcPH6zz1cRtHWiuSawzUsP5OZhHIjRqy7Cyg4GMffn4B6on3TsFXo3uDe7GLhUfk6smvoRDUEiFWLN6SEHhUb1EI/ypHjyMjxaelzsNqez9MXtRmVc1JrqGKy6ptVPVUUdbS3j1rXNTzMypKJomUKSCM5J8dKiOkuFsqX0tUQyyS0BkRZNy7gDNKQzj43YB4/+p0Y2TQ+tNX1cEdDbLxXIjCohqF3lY3DAExyN7VYYOMHPnHVx+zfYbROjtM1Oru59jtmoNSXyL2Q11JFWSwxDBVcur+7aQSc8NszxkdN4E+ZrwNWL1C/XWJutQYqitt1dWWGllooc7XlVsqT7cDBIH3OOmJpf8SWuLNpyp0rQagNLR1kmZ4P5YZ88Y/QW2kAZ2sM/OerRx9vO3j3GVm7U6ZT0MwotRbabADKmJCojDuNyPgkg+4nnOF7Kztz27orC7L280fHXCVZIo4bNT8n2eGdWx5cYwMbfnI68knThJIqhUK9nNVT0Tpi069vNabnLPQOKSWr9SijVNrryf5eMYxn2jBJ6YOl9DfiMoaGO6Wu3vWUrqTT0l5CLJVQAZEq7m90ZyQp384yB4PR5PHpu01TxWjS1qpqhmbfIlJGjfq/T/LVMjGM53efjnOys1tfYVig/irrFSU6U1LDE2xKaNBhEiGPaoHAHAAUAdJzplvNl0yhPepx/wCvwrb2Hyi65ZItpeOt1EOSzqJAPYx7a+jCgms7k9wLP/L1p2nuUZUDdUUof08Yzge1l8H/ADcDqPqO+ej5QVa1XOjmSL0tphQbP+Ecn28Dg/AHnyWTR3q41tPmW6VLH0jsBlYnLBjyDkeQG+OR1peC1VNbLcaiioql33bpmh9TK4AwWOSftngeehY9Lu4H4kl94z/bFN5+mXRm7X/jNK4Se+KdlHorrIPxpZVvfTTt10PUaEuFqlkpjcDcI55PeSSgTZtI4G1F+/npVNcLKGuVHHTRC31835pYYaUK/q7Aq5II9o54GMhjnPHVorBRX3TNouc0Nq0LURy1Mtca652CCpaGLJG0NMpGxURc4ON25h+rJ5dVQ6wns1PLq2x6UtlJX7aq11VqsEFFLKij3OrQqHeMpKP1e05G3ODhkINQ/wA5R/Kf9VL5OkfQtf8Apkze24AHvEOapno+xfwK4VVKlUK+eQqUgGUZQM53IGycg856YkX+1dZSpSirhoKaKMlI4yF9v0GM8/uei8WDTK3M3KbTtJLXM2DUflIvXcYHuLjLe45PPOAOpN7QtW38u0rD6rYVWiw7H5xxyT0LJpk10fppifIYH4D400tPlJ0/TY+HSdKijPixaVvaC54f6aDrVomlf056yKad3Ab+ccAn54Hx48noitVktsdW7Sw0ayUv8yNnRR6ZBOME/v46I37d327GBIbrHQiLIeP8p62FdTH7wXTG0uGGWA3KoIPjob1F2o7d6AWO66tvM8xKPDGlazMHKgZABYiTA4JYeQThc5atdJhtjw5waB1H5RdS1pg14S+OWTsPYBsPQCkP3j7faKe8V98XXNNHcaht5ow6vk7QMDYCRwPnpKVcVHR0ixLVNJIkoZZI4n9y/OdwBznHI456uFXXDRUMy09L2x0sKdXy0D2iN1fAxljkOScf/UC/bqBagtVzE60ejrDSwlgzxU9tp40U549oyMAD6knptBeparwk5rJ3mk3OqydcFAzVaqWw32704uVPaZXgY+2olkCxLzyHYnjwQB0ZaH7S3zVit69xoKS30wZ3l9fAGMnbuPC+3JyQc4GOm3qbtjeEs73Kjttlkp2ImNNTUgUBMcOFIxnznAz0kO40OqNCXo0lmv8AcKenlij9f8tVSJCHGcqVBHjOPp9OmEF+lztGd6S32kzacuZ1NMWh7T6PhstbXW29y3+JisLpGyq8rYU7Y18yDJ+QuScDx1uotK6zrJ46Swdv5bfp2mmcQ1FXAsXrFhwyNOrZXgM23hchc5bpGUN/1Jb5Y7ratQ3CnnWYOrw1Tq6P/mBByD9+jSfu93a1BNSyah7pawuklP8Ayqf87e6mf04yylkXe52qSEJHAOAfjr2aXgGTSZJeBguOdWzoqKGzWWhjrquFaxIUjlKFXXKjHtYjJHnz5JPWdSX4epLTfLSlTqSKnuUrwh3esQTMoJ4OXB8nrOlZuwTyNPDYSnfIFPfVOhr3V6Q0tSU9po1rNGVM9ND6gZlnpXaTB9NWG5ctBnDKSHl8sEHRpTXOChkiiqK5qiVUCn0lLNuUY85KjJDMFzwGHJzxJUC+rHSvJJIxlrEgYeo2PTPJUDOAM/Ax0I0cjXKpqKmsw8sbRlWCheWznO3Gf69e3JaQDJ2oqHERIWtMvcS1XmoqbbaKuigq3AQtcC485H6FHu/owx8+R0iNddsdRa6oNL3SytG1VAVqJXkp1pYUjkSPKqwLScsgPuz/AE56cGqYaejWqhpqWnRWRwR6Kk/pz5Iz0M2+sqpFakaok9CArFHGGIVFG3AAHjqMDmInFQucTLwtRPZrpQ0B9Gant9CyowSBGMzZAICEjAA4XkDPUpedRyXhY4SaiVYFAUMNsQwAFAA5KjAJycnoFmijp6mKWGNVc7vdjn9R6m6d2SLKsRuxnnqMkjytkmqIIlj/AEa7JZ6hG9aapC+iowgPtQHwB9gAB5+Ooa56ppIm9Yzq7RZxnPuJ6571PMFbEh5IB+46CLrLIlTKFOB7T4+eerQudzXzHhFfF91FRUUdXf6qVmgp1Mj7DkjPwMeTx4HJ4x0FXvuXa6TSM2sdjzUzDbFGTgyMWCjOcYx+48j6jqI7poqaZuMCFljmkpg6hiAd0yZ/bwOpbQtltM+grRQVFvhlpmqXhMUi7lKCSUgEH/yj+w6IhwNiKCfL71BaM7x3rUt+noJbXUUVuiiMySUVvqJp9ykBVIM233e5AxGAW3eASGNdLlqcU9qlWSnh9KSGWuE9WkSTKkis8THBchlypUYwMe74MVqyqnsWhrVVWiT8rLPUSK7RgZK+nJwPoOBwPp0ntQ3e5CGWb825cnJJweT56skOSMVJYi6gE09NWa1tN0nW4VNRQWmFZBJ+SstMxpv0bHVNxQAP7nJwQZJZH2qWI6EKjuDZKRdtFTzSlUCE1E2TIBwvPngYH9PHSmpJpaiON55Xct5yxx/bx1PSxRLbw4jXdxzgZ6rdyGxTCO3UjtVO1XcW8Vnsp2NPESAFj9pA/cjLdGHarWd2tN5eohqPfOPT9RgjMBkEg70fzjGQVI+/wpQArkj5xn/Xoi0azC5QkMeHJ89F6Zw/OgHGRQ98OGL6PanzatC9xu4VfXW2xagWnWKjqa7KLhwAwBUZx6hPq4Ax4BPkL0q/xP6T1VT9ktHT6hpUa5WsmC4oXDshaFkXDg4ILKBuzzvUk/PT37YVtbT1P5qmraiGbZw8UrIwzNEhwQRj2SOv7Mw+epr8XGnLHbO22q7Jb7ZBT0NqtE8tFAgwsDU7N6JX/wAvppj/AMoB6L1tYjOOBcUHpTu0ZLnODVOI4KmotdFc2gzHNTRyGTJHLDIyD9ST/r9OuqyVzg+hFGVhkUtyB8HBwfnk9TvbNVqu2dLWVA3zRTKiOfKqCAAPtgDoOQmW+1MknuZY5WBPwQWAP+p6x80PNQa6FbX+yHHKnNpisSe20gTLMIpEbcQwKjcpGP3B/t9x1VDXbrN3AvdsuFPIaVKyeJUkB/mKrNkr9eQw/cHq4nb2ipl01WERn+XUTxp7jhRhSMc8EEkg+Rk4I6iO71mtM8ly9W3U5aNXKuIwHysjxglhyfZGg5PwT5JJlpds0Lk551DpJdx3kKkLgiqV3jQdpelar0rcKp9pHqUMgHqox5GPt0RaZ0PcJrM9ZLZZZFpjEcoQWwzgSlsH/Cm5v/tOOt+oKWCgqqWejVopJJcOyuckDx89OntE7S6SatfBnqKmSGV8frRUYqCPHB6ZzuEQGs1ZWMF5MocbmiDsvJJLTfl7ZGFikUhMeMAA4P8AbP8AX7jrOtugWNsvVZSUH8mGMllRfALKrH/Vj1nQiHi3FRu4JIJmjDbCv//Z";
    cadena=JSON.parse(localStorage.getItem('imagenPlanoSuelo1'));


    cadena1=cadena.substring(22, cadena.length-1);
    console.log("Cadena: "+cadena1);

    //cadena1=""


    this.lcnService.ObtenerReportePdfPlanoUsoTerreno({
        "idplanosuelo": this.idplanoSuelo ,
        "imagen64": cadena1}).subscribe((resp)=>{
      this.urlReporte=resp;
      console.log(resp);
      //this.urlReporte=resp;
      console.log("RutaReporte"+this.urlReporte.urldocumento);
      this.goToLink(this.urlReporte.urldocumento);
    });

    this.borrarAlerta();
    //alert("El pago ha sido realizado");

    Swal.fire({
      position:'center',
      icon:'success',
      title: 'El pago ha sido realizado',
      showConfirmButton: false,
      timer: 2000
    });
    

     /* this.lcnService.ObtenerReportePdfJurisdiccion({
        "idlineamiento": 1
        //"idlineamiento": this.actualizarEstado.idlineamiento
        }).subscribe((resp)=>{
      this.urlReporte=resp;
      console.log(resp);
      //this.urlReporte=resp;
      console.log("RutaReporte"+this.urlReporte.urldocumento);
      this.goToLink(this.urlReporte.urldocumento);
    });*/
  }

  goToLink(url: string){ window.open(url, "_blank"); }

  reset(){
    //alert("aaa");
     //this.router.navigate(['dashboard/jurisdiccion/jurisdiccion-ubicacion']);
     location.reload();
  };


  onMapReady1(map) {

    this.map_export =map;// Map.map('map_export').setView([-17.7876515, -63.1825049], 16);


        /*L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(this.map_export);*/

        /*this.map_export.attributionControl= false;
        this.map_export.zoomControl=  false;
        this.map_export.fadeAnimation=  false;
        this.map_export.zoomAnimation= false;*/




       /* const tileLayer = Map.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
          detectRetina: true,
          attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
          maxZoom: 19,
        }).addTo(this.map_export);*/

        setTimeout(() => {
          this.map_export = map;
          map.invalidateSize();
        }, 0);



        //let mapElement:any = document.getElementById('map_export');

        //document.getElementById("map_export").style.display = "none";


  }


  imagen(lat:number,lng:number){

    //this.toDataURL("a");
      //console.log(lat);
    //this.CargarMap1(lat,lng);

    let doc:String='4714079';
    lat= -17.7504019;
    lng = -63.1046423;
    //-17.7504019,-63.1046423
    //this.CargarMap2(lat,lng);
    this.CargarMap1(doc,lat,lng);
    //this.getMapaWithAsync(lat,lng);
    //this.getMapaWithAsync(lat,lng);


  }

  /*async getMapaWithAsync(lat,lng) {
    const value = <any>await this.CargarMap1(lat,lng);
    console.log('Image64:'+ value);
    this.resolveAfter2Seconds(20,value);
  }*/

  resolveAfter2Seconds(image64,mapElement) {
    return new Promise(resolve => {
      setTimeout(() => {

          htmlToImage.toPng(mapElement , { quality: 0.95 })
        .then((dataUrl) => {
           console.log("Imagen_64"+dataUrl);
           image64=dataUrl;// this prints only data:,
           localStorage.setItem('imagenPlanoSuelo1', JSON.stringify(dataUrl) );
            return image64;
        });
      }, 5000);

    });
  }

  getValueWithPromise(mapElement) {
    this.resolveAfter2Seconds(2,mapElement).then(value => {
      console.log(`promise result: ${value}`);


    }).catch(function (error) {
      console.error('oops, something went wrong!', error);
    });
    ;
    console.log('I will not wait until promise is resolved');
  }


  CargarMap2(lat:number,lng:number) {
    const width = 200;
    const height = 200;
    this.ContadorMapa++;

    if (this.marker_export !=null){
      if(this.map_export.hasLayer(this.marker_export) ) {

      //console.log(this.geoJSon);
        this.map_export.removeLayer(this.marker_export);
      //this.map.removeLayer(marker);
      }
    }

    this.map_export.setView([lat, lng], 18);
    var markerLin;
    this.marker_export=Map.marker([lat, lng]).addTo( this.map_export);


    Map.circle([lat, lng], {
      color: "red",
      fillColor: "#f03",
      fillOpacity: 0.1,
      radius: 50
    }).addTo(this.map_export);






      var x = document.getElementById("map_export");
      this.getValueWithPromise(x);



  }



}
