import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlanosueloRoutingModule } from './planosuelo-routing.module';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { SharedModule } from 'src/app/shared/shared.module';
import { IniciarTramiteComponent } from './iniciar-tramite/iniciar-tramite.component';


@NgModule({
  declarations: [    
    ...PlanosueloRoutingModule.components, IniciarTramiteComponent
  ],
  imports: [
    CommonModule,
    PlanosueloRoutingModule,    
    SharedModule,
    LeafletModule
  ]

  
})
export class PlanosueloModule { }
