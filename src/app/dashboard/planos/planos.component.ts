import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { finalize } from 'rxjs/operators';


import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/core/services/auth.service';
import { LoadingBackdropService } from 'src/app/core/services/loading-backdrop.service';
import { DashboardService } from '../dashboard.service';
import { LcnPlanoSuelo } from '../interfaces/dashboard.interfaces';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-planos',
  templateUrl: './planos.component.html',
  styles: [
    `
    table {
      width: 100%;
    }
    `
  ]
})
export class PlanosComponent implements OnInit {
  
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator!: MatPaginator;

  idpersona: number;

  dataSource = new MatTableDataSource<LcnPlanoSuelo>();
  displayedColumns: string[] = [
    'idplanosuelo',
    'numerplano',
    'superficietitulo',
    'superficiemensura',
    'zona',
    'uv',
    'manzana'
  ];

  constructor(
    private authService: AuthService,
    private loadingBackdropService: LoadingBackdropService,
    private dashboardService: DashboardService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.idpersona =this.authService.getidPersona();
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.loadPlanos();
  }

  loadPlanos(){
  
    this.loadingBackdropService.show();

    this.dashboardService
      .getPlanos(this.idpersona)
      .pipe(finalize(() => this.loadingBackdropService.hide()))
      .subscribe((data) => {
        this.dataSource.data = data.planos;
      });
  }

  onLineamientoDetailNavigate(plano: LcnPlanoSuelo) {
    this.router.navigate([plano.idplanosuelo], { relativeTo: this.route });
  }

}
