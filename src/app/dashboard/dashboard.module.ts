import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';
import { DashboarRoutingModule } from './dashboard-routing.module';
import { DashboardLayoutModule } from './layouts/layouts.module';
import { LineamientosComponent } from './lineamientos/lineamientos.component';
import { PlanosComponent } from './planos/planos.component';
import { CambiarPasswordComponent } from './cambiar-password/cambiar-password.component';
import { PerfilFotosComponent } from './perfil-fotos/perfil-fotos.component';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';

@NgModule({
  imports: [    
    SharedModule,
    DashboardLayoutModule,
    DashboarRoutingModule,
    NgxDropzoneModule,
    LeafletModule,
  ],
  declarations: [
    ...DashboarRoutingModule.components,
    LineamientosComponent,
    PlanosComponent,
    CambiarPasswordComponent,
    PerfilFotosComponent,
    
  ]
})
export class DashboardModule { }
