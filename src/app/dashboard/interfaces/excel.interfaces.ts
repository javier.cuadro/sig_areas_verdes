export class ProrrateoUrb {
    idprorrateo_urb_cerr: number
    nombre: string;
    uv: string;
    superficie_total: string;
    superficie_util: string;
    superficie_vias_internas: string;
    superficie_areas_verdes: string;
    superficie_areas_comunes: string;
    nota: string;
}

export class ProrrateoUrbDet {
    idprorrateo_urb_cerr_det: number;
    uv: string;
    manzana: string;
    lote: string;
    area_util_m2: string;
    area_comun_porc: string;
    area_comun_m2: string;
    lote_ideal_m2: string;    
}

export class ParametroUrbanizacion{
    encabezado: ProrrateoUrb;
    detalles: ProrrateoUrbDet [];
}

export class ProrrateoEdificio {
    idprorrateo_urb_ph: number;
    nombre: string;
    superficie_terreno: string;
    superficie_exclusiva_c_a_c: string;
    superficie_exclusiva_s_a_c: string;
    superficie_exclusiva: string;
    superficie_comun_cubiertas: string;
    superficie_construida_total: string;
    uv: string;
    manzana: string;
    lote: string;
}

export class ProrrateoEdificioDet {
    idprorrateo_urb_ph_det: number;
    planta: string;
    nro: string;
    unidad: string;
    sup_total_exclusiva: string;
    participacion_porc: string;
    area_comun: string;
    sup_total: string;
    fit_porc: string;
    fit_m2: string;
}

export class ParametroEdificio{
    encabezado: ProrrateoEdificio;
    detalles: ProrrateoEdificioDet [];
}

export class RespuestaProrrateoCerr{
    codigo: number;
    mensaje: string;
    lista: ProrrateoUrb [];
}

export class RespuestaProrrateoPh{
    codigo: number;
    mensaje: string;
    lista: ProrrateoEdificio [];
}

export class RespuestaProrrateoPhDet {
    codigo: number;
    mensaje: string;
    encabezado: ProrrateoEdificio;
    lista: ProrrateoEdificioDet [];
}

export class RespuestaProrrateoCerrDet {
    codigo: number;
    mensaje: string;
    encabezado: ProrrateoUrb;
    lista: ProrrateoUrbDet [];
}

export class PlantaEdificio {
    nro: string;
    nombre: string;    
}

export class UnidadEdificio {
    nro: number;
    nombre: string;
}