export interface LcnLineamiento {
    idlineamiento: number;
    fechatramite: string;
    estadopago: number;
}

export class RespuestaLineamiento {
    codigo: number;
    mensaje: string;
    lineamientos: LcnLineamiento[];
}

export class RespuestaNormal {
    codigo: number;
    mensaje: string;
}

export class RespuestaPlanos {
    codigo: number;
    mensaje: string;
    planos: LcnPlanoSuelo[];
}

export class Dashboard {
    codigo: number;
    mensaje: string;
    cantlineamientos: number;
    cantplanos: number;
}

export interface LcnPlanoSuelo {
    idplanosuelo: number;
    numerplano: string,
    superficietitulo: string,
    superficiemensura: string,
    zona: string,
    uv: string,
    manzana: string
}

