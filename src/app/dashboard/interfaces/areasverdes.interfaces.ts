export class ParametroAreaVerdeDocumento {
    idareaverde: number;
    idtipodocumento: number;
    base64: string;
	login: string;
	motivo: string;
}

export class ParametroAreaMunicipal {
    idareaverde: number;
    distrito: string;	
	uv: string;	
	sit_legal: string;
	nom: string;
	uso: string;
	uso_especi: string;
	ley_mcpal: string;
	instrument: string;
	matricula: string;
	sup_titulo: number;
	sup_men: number;
	sup_cc: number;
	n_plano: string;
	f_emision: string;
	obs: string;
}

export interface DocumentoArea{
    iddocumento: number;
	idtipodocumento: number;
    nombre: string;
    idareaverde: Date;
}

export class RespuestaAreas {
	codigo: number;
	mensaje: string;
	lista: DocumentoArea [];
}