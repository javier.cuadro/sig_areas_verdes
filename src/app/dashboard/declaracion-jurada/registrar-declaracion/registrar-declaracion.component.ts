import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MY_FORMATS } from '../../perfil-fotos/perfil-fotos.component';
import { Construccion, Lista, Minuta, MinutaPar, ParametroDJUPOST, ConstruccionPar, ServicioBasico, Base64Doc, TipoDocumento } from '../interfaces/declaracion-jurada.interfaces';
import { LoadingBackdropService } from '../../../core/services/loading-backdrop.service';
import { DeclaracionJuradaService } from '../services/declaracion-jurada.service';
import { finalize, Observable, ReplaySubject } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AgregarConstruccionComponent } from '../agregar-construccion/agregar-construccion.component';
import Swal from 'sweetalert2';

import "leaflet";
import { AuthService } from '../../../core/services/auth.service';
import { Router } from '@angular/router';
import { PublicidadService } from '../../publicidad/services/publicidad.service';
import { ParametroAreaMunicipal } from '../../interfaces/areasverdes.interfaces';
import { ParametroTareaDocumentoPost, ParametroTramiteWF, PubTipoTramiteRequisito } from '../../publicidad/interfaces/publicidad.interfaces';
declare let L;

@Component({
  selector: 'app-registrar-declaracion',
  templateUrl: './registrar-declaracion.component.html',
  styleUrls: ['./registrar-declaracion.component.css'],
  providers: [
    {

      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE],
    },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
    DatePipe,
  ],
})
export class RegistrarDeclaracionComponent implements OnInit {  
  map;
  

  i=0;
  lat1=0;
  lng1=0;
  marker;

  X: number = 0;
  Y: number = 0;

  latitud: number;
  longitud: number;

  total_sup: number = 0;

  cantDocumentos: number = 0;
  documentos: Base64Doc[] = [];

  tipoDocumentos  : TipoDocumento [] = [];

  form1: FormGroup;
  form2: FormGroup;
  form3: FormGroup;
  form4: FormGroup;
  form5: FormGroup;
  form6: FormGroup;

  formM: FormGroup;

  tipotramites: Lista[] = [];
  monedas: Lista[] = [];   
  tipoconstrucciones: Lista[] = [];
  tipovias: Lista[] = [];
  sexos: Lista[] = [];
  estados: Lista[] = [];
  servicios: ServicioBasico[] = [];

  minutas: Minuta[] = [];

  construcciones: Construccion[] = [];

  streetMaps =L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
   detectRetina: true,
   attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
   maxZoom: 19,
 });
 

  esri = L.tileLayer(  
    'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
    maxZoom: 19,
    minZoom: 1,
    attribution: 'Tiles © Esri',
  });

  wms_lim_jur = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
      layers: "limite_jurisdiccion",
      format: 'image/png',
      maxZoom: 19,
      transparent: true,
      version: '1.3.0',
      attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
    }
  );
  
  wms_lim_mun = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
      layers: "limite_municipio",
      format: 'image/png',
      maxZoom: 19,
      transparent: true,
      version: '1.3.0',
      attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
    }
  );

  wms_uv_yina = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
      layers: "unidades_vecinales_yina",
      format: 'image/png',
      maxZoom: 19,
      transparent: true,
      version: '1.3.0',
      attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
    }
  );

  wms_manzanas_yina = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
      layers: "manzanas_yina",
      format: 'image/png',
      maxZoom: 19,
      transparent: true,
      version: '1.3.0',
      attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
    }
  );

  wms_lotes = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
      layers: "lotes_cartografia_yina",
      format: 'image/png',
      maxZoom: 19,
      transparent: true,
      version: '1.3.0',
      attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
    }
  );

  options = {
    layers: [ this.streetMaps ],
    zoom: 15,
    center: L.latLng([ -17.7876515, -63.1825049 ]),
    //maxBounds: [[-17.622216426146586, -63.28947607997527],[-17.902058376130743, -63.00586013670787]],
  
    //lat = -17.7876515;
    //lng = -63.1825049;
  };

  constructor(
    private authService: AuthService,
    private datePipe: DatePipe,
    private declaracionJuradaService: DeclaracionJuradaService,
    private publicidadService: PublicidadService,
    private fb: FormBuilder,
    private loadingBackdropService: LoadingBackdropService,    
    private modalService: NgbModal,
    private router: Router
    
  ) { 
    this.form1 = this.fb.group({
      /*idtipo_tramite: [1, Validators.required],
      glosa_otro: [''] ,*/
    },
      { updateOn: "blur" }
    );

    this.formM = this.fb.group({
      fecha_minuta: ['', [Validators.required ] ],
      monto: [0 , [Validators.required ]],
      moneda: [1, [Validators.required ]],
    });

    
    this.form2 = this.fb.group({
      codigo_catastral: ['', [Validators.required ]],
      numero_inmueble: ['', [Validators.required ]],
      uv: ['', [Validators.required ]],
      manzana: ['', [Validators.required ]],
      lote: ['', [Validators.required ]],
      direccion_inmueble: ['', [Validators.required ]],
      barrio_inmueble: ['', ],      
    },
      { updateOn: "blur" }
    );
   
    this.form3 = this.fb.group({
      superficie_terrenom2: ['', [Validators.required ]],  
      idtipovia: [1, [Validators.required ]],
      /*idservicio_basico1: ['', [Validators.required ]], 
      idservicio_basico2: ['', [Validators.required ]], 
      idservicio_basico3: ['', [Validators.required ]], 
      idservicio_basico4: ['', [Validators.required ]], */
    },
      { updateOn: "blur" }
    );

    this.form4 = this.fb.group({
      idtipo_documento_identidad1: ['', [Validators.required ]],
      numero_documento_identidad1: ['', [Validators.required ]],      
      expendido_documento_identidad1: ['', [Validators.required ]],
      idtipo_propietario1: ['', [Validators.required ]],
      apellido_paterno1: ['', [Validators.required ]],
      apellido_materno1: ['', [Validators.required ]],
      nombres1: ['', [Validators.required ]],
      direccion_propietario: ['', [Validators.required ]],
      barrio_propietario: ['', ],
      telefono_fijo1: [''],
      telefono_fijo2: [''],
      telefono_movil1: [''],
      telefono_movil2: [''],
      fecha_nacimiento: [''],
      idsexo: [1],
      idestado_civil: [1],
    },
      { updateOn: "blur" }
    );

    this.form5 = this.fb.group({
      idtipo_documento_identidad2: ['', [Validators.required ]],
      numero_documento_identidad2: [''],
      expendido_documento_identidad2: [''],      
      apellido_paterno2: [''],
      apellido_materno2: [''],
      nombres2: [''],
      numero_poder: [''],
      fecha_poder: [''],
      numero_notario: [''],
      nombre_notario: [''],
      observaciones: [''],
    },
      { updateOn: "blur" }
    );

    this.form6 = this.fb.group({

    },
      { updateOn: "blur" }
    );  
    
  }

  
  

  ngOnInit(): void {    
    this.cargarTipoTramites();
    this.cargarMonedas();
    this.cargarTipoConstrucciones();
    this.cargarTipoVias();
    this.cargarSexos();
    this.cargarEstados();
    this.cargarServicios();
    this.cargarTipoDocumentos();

    this.form4.get("numero_documento_identidad1").valueChanges
    .subscribe(ci => {
      ci = ci;
      if (ci.length>2){
        this.getPersonaByCI(ci);
      }
    });
  }

  getPersonaByCI(ci: string) {
    this.loadingBackdropService.show();

    this.authService
      .getPersonaByCI(ci)
      .pipe(finalize(() => this.loadingBackdropService.hide()))
      .subscribe((data) => {       

        if (data.codigo == 0) {

          let fecha_nacimiento: Date;

          if (data.persona.fecha_nacimiento!=null) {
            let [dia, mes, anho] = data.persona.fecha_nacimiento.split('/');
            fecha_nacimiento = new Date(+anho, +mes - 1, +dia);
          }          

          this.form4.patchValue({
            expendido_documento_identidad1 : data.persona.expedicion ||'',
            apellido_paterno1: data.persona.apellido_paterno ||'',
            apellido_materno1: data.persona.apellido_materno ||'',
            nombres1: data.persona.nombre ||'',            
            fecha: data.persona.direccion ||'',
            fecha_nacimiento: (data.persona.fecha_nacimiento!=null) ? fecha_nacimiento : '',
            telefono_fijo1: data.persona.telefono_fijo ||'',
            telefono_movil1: data.persona.telefono_movil ||'',
            direccion_propietario: data.persona.direccion || 'S/N',
          });
        } else {
          this.form4.patchValue({
            expendido_documento_identidad1 : '',
            apellido_paterno1: '',
            apellido_materno1: '',
            nombres1: '',            
            fecha: '',
            fecha_nacimiento: '',
            telefono_fijo1: '',
            telefono_movil1: '',
          });
        }
      });
  }
  

  cargarServicios() {
    this.loadingBackdropService.show();

    this.declaracionJuradaService
      .getServiciosBasicos()
      .pipe(finalize(() => this.loadingBackdropService.hide()))
      .subscribe((data) => {        

      this.servicios = [];
      data.lista.forEach( (servicio) => {
        this.servicios.push({
          codigo: servicio.codigo,
          nombre: servicio.nombre,
          checked: false
        });
      });        
      });
  }

  cargarEstados() {
    this.loadingBackdropService.show();

    this.declaracionJuradaService
      .getEstados()
      .pipe(finalize(() => this.loadingBackdropService.hide()))
      .subscribe((data) => {        
        this.estados= data.lista;
      });
  }

  cargarSexos() {
    this.loadingBackdropService.show();

    this.declaracionJuradaService
      .getSexos()
      .pipe(finalize(() => this.loadingBackdropService.hide()))
      .subscribe((data) => {        
        this.sexos= data.lista;
      });
  }

  cargarTipoVias(){
    this.loadingBackdropService.show();

    this.declaracionJuradaService
      .getTipoVias()
      .pipe(finalize(() => this.loadingBackdropService.hide()))
      .subscribe((data) => {        
        this.tipovias= data.lista;
      });
  }
  
  cargarTipoTramites(){
    this.loadingBackdropService.show();

    this.declaracionJuradaService
      .getTipoTramites()
      .pipe(finalize(() => this.loadingBackdropService.hide()))
      .subscribe((data) => {        
        this.tipotramites= data.lista;
      });
  }

  cargarMonedas(){
    this.loadingBackdropService.show();

    this.declaracionJuradaService
      .getMonedas()
      .pipe(finalize(() => this.loadingBackdropService.hide()))
      .subscribe((data) => {       
        this.monedas = data.lista;
      });
  }

  cargarTipoConstrucciones(){
    this.loadingBackdropService.show();

    this.declaracionJuradaService
      .getTipoContrucciones()
      .pipe(finalize(() => this.loadingBackdropService.hide()))
      .subscribe((data) => {        
        this.tipoconstrucciones = data.lista;
      });
  }

  onMapReady(map) {     
    var fitBoundsSouthWest = new L.LatLng(-17.606754259255016, -63.394996841180316);
    var fitBoundsNorthEast = new L.LatLng(-17.9500307832854, -62.98270820363417);
    var fitBoundsArea = new L.LatLngBounds(fitBoundsSouthWest, fitBoundsNorthEast);


    L.Icon.Default.imagePath = "assets/leaflet/";

    let geojson;
    
    this.map = map;
    this.map.setMaxBounds(fitBoundsArea);
  
    var baseMaps = {      
      
      "Satelital":this.esri,
      "General": this.streetMaps
    };       

      var overlayMaps = {
        "Limite Municipio": this.wms_lim_mun,
        "Limite Jurisdicción": this.wms_lim_jur,
        "Uv": this.wms_uv_yina,
        "Manzanas": this.wms_manzanas_yina,                 
        "Lotes": this.wms_lotes, 
                       
      };
      
      L.control.scale().addTo(this.map);

      var markersLayer = new L.LayerGroup();	//layer contain searched elements    
      map.addLayer(markersLayer);  


      setTimeout(() => {
        this.map = map;
        map.invalidateSize();        
      }, 0);

  }

  agregarMarcadorJurisdiccion(evt){ 
    
    let i = 0;
    
    var coordenada =  evt.latlng;
    var latitud = coordenada.lat; // lat  es una propiedad de latlng
    var longitud = coordenada.lng; 

    this.GeogToUTM(latitud,longitud);

  console.log("Coordenadas en Grados " + latitud+"-" +longitud );
  console.log("Coordenadas en UTM (X Y) "+ this.lat1 +"-" +  this.lng1);
  
  this.latitud = latitud;
  this.longitud = longitud;

  this.X = this.lat1;
  this.Y = this.lng1;

  //this.map.removeLayer(this.marker);
      if (this.i>0){
        this.map.removeLayer(this.marker);
        
      }
    this.marker  = L.marker([coordenada.lat, coordenada.lng]).bindPopup('Punto');
        //this.marker.push(LamMarker);
        //LamMarker.addTo(this.map);
        //this.map.fitBounds(this.marker);
        this.map.addLayer(this.marker);
        this.i=1;
  }

  GeogToUTM(latd0: number,lngd0 : number){
    //Convert Latitude and Longitude to UTM
    //Declarations();
    let k0:number;
    let b:number;
    let a:number;
    let e:number;
    let f:number;
    let k:number;
    //let latd0:number;
    //let lngd0 :number;
    let lngd :number;
    let latd :number;
    let xd :number;
    let yd :number;
    let phi :number;
    let drad :number;
    let utmz :number;
    let lng :number;
    let latz :number;
    let zcm :number;
    let e0 :number;
    let esq :number;
    let e0sq :number;
    let N :number;
    let T :number;
    let C :number;
    let A :number;
    let M0 :number;
    let x :number;
    let M :number;
    let y :number;
    let yg :number;
    let g :number;
    let usft=1;
    let DatumEqRad:number[];
    let DatumFlat:number[];
    let Item;
  
    let lng0 :number;  
  
    DatumEqRad = [6378137.0, 6378137.0, 6378137.0, 6378135.0, 6378160.0, 6378245.0, 6378206.4,
      6378388.0, 6378388.0, 6378249.1, 6378206.4, 6377563.4, 6377397.2, 6377276.3, 6378137.0];	
      DatumFlat = [298.2572236, 298.2572236, 298.2572215,	298.2597208, 298.2497323, 298.2997381, 294.9786982,
      296.9993621, 296.9993621, 293.4660167, 294.9786982, 299.3247788, 299.1527052, 300.8021499, 298.2572236]; 
      Item = 1;//Default
  
    //drad = Math.PI/180;//Convert degrees to radians)
    k0 = 0.9996;//scale on central meridian
    a = DatumEqRad[Item];//equatorial radius, meters. 
          //alert(a);
    f = 1/DatumFlat[Item];//polar flattening.
    b = a*(1-f);//polar axis.
    e = Math.sqrt(1 - b*b/a*a);//eccentricity
    drad = Math.PI/180;//Convert degrees to radians)
    latd = 0;//latitude in degrees
    phi = 0;//latitude (north +, south -), but uses phi in reference
    e0 = e/Math.sqrt(1 - e*e);//e prime in reference
    N = a/Math.sqrt(1-Math.pow(e*Math.sin(phi),2));
    
    
    T = Math.pow(Math.tan(phi),2);
    C = Math.pow(e*Math.cos(phi),2);
    lng = 0;//Longitude (e = +, w = -) - can't use long - reserved word
    lng0 = 0;//longitude of central meridian
    lngd = 0;//longitude in degrees
    M = 0;//M requires calculation
    x = 0;//x coordinate
    y = 0;//y coordinate
    k = 1;//local scale
    utmz = 30;//utm zone
    zcm = 0;//zone central meridian
  
    k0 = 0.9996;//scale on central meridian
    b = a*(1-f);//polar axis.
    
    e = Math.sqrt(1 - (b/a)*(b/a));//eccentricity
    
  
    lngd=lngd0;
    latd=latd0;
    
    
    
    xd = lngd;
    yd = latd;
  
  
    phi = latd*drad;//Convert latitude to radians
    lng = lngd*drad;//Convert longitude to radians
    utmz = 1 + Math.floor((lngd+180)/6);//calculate utm zone
    latz = 0;//Latitude zone: A-B S of -80, C-W -80 to +72, X 72-84, Y,Z N of 84
    if (latd > -80 && latd < 72){latz = Math.floor((latd + 80)/8)+2;}
    if (latd > 72 && latd < 84){latz = 21;}
    if (latd > 84){latz = 23;}
      
    zcm = 3 + 6*(utmz-1) - 180;//Central meridian of zone
    //alert(utmz + "   " + zcm);
    //Calculate Intermediate Terms
    e0 = e/Math.sqrt(1 - e*e);//Called e prime in reference
    esq = (1 - (b/a)*(b/a));//e squared for use in expansions
    e0sq = e*e/(1-e*e);// e0 squared - always even powers
    //alert(esq+"   "+e0sq)
    N = a/Math.sqrt(1-Math.pow(e*Math.sin(phi),2));
    //alert(1-Math.pow(e*Math.sin(phi),2));
    //alert("N=  "+N);
    T = Math.pow(Math.tan(phi),2);
    //alert("T=  "+T);
    C = e0sq*Math.pow(Math.cos(phi),2);
    //alert("C=  "+C);
    A = (lngd-zcm)*drad*Math.cos(phi);
    //alert("A=  "+A);
    //Calculate M
    M = phi*(1 - esq*(1/4 + esq*(3/64 + 5*esq/256)));
    M = M - Math.sin(2*phi)*(esq*(3/8 + esq*(3/32 + 45*esq/1024)));
    M = M + Math.sin(4*phi)*(esq*esq*(15/256 + esq*45/1024));
    M = M - Math.sin(6*phi)*(esq*esq*esq*(35/3072));
    M = M*a;//Arc length along standard meridian
    
    M0 = 0;//M0 is M for some origin latitude other than zero. Not needed for standard UTM
    
    //Calculate UTM Values
    x = k0*N*A*(1 + A*A*((1-T+C)/6 + A*A*(5 - 18*T + T*T + 72*C -58*e0sq)/120));//Easting relative to CM
    x=x+500000;//Easting standard 
    y = k0*(M - M0 + N*Math.tan(phi)*(A*A*(1/2 + A*A*((5 - T + 9*C + 4*C*C)/24 + A*A*(61 - 58*T + T*T + 600*C - 330*e0sq)/720))));//Northing from equator
    yg = y + 10000000;//yg = y global, from S. Pole
    if (y < 0){y = 10000000+y;}
    //Output into UTM Boxes
          //alert(utmz);
    //console.log(utmz)  ;
    //console.log(Math.round(10*(x))/10 / usft)  ;
    //console.log(Math.round(10*y)/10 /usft)  ;
    this.lat1=Math.round(10*(x))/10 / usft  ;
    this.lng1=Math.round(10*y)/10 /usft;  
  }//close Geog to UTM

  agregarMinuta() {
    let idmoneda:number = this.formM.value.moneda;

    let moneda: Lista[] = this.monedas.filter((obj) => {
      return obj.codigo === idmoneda;
    })

    let minuta: Minuta = {
      idmoneda: this.formM.value.moneda,
      fecha_minuta: this.datePipe.transform(this.formM.value.fecha_minuta,"dd/MM/yyyy"),
      moneda: moneda[0].nombre,
      monto: this.formM.value.monto
    }

    this.minutas.push(minuta);
  }


  eliminarConstruccion(i:number){
    Swal.fire({
      title: 'Está seguro?',
      text: "De Eliminar esta construcción!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, quiero eliminar!',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.isConfirmed) {
        this.construcciones.splice(i,1);
      }
    });  
  }

  eliminarMinuta(i:number){
    Swal.fire({
      title: 'Está seguro?',
      text: "De Eliminar esta minuta!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, quiero eliminar!',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.isConfirmed) {
        this.minutas.splice(i,1);
      }
    });  
  }

  openDialog() {
    
    const modalRef = this.modalService.open(AgregarConstruccionComponent, {      
      centered: true,
      backdrop: 'static',
      windowClass: 'my-class'
    });    
    
    modalRef.componentInstance.fromParent = this.tipoconstrucciones;    
    modalRef.result.then((result) => {
      if (result!=null){
        this.construcciones.push(result);

        this.total_sup = this.construcciones.reduce((
          acc,
          obj,
        ) => acc + (obj.superficie_construidam2),
        0);
      }     
      
    }).catch((error) => {
      console.log(error);
    });
  }  

  
  
  registrarDeclaracion() {
    this.loadingBackdropService.show();

    let serv : number[]=[];
    this.servicios.forEach((servicio) => {
      if (servicio.checked) {
        serv.push(1)
      } else { 
        serv.push(0);
      }
    }); 
    
    let minutasPar : MinutaPar[] = [];
    this.minutas.forEach(function (minuta){
      minutasPar.push({
        fecha_minuta_str: minuta.fecha_minuta,
        idmoneda: minuta.idmoneda,
        monto: minuta.monto
      });
    });

    let construccionesPar : ConstruccionPar[] = [];
    this.construcciones.forEach(function (construccion){
      construccionesPar.push({
        antiguedad_anios: construccion.antiguedad_anios,
        idtipo_construccion: construccion.idtipo_construccion,
        superficie_construidam2: construccion.superficie_construidam2
      })
    });


    let parametroDJUPOST : ParametroDJUPOST = {
      idtipo_tramite: 5,
      glosa_otro: 'ACTUALIZACIÓN DE DATOS TÉCNICOS',
      minutas: minutasPar,

      codigo_catastral: this.form2.value.codigo_catastral,
      numero_inmueble: this.form2.value.numero_inmueble,
      uv: this.form2.value.uv,
      manzana: this.form2.value.manzana,
      lote: this.form2.value.lote,
      direccion_inmueble: this.form2.value.direccion_inmueble,
      barrio_inmueble: this.form2.value.barrio_inmueble,
      latitud: this.latitud,
      longitud: this.longitud,
      superficie_terrenom2: this.form3.value.superficie_terrenom2,
      idtipovia: this.form3.value.idtipovia,
      idservicio_basico1: serv[0],
      idservicio_basico2: serv[1],
      idservicio_basico3: serv[2],
      idservicio_basico4: serv[3],
      construcciones: construccionesPar,      

      idtipo_documento_identidad1: this.form4.value.idtipo_documento_identidad1,
      numero_documento_identidad1: this.form4.value.numero_documento_identidad1,
      expendido_documento_identidad1: this.form4.value.expendido_documento_identidad1,
      idtipo_propietario1: this.form4.value.idtipo_propietario1,
      apellido_paterno1: this.form4.value.apellido_paterno1,
      apellido_materno1: this.form4.value.apellido_materno1,
      nombres1: this.form4.value.nombres1,
      direccion_propietario: this.form4.value.direccion_propietario,
      barrio_propietario: this.form4.value.barrio_propietario,
      telefono_fijo1: this.form4.value.telefono_fijo1,
      telefono_fijo2: this.form4.value.telefono_fijo2,
      telefono_movil1: this.form4.value.telefono_movil1,
      telefono_movil2: this.form4.value.telefono_movil2,
      fecha_nacimiento: this.datePipe.transform(this.form4.value.fecha_nacimiento,"dd/MM/yyyy"),
      idsexo: this.form4.value.idsexo,
      idestado_civil: this.form4.value.idestado_civil,

      idtipo_documento_identidad2: (this.form4.value.idtipo_propietario1!='1') ? this.form5.value.idtipo_documento_identidad2: '',
      numero_documento_identidad2: (this.form4.value.idtipo_propietario1!='1') ? this.form5.value.numero_documento_identidad2: '',
      expendido_documento_identidad2: (this.form4.value.idtipo_propietario1!='1') ? this.form5.value.expendido_documento_identidad2: '',
      idtipo_propietario2: 1,
      apellido_paterno2: (this.form4.value.idtipo_propietario1!='1') ? this.form5.value.apellido_paterno2: '',
      apellido_materno2: (this.form4.value.idtipo_propietario1!='1') ? this.form5.value.apellido_materno2: '',
      nombres2: (this.form4.value.idtipo_propietario1!='1') ? this.form5.value.nombres2 : '',
      numero_poder: (this.form4.value.idtipo_propietario1!='1') ? this.form5.value.numero_poder: '',
      fecha_poder: (this.form4.value.idtipo_propietario1!='1') ? this.datePipe.transform(this.form5.value.fecha_poder,"dd/MM/yyyy"): '',
      numero_notario: (this.form4.value.idtipo_propietario1!='1') ? this.form5.value.numero_notario: '',
      nombre_notario: (this.form4.value.idtipo_propietario1!='1') ? this.form5.value.nombre_notario: '',
      observaciones: (this.form4.value.idtipo_propietario1!='1') ? this.form5.value.observaciones: '',

      idflujo: 0,
      usuario_alta: this.authService.getidUsuario(),      
    }
    console.log(parametroDJUPOST);

    let parametroTramiteWF : ParametroTramiteWF = {
      idusuario_responsable: this.authService.getidUsuario(), //this.authService.getidUsuario()
      idproceso: 3,
      descripcion: "PRUEBA DECLARACION JURADA"
    }

    let idDeclaracion : number = 0;
    

    this.publicidadService
      .registrarTramiteWF(parametroTramiteWF)      
      .subscribe((data) => {        
          if (data.codigo == 0) {
            parametroDJUPOST.idflujo = data.idnuevo;
            this.declaracionJuradaService
              .registrarDeclaracionJurada(parametroDJUPOST)             
              .subscribe((dataT) => {    
                this.loadingBackdropService.hide();   
                if (dataT.codigo == 0) {
                  idDeclaracion= dataT.idnuevo;

                  this.documentos.forEach((item, index)=> {
                    this.registrarDocumento(index, data.idnuevo);
                  });

                  this.reporteDetallado(idDeclaracion)
                  
                } else {
                  Swal.fire({
                    position:'center',
                    icon:'warning',
                    title: dataT.mensaje,
                    showConfirmButton: false,
                    timer: 3000
                  });
                }
              });
            }         
        },        
      );      
  }

  registrarDocumento(i: number, idflujo: number) {
    let parametro : ParametroTareaDocumentoPost = {
      idtareadocumento: this.documentos[i].idtareadocumento,
      estado: 'ACTIVO',
      usuario_alta: this.authService.getLogin(),
      idseguimiento: 0,
      numero_item: i,
      idflujo: idflujo,
      idtarea: 12,
      iddocumento: this.documentos[i].iddocumento,
      tipo_documento: 1,
      descripcion: this.documentos[i].nombre_documento,
      codigo_referencia: '',
      nombre_archivo: '',
      link_archivo: '',
      archivo_binario: this.documentos[i].base64
    }

    this.publicidadService
      .registrarTareaDocumento(parametro)     
      .subscribe((data) => {       
        if (data.codigo==0){
          console.log('OK, documento:'+ i)         
        } else {
          console.log('Error, documento:'+ i)         
        }
      });
 
  }

  reporteDetallado(idNuevo: number) {   
    this.declaracionJuradaService
    .reporteDetallado(idNuevo)
    .pipe(finalize(() => {      
      this.router.navigate(['dashboard/declaracion-jurada/listado']);      
      Swal.fire({
        position:'center',
        icon:'success',
        title: 'Registro OK',
        text: 'Declaración Jurada Nro. '+ idNuevo,
        allowEscapeKey: false,
        confirmButtonText: 'OK',
        confirmButtonColor: '#006b65',  
      });
    }))
    .subscribe((data) => {   
      this.goToLink(data.urldocumento);     
    });
  }

  goToLink(url: string){ window.open(url, "_blank"); }


  public onFileChange(i: number, event) {
    
    if (event.target.files[0].size>=20000000){
      Swal.fire({
        position:'center',
        icon:'warning',
        title: 'El archivo debe ser inferior a 20 megas',
        showConfirmButton: false,
        timer: 3000
      });
      
      event.target.value = null;
      
      this.contarDocumentos();
     
      return;
    }

    this.loadingBackdropService.show();    

    this.convertFile(event.target.files[0]).subscribe(base64 => {
      this.documentos[i].iddocumento = this.tipoDocumentos[i].iddocumento;

      this.documentos[i].base64 = base64;
      this.contarDocumentos();
    });    

    this.loadingBackdropService.hide();
   
  }

  convertFile(file : File) : Observable<string> {
    const result = new ReplaySubject<string>(1);
    const reader = new FileReader();
    reader.readAsBinaryString(file);
    reader.onload = (event) => result.next(btoa(event.target.result.toString()));
    return result;
  } 

  cargarTipoDocumentos(){
    this.loadingBackdropService.show();

    this.declaracionJuradaService
      .getTipoDocumentos()
      .pipe(finalize(() => this.loadingBackdropService.hide()))
      .subscribe((data) => {       
        this.tipoDocumentos = data.documentos;
        
        let docAux: Base64Doc[] = [];
        this.documentos = [];               

        this.tipoDocumentos.forEach(function(item,index) {
          let doc: Base64Doc = {
            iddocumento: item.iddocumento,
            base64: '',
            idtareadocumento: item.idtareadocumento,
            nombre_documento: item.nombre_documento
          }            
          docAux.push(doc);
        });

        this.cantDocumentos = 0;
        this.documentos = docAux;        
      });
  } 

  contarDocumentos(){
    this.cantDocumentos = 0;
    this.documentos.forEach((item ) =>{
      if (item.base64!=""){
        this.cantDocumentos ++;
      }
    });

    console.log('Cantidad:'+this.cantDocumentos);
  }
}
