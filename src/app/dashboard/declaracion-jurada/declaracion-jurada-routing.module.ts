import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegistrarDeclaracionComponent } from './registrar-declaracion/registrar-declaracion.component';
import { ListadoComponent } from './listado/listado.component';

const routes: Routes = [
  {
    path: '',
    component: RegistrarDeclaracionComponent,
  },
  {
    path: 'registrar-declaracion',
    component: RegistrarDeclaracionComponent
  },
  {
    path: 'listado',
    component: ListadoComponent
  } 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeclaracionJuradaRoutingModule { 
  static components = [RegistrarDeclaracionComponent, ListadoComponent];
}
