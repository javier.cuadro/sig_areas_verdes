import { HttpBackend, HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { map, Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { ParametroDJUPOST, RespuestaListado, RespuestaRegistroDJU, RespuestaReporteDetallado, RespuestaTipoDocumento, RespuestaTipos } from "../interfaces/declaracion-jurada.interfaces";

@Injectable({
    providedIn: 'root'
})

export class DeclaracionJuradaService {

  httpOptions = {
    headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*'
    })
  };

    constructor(
        private handler: HttpBackend,
        private http: HttpClient
      ) {
        this.http = new HttpClient(handler); /// to skip interceptors, becouse this service hits third backend provider
    }

    getTipoTramites(): Observable<RespuestaTipos> {        
    
        return this.http.get(`${environment.backend.host}/dju/tipostramites`, this.httpOptions)
        .pipe(
          map((response: any) => {
            return response;
          })
        );
    }

    getMonedas(): Observable<RespuestaTipos> {       
    
        return this.http.get(`${environment.backend.host}/dju/monedas`, this.httpOptions)
        .pipe(
          map((response: any) => {
            return response;
          })
        );
    }

    getTipoContrucciones(): Observable<RespuestaTipos> {      
  
      return this.http.get(`${environment.backend.host}/dju/tiposconstrucciones`, this.httpOptions)
      .pipe(
        map((response: any) => {
          return response;
        })
      );
    }

    getTipoVias(): Observable<RespuestaTipos> {      
  
      return this.http.get(`${environment.backend.host}/dju/tiposvia`, this.httpOptions)
      .pipe(
        map((response: any) => {
          return response;
        })
      );
    }

    getServiciosBasicos(): Observable<RespuestaTipos> {      
    
        return this.http.get(`${environment.backend.host}/dju/serviciosbasicos`, this.httpOptions)
        .pipe(
          map((response: any) => {
            return response;
          })
        );
    }

    getSexos(): Observable<RespuestaTipos> {      
  
      return this.http.get(`${environment.backend.host}/dju/sexos`, this.httpOptions)
      .pipe(
        map((response: any) => {
          return response;
        })
      );
    }

    getEstados(): Observable<RespuestaTipos> {      
  
      return this.http.get(`${environment.backend.host}/dju/estadocivil`, this.httpOptions)
      .pipe(
        map((response: any) => {
          return response;
        })
      );
    }

    registrarDeclaracionJurada(parametro: ParametroDJUPOST): Observable<RespuestaRegistroDJU> {

      const httpOptions = {
        headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*'
        })
      };

      return this.http.post(`${environment.backend.host}/dju/registrar`, parametro, httpOptions)
        .pipe(
          map((response: any) => {
            return response;
          })
        );
    }

    reporteDetallado(iddeclaracionjuh: number): Observable<RespuestaReporteDetallado> {

      const httpOptions = {
        headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*'
        })
      };

      let parametro = {
        "iddeclaracionjuh": iddeclaracionjuh
      }
      console.log('Llamando petición:');

      return this.http.post(`${environment.backend.host}/dju/reportedetallado`, parametro, httpOptions)
        .pipe(
          map((response: any) => {
            return response;
          })
        );
    }

    listadoTramites(idusuario: number) : Observable<RespuestaListado> {
      const httpOptions = {
        headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*'
        })
      };

      let parametro = {
        "idusuario": idusuario
      }

      return this.http.post(`${environment.backend.host}/dju/listado`, parametro, httpOptions)
        .pipe(
          map((response: any) => {
            return response;
          })
        );
    }

    getTipoDocumentos(): Observable<RespuestaTipoDocumento>{          
      return this.http.get(`${environment.backend.host}/wfltareaactual/listadoporflujo/0/3`)
        .pipe(
          map((data: any) => {
            return data;
          })
        );
    }
      
}