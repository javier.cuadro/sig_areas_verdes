import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { finalize } from 'rxjs';
import { AuthService } from 'src/app/core/services/auth.service';
import { LoadingBackdropService } from 'src/app/core/services/loading-backdrop.service';
import { Declaracion } from '../interfaces/declaracion-jurada.interfaces';
import { DeclaracionJuradaService } from '../services/declaracion-jurada.service';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.css']
})
export class ListadoComponent implements OnInit {

  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator!: MatPaginator;
 
  idusuario: number;
  
  dataSource = new MatTableDataSource<Declaracion>();
  displayedColumns: string[] = [
    'iddeclaracionjuh',
    'fecha',    
    'codigo_formulario',
    'numero_orden',   
    'nombre_tipo_tramite',
    'codigo_catastral',
    'numero_inmueble',
    'nombre_propietario',
    'pdf'
  ];


  constructor(
    private authService: AuthService,
    private loadingBackdropService: LoadingBackdropService,    
    private declaracionJuradaService: DeclaracionJuradaService,    
  ) {
    this.idusuario= this.authService.getidUsuario();
   }

  ngOnInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.loadTramites();
  }

  loadTramites(){
  
    this.loadingBackdropService.show();

    this.declaracionJuradaService
      .listadoTramites(this.idusuario)
      .pipe(finalize(() => this.loadingBackdropService.hide()))
      .subscribe((data) => {       
        this.dataSource.data = data.data;
      });
  }

  goToLink(id: number)
  { 
    let url='http://207.244.229.255:5007/pdfreportes/dju_' + id + '.pdf';
    window.open(url, "_blank"); 
  }

}
