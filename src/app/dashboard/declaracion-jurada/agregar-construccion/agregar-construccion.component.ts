import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Lista, Construccion } from '../interfaces/declaracion-jurada.interfaces';

@Component({
  selector: 'app-agregar-construccion',
  templateUrl: './agregar-construccion.component.html',
  styleUrls: ['./agregar-construccion.component.css'],
})
export class AgregarConstruccionComponent implements OnInit {
  @Input() fromParent;
  form1: FormGroup;
  
  tipoConstrucciones: Lista[] = [];

  constructor(
    private fb: FormBuilder,
    private activeModal: NgbActiveModal,
  ) {
    this.form1 = this.fb.group({
      tipo_construccion: [1, Validators.required],
      superficie_construidam2: ['', Validators.required],
      antiguedad_anios: ['', Validators.required], 
    },
      { updateOn: "blur" }
    );
   }

  ngOnInit(): void {    
    this.tipoConstrucciones = this.fromParent;
  }

  agregar(){
    let idtipo:number = this.form1.value.tipo_construccion;

    let tipo: Lista[] = this.tipoConstrucciones.filter((obj) => {
      return obj.codigo === idtipo;
    })

    let construccion: Construccion = {
      antiguedad_anios: this.form1.value.antiguedad_anios,
      superficie_construidam2: this.form1.value.superficie_construidam2,
      idtipo_construccion: this.form1.value.tipo_construccion,
      tipo: tipo[0].nombre
    };
    this.activeModal.close( construccion );
  }

  cerrar(){
    this.activeModal.close();
  }

}
