export interface RespuestaTipos {
    codigo:  number;
    mensaje: string;
    lista:   Lista[];
}

export interface Lista {
    prefijo: number;
    codigo:  number;
    nombre:  string;
}

export interface ServicioBasico {
    codigo: number;
    nombre: string;
    checked: boolean;
}

export interface Construccion {
    superficie_construidam2: number;
    antiguedad_anios: number;
    idtipo_construccion: number;
    tipo: string;
}

export interface Minuta {
    idmoneda: number;
    moneda: string;
    fecha_minuta: string;
    monto: number;
}

export interface ConstruccionPar {
    antiguedad_anios:        number;
    idtipo_construccion:     number;
    superficie_construidam2: number;
}

export interface MinutaPar {
    idmoneda:         number;
    fecha_minuta_str: string;
    monto:            number;
}

export interface ParametroDJUPOST {
    apellido_materno1:              string;
    apellido_materno2:              string;
    apellido_paterno1:              string;
    apellido_paterno2:              string;
    barrio_inmueble:                string;
    barrio_propietario:             string;
    codigo_catastral:               string;
    construcciones:                 ConstruccionPar[];
    direccion_inmueble:             string;
    direccion_propietario:          string;
    expendido_documento_identidad1: string;
    expendido_documento_identidad2: string;
    fecha_nacimiento:               string;
    fecha_poder:                    string;
    glosa_otro:                     string;
    idestado_civil:                 number;
    idflujo:                        number;
    idservicio_basico1:             number;
    idservicio_basico2:             number;
    idservicio_basico3:             number;
    idservicio_basico4:             number;
    idsexo:                         number;
    idtipo_documento_identidad1:    number;
    idtipo_documento_identidad2:    number;
    idtipo_propietario1:            number;
    idtipo_propietario2:            number;
    idtipo_tramite:                 number;
    idtipovia:                      number;
    latitud:                        number;
    longitud:                       number;
    lote:                           string;
    manzana:                        string;
    minutas:                        MinutaPar[];
    nombre_notario:                 string;
    nombres1:                       string;
    nombres2:                       string;
    numero_documento_identidad1:    string;
    numero_documento_identidad2:    string;
    numero_inmueble:                string;
    numero_notario:                 string;
    numero_poder:                   string;
    observaciones:                  string;
    superficie_terrenom2:           number;
    telefono_fijo1:                 string;
    telefono_fijo2:                 string;
    telefono_movil1:                string;
    telefono_movil2:                string;
    usuario_alta:                   number;
    uv:                             string;
}

export interface RespuestaRegistroDJU {
    codigo:  number;
    idnuevo: number;
    mensaje: string;
}

export interface RespuestaReporteDetallado {
    codigo:       number;
    mensaje:      string;
    urldocumento: string;
}

export interface RespuestaListado {
    codigo:  number;
    mensaje: string;
    data:    Declaracion[];
}

export interface Declaracion {
    iddeclaracionjuh:    number;
    fecha:               string;
    idusuario_alta:      number;
    codigo_formulario:   string;
    numero_orden:        string;
    idtipo_tramite:      number;
    nombre_tipo_tramite: string;
    codigo_catastral:    string;
    numero_inmueble:     string;
    nombre_propietario:  string;
}

export interface Base64Doc {
    iddocumento: number;
    nombre_documento: string;
    idtareadocumento: number;
    base64: string;
}

export interface RespuestaTipoDocumento {
    idproceso:              number;
    nombre_proceso:         string;
    idtarea_actual:         number;
    nombre_tarea_actual:    string;
    idflujo:                number;
    idseguimiento:          number;
    fecha_alta:             null;
    idusuario:              number;
    asunto:                 string;
    dias_transcurridos:     number;
    fecha_asignacion:       null;
    estado:                 null;
    idtarea_siguiente:      number;
    nombre_tarea_siguiente: string;
    url_formulario:         string;
    pk:                     number;
    codigo:                 number;
    mensaje:                string;
    usuarios:               any[];
    documentos:             TipoDocumento[];
}

export interface TipoDocumento {
    iddocumento:      number;
    nombre_documento: string;
    es_requerido:     string;
    idtareadocumento: number;
    swarchivo:        boolean;
}