import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DeclaracionJuradaRoutingModule } from './declaracion-jurada-routing.module';
import { RegistrarDeclaracionComponent } from './registrar-declaracion/registrar-declaracion.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AgregarConstruccionComponent } from './agregar-construccion/agregar-construccion.component';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { ListadoComponent } from './listado/listado.component';


@NgModule({
  declarations: [
    ...DeclaracionJuradaRoutingModule.components,
    RegistrarDeclaracionComponent,
    AgregarConstruccionComponent,
    ListadoComponent
  ],
  imports: [
    CommonModule,
    DeclaracionJuradaRoutingModule,
    FormsModule,
    LeafletModule,
    ReactiveFormsModule,
    SharedModule,
  ]
})
export class DeclaracionJuradaModule { }
