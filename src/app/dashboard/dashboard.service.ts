import { Injectable } from '@angular/core';
import { HttpClient, HttpBackend, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, delay } from 'rxjs/operators';

import { environment } from 'src/environments/environment';

import { RespuestaLineamiento, Dashboard, RespuestaPlanos, RespuestaNormal } from './interfaces/dashboard.interfaces';
import { ParametroEdificio, ParametroUrbanizacion, RespuestaProrrateoCerr, RespuestaProrrateoCerrDet, RespuestaProrrateoPh, RespuestaProrrateoPhDet } from './interfaces/excel.interfaces';

@Injectable({
    providedIn: 'root'
  })
export class DashboardService {

  constructor(
    private handler: HttpBackend,
    private http: HttpClient
  ) {
    this.http = new HttpClient(handler); /// to skip interceptors, becouse this service hits third backend provider
  }
 
  getProrrateoPhDetalle(id: number): Observable<RespuestaProrrateoPhDet>{
    let params = new HttpParams();
    params = params.append('id', id);

    return this.http.get(`${environment.backend.host}/adm_usuarios/prorrateo_ph`, { params })
      .pipe(
        map((data: any) => {
          console.log(data);
          return data;
        })
      );
  }

  getProrrateoCerrDetalle(id: number): Observable<RespuestaProrrateoCerrDet>{
    let params = new HttpParams();
    params = params.append('id', id);

    return this.http.get(`${environment.backend.host}/adm_usuarios/prorrateo_cerr`, { params })
      .pipe(
        map((data: any) => {
          console.log(data);
          return data;
        })
      );
  }

  getProrrateosCerr(): Observable<RespuestaProrrateoCerr>{
    let params = new HttpParams();

    return this.http.get(`${environment.backend.host}/adm_usuarios/listado_prorrateo_cerr`)
      .pipe(
        map((data: any) => {
          console.log(data);
          return data;
        })
      );
  }

  getProrrateosPh(): Observable<RespuestaProrrateoPh>{
    let params = new HttpParams();

    return this.http.get(`${environment.backend.host}/adm_usuarios/listado_prorrateo_ph`)
      .pipe(
        map((data: any) => {
          console.log(data);
          return data;
        })
      );
  }

  getLineamientos(id: number): Observable<RespuestaLineamiento> {
    let params = new HttpParams();
    params = params.append('idpersona', id);
    return this.http.get(`${environment.backend.host}/lcnlineamiento/listado`, { params })
      .pipe(
        map((data: any) => {
          console.log(data);
          return data;
        })
      );
  }

  getPlanos(id: number): Observable<RespuestaPlanos> {
    let params = new HttpParams();
    params = params.append('idpersona', id);
    return this.http.get(`${environment.backend.host}/lcnplanosuelo/listado`, { params })
      .pipe(
        map((data: any) => {
          console.log(data);
          return data;
        })
      );
  }

  dashboard(id: number): Observable<Dashboard> {
    let params = new HttpParams();
    params = params.append('idpersona', id);
    return this.http.get(`${environment.backend.host}/lcnpersona/dashboard`, { params })
      .pipe(
        map((data: any) => {
          console.log(data);
          return data;
        })
      );
  }

  registrarProrrateoEdificio(parametroEdificio: ParametroEdificio): Observable<RespuestaNormal> {
    
    const httpOptions = {
      headers: new HttpHeaders({
                  'Content-Type': 'application/json',
                  'Access-Control-Allow-Origin': '*'
      })
    };
    return this.http.post(`${environment.backend.host}/lcnprorrateo/registro_urb_ph`, parametroEdificio, httpOptions)
      .pipe(
        map((response: any) => {
          return response;
        })
      );
  }

  registrarProrrateoUrbanizacion(parametroUrbanizacion: ParametroUrbanizacion): Observable<RespuestaNormal> {
    
    const httpOptions = {
      headers: new HttpHeaders({
                  'Content-Type': 'application/json',
                  'Access-Control-Allow-Origin': '*'
      })
    };
    return this.http.post(`${environment.backend.host}/lcnprorrateo/registro_urb_cerr`, parametroUrbanizacion, httpOptions)
      .pipe(
        map((response: any) => {
          return response;
        })
      );
  }
}
