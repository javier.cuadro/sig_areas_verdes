import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { finalize, Observable, ReplaySubject } from 'rxjs';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Documento, ParametroFlujo, PubTareas, Usuario, ParametroTareaDocumentoPost } from '../interfaces/publicidad.interfaces';
import { PublicidadService } from '../services/publicidad.service';
import { VerTramiteComponent } from '../ver-tramite/ver-tramite.component';

import Swal from 'sweetalert2';
import { DatePipe } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import { AuthService } from 'src/app/core/services/auth.service';
import { PagoCiudadanoComponent } from '../pago-ciudadano/pago-ciudadano.component';


@Component({
  selector: 'app-seguimiento-tareas',
  templateUrl: './seguimiento-tareas.component.html',
  styles: [`
    .mat-radio-button ~ .mat-radio-button {
      margin-left: 16px;
    }
    th {
      background: rgba(189, 215, 238, 0.87);
    }
    td {
      padding: 5px;
      
    }
    td {
      border: 1px inset rgba(189, 215, 238, 0.87);
      border-collapse: collapse;
      border-spacing: 0;
    }    
  `
  ]
})
export class SeguimientoTareasComponent implements OnInit {

  @Input() fromParent;
  usuarios: Usuario [];
  documentos: Documento [];
  form: FormGroup;
  tarea: PubTareas;
  tareaPago: boolean = false;
  cargando  : boolean = false;

  base64_pdf: string[] = [];

  public displayedColumns = ['nombre_documento', 'idtareadocumento','es_requerido', 'seleccionar', 'adjuntar', 'eliminar'];
  public dataSource = new MatTableDataSource<Documento>();

  constructor(
    private activeModal: NgbActiveModal,
    private authService: AuthService,
    private datePipe: DatePipe,
    private fb: FormBuilder,
    private modalService: NgbModal,
    private publicidadService: PublicidadService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    
    this.tarea = this.fromParent;

    let proceso: string;    

    this.form = this.fb.group({      
      idflujo: [ '' ],
      idtramitema: [ ''],
      fecha_alta: [ '' ],
      accion: ['A', [
        Validators.required,        
      ]],
      tarea_actual: [''],
      fecha: [''],
      proceso: [ '' ],
      estado: [ '' ],
      observacion: ['',[
        Validators.required,        
      ]],
      siguiente_tarea: [ '' ],
      responsable: ['', [
        Validators.required,        
      ]],
      idseguimiento: [''],
      idtarea: [''],
    });

    this.loadFlujo(this.tarea.idflujo);
  }

  nombreTarea(idtarea:number):string {
    let proceso=""
    if (idtarea==1) proceso="ETAPA 1 - INICIO FLUJO USUARIO LETRERO";
    if (idtarea==2) proceso="ETAPA 2 - REVISION 1 ENCARGADO LETRERO";
    if (idtarea==3) proceso="ETAPA 3 - REVISION 1 INSPECTOR LETRERO";
    if (idtarea==4) proceso="ETAPA 4 - REVISION 2 ENCARGADO LETRERO";
    if (idtarea==5) proceso="ETAPA 5 - REVISION 1 JEFE LETRERO";
    if (idtarea==6) proceso="ETAPA 6 - PAGO 1 USUARIO LETRERO";
    if (idtarea==7) proceso="ETAPA 7 - FIN FLUJO LETRERO";
    return proceso;
  }

  loadFlujo(idflujo:number){
    let tareas: PubTareas [];
   
    this.publicidadService
      .listadoPorFlujo(idflujo, 1)    
      .subscribe((data) => {
        if (data.codigo==0){
          this.form.patchValue({
            idflujo: data.idflujo,            
            fecha_alta: this.datePipe.transform(data.fecha_alta,"dd/MM/yyyy"),
            fecha: this.datePipe.transform(data.fecha_asignacion,"dd/MM/yyyy"),
            tarea_actual: this.nombreTarea(data.idtarea_actual),
            proceso: data.nombre_proceso,
            siguiente_tarea: this.nombreTarea(data.idtarea_siguiente),
            idseguimiento: data.idseguimiento,
            idtarea: data.idtarea_actual
          });
          this.usuarios = data.usuarios;
          if (data.idtarea_actual<=4){            
            this.tareaPago = false;
          } else {            
            this.form.patchValue({
              responsable: data.usuarios[0].idusuario
            });
            this.tareaPago = true;
          }
          
          this.documentos = data.documentos;
          this.dataSource.data = data.documentos;
        }        
      });
  }

  openDialog() {

    let modalRef: NgbModalRef = null;

    if (this.tarea.idtarea_actual == 2){
      modalRef = this.modalService.open(VerTramiteComponent, {      
        centered: true,
        backdrop: 'static',
        windowClass: 'my-class'
       });
    } else if (this.tarea.idtarea_actual == 3){
      modalRef = this.modalService.open(VerTramiteComponent, {      
        centered: true,
        backdrop: 'static',
        windowClass: 'my-class'
       });   
    } else if (this.tarea.idtarea_actual == 4){
      modalRef = this.modalService.open(VerTramiteComponent, {      
        centered: true,
        backdrop: 'static',
        windowClass: 'my-class'
       });  
    } else if (this.tarea.idtarea_actual == 5){
      modalRef = this.modalService.open(VerTramiteComponent, {      
        centered: true,
        backdrop: 'static',
        windowClass: 'my-class',
        size: 'sm'
       });  
    } else if (this.tarea.idtarea_actual == 6){
      modalRef = this.modalService.open(PagoCiudadanoComponent, {      
        centered: true,
        backdrop: 'static',
        windowClass: 'my-class'
       });  
    }

    modalRef.componentInstance.fromParent = this.form.value.idflujo;
    modalRef.result.then((result) => {      
    }, (reason) => {
    });
    
  }



  procesarFlujo() {
    let parametroFlujo: ParametroFlujo = {
      idflujo: this.form.value.idflujo,
      observacion: this.form.value.siguiente_tarea + ' con la siguiente observación: ' + this.form.value.observacion,
      accion: this.form.value.accion,
      idusuario_responsable: this.form.value.responsable,
      usuario_que_envio: this.authService.getUsuario()
    }
    this.publicidadService
      .procesarFlujo(parametroFlujo)    
      .subscribe((data) => {
        if (data.codigo==0){
          Swal.fire({
            position:'center',
            icon:'success',
            title: data.mensaje,
            showConfirmButton: false,
            timer: 2000
          })
          this.router.navigate(['dashboard/publicidad/panel-control']) .then(() => { window.location.reload(); });
          
        } else {
          Swal.fire({
            position:'center',
            icon:'warning',
            title: data.mensaje,
            showConfirmButton: false,
            timer: 2000
          })
        }  
      });
  }

  descargarPdf(nombre:string, idtareadocumento: number){   
    this.cargando = true;
    this.publicidadService
      .obtenerTareaDocumento(idtareadocumento)
      .pipe(finalize(() => this.cargando = false))  
      .subscribe((data) => {       
        if (data.codigo==0 && data.mensaje.length>10){
          const source = `data:application/pdf;base64,${data.mensaje}`;
          const link = document.createElement("a");
          link.href = source;
          link.download = nombre + "_" + idtareadocumento + ".pdf";
          link.click();
        } else {
          Swal.fire({
            position:'center',
            icon:'error',
            title: data.mensaje,
            showConfirmButton: false,
            timer: 3000
          })
        }
      });
  }

  public onFileChange(iddocumento : number, event) {   
    
    if (event.target.files[0].size>=25*1000*1024){
      Swal.fire({
        position:'center',
        icon:'warning',
        title: 'El Archivo PDF debe ser inferior o igual a 25 Megas',
        showConfirmButton: false,
        timer: 3000
      });

      this.base64_pdf[iddocumento]="";      

      return;
    }
   
    this.convertFile(event.target.files[0]).subscribe(base64 => {      
      this.base64_pdf[iddocumento]= base64;  
    });    
  }

  convertFile(file : File) : Observable<string> {
    const result = new ReplaySubject<string>(1);
    const reader = new FileReader();
    reader.readAsBinaryString(file);
    reader.onload = (event) => result.next(btoa(event.target.result.toString()));
    return result;
  }

  cargarPdf(idtareadocumento: number, iddocumento:number, numero_item: number, descripcion: string){
    
    if (this.base64_pdf[iddocumento]==undefined || this.base64_pdf[iddocumento]==''){
      Swal.fire({
        position:'center',
        icon:'warning',
        title: 'Debe Seleccionar Archivo PDF para Cargar',
        showConfirmButton: false,
        timer: 3000
      })
      return;
    }

    let parametro : ParametroTareaDocumentoPost = {
      idtareadocumento: idtareadocumento,
      estado: 'ACTIVO',
      usuario_alta: this.authService.getLogin(),
      idseguimiento: this.form.value.idseguimiento,
      numero_item:numero_item,
      idflujo: this.form.value.idflujo,
      idtarea: this.form.value.idtarea,
      iddocumento: iddocumento,
      tipo_documento: 1,
      descripcion: descripcion,
      codigo_referencia: '',
      nombre_archivo: '',
      link_archivo: '',
      archivo_binario: this.base64_pdf[iddocumento]

    }

    this.cargando = true;
    this.publicidadService
      .registrarTareaDocumento(parametro)
      .pipe(finalize(() => this.cargando = false))
      .subscribe((data) => {       
        if (data.codigo==0){
          Swal.fire({
            position:'center',
            icon:'success',
            title: 'Se adjuntó correctamente',
            showConfirmButton: false,
            timer: 3000
          });
          this.base64_pdf[iddocumento]=undefined;
          this.loadTabla(this.form.value.idflujo);
        } else {
          Swal.fire({
            position:'center',
            icon:'error',
            title: data.mensaje,
            showConfirmButton: false,
            timer: 3000
          });
          this.base64_pdf[iddocumento]=undefined;
        }
      });
 
    //console.log(this.base64_documento);
  }

  loadTabla(idflujo:number){
    let tareas: PubTareas [];
   
    this.publicidadService
      .listadoPorFlujo(idflujo,1 )    
      .subscribe((data) => {
        if (data.codigo==0){          
          this.documentos = data.documentos;
          this.dataSource.data = data.documentos;
        }        
      });
  }

  eliminar_documento(idtareadocumento: number){
    Swal.fire({
      title: 'Está seguro?',
      text: "De Eliminar este documento!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, quiero eliminar!',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.isConfirmed) {
       
        this.cargando = true;
        this.publicidadService
          .eliminarTareaDocumento(idtareadocumento, this.authService.getLogin())
          .pipe(finalize(() => this.cargando = false))
          .subscribe((data) => {       
            if (data.codigo==0){
              Swal.fire({
                position:'center',
                icon:'success',
                title: 'Se eliminó correctamente',
                showConfirmButton: false,
                timer: 3000
              });
              this.loadTabla(this.form.value.idflujo);
            } else {
              Swal.fire({
                position:'center',
                icon:'error',
                title: data.mensaje,
                showConfirmButton: false,
                timer: 3000
              });
            }
          });
      }
    });
    return;
    
  }
  
  cerrar(){
    this.activeModal.close();
  }
}
