import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { finalize } from 'rxjs';
import { variables } from 'src/app/classes/variables';
import { ParametroConsultaQR, PubTramiteFlujo } from '../interfaces/publicidad.interfaces';
import { PublicidadService } from '../services/publicidad.service';

@Component({
  selector: 'app-pago-ciudadano',
  templateUrl: './pago-ciudadano.component.html',
  styles: [
  ]
})
export class PagoCiudadanoComponent implements OnInit {
  @Input() fromParent;
  tramiteFlujo: PubTramiteFlujo;
  id;

  qrcode: string = '';
  qrcodeGenerado: string;
  qrcodeNoVigente: string;
  
  imagenQR : string;

  authorization: string = '';
  idqr: string ='';

  public identificadorDeTemporizador_tiempo:number=0; 

  constructor(
    private activeModal: NgbActiveModal,
    private publicidadService: PublicidadService
  ) { }

  ngOnInit(): void {
    this.tramiteFlujo = this.fromParent;
    this.qrcode= variables.qrcode;
    this.qrcodeGenerado = variables.qrcodeGenerado;
    this.qrcodeNoVigente = variables.qrcodeNoVigente;

    this.imagenQR = this.qrcode;
  }

  temporizadorDeRetraso() {
    //alert("Three seconds have elapsed." +this.identificadorDeTemporizador_tiempo) ;
    this.identificadorDeTemporizador_tiempo=0;
    //this.identificadorDeTemporizador = setInterval(this.funcionConRetraso,this.identificadorDeTemporizador_tiempo, 3000);
    this.id = setInterval(()=>{
      this.identificadorDeTemporizador_tiempo=this.identificadorDeTemporizador_tiempo+1;
      //alert(this.identificadorDeTemporizador_tiempo);
      
      this.ConsultarQR();
      if (this.identificadorDeTemporizador_tiempo===6 ){
        //this.FormGroupBusqueda.get('busx')?.value
              //alert("Three seconds have elapsed.");
        this.borrarAlerta();
        this.imagenQR = this.qrcodeNoVigente;
        console.log("QR No vigente");
        
      }
      
    }, 5000);
  }

  ConsultarQR()
  {

    let parametro: ParametroConsultaQR ={
      operation: Number(this.idqr),
      authorization: this.authorization
    }
    const body = { 'operation': this.idqr};
    this.publicidadService
      .qrConsultar(parametro)
      .subscribe((result)=>{
        if (result.codigo==0){
          if (result.estado=="PAG"){
            this.borrarAlerta();
            //this.procesarDocumentos();
          } else {
            console.log('Esperando Pago: '+ this.idqr);
          }
        }
    
     });            
  }
  

 generarQR(){     

    if (this.id) {
      clearInterval(this.id);
    }
   
    this.publicidadService
      .generarQR(this.tramiteFlujo.monto)
      .pipe(finalize(() => {
       
      }))
      .subscribe((data) => {
        
        if (data.codigo == 0){
          console.log("idqr: "+ data.idqr);
          
          this.idqr = data.idqr;
          this.authorization = data.authorization;
          this.imagenQR = "data:image/png;base64," + data.imagen64;
          this.temporizadorDeRetraso();
        } else {
          
        }
        
      });
    
  }

  borrarAlerta() {
    if (this.id) {
      clearInterval(this.id);
    }
  }


  cerrar(){
    this.activeModal.close();
  }

  goToLink(id: number): string{
    return 'http://207.244.229.255:5007/pdfreportes/lineamiento' + (id-1) + '.pdf';
  }
}
