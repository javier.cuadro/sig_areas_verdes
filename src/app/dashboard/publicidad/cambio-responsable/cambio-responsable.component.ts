import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ParametroCambioResponsable, RespuestaFlujoUsuario, UsuarioCombo } from '../interfaces/publicidad.interfaces';
import { PublicidadService } from '../services/publicidad.service';
import { LoadingBackdropService } from '../../../core/services/loading-backdrop.service';
import { finalize } from 'rxjs';
import Swal from 'sweetalert2';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-cambio-responsable',
  templateUrl: './cambio-responsable.component.html',
  styles: [
  ]
})
export class CambioResponsableComponent implements OnInit {
  flujo_usuario : RespuestaFlujoUsuario;
  sw:boolean = false;
  usuarios: UsuarioCombo[] = [];

  form = this.fb.group({
    idflujo: ['', Validators.required],
  },
    { updateOn: "blur" }
  );

  form2 = this.fb.group({
    usuarionuevo: ['', Validators.required],
  },
    { updateOn: "blur" }
  );


  constructor(
    private fb: FormBuilder,
    private publicidadService: PublicidadService,
    private loadingBackdropService: LoadingBackdropService,
    private modalService: NgbModal
  ) { }

  ngOnInit(): void {
  }

  consultar(){
    this.flujo_usuario=null;
    this.usuarios=[];
    this.loadingBackdropService.hide()

    this.sw = false;
    this.publicidadService
      .buscarFlujo(this.form.value.idflujo)
      .pipe(finalize(() => this.cargarUsuarios()))
      .subscribe(
        dataT => {          
          this.flujo_usuario = dataT;
          this.usuarios=[];
          this.sw = true;
        },
        error => {
          
        }
      );
  }

  cargarUsuarios(){
    
    if (this.flujo_usuario==null) return;
    this.publicidadService
      .buscarUsuarioPorTarea(this.flujo_usuario.idtarea)
      .pipe(finalize(() => this.loadingBackdropService.hide()))
      .subscribe(
        dataT => {          
          if (dataT.codigo==0){
            this.usuarios = [];
            dataT.lista.forEach(element => {
              if (element.idusuario !=this.flujo_usuario.idusuario){
                this.usuarios.push(element);
              }
            });
            
          } else {
            
          }
         
        },
        error => {
          console.log('error en el componente', error);
          Swal.fire({
            position:'center',
            icon:'error',
            title:'El servicio se encuentra fuera de línea',
            showConfirmButton: false,
            timer: 2000
          });
          
        }
      );
  }

  cambiarResponsable(){
    Swal.fire({
      title: 'Está seguro?',
      text: "De Cambiar el Responsable para esta tarea!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, quiero actualizar!',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.isConfirmed) {
        let parametro : ParametroCambioResponsable = {
          idflujo: this.flujo_usuario.idflujo,
          idusuario: this.flujo_usuario.idusuario,
          idusuarionuevo: this.form2.value.usuarionuevo
        }
        
        this.publicidadService
          .cambioResponsable(parametro)      
          .subscribe((data) => {       
            if (data.codigo==0){
              this.sw = false;
              this.usuarios = [];
              this.flujo_usuario = null;
              Swal.fire({
                position:'center',
                icon:'success',
                title: data.mensaje,
                showConfirmButton: false,
                timer: 3000
              })
              
              
            } else {
              Swal.fire({
                position:'center',
                icon:'error',
                title: data.mensaje,
                showConfirmButton: false,
                timer: 3000
              })
              
            }
          });
      }
    });
    return;
  }
}
