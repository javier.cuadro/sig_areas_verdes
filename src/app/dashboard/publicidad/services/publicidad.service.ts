import { Injectable } from '@angular/core';
import { HttpClient, HttpBackend, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, delay } from 'rxjs/operators';

import { environment } from 'src/environments/environment';
import { ParametroTramite, ParametroTramitePrecio, ParametroTramiteRequisito, RespuestaPublicidad, RespuestaRequisito, RespuestaTipoTramite, RespuestaTramite, RespuestaTramites, RespuestaTramitePrecio, ParametroTramiteWF, RespuestaTramiteWF, RespuestaTareas, RespuestaListadoFlujo, ParametroFlujo, ParametroTareaDocumentoPost, RespuestaFlujoUsuario, RespuestaUsuarioTareas, ParametroCambioResponsable, RespuestaDetalleFlujo, RespuestaConsultaQR, ParametroConsultaQR, RespuestaQR } from '../interfaces/publicidad.interfaces';
import { RespuestaNormal } from '../../interfaces/dashboard.interfaces';


@Injectable({
    providedIn: 'root'
  })

export class PublicidadService {
 
  
  constructor(
    private handler: HttpBackend,
    private http: HttpClient
  ) {
    this.http = new HttpClient(handler); /// to skip interceptors, becouse this service hits third backend provider
  }

  detalleFlujo(idflujo: number): Observable<RespuestaDetalleFlujo> {
    const httpOptions = {
      headers: new HttpHeaders({
                  'Content-Type': 'application/json',
                  'Access-Control-Allow-Origin': '*'
      })
    };

    return this.http.get(`${environment.backend.host}/pubtramite/detalleflujo?idflujo=${idflujo}`, httpOptions)
    .pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  cambioResponsable(parametro: ParametroCambioResponsable): Observable<RespuestaNormal>{
    const httpOptions = {
      headers: new HttpHeaders({
                  'Content-Type': 'application/json',
                  'Access-Control-Allow-Origin': '*'
      })
    };

    return this.http.post(`${environment.backend.host}/wfltareaactual/cambioresponsable`, parametro, httpOptions)
      .pipe(
        map((response: any) => {
          return response;
        })
      );
  }

  buscarFlujo(idflujo: number): Observable<RespuestaFlujoUsuario> {
    const httpOptions = {
      headers: new HttpHeaders({
                  'Content-Type': 'application/json',
                  'Access-Control-Allow-Origin': '*'
      })
    };

    return this.http.get(`${environment.backend.host}/wfltareaactual/buscarflujo/` + idflujo, httpOptions)
    .pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  buscarUsuarioPorTarea(idtarea: number): Observable<RespuestaUsuarioTareas> {
    const httpOptions = {
      headers: new HttpHeaders({
                  'Content-Type': 'application/json',
                  'Access-Control-Allow-Origin': '*'
      })
    };

    return this.http.get(`${environment.backend.host}/wfltareaactual/buscarusuariosportarea/` + idtarea, httpOptions)
    .pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  eliminarTareaDocumento(idtareadocumento: number, usuario: string): Observable<RespuestaNormal> {    
   
    const httpOptions = {
      headers: new HttpHeaders({
                  'Content-Type': 'application/json',
                  'Access-Control-Allow-Origin': '*'
      })
    };
    return this.http.delete(`${environment.backend.host}/wfltareadocumento?idtareadocumento=${idtareadocumento}&usuario=${usuario}`, httpOptions)
      .pipe(
        map((response: any) => {
          return response;
        })
      );
  }

  registrarTareaDocumento(parametro: ParametroTareaDocumentoPost): Observable<RespuestaTramiteWF> {
    const httpOptions = {
      headers: new HttpHeaders({
                  'Content-Type': 'application/json',
                  'Access-Control-Allow-Origin': '*'
      })
    };

    return this.http.post(`${environment.backend.host}/wfltareadocumento/registrar`, parametro, httpOptions)
      .pipe(
        map((response: any) => {
          return response;
        })
      );
  }

  obtenerTareaDocumento(idtareadocumento: number) : Observable<RespuestaNormal> {
    const httpOptions = {
      headers: new HttpHeaders({
                  'Content-Type': 'application/json',
                  'Access-Control-Allow-Origin': '*'
      })
    };

    return this.http.get(`${environment.backend.host}/wfltareadocumento/` + idtareadocumento, httpOptions)
      .pipe(
        map((response: any) => {
          return response;
        })
      );
  }


  listadoPorFlujo(idflujo:number, idproceso: number) : Observable<RespuestaListadoFlujo>{
    const httpOptions = {
      headers: new HttpHeaders({
                  'Content-Type': 'application/json',
                  'Access-Control-Allow-Origin': '*'
      })
    };

    return this.http.get(`${environment.backend.host}/wfltareaactual/listadoporflujo/${idflujo}/${idproceso}`, httpOptions)
      .pipe(
        map((response: any) => {
          return response;
        })
      );
  }

  listarTareasPorUsuario(idusuario: number) : Observable<RespuestaTareas> {
    const httpOptions = {
      headers: new HttpHeaders({
                  'Content-Type': 'application/json',
                  'Access-Control-Allow-Origin': '*'
      })
    };

    return this.http.get(`${environment.backend.host}/wfltareaactual/listadoporusuario/` + idusuario, httpOptions)
      .pipe(
        map((response: any) => {
          return response;
        })
      );
  } 

  procesarFlujo(parametroFlujo: ParametroFlujo) : Observable<RespuestaNormal> {
    const httpOptions = {
      headers: new HttpHeaders({
                  'Content-Type': 'application/json',
                  'Access-Control-Allow-Origin': '*'
      })
    };

    return this.http.post(`${environment.backend.host}/wflflujo/procesar`, parametroFlujo, httpOptions)
      .pipe(
        map((response: any) => {
          return response;
        })
      );
  } 


  registrarTramiteWF(parametroTramiteWF: ParametroTramiteWF) : Observable<RespuestaTramiteWF> {
    const httpOptions = {
      headers: new HttpHeaders({
                  'Content-Type': 'application/json',
                  'Access-Control-Allow-Origin': '*'
      })
    };

    return this.http.post(`${environment.backend.host}/wflflujo/registrar`, parametroTramiteWF, httpOptions)
      .pipe(
        map((response: any) => {
          return response;
        })
      );
  } 


  obtenerPrecio(parametroTramitePrecio: ParametroTramitePrecio): Observable<RespuestaTramitePrecio>  {
    const httpOptions = {
      headers: new HttpHeaders({
                  'Content-Type': 'application/json',
                  'Access-Control-Allow-Origin': '*'
      })
    };
    return this.http.post(`${environment.backend.host}/pubtramite/obtenerprecio`, parametroTramitePrecio, httpOptions)
      .pipe(
        map((response: any) => {
          return response;
        })
      );
  }

  obtenerDocumento(parametro: ParametroTramiteRequisito): Observable<RespuestaNormal> {
    const httpOptions = {
      headers: new HttpHeaders({
                  'Content-Type': 'application/json',
                  'Access-Control-Allow-Origin': '*'
      })
    };
    return this.http.post(`${environment.backend.host}/pubtramite/obtenerdocumento`, parametro, httpOptions)
      .pipe(
        map((response: any) => {
          return response;
        })
      );
  }


  getTramiteDetalle(idflujo: number): Observable<RespuestaTramite>{
    return this.http.get(`${environment.backend.host}/pubtramite/tramite?idflujo=${idflujo}`)
    .pipe(
      map((data: any) => {
        return data;
      })
    );
  }

  getUFV(fecha: string): Observable<RespuestaNormal>{
    
    return this.http.get(`${environment.backend.host}/pubtramite/getufv?fecha=${fecha}`)
      .pipe(
        map((data: any) => {
          return data;
        })
      );
  }

  getTramites(idusuario: number): Observable<RespuestaTramites>{
    console.log('obteniendo tramites');
    return this.http.get(`${environment.backend.host}/pubtramite/listado?idusuario=`+idusuario)
      .pipe(
        map((data: any) => {
          return data;
        })
      );
  }

  registrarTramite(parametroTramite: ParametroTramite): Observable<RespuestaNormal>{
    const httpOptions = {
      headers: new HttpHeaders({
                  'Content-Type': 'application/json',
                  'Access-Control-Allow-Origin': '*'
      })
    };
    return this.http.post(`${environment.backend.host}/pubtramite/registro`, parametroTramite, httpOptions)
      .pipe(
        map((response: any) => {
          return response;
        })
      );
  }

  getTipoPublicidades(): Observable<RespuestaPublicidad>{
   
    return this.http.get(`${environment.backend.host}/pubtramite/listadotipopublicidad`)
      .pipe(
        map((data: any) => {
          return data;
        })
      );
  }

  getTipoTramites(): Observable<RespuestaTipoTramite>{
   
    return this.http.get(`${environment.backend.host}/pubtramite/listadotipotramite`)
      .pipe(
        map((data: any) => {
          return data;
        })
      );
  }

  getTipoRequisitos(idtipotramite: number): Observable<RespuestaRequisito>{
    let params = new HttpParams();
    params = params.append('idtipotramite', idtipotramite);
  
    return this.http.get(`${environment.backend.host}/pubtramite/listadorequisitos`, { params })
      .pipe(
        map((data: any) => {
          return data;
        })
      );
  }

  generarQR(importe: number): Observable<RespuestaQR> {
    const httpOptions = {
      headers: new HttpHeaders({
                  'Content-Type': 'application/json',
                  'Access-Control-Allow-Origin': '*'
      })
    };

    const parametro = {
      importe: importe
    }

    return this.http.post(`${environment.backend.host}/qr/generar`, parametro, httpOptions)
    .pipe(
      map((data: any) => {
        return data;
      })
    );

    
  }

  qrConsultar(parametro: ParametroConsultaQR): Observable<RespuestaConsultaQR> {
    const httpOptions = {
      headers: new HttpHeaders({
                  'Content-Type': 'application/json',
                  'Access-Control-Allow-Origin': '*'
      })
    };   

    return this.http.post(`${environment.backend.host}/qr/consultar`, parametro, httpOptions)
    .pipe(
      map((data: any) => {
        return data;
      })
    );

    
  }

}

