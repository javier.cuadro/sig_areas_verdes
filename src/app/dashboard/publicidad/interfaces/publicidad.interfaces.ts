export class RespuestaTramites {
    codigo: number;
    mensaje: string;
    lista: PubTramiteFlujo[];
}

export interface PubTramiteFlujo{
    idtramitema:         number;
    idflujo:             number;
    idtarea:             number;
    nombre_tarea:        string;
    fecha_alta_tarea:    Date;
    usuario_responsable: string;
    expediente:          string;
    fecha_inicio:        Date;
    fecha_termino:       Date;
    tipopublicidad:      string;
    tipotramite:         string;
    tiempo:              string;
    monto:               number;
}

export interface RespuestaDetalleFlujo {
    codigo:  number;
    mensaje: string;
    lista:   PubFlujoTarea[];
}

export interface PubFlujoTarea {
    idtareaactual:         number;
    idseguimiento:         number;
    idtarea:               number;
    nombre_tarea:          string;
    idusuario_responsable: number;
    usuario_responsable:   string;
    fecha_alta:            Date;
    estado:                string;
}


export class PubTramite {
    idtramitema:    number;
    persona:        string;
    expediente:     string;
    fecha_inicio:   string;
    fecha_termino:  string;
    tipopublicidad: string;
    tipotramite:    string;
    tiempo:         string;
    mat_acero:      boolean;
    mat_papel:      boolean;
    mat_aluminio:   boolean;
    mat_acrilico:   boolean;
    mat_madera:     boolean;
    mat_carton:     boolean;
    mat_plastico:   boolean;
    mat_lona:       boolean;
    mat_tela:       boolean;
    mat_otro:       string;
    monto:          number;
}

export class ParametroTramite{
    idtramitema: number;
    idusuario: number;
    expediente: string;
    idtiempo: number;
    fecha_inicio: string;
    fecha_termino: string;
    idtipopublicidad: number;
    idtipotramite: number;
    mat_acero: boolean;
    mat_papel: boolean;
	mat_aluminio: boolean;
    mat_acrilico: boolean;
    mat_madera: boolean;
    mat_carton: boolean;
    mat_plastico: boolean;
    mat_lona: boolean;
    mat_tela: boolean;
    mat_otro: string;
    idflujo: number;
    
	public listaRequisitos : PubTramiteRequisito[];
	public listaPublicidad: PubTramitePublicidad[];
}

export class PubTramiteRequisito{
    idrequisito: number;
    base64: string;
}

export class PubTipoPublicidad{
    idtipopublicidad: number;
    descripcion: string;
}

export class RespuestaPublicidad{
    codigo: number;
    mensaje: string;
    lista: PubTipoPublicidad[];
}

export class PubTramitePublicidad{
    tipovia:   string;
    direccion: string;
    zona:      string;
    largo:     number;
    alto:      number;
    monto:     number;
}

export interface PubTipoTramite {
    idtipotramite: number;
    descripcion: string;
}

export class RespuestaTipoTramite {
    codigo: number;
    mensaje: string;
    lista: PubTipoTramite[];
}


export class RespuestaTramite {
    codigo: number;
    mensaje: string;
    tramite: PubTramite;
    listaRequisitos: PubTramiteRequisitoDet[];
    listaPublicidades: PubTramitePublicidad[];

}

export interface PubTipoTramiteRequisito {
    idrequisito: number;
    descripcion: string;
    error: boolean;
}

export class RespuestaRequisito {
    codigo: number;
    mensaje: string;
    lista: PubTipoTramiteRequisito[];
}

export class PubTramiteRequisitoDet {
    idrequisito:  number;
    descripcion:  string;
    tieneArchivo: boolean;
}

export class ParametroTramiteRequisito{
    idtramitema: number;
    idrequisito: number;
}

export class ParametroTramitePrecio {
    idtipopublicidad: number;
    tipovia: string;
    zona: string;
}

export class RespuestaTramitePrecio {
    codigo: number;
    mensaje: string;
    precio: number;
}

export class ParametroTramiteWF {
    idusuario_responsable: number;
    idproceso: number;
    descripcion: string;
}

export class RespuestaTramiteWF{
    codigo: number;
    mensaje: string;
    idnuevo: number;
}

export class PubTareas{
    idproceso:          number;
    nombre_proceso:     string;
    idtarea_actual:               number;
    nombre_tarea_actual:       string;
    idflujo:            number;
    idseguimiento:      number;
    fecha_alta:         string;
    idusuario:          number;
    nombre_usuario:     string;
    asunto:             string;
    dias_transcurridos: number;
    fecha_asignacion:   string;
    estado:             string;
    color:              string;
}

export class RespuestaTareas {
    codigo: string;
    mensaje: string;
    lista: PubTareas [];
}

export interface RespuestaListadoFlujo {
    idproceso:              number;
    nombre_proceso:         string;
    idtarea_actual:         number;
    nombre_tarea_actual:    string;
    idflujo:                number;
    idseguimiento:          number;
    fecha_alta:             null;
    idusuario:              number;
    asunto:                 string;
    dias_transcurridos:     number;
    fecha_asignacion:       null;
    estado:                 null;
    idtarea_siguiente:      number;
    nombre_tarea_siguiente: string;
    url_formulario:         string;
    pk:                     number;
    codigo:                 number;
    mensaje:                string;
    usuarios:               Usuario[];
    documentos:             Documento[];
}

export interface Documento {
    iddocumento:      number;
    nombre_documento: string;
    es_requerido:     boolean;
    idtareadocumento: number;
    swarchivo:        boolean;
}

export interface Usuario {
    idusuario:      number;
    nombre_usuario: string;
}

export interface ParametroFlujo {
    idflujo: number;
    observacion: string;
    accion: number;
    idusuario_responsable: number;
    usuario_que_envio: string;
}

export interface ParametroCambioResponsable {
    idflujo         : number;
    idusuario       : number;
    idusuarionuevo  : number;
}

export interface ParametroTareaDocumentoPost {
    idtareadocumento:  number;
    estado:            string;
    usuario_alta:      string;
    idseguimiento:     number;
    numero_item:       number;
    idflujo:           number;
    idtarea:           number;
    iddocumento:  number;
    tipo_documento:    number;
    descripcion:       string;
    codigo_referencia: string;
    nombre_archivo:    string;
    link_archivo:      string;
    archivo_binario:   string;
}

export interface RespuestaFlujoUsuario {
    idflujo:            number;
    fecha_alta:         Date;
    descripcion:        string;
    fecha_inicio:       Date;
    idusuario:          number;
    nombreUsuario:      string;
    idtarea:            number;
    nombreTarea:        string;
    dias_transcurridos: number;
    color:              string;
}

export interface RespuestaUsuarioTareas {
    codigo:  number;
    mensaje: string;
    lista:   UsuarioCombo[];
}

export interface UsuarioCombo {
    idusuario:     number;
    nombreusuario: string;
}

export interface RespuestaQR {
    codigo : number;
    mensaje : string;
    idqr: string;
    imagen64: string;
    authorization: string;
}

export interface ParametroConsultaQR{
    operation: number;
    authorization: string;
}

export interface RespuestaConsultaQR {
    codigo: number;
    mensaje: string;
    estado: string;
}
