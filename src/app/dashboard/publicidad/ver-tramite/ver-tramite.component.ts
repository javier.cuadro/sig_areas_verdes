import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { finalize } from 'rxjs/operators';

import { LoadingBackdropService } from 'src/app/core/services/loading-backdrop.service';
import Swal from 'sweetalert2';
import { ParametroTramiteRequisito, PubTramite, PubTramitePublicidad, PubTramiteRequisitoDet } from '../interfaces/publicidad.interfaces';
import { PublicidadService } from '../services/publicidad.service';

@Component({
  selector: 'app-ver-tramite',
  templateUrl: './ver-tramite.component.html',
  styles: [ `
    ::ng-deep .mat-slide-toggle.mat-disabled {
      opacity: 1 !important;
    }

    table {
      width: 100%;
    }

    .mat-header-row {
      height: 70%;
      width: 100%;
      background: #005173;
    }
    .mat-header-cell {
      color: white;
    }

    .mat-icon-button {
      vertical-align: middle;
    }
    
    input:-moz-read-only { /* For Firefox */
      color: #003153;
      cursor: not-allowed;
    }

    input:read-only { 
      color: #003153;
      cursor: not-allowed;
    }
  ` ]
})
export class VerTramiteComponent implements OnInit, OnDestroy {

  @Input() fromParent;
  cargando  : boolean = false;

  idflujo: number;
  idtramitema: number;
  private sub: any;

  encabezado: PubTramite = {
    idtramitema: 0,
    persona: '',
    expediente: '',
    fecha_inicio: '',
    fecha_termino: '',
    tipopublicidad: '',
    tipotramite: '',
    tiempo: '',
    mat_acero: false,
    mat_papel: false,
    mat_aluminio: false,
    mat_acrilico: false,
    mat_madera: false,
    mat_carton: false,
    mat_plastico: false,
    mat_lona: false,
    mat_tela: false,
    mat_otro: '',
    monto: 0
  }

  detallesReq: PubTramiteRequisitoDet [] = [];
  detallesPub: PubTramitePublicidad [] = [];

  public displayedColumns = ['idrequisito', 'descripcion', 'tieneArchivo'];
  public dataSource = new MatTableDataSource<PubTramiteRequisitoDet>();

  constructor(
    private activeModal: NgbActiveModal,   
    private modalService: NgbModal,
    private route: ActivatedRoute,
    private router: Router,
    private publicidadService: PublicidadService
  ) { }

  ngOnInit(): void {

    this.idflujo = this.fromParent;
    
    this.loadTramiteDetalle();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  
  loadTramiteDetalle(){
    this.cargando = true;

    this.publicidadService
      .getTramiteDetalle(this.idflujo)
      .pipe(finalize(() =>  this.cargando = false))
      .subscribe((data) => {
        this.idtramitema = data.tramite.idtramitema;
        this.encabezado = data.tramite;
        this.detallesReq = data.listaRequisitos;
        this.detallesPub = data.listaPublicidades;
        console.log(this.detallesPub);
        this.dataSource.data = this.detallesReq;
               
      });
  }
  

  descargarPdf(idrequisito: number, nombre: string){
    Swal.fire({
      title: 'Está seguro?',
      text: "De descargar este documento",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí!',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.isConfirmed) {
        let parametro : ParametroTramiteRequisito = {
          idtramitema: this.idtramitema,
          idrequisito: idrequisito
        }
        
        this.cargando = true;
        this.publicidadService
          .obtenerDocumento(parametro)
          .pipe(finalize(() => this.cargando=false ))  
          .subscribe((data) => {       
            if (data.codigo==0 && data.mensaje.length>10){
              const source = `data:application/pdf;base64,${data.mensaje}`;
              const link = document.createElement("a");
              link.href = source;
              link.download = nombre+ "_" + idrequisito + ".pdf";
              link.click();
            } else {
              Swal.fire({
                position:'center',
                icon:'error',
                title: data.mensaje,
                showConfirmButton: false,
                timer: 3000
              })
            }
          });
      }
    });
    return;
  }

  cerrar(){
    this.activeModal.close();
  }
}
