import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PublicidadService } from '../services/publicidad.service';

@Component({
  selector: 'app-emision-informe-tecnico',
  templateUrl: './emision-informe-tecnico.component.html',
  styles: [
  ]
})
export class EmisionInformeTecnicoComponent implements OnInit {
  @Input() fromParent;
  cargando  : boolean = false;
  
  constructor(
    private activeModal: NgbActiveModal,    
    private modalService: NgbModal,
    private route: ActivatedRoute,
    private router: Router,
    private publicidadService: PublicidadService
  ) { }

  ngOnInit(): void {
  }

}
