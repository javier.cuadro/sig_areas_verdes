import { Component, EventEmitter, OnInit, Output, Query, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';

import { LoadingBackdropService } from 'src/app/core/services/loading-backdrop.service';
import { PubTramitePublicidad, PubTipoTramite, PubTipoTramiteRequisito, PubTipoPublicidad, ParametroTramite, PubTramiteRequisito, ParametroTramitePrecio, ParametroTramiteWF } from '../interfaces/publicidad.interfaces';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { MatStepper } from '@angular/material/stepper';
import { switchMap, tap } from 'rxjs/operators';
import { Observable, ReplaySubject } from 'rxjs';
import Swal from 'sweetalert2';
import { DatePipe } from '@angular/common';
import { PublicidadService } from '../services/publicidad.service';
import { AuthService } from 'src/app/core/services/auth.service';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { DateAdapter } from '@angular/material/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ZonaMapaComponent } from '../zona-mapa/zona-mapa.component';


@Component({
  selector: 'app-iniciar-tramite',
  templateUrl: './iniciar-tramite.component.html',
  styles: [ `
  ::ng-deep .mat-form-field-infix { border: 0px !important; }

    .mat-label {
        width: 50%;
    }
    .mat-header-row {
      height: 70%;
      width: 100%;
      background: #005173;
    }
    .mat-header-cell {
      color: white;
    }

    .mat-icon-button {
      vertical-align: middle;
    }
    
    input:-moz-read-only { /* For Firefox */
      color: #003153;
      cursor: not-allowed;
    }

    input:read-only { 
      color: #003153;
      cursor: not-allowed;
    }
  ` ],
  providers: [
    {
      provide: STEPPER_GLOBAL_OPTIONS,
      useValue: {showError: true},
    },
  ],
})
export class IniciarTramiteComponent implements OnInit {

  @ViewChild('stepper') private stepper: MatStepper;          
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator!: MatPaginator;

  base64_ci : string;
  base64_licencia : string;
  base64_nit: string;
  base64_suelo: string;
  base64_panoramica: string;
  base64_tecnico: string;
  base64_jurada: string;

  ufv: number;

  file_ci: File;
  file_licencia: File;
  file_nit: File;
  file_suelo: File;
  file_panoramica: File;
  file_tecnico: File;
  file_jurada: File;

  TipoTramites    : PubTipoTramite[] = [];
  Requisitos  : PubTipoTramiteRequisito [] = [];
  TipoPublicidades : PubTipoPublicidad [] = [];
  Publicidades: PubTramitePublicidad [] = [];

  parametroTramite: ParametroTramite;
  
  dataSource = new MatTableDataSource<PubTramitePublicidad>();
  displayedColumns: string[] = [    
    'tipovia',
    'direccion',
    'zona',
    'largo',
    'alto',
    'monto',
    'eliminar'
  ];

  form1 = this.fb.group({
    expediente:['1001', [Validators.required ]],  
    fecha_inicio: [ '08/07/2022', [Validators.required ] ],
    fecha_termino: [ '31/12/2022', [Validators.required ] ],    
    tipo_publicidad:['', [Validators.required ]],
    tiempo: [ '', [Validators.required ]],
  },
    { updateOn: "blur" }
  );

  form2 = this.fb.group({
    tipo_tramite:['', [Validators.required ]],    
    fotocopia_ci: [''],
    fotocopia_licencia: [''],
    fotocopia_nit: [''],
    fotocopia_uso_suelo: [''],
    fotografia_panoramica: [''],
    informe_tecnico: [''],    
  },
  { updateOn: "blur" }
  );

  form3 = this.fb.group({
    direccion: ['',], 
    tipovia:['', ],
    alto:['', ],
    largo:['',],
    monto: [''],
    zona: [''],
    acero: [''],
    papel: [''],
    aluminio: [''],
    acrilico: [''],
    madera: [''],
    carton: [''],
    plastico: [''],
    lona: [''],
    tela: [''],
    otro: [''],

  },
  { updateOn: "blur" }
  );

  constructor(
    private router:Router,
    private fb: FormBuilder,
    private datePipe: DatePipe,
    private authService: AuthService,
    private publicidadService: PublicidadService,
    private loadingBackdropService: LoadingBackdropService,
    private dateAdapter: DateAdapter<Date>,
    private datepipe: DatePipe,
    private modalService: NgbModal
  ) { 
    this.dateAdapter.setLocale('en-GB'); //dd/MM/yyyy
  }

  
  
  ngOnInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.cargarTipoTramites();    
    this.cargarTipoPublicidades();

    this.form2.get('tipo_tramite')?.valueChanges
      .pipe (
        tap( ( _ ) => {
          //this.miFormulario.get('pais')?.reset('');
          //this.cargando = true;
        }),
        switchMap( tipo_tramite => this.publicidadService.getTipoRequisitos(tipo_tramite))
      )
      .subscribe( data => {
        this.Requisitos= data.lista;
    });

    this.obtenerUFV();
  }

  @Output()
  dateChange: EventEmitter<MatDatepickerInputEvent<any>> = new EventEmitter();
  
  onDateChange(event: MatDatepickerInputEvent<Date>): void {
    if (this.form1.get('tiempo').value==1){
      this.dateChange.emit();
      let fecha_ini = event.value;
      let mes = fecha_ini.getMonth()+1;
      let dia = fecha_ini.getDate();
      const anho = fecha_ini.getFullYear()+1;
  
      let Mes=""; let Dia ="";
      if (mes<10)
        Mes="0"+mes;
      else
        Mes=mes+"";

      if (dia<10)
        Dia="0"+dia;
      else
        Dia=dia+"";
      
      let fecha_termino = anho + "-" + Mes + "-" + Dia;
      this.form1.controls['fecha_termino'].setValue(fecha_termino);
    }
  }
  
 
  cargarTipoTramites(){
    this.loadingBackdropService.show();

    this.publicidadService
      .getTipoTramites()
      .pipe(finalize(() => this.loadingBackdropService.hide()))
      .subscribe((data) => {       
        this.TipoTramites= data.lista
      });
  }

  cargarTipoPublicidades(){
    this.loadingBackdropService.show();

    this.publicidadService
      .getTipoPublicidades()
      .pipe(finalize(() => this.loadingBackdropService.hide()))
      .subscribe((data) => {
        this.TipoPublicidades= data.lista;
      });
  }

  cargarTipoRequisitos(idtipotramite: number){
    this.loadingBackdropService.show();

    this.publicidadService
      .getTipoRequisitos(idtipotramite)
      .pipe(finalize(() => this.loadingBackdropService.hide()))
      .subscribe((data) => {
        this.Requisitos= data.lista;
       
      });
      
  }

  obtenerUFV(){
    const fecha=new Date();
    let fecha_actual =this.datepipe.transform(fecha, 'dd/MM/yyyy');

    this.publicidadService
    .getUFV(fecha_actual)      
    .subscribe((data) => {
      if (data.codigo==0){
        this.ufv = parseFloat(data.mensaje);
      }   
      
    });
  }
 
  LimpiarTabla(){
    this.Publicidades = [];
    this.dataSource.data = this.Publicidades;
  }

  adjuntarPublicidad(){
    this.loadingBackdropService.show();

    let idtipopub:number = parseInt(this.form1.get('tipo_publicidad').value);
    let publicidad: PubTramitePublicidad = {
      direccion : this.form3.get('direccion').value,
      tipovia: this.form3.get('tipovia').value,
      alto: this.form3.get('alto').value,
      largo: this.form3.get('largo').value,
      monto: 0,      
      zona: this.form3.get('zona').value,
    }

    

    let parametroTramitePrecio : ParametroTramitePrecio = {
      idtipopublicidad: idtipopub,      
      tipovia: this.form3.get('tipovia').value ,
      zona: this.form3.get('zona').value,
    }

    this.publicidadService
      .obtenerPrecio(parametroTramitePrecio)
      .pipe(finalize(() => this.loadingBackdropService.hide()))
      .subscribe(
        data => {
          if (data.codigo == 0) {
            let alto: number = this.form3.get('alto').value;
            let largo: number = this.form3.get('largo').value;
            
            publicidad.monto=  alto * largo * data.precio * this.ufv /1.54;
            
            this.Publicidades.push(publicidad);
            this.dataSource.data = this.Publicidades;
            this.form3.get('direccion').reset();
            this.form3.get('tipovia').reset();
            this.form3.get('alto').reset();
            this.form3.get('largo').reset();
            this.form3.get('zona').reset();
            
          } else {
            Swal.fire({
              position:'center',
              icon:'warning',
              title: data.mensaje,
              showConfirmButton: false,
              timer: 3000
            })
          }
         
        },
        error => {
          console.log('error en el componente', error);
          Swal.fire({
            position:'center',
            icon:'error',
            title:'El servicio se encuentra fuera de línea',
            showConfirmButton: false,
            timer: 2000
          });
         
        }
      );

    
  }

  public onFileChange(i: number, event) {
    
    if (event.target.files[0].size>=500000){
      Swal.fire({
        position:'center',
        icon:'warning',
        title: 'El archivo debe ser inferior a 500 kb',
        showConfirmButton: false,
        timer: 3000
      });

      if (this.TipoTramites[i].idtipotramite==1) this.base64_ci = "";
      if (this.TipoTramites[i].idtipotramite==2) this.base64_licencia= "";
      if (this.TipoTramites[i].idtipotramite==3) this.base64_nit = "";
      if (this.TipoTramites[i].idtipotramite==4) this.base64_suelo = "";
      if (this.TipoTramites[i].idtipotramite==5) this.base64_panoramica= "";
      if (this.TipoTramites[i].idtipotramite==6) this.base64_tecnico = "";
      if (this.TipoTramites[i].idtipotramite==7) this.base64_jurada = "";

      this.Requisitos[i].error=true;
      return;
    }

    this.loadingBackdropService.show();

    this.Requisitos[i].error=false;
    this.convertFile(event.target.files[0]).subscribe(base64 => {

      if (this.TipoTramites[i].idtipotramite==1) this.base64_ci = base64;
      if (this.TipoTramites[i].idtipotramite==2) this.base64_licencia= base64;
      if (this.TipoTramites[i].idtipotramite==3) this.base64_nit =base64;
      if (this.TipoTramites[i].idtipotramite==4) this.base64_suelo = base64;
      if (this.TipoTramites[i].idtipotramite==5) this.base64_panoramica= base64;
      if (this.TipoTramites[i].idtipotramite==6) this.base64_tecnico = base64;
      if (this.TipoTramites[i].idtipotramite==7) this.base64_jurada = base64;


    });

    this.loadingBackdropService.hide();
  }

  convertFile(file : File) : Observable<string> {
    const result = new ReplaySubject<string>(1);
    const reader = new FileReader();
    reader.readAsBinaryString(file);
    reader.onload = (event) => result.next(btoa(event.target.result.toString()));
    return result;
  } 

  eliminarPublicidad(i:number){
    if (i > -1) {
      this.Publicidades.splice(i, 1);
      this.dataSource.data = this.Publicidades;
    }
  }

  public guardar(){

    let montoTotal: number=0;
    for (let publicidad of this.Publicidades){
      montoTotal = montoTotal +publicidad.monto;
    }

    if (montoTotal==0){
      Swal.fire({
        position:'center',
        icon:'warning',
        title: 'Falta escoger Publicidad y ubicaciones para determinar el monto',
        showConfirmButton: false,
        timer: 3000
      })
    }
    

    this.loadingBackdropService.show();
    
    let lista: PubTramiteRequisito[] =[];
   
    for (let requisito of this.Requisitos){
      let req : PubTramiteRequisito ={
        idrequisito: 0,
        base64: ""
      }
      req.idrequisito = requisito.idrequisito;
      if (requisito.idrequisito==1) req.base64 = this.base64_ci;
      if (requisito.idrequisito==2) req.base64 = this.base64_licencia;
      if (requisito.idrequisito==3) req.base64 = this.base64_nit;
      if (requisito.idrequisito==4) req.base64 = this.base64_suelo;
      if (requisito.idrequisito==5) req.base64 = this.base64_panoramica;
      if (requisito.idrequisito==6) req.base64 = this.base64_tecnico;
      if (requisito.idrequisito==7) req.base64 = this.base64_jurada;
      lista.push(req);
    }
    
    let parametroTramiteWF : ParametroTramiteWF = {
      idusuario_responsable: this.authService.getidUsuario(), //this.authService.getidUsuario()
      idproceso: 1,
      descripcion: "Tramite de Publicidad"
    }

    this.publicidadService
      .registrarTramiteWF(parametroTramiteWF)
      .pipe(finalize(() => this.loadingBackdropService.hide()))
      .subscribe(
        data => {
          if (data.codigo == 0) {
            
            this.parametroTramite = {
              idtramitema: 0,
              idusuario: this.authService.getidUsuario(),
              expediente: this.form1.value.expediente,
              idtiempo: this.form1.value.tiempo,
              fecha_inicio: this.datePipe.transform(this.form1.value.fecha_inicio,"dd/MM/yyyy"), 
              fecha_termino: this.datePipe.transform(this.form1.value.fecha_termino,"dd/MM/yyyy"),
              idtipopublicidad: this.form1.value.tipo_publicidad,      
              idtipotramite: this.form2.value.tipo_tramite,
              mat_acero: this.form3.value.acero,
              mat_papel: this.form3.value.papel,
              mat_aluminio: this.form3.value.aluminio,
              mat_acrilico: this.form3.value.acrilico,
              mat_madera: this.form3.value.madera,
              mat_carton: this.form3.value.carton,
              mat_plastico: this.form3.value.plastico,
              mat_lona: this.form3.value.lona,
              mat_tela: this.form3.value.tela,
              mat_otro: this.form3.value.otro,
              idflujo: data.idnuevo,
              listaPublicidad : this.Publicidades,
              listaRequisitos: lista
            }
        
            this.publicidadService
              .registrarTramite(this.parametroTramite)
              .pipe(finalize(() => this.loadingBackdropService.hide()))
              .subscribe(
                dataT => {
                  if (dataT.codigo == 0) {
                    this.router.navigate(['dashboard/home']);      
                    Swal.fire({
                      position:'center',
                      icon:'success',
                      title: dataT.mensaje,
                      allowEscapeKey: false,
                      confirmButtonText: 'OK',
                      confirmButtonColor: '#006b65',  
                    })
                  } else {
                    Swal.fire({
                      position:'center',
                      icon:'warning',
                      title: dataT.mensaje,
                      showConfirmButton: false,
                      timer: 3000
                    })
                  }
                 
                },
                error => {
                  console.log('error en el componente', error);
                  Swal.fire({
                    position:'center',
                    icon:'error',
                    title:'El servicio se encuentra fuera de línea',
                    showConfirmButton: false,
                    timer: 2000
                  });
                 
                }
              );

          } else {
            Swal.fire({
              position:'center',
              icon:'warning',
              title: data.mensaje,
              showConfirmButton: false,
              timer: 3000
            })
          }
         
        },
        error => {
          console.log('error en el componente', error);
          Swal.fire({
            position:'center',
            icon:'error',
            title:'El servicio se encuentra fuera de línea',
            showConfirmButton: false,
            timer: 2000
          });
         
        }
      );
        
  }


  openDialog(){
      
    const modalRef = this.modalService.open(ZonaMapaComponent, {      
      centered: true,
      backdrop: 'static',
      windowClass: 'my-class'
     });
    

    //modalRef.componentInstance.fromParent = tarea;
    modalRef.result.then((result) => {
      console.log(result);
    }, (reason) => {
    });
  }
}

