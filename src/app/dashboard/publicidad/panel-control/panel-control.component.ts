import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs';

import { PubTareas } from '../interfaces/publicidad.interfaces';
import { PublicidadService } from '../services/publicidad.service';
import { LoadingBackdropService } from '../../../core/services/loading-backdrop.service';
import { AuthService } from '../../../core/services/auth.service';
import { SeguimientoTareasComponent } from '../seguimiento-tareas/seguimiento-tareas.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-panel-control',
  templateUrl: './panel-control.component.html',
  styleUrls: ['./panel-control.component.css']
})
export class PanelControlComponent implements OnInit {
  tareas1 : PubTareas [] = [];
  tareas2 : PubTareas [] = [];
  tareas3 : PubTareas [] = [];
  tareas4 : PubTareas [] = [];
  tareas5 : PubTareas [] = [];
  tareas6 : PubTareas [] = [];
  tareas7 : PubTareas [] = [];

  idusuario: number;

  constructor(
    private loadingBackdropService: LoadingBackdropService,
    private publicidadService: PublicidadService,
    private authService: AuthService,
    private modalService: NgbModal
  ) { }

  ngOnInit(): void {
    this.idusuario= this.authService.getidUsuario(),
    this.loadTareas();
  }

  public loadTareas(){
    let tareas: PubTareas [];
    this.loadingBackdropService.show();

    this.publicidadService
      .listarTareasPorUsuario(this.idusuario)
      .pipe(finalize(() => this.loadingBackdropService.hide()))
      .subscribe((data) => {
        if (data.codigo=="0"){
          tareas = data.lista;
          this.tareas1 = [];
          this.tareas2 = [];
          this.tareas3 = [];
          this.tareas4 = [];
          this.tareas5 = [];
          this.tareas6 = [];
          this.tareas7 = [];

          for (var tarea of tareas){
            if (tarea.idtarea_actual==1){
              this.tareas1.push(tarea);
            }
            if (tarea.idtarea_actual==2){
              this.tareas2.push(tarea);
            }
            if (tarea.idtarea_actual==3){
              this.tareas3.push(tarea);
            }
            if (tarea.idtarea_actual==4){
              this.tareas4.push(tarea);
            }
            if (tarea.idtarea_actual==5){
              this.tareas5.push(tarea);
            }
            if (tarea.idtarea_actual==6){
              this.tareas6.push(tarea);
            }
            if (tarea.idtarea_actual==7){
              this.tareas7.push(tarea);              
            }
          }
        }
        
      });
  }

  openDialog(tarea: PubTareas) {
    
    const modalRef = this.modalService.open(SeguimientoTareasComponent, {      
      centered: true,
      backdrop: 'static',
      windowClass: 'my-class'
     });
    
    modalRef.componentInstance.fromParent = tarea;
    modalRef.result.then((result) => {
      console.log(result);
    }, (reason) => {
    });
  }

    
}
