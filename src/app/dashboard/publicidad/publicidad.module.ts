import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PublicidadRoutingModule } from './publicidad-routing.module';
import { IniciarTramiteComponent } from './iniciar-tramite/iniciar-tramite.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ListadoTramitesComponent } from './listado-tramites/listado-tramites.component';
import { VerTramiteComponent } from './ver-tramite/ver-tramite.component';
import { PanelControlComponent } from './panel-control/panel-control.component';
import { SeguimientoTareasComponent } from './seguimiento-tareas/seguimiento-tareas.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ZonaMapaComponent } from './zona-mapa/zona-mapa.component';
import { EmisionInformeTecnicoComponent } from './emision-informe-tecnico/emision-informe-tecnico.component';
import { AprobacionJefaturaComponent } from './aprobacion-jefatura/aprobacion-jefatura.component';
import { AprobacionDireccionComponent } from './aprobacion-direccion/aprobacion-direccion.component';
import { PagoCiudadanoComponent } from './pago-ciudadano/pago-ciudadano.component';
import { CambioResponsableComponent } from './cambio-responsable/cambio-responsable.component';
import { VerFlujoComponent } from './ver-flujo/ver-flujo.component';


@NgModule({  
  declarations: [
    ...PublicidadRoutingModule.components,
    IniciarTramiteComponent,
    ListadoTramitesComponent,
    VerTramiteComponent,
    PanelControlComponent,
    SeguimientoTareasComponent,
    ZonaMapaComponent,
    EmisionInformeTecnicoComponent,
    AprobacionJefaturaComponent,
    AprobacionDireccionComponent,
    PagoCiudadanoComponent,
    CambioResponsableComponent,
    VerFlujoComponent
    
  ],
  imports: [
    CommonModule,
    SharedModule,
    PublicidadRoutingModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  
})
export class PublicidadModule { }
