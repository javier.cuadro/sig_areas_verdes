import { DatePipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from 'src/app/core/services/auth.service';
import { PublicidadService } from '../services/publicidad.service';
import { PubFlujoTarea, PubTramiteFlujo } from '../interfaces/publicidad.interfaces';
import { PagoCiudadanoComponent } from '../pago-ciudadano/pago-ciudadano.component';

@Component({
  selector: 'app-ver-flujo',
  templateUrl: './ver-flujo.component.html',
  styles: [`
    tbody tr
    {
      background-color: rgba(204, 222, 255, 1);
      color: #000;
    }

    tbody tr:nth-child(odd)
    {
      background-color: #fff;
      color: #000;
    }
  `
  ]
})
export class VerFlujoComponent implements OnInit {
  @Input() fromParent;

  tramiteFlujo: PubTramiteFlujo;
  tareas: PubFlujoTarea[];


  constructor(
    private activeModal: NgbActiveModal,
    private authService: AuthService,
    private datePipe: DatePipe,
    private fb: FormBuilder,
    private modalService: NgbModal,
    private publicidadService: PublicidadService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.tramiteFlujo = this.fromParent;

    this.loadTareas(this.tramiteFlujo.idflujo);
  }

  loadTareas(idflujo: number){
    this.tareas  = [];
   
    this.publicidadService
      .detalleFlujo(idflujo)    
      .subscribe((data) => {
        if (data.codigo==0){          
          this.tareas = data.lista;
        }        
      });
  }

  pagar(){
    let modalRef: NgbModalRef = null;

    modalRef = this.modalService.open(PagoCiudadanoComponent, {      
      centered: true,
      backdrop: 'static',
      windowClass: 'my-class'
     });
    
    modalRef.componentInstance.fromParent = this.tramiteFlujo;
    
    modalRef.result.then((result) => {      
    }, (reason) => {
    });
  }
 
  cerrar(){
    this.activeModal.close();
  }

}
