import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-zona-mapa',
  templateUrl: './zona-mapa.component.html',
  styleUrls: ['./zona-mapa.component.scss']
})
export class ZonaMapaComponent implements OnInit {
  @Input() fromParent;
 

  constructor(
    private activeModal: NgbActiveModal,
    private fb: FormBuilder,
  ) { }

  ngOnInit(): void {
  }

  
}
