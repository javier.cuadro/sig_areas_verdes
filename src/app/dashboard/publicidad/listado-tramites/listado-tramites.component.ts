import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { finalize } from 'rxjs';
import { AuthService } from 'src/app/core/services/auth.service';

import { LoadingBackdropService } from 'src/app/core/services/loading-backdrop.service';
import { PubTramiteFlujo } from '../interfaces/publicidad.interfaces';
import { PublicidadService } from '../services/publicidad.service';
import { VerFlujoComponent } from '../ver-flujo/ver-flujo.component';
import { VerTramiteComponent } from '../ver-tramite/ver-tramite.component';

@Component({
  selector: 'app-listado-tramites',
  templateUrl: './listado-tramites.component.html',
  styles: [`
    .mat-header-row {
      height: 70%;
      width: 100%;
      background: #005173;
    }
    .mat-header-cell {      
      color: white;
    }

    .mat-icon-button {
      vertical-align: middle;
    }
    
    input:-moz-read-only { /* For Firefox */
      color: #003153;
      cursor: not-allowed;
    }

    input:read-only { 
      color: #003153;
      cursor: not-allowed;
    }
    .make-gold {
        background-color: gold
    }
    `
  ]
})
export class ListadoTramitesComponent implements OnInit {

  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator!: MatPaginator;
 
  idusuario: number;
  
  dataSource = new MatTableDataSource<PubTramiteFlujo>();
  displayedColumns: string[] = [
    'idflujo',
    'usuario_responsable',
    'nombre_tarea',
    'fecha_inicio',
    'fecha_termino',   
    'tipotramite',
    'monto',
    'flujo',
    'detalle'
  ];

  constructor(
    private authService: AuthService,
    private loadingBackdropService: LoadingBackdropService,
    private modalService: NgbModal,
    private publicidadService: PublicidadService,    
  ) { 
    this.idusuario= this.authService.getidUsuario();
  }

  ngOnInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.loadTramites();
  }

  loadTramites(){
  
    this.loadingBackdropService.show();

    this.publicidadService
      .getTramites(this.idusuario)
      .pipe(finalize(() => this.loadingBackdropService.hide()))
      .subscribe((data) => {       
        this.dataSource.data = data.lista;
      });
  }

  openDialogFlujo(tramiteFlujo: PubTramiteFlujo) {
    
    const modalRef = this.modalService.open(VerFlujoComponent, {      
      centered: true,
      backdrop: 'static',
      windowClass: 'my-class',
      size: 'lg'
     });
    
    modalRef.componentInstance.fromParent = tramiteFlujo;
    modalRef.result.then((result) => {
    
    }, (reason) => {
    });
  }

  openDialogTramite(idflujo: number) {

    let modalRef: NgbModalRef = null;

    modalRef = this.modalService.open(VerTramiteComponent, {      
      centered: true,
      backdrop: 'static',
      windowClass: 'my-class',
      size: 'xl'
     });    

    modalRef.componentInstance.fromParent = idflujo
    modalRef.result.then((result) => {      
    }, (reason) => {
    });
    
  }


}
