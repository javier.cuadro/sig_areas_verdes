import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IniciarTramiteComponent } from './iniciar-tramite/iniciar-tramite.component';
import { ListadoTramitesComponent } from './listado-tramites/listado-tramites.component';
import { VerTramiteComponent } from './ver-tramite/ver-tramite.component';
import { PanelControlComponent } from './panel-control/panel-control.component';
import { SeguimientoTareasComponent } from './seguimiento-tareas/seguimiento-tareas.component';
import { EmisionInformeTecnicoComponent } from './emision-informe-tecnico/emision-informe-tecnico.component';
import { CambioResponsableComponent } from './cambio-responsable/cambio-responsable.component';

const routes: Routes = [
  {
    path: '',
    component: ListadoTramitesComponent,
  },
  {
    path: 'panel-control',
    component: PanelControlComponent
  },  
  {
    path: 'registrar-tramite',
    component: IniciarTramiteComponent
  },
  {
    path: 'cambio-responsable',
    component: CambioResponsableComponent
  }, 
  {
    path: 'listado-tramites',
    component: ListadoTramitesComponent
  }, 
  {
    path: 'details/:idtramitema',
    component: VerTramiteComponent
  }, 
  {
    path: 'emision-informe-tecnico',
    component: EmisionInformeTecnicoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicidadRoutingModule {
  static components = [IniciarTramiteComponent, ListadoTramitesComponent, CambioResponsableComponent,
       VerTramiteComponent, PanelControlComponent, SeguimientoTareasComponent ];
 }
