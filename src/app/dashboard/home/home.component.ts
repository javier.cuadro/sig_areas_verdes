import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth.service';
import { DataService } from 'src/app/services/data.service';
import { DashboardService } from '../dashboard.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  static path(): any[] {
    throw new Error('Method not implemented.');
  }

  idpersona: number;
  cantlineamientos: number;
  cantplanos: number;

  constructor(
    private dataService:DataService,
    private authService: AuthService,
    private dashboardService: DashboardService,   
    ) { }

  ngOnInit() {
    this.dataService.nombreTramite="BIENVENIDO AL SISTEMA DE TRÁMITES EN LÍNEA";
    this.idpersona=this.authService.getidPersona();
    this.loadDashboard();
  }
  
  loadDashboard(){
    this.dashboardService
      .dashboard(this.idpersona)      
      .subscribe((data) => {
        if (data.codigo==0){
          this.cantlineamientos = data.cantlineamientos;
          this.cantplanos = data.cantplanos;
        } else {
          this.cantlineamientos = 0;
          this.cantplanos = 0;
        }
      });
  } 
 
}
