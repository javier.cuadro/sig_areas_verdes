import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent } from './dashboard.component';
import { HomeComponent } from './home/home.component';
import { LineamientosComponent } from './lineamientos/lineamientos.component';
import { PlanosComponent } from './planos/planos.component';
import { CambiarPasswordComponent } from './cambiar-password/cambiar-password.component';
import { PerfilFotosComponent } from './perfil-fotos/perfil-fotos.component';

export const DashboardRoutes: Routes = [
    {
        path: '',
        component: DashboardComponent,
        children: [
            {
                path: '',
                redirectTo: 'home',
                pathMatch: 'full'
            },
            {
                path: 'home',
                //loadChildren: () => import('./visores/visores.module').then(m => m.VisoresModule )
                component: HomeComponent
            },            
            {
                path: 'visores',
                loadChildren: () => import('./visores/visores.module').then(m => m.VisoresModule ),
            },
            {
                path: 'cambio_password',
                component: CambiarPasswordComponent
            },
            {
                path: 'perfil_fotos',
                component: PerfilFotosComponent
            },              
            
            {
                path: 'lineamientos',
                component: LineamientosComponent
            },
            {
                path: 'planos',
                component: PlanosComponent
            },
            {
                path: 'planosuelo',
                loadChildren: () => import('./planosuelo/planosuelo.module').then(m => m.PlanosueloModule ),
            },
            {
                path: 'prorrateos',
                loadChildren: () => import('./prorrateos/prorrateos.module').then(m => m.ProrrateosModule ),
            },
            {
                path: 'jurisdiccion',
                loadChildren: () => import('./jurisdiccion/jurisdiccion.module').then(m => m.JurisdiccionModule ),
            },           
            {
                path: 'lcn',
                loadChildren: () => import('./lcn/lcn.module').then(m => m.LcnModule)
            },
            {
                path: 'publicidad',
                loadChildren: () => import('./publicidad/publicidad.module').then(m => m.PublicidadModule)
            },
            {
                path: 'declaracion-jurada',
                loadChildren: () => import('./declaracion-jurada/declaracion-jurada.module').then(m => m.DeclaracionJuradaModule)
            },
            {
              path: 'settings',
              loadChildren: () => import('./settings/settings.module').then(m => m.SettingsModule)
          },

            /*path: 'planosuelo-edificio',
      component: PlanosueloEdificioComponent
    },  

    {
      path: 'planosuelo-terreno',
      component: PlanosueloTerrenoComponent
    },  

    {
      path: 'planosuelo-urbanizacion-mixta',*/

            
        ]
    }
];

@NgModule({
    imports: [ RouterModule.forChild(DashboardRoutes) ],
    exports: [ RouterModule ]
})

export class DashboarRoutingModule {
  static components = [DashboardComponent, HomeComponent];
}
