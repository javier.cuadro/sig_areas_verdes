import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LcnRoutingModule } from './lcn-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';





@NgModule({  
  imports: [
    CommonModule,
    LcnRoutingModule,
    SharedModule,
    LeafletModule,
    

  ],
  declarations: [
    ...LcnRoutingModule.components
  ],
})
export class LcnModule { }
