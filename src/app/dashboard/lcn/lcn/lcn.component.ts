import { Component,ElementRef,Inject,OnInit, ViewChild } from '@angular/core';
//import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {MatDialog, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {FormControl} from '@angular/forms';

import * as htmlToImage from 'html-to-image';


import { MapasService } from 'src/app/services/mapas.service';
import { Marcador } from 'src/app/classes/marcador.class';

import { LeafletModule } from '@asymmetrik/ngx-leaflet';
//import * as L from 'leaflet';

import "leaflet";
declare let L;

import * as Map from 'leaflet';
//declare let M;
//import * as L from 'leaflet';
import "leaflet-easybutton";


import { CallesService } from 'src/app/services/calles.service';

import { Calle } from 'src/app/classes/calle.class';

import { Router } from '@angular/router';
import { Actividades, Categorias, Clasifi_Activi_Regla, resultadoLineamiento, Subcategorias } from 'src/app/interfaces/lcn.interfaces';
import { PlanoSuelo } from 'src/app/classes/planosuelo';
import { LCN } from 'src/app/classes/lcn';
import { TipoEdificacion } from 'src/app/classes/tipoedificacion';
import { ResizeService } from 'src/app/services/resize.service';

import { fromEvent, Observable, Subscription, switchMap, tap } from "rxjs";


import "leaflet-multicontrol";
import { Clasificacion, GrupoClasificacion } from '../../../interfaces/lcn.interfaces';
import { LcnService } from 'src/app/services/lcn.service';
import { DataService } from 'src/app/services/data.service';
import { DatePipe, DOCUMENT } from '@angular/common';
import { ActualizarEstado } from 'src/app/classes/actualizarEstado';
import { ClsActualizarEstadoPago } from 'src/app/classes/clsActualizarEstadopago';
import { URLReporte } from 'src/app/classes/urlReporte';
import { MatStepper } from '@angular/material/stepper';


//import { DatePipe } from '@angular/common';
import { marker } from 'leaflet';
import { QrbnbService } from 'src/app/services/qrbnb.service';
import { respuestaQR } from 'src/app/classes/respuestaQr';

import Swal from 'sweetalert2';





@Component({
  selector: 'app-lcn',
  templateUrl: './lcn.component.html',
  styleUrls: ['./lcn.component.css']
})
export class LcnComponent implements OnInit  {
  @ViewChild('map_export') mapElement: ElementRef;
  map_export;//: Map.Map;
  marker_export;

  @ViewChild('stepper') stepper: MatStepper;


  ProcesoCompleto=0;
  ourCustomControl;
  groupBuscador;
 resBusquedaCoordenada;
 resBuscarGeografico;

  displayStyle = "none";

  private resizeSubscription: Subscription;
  private resizeObservable$: Observable<Event>;
  private resizeSubscription$: Subscription;

  firstFormGroup = this._formBuilder.group({
    firstCtrl: ['', Validators.required],
  });
  secondFormGroup = this._formBuilder.group({
    secondCtrl: ['', Validators.required],
  });
  secondFormGroup5 = this._formBuilder.group({
    secondCtrl: ['', Validators.required],
  });

  //FormGroupBusqueda: FormGroup;

  FormGroupBusqueda = this.fbBusqueda.group({

    busdm:[''],
    busuv:[''],
    busmz:[''],
    buslt:[''],
    busx:[''],
    busy:[''],
    firstCtrl: ['', Validators.required]
  });

  /*FormGroupBusqueda: FormGroup = this.fbBusqueda.group({

    busdm:[''],
    busuv:[''],
    busmz:[''],
    buslt:[''],
    busx:[''],
    busy:[''],
    firstCtrl: ['', Validators.required],
  })*/

  isLinear = true;
  ContadorMapa:number=0;

   public geo_json_data;
    title = 'Sig alcaldia';
    json;
    layerFarmacias;
    map;
    //map_export;
    marcadores : Marcador[]=[];
    marker;
    marker_circle;
    marker_circleExport;
    i=0;


    lat = -17.7876515;
    lng = -63.1825049;
    lat1=0;
    lng1=0;
    calle= new Calle(0,'','','','');

///////////////////////////////////////////construccion
clasificacion= this.fb_construccion.control('');
subcategoria= this.fb_construccion.control('');
activivdad= this.fb_construccion.control('');

miFormulario_Construccion: FormGroup = this.fb_construccion.group({
  'clasificacion':['',Validators.required],
  'subcategoria':['',Validators.required],
  'actividad':['',Validators.required]

})


clasificaciones    :  Clasificacion[]=[];
subcategorias :  Subcategorias[]=[];
actividades   :  Actividades[]=[];

cargando: boolean= false;

//marcadores : Marcador[]=[];
tipoEdificacion: TipoEdificacion;


//////////////////////////////////////////////////////

/*planoSuelo:PlanoSuelo;
lineamiento:LCN;*/

   ////////////////////////////////////////////////// Catastro

   miFormulario_Catastro: FormGroup = this.fb.group({

    numerplano: [ '1', Validators.required ],
	  superficietitulo: [ '300', Validators.required ],
	  superficiemensura: [ '300', Validators.required ],
	  zona: [ 'NORTE', Validators.required ],
	  manzana: [ '6', Validators.required ],
	  distrito: [ '5', Validators.required ],
	  lote: [ '9', Validators.required ],
	  tipousosuelo: [ 'VIVIENDA', Validators.required ],
	  //ubicacionlote: [ '', Validators.required ],
    /*
	  tipovia: [ '', Validators.required ],
	  idpersona:[ '', Validators.required ],
    anchoVia1:[ '', Validators.required ]*/
  })


  //categorias    :  Categorias[]=[];
  //subcategorias :  Subcategorias[]=[];
  //actividades   :  Actividades[]=[];
  Clasifi_Activi_Regla :Clasifi_Activi_Regla;//[]=[];


  respuestaPlanoSuelo:any;
  respuestaLineamiento:any;



  //marcadores : Marcador[]=[];
  anchoVia:string;
  planoSuelo:PlanoSuelo;
  lineamiento:LCN;
  idplanoSuelo:number;
  idtipoLineamiento:number;
  idActividad:number;
  idgrupoLineamiento:number;
  idClasifi_Activi_Regla:number;
////////////////////////////Pago

actualizarEstado:ActualizarEstado

resultadolineamiento:resultadoLineamiento;
idLineamiento :number;
ActualizarEstadoPagoLineamiento:ClsActualizarEstadoPago;
urlReporte:any;
respuestaQr:any= new respuestaQR(0,'','','');
respuestaConsultaQr:any;


////////////////////////Pago QR
estadoqr = new FormControl('');
imagenqr = new FormControl('');
objtoken:any;
objqr:any;
token:any;
public base64:string='';
public idqr:string='';
identificadorDeTemporizador;
public identificadorDeTemporizador_tiempo:number=0;



  //constructor(@Inject(DOCUMENT) private document: Document)

   /////////////////////////////////////////////////
   constructor(
    private dataService:DataService,
    private resizeService: ResizeService,
    private fbBusqueda: FormBuilder ,
    private fb_construccion: FormBuilder,
    private fb: FormBuilder,
    private _formBuilder: FormBuilder,
    private mapaService:MapasService,
    private calleService:CallesService,
    private router:Router,
    private lcnService:LcnService,
    private datePipe: DatePipe,
    private qr:QrbnbService
    ) {


    //console.log('ss');
  }

   ngOnInit() {
    this.base64="iVBORw0KGgoAAAANSUhEUgAAAJoAAACYCAIAAACTVWdLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAVPSURBVHhe7ZrtkesgDEW3rhSUelJNmkkx+zCIDyFh8HM2zty5589usBCCYzvYk59fAgR1QkGdUFAnFNQJBXVCQZ1QUCcU1AkFdUJBnVBQJxTUCQV1QkGdUFAnFNQJBXVCQZ1QUCcU1AkFdUJBnVBQJxTUCQV1QkGdUFAnFNQJBXVCQZ1QUCcU1AkFdUJBnVBQJxTUCQV1QkGdUFAnFNQJBXVCQZ1QUCcU1AkFdUJBnVBQJxTUCQV1QkGdUFAnFNQJBXVCQZ1QUCcU1AkFdUJBnVBQJxQjnc/7z4j7U2I2Xs/7Tdp/brfH8yXtgwy3272ETIcYBLQ5Cq9HLkOVV5PoZmmVRm+gMEo7G+H1fNxvzYRDJSamxYmXIxtHJrjGKZ11DSvl6DjDziIKKWInoCki0laiD9Ukbbu0Tiu5PZp19aYbUDEt0/gDE1xkonM3qZQr51K4TmOXXK3NEE7VNL8UMh3CDchr5Cz07Z7uFKqDJInUAzq1M1C565RxysDl0rExLXngnfj1Ca7yDp153Oka6cbpEIOA1NzOVmw+XlJR20OSJI6UWtY1Ne8WY5rX4ndjPq4zB4Wht1OwG91meOvV2eatEq3PnOSe/soi6dSDSiRZ7DIsNh0wa78U7wY5E1zn8HenLlyHqe/wYYZc6HSIcQZVhVJofEqSUJgsU+xaWnVM/FRJ7U0HPfmIGTCxFC/jejg9VzipMxA3b3JwIwd4GYLvul2cDuEGmA1nt5798kqS7XMWGv5vWnWMIrXHcpp/NW/X6e6oVzl1s1XUR5bUaZrhWEDeaTVbi0h21JPTqiQ5R771StCgEknd6DQh+YDxNolPB1TQaIKHOKHThrTzn2c4HFDMNYs3slm6dUnko+DHZFSzfOi85eH7rn3OEJf2FypNP643wWOcuTpLaelsKjsdNf9xhv8IMPOVBj191c8kkYaItDqVdNu2QO5XLx7z4KHQ8fXetSEd7LhnhR7+7mxHKoO35MPOGmmmQ3gZ9Hxdm7qjTdJULa3jSlRmd7p29MIgvhYzn+BRTukMbGddKXrb6ZRjXq2K6RCDDLlfuCv4NlVPL0lZMmn1KlG7tkLc96n57n/Pmfh4jeZu0wnu5vYY6SR/xmsz3Ct8E9QJBXVCQZ1QUCcU1AkFdULx1zpXHum8GMHu593gbePfP6PVh3j/oUA9MfuPkOapcfJTkuu5QmeiPiSf1xnRse0rGZNl9L6mfXAfxPzHs/0H+ZDOdkHNq04nZowNtq9XA0mG+3OTIsp5+1oid2K+WegFOgN5sVLzSZ0B0yg2935u0g0XApv77aCkQ5VewTU6s890oh9aJBvsXJ2NROPTEWwYVpQOfO/1eZFOtS4SY/GWbRg8MtbrWzEyjFk5F64EQme3Le3WvPvYq5LDmRjWxxSoc5u+mb8si9K5tkbz4M5PQbr0RsY6nUGGnr+Ei3Sq5rmhhmnwyGbpIxl6J15JXUxOvVbpFVyg02xdpoZaZsGy5FqE7pStNO8e4guD2Cgx0oMPKpq8LJa6KuMYZ+kmOl2bpldQEz8bgr0UUaV3fLPMa3T2L9TeqHNg0+tWL8iNtZd8NuTL+Gud5KNQJxTUCQV1QkGdUFAnFNQJBXVCQZ1QUCcU1AkFdUJBnVBQJxTUCQV1QkGdUFAnFNQJBXVCQZ1QUCcU1AkFdUJBnVBQJxTUCQV1QkGdUFAnFNQJBXVCQZ1QUCcU1AkFdUJBnVBQJxTUCQV1QkGdUFAnFNQJBXVCQZ1QUCcU1AkFdUJBnVBQJxTUCQV1QkGdUFAnFNQJBXVCQZ1QUCcU1AkFdUJBnUD8/v4DngF37uJ5l38AAAAASUVORK5CYII=";

    this.tipoEdificacion= new TipoEdificacion(0,0,0);
    this.planoSuelo= new PlanoSuelo('','','','','','','','','','',0,"",0);
    this.lineamiento= new LCN("","","","",1,1,1,1,1,1,0,0);
    this.resizeSubscription = this.resizeService.onResize$
      .subscribe(size => console.log(size));


      this.resizeObservable$ = fromEvent(window, 'resize')
      this.resizeSubscription$ = this.resizeObservable$.subscribe( evt => {
        //console.log('event: ', evt)
      })
/////////////////////////////////////////////////


      this.miFormulario_Construccion.reset({
        clasificacion:['',Validators.required],
        subcategoria:['',Validators.required]
        //actividades:['',Validators.required]

      });


      //this.lcnService.getGrupoCategorias(this.marcadores[0].grupolineamiento).subscribe(grupoCategorias=>{
      //this.lcnService.getGrupoCategorias(24).subscribe(grupoCategorias=>{

        //console.log(grupoCategorias);
        //this.clasificaciones=grupoCategorias;

      //});


      this.miFormulario_Construccion.get('clasificacion')?.valueChanges
      .pipe(
        ///esto es para resetear
        tap(( _ )=>{
          this.miFormulario_Construccion.get('subcategoria')?.reset('');
          //this.miFormulario.get('frontera')?.disable();
          //alert("clasificacion");
          this.cargando=true;
          //alert("clasificacion")  ;

        }),
        switchMap(clasificacion=> this.lcnService.getSubcategorias(this.idgrupoLineamiento,clasificacion))
      )
      .subscribe(subcategorias=>{
        //console.log(subcategorias);
        this.subcategorias  = subcategorias;
        this.cargando=false;

      });
  /////////////////////////////////////////////
    this.miFormulario_Construccion.get('subcategoria')?.valueChanges
    .pipe(
      ///esto es para resetear
      tap(( _ )=>{
        this.miFormulario_Construccion.get('actividad')?.reset('');
        this.cargando=true;
        //alert("subcategoria")  ;
      }),
      switchMap(subcategoria=> this.lcnService.getActividadesPorCodigo(this.idgrupoLineamiento,parseInt(this.miFormulario_Construccion.get('clasificacion')?.value),subcategoria)  )
    )
    .subscribe(actividades=>{
      console.log(actividades);
      //alert("actividad")  ;
      this.actividades  = actividades;
      this.cargando=false;




    //localStorage.setItem('tipoEdificacion', JSON.stringify(this.tipoEdificacion));

    /*if(localStorage.getItem('tipoEdificacion')){
      this.tipoEdificacion = JSON.parse(localStorage.getItem('tipoEdificacion'));
      this.idActividad=this.tipoEdificacion.idactividad;

      console.log(this.tipoEdificacion);
    }*/

    });

    //////////////////////
    this.urlReporte=new URLReporte(0,'','');


  }
  ngOnDestroy() {
    if (this.resizeSubscription) {
      this.resizeSubscription.unsubscribe();
    }
    this.resizeSubscription$.unsubscribe()
  }





    geoJSon;//:GeoJSON.GeoJSON;

    toppings = new FormControl('');

  toppingList: string[] = ['Extra cheese', 'Mushroom', 'Onion', 'Pepperoni', 'Sausage', 'Tomato'];


    // Define our base layers so we can reference them multiple times
    streetMaps =L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      detectRetina: true,
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
      maxZoom: 19,
    });

    esri = L.tileLayer(
      'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
      maxZoom: 19,
      minZoom: 1,
      attribution: 'Tiles © Esri',
    });

    wMaps = L.tileLayer('http://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png', {
      detectRetina: true,
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    });
    etiquetasEsri= L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/toner-labels/{z}/{x}/{y}{r}.{ext}',{
      attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
      subdomains: 'abcd',
      minZoom: 0,
      maxZoom: 19,
      //ext: 'png'
      }
    );

    //http://207.244.229.255:9080/geoserver/gamsc
    //http://207.244.229.255:9080/geoserver/gamsc
    wmsLayers = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
        layers: "ejes_vias",
        format: 'image/png',
        transparent: true,
        version: '1.3.0',
        attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
      }
    );
    wmsLayers_uv = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
        layers: "uv",
        format: 'image/png',
        transparent: true,
        version: '1.3.0',
        attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
      }
    );

    wms_lotes = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
    layers: "lotes_cartografia_yina",
    format: 'image/png',
    maxZoom: 19,
    transparent: true,
    version: '1.3.0',
    attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
  }
);


wms_uv_yina = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
    layers: "unidades_vecinales_yina",
    format: 'image/png',
    maxZoom: 19,
    transparent: true,
    version: '1.3.0',
    attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
  }
);
wms_manzanas_yina = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
    layers: "manzanas_yina",
    format: 'image/png',
    maxZoom: 19,
    transparent: true,
    version: '1.3.0',
    attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
  }
);




    // Layers control object with our two base layers and the three overlay layers
    /*layersControl = {
      baseLayers: {
        'Street Maps': this.streetMaps,
        'Wikimedia Maps': this.wMaps
      },
      overlays: {
        'Mt. Rainier Summit': this.summit,
        'Mt. Rainier Paradise Start': this.paradise,
        'Mt. Rainier Climb Route': this.route
      }
    };*/



    options = {
      layers: [ this.streetMaps],
      zoom: 13,
      center: L.latLng([ -17.7876515, -63.1825049 ])

      //lat = -17.7876515;
      //lng = -63.1825049;
    };

    options1 = {
      layers: [ this.esri],
      zoom: 18,
      center: L.latLng([ -17.7876515, -63.1825049 ]),
        attributionControl: false,
      zoomControl: false,
      fadeAnimation: false,
      zoomAnimation: false



      //lat = -17.7876515;
      //lng = -63.1825049;
    };






    /*geoJSON = {
      id: 'geoJSON',
      name: 'Geo JSON Polygon',
      enabled: true,
      layer: L.geoJSON(this.geo_json_data)
    };*/
     getColor(d) {
      return d > 100000000 ? '#800026' :
      d > 50000000 ? '#BD0026' :
      d > 4 ? '#EFEFEF' :
      d > 3 ? '#000287' :
      d > 2 ? '#ccff00' :
      d > 1 ? '#d9880f' :
      d > 0 ? '#0b5e05' :
      '#FFEDA0'}



    style = feature => {
      return {
        weight: 3,
        opacity: 1,
        //color: 'blue',
        dashArray: '1',
        fillOpacity: 0.7,
        color: this.getColor(feature.properties.tiposvia)
      };
    }

     popup(feature, layer) {
      if (feature.properties && feature.properties.nombrevia) {
        layer.bindPopup(feature.properties.nombrevia);
      }

      /*for(let i in layer.features)
      layer.features[i].properties.color = this.getColor( layer.features[i].properties.jerarquia );*/
    }



    /*resetHighlight = e => {
      geojson.resetStyle(e.target);
      info.update();
    }*/

    //zoomToFeature = e => {
  //    this.map.fitBounds(e.target.getBounds());
  //  }


    /*onEachFeature = (feature, layer) => {
      layer.on({
        //mouseover: this.highlightFeature,
        //mouseout: this.resetFeature,
        //click: this.zoomToFeature
        html = "",
        for (prop in feature.properties) {
            html += prop + ": " + feature.properties[prop] + "<br>";
        };
        layer.bindPopup(html);
      });
    }*/

     onEachFeature(feature, layer) {
      //if (feature.properties && feature.properties.popupContent) {
          //layer.bindPopup(feature.properties.popupContent);
          for(let i in layer.features){
            layer.features[i].properties.color = this.getColor( layer.features[i].properties.tiposvia );
            //console.log(layer.features[i].properties.jerarquia);
          }

          /*let html = "";
        for (let prop in feature.properties) {
            html += prop + ": " + feature.properties[prop] + "<br>";
        };
        layer.bindPopup(html);         */
      //}
      }





    onMapReady(map) {

      localStorage.setItem('marcadores', '' );
      this.dataService.nombreTramite="TRÁMITE: LINEAMIENTO DE CONSTRUCCIÓN";






    //this.IDMAP.nativeElement.remove();





      //alert('aaa');
      L.Icon.Default.imagePath = "assets/leaflet/";
      //L.Icon.Default.imagePath=
      let geojson;
      //let info =  L.control;
      this.map = map;


    var fitBoundsSouthWest = new L.LatLng(-17.606754259255016, -63.394996841180316);
    var fitBoundsNorthEast = new L.LatLng(-17.9500307832854, -62.98270820363417);
    var fitBoundsArea = new L.LatLngBounds(fitBoundsSouthWest, fitBoundsNorthEast);
    this.map.setMaxBounds(fitBoundsArea);
      //console.log(SL);
      //console.log('ss');

      //////////////////////////////
      if(localStorage.getItem('marcadores')){
        this.marcadores = JSON.parse(localStorage.getItem('marcadores'));
        //alert('Entro');

      }

      for(let marcador of this.marcadores){
        this.marker  = L.marker([marcador.lat, marcador.lng]).bindPopup('Punto');
        //this.marker.push(LamMarker);
        //LamMarker.addTo(this.map);
        //this.map.fitBounds(this.marker);
        this.map.addLayer(this.marker);
        //map.fitBounds( latlng.layer.getBounds() );
        //var zoom = map.getBoundsZoom(this.marker.layer.getBounds());
        //map.setView(this.marker.latlng, 12); // access the zoom

        this.i=1;
      }
      ////////////////////////////////////////


      


      const legend = new (L.Control.extend({
        options: { position: 'bottomright' }
      }));


      legend.onAdd = function (map) {
        const div = L.DomUtil.create('div', 'Leyenda');

        div.innerHTML = '<div><b>Referencias</b></div>';

          div.innerHTML += '<img alt="legend" src="../../../assets/leyenda.png " width="250" height="80" />';
          //div.innerHTML += '<img alt="legend" src="../.. " width="127" height="80" />';



        return div;
      };
      legend.addTo(this.map);



      var baseMaps = {

        "Detallado": this.wMaps,
        "Predeterminado": this.streetMaps,
        "UV": this.wmsLayers_uv,
        //"Lineamientos":this.wmsLayers,
        "Satélite": this.esri
    };

    var overlayMaps = {
      "Lineamientos": this.wmsLayers
    };

      //this.esri,this.wMaps
    /*L.control.layers(baseMaps,overlayMaps,{
      position: 'topright', // 'topleft', 'bottomleft', 'bottomright'
      collapsed: false // true
    }).addTo(this.map);*/



   /* var baseMaps = {

        "Detallado": this.wMaps,
        "Predeterminado": this.streetMaps,
        "UV": this.wmsLayers_uv,
        //"Lineamientos":this.wmsLayers,
        "Satélite": this.esri
    };

    var overlayMaps = {
      "Lineamientos": this.wmsLayers
    };
      //this.esri,this.wMaps
    L.control.layers(baseMaps,overlayMaps,{
      position: 'topright', // 'topleft', 'bottomleft', 'bottomright'
      collapsed: false // true
    }).addTo(this.map);*/


      this.mapaService.obtenerMapas(0,0).subscribe((data)=>{
        this.geo_json_data = data;

        /*this.geoJSon  = L.geoJSON(this.geo_json_data, {
          //style: this.style,
          //onEachFeature: this.popup,
        }).addTo(map);          */






        L.control.scale().addTo(this.map);

        var markersLayer = new L.LayerGroup();	//layer contain searched elements
        map.addLayer(markersLayer);

     /*let searchText=new L.control.search({
        position:'topleft',
        layer: markersLayer,
        initial: false,
        zoom: 12,
        circleLocation: false,
        marker: false,
        textPlaceholder: 'Búsqueda de UV',
      });


      searchText.on('search:locationfound', function(e) {
            //console.log('search:locationfound', );

            //map.removeLayer(this._markerSearch)
            alert('search:locationfound');

            e.layer.setStyle({fillColor: '#3f0', color: '#0f0'});
            if(e.layer._popup)
              e.layer.openPopup();

          }).on('search:collapsed', function(e) {


            /*this..eachLayer(function(layer) {	//restore feature color
              this.geoJSon.resetStyle(layer);
            });
      });
      map.addControl( searchText );*/



      let data1 = [
        {"loc":[41.575330,13.102411], "title":"aquamarine"},
        {"loc":[41.575730,13.002411], "title":"black"},
        {"loc":[41.807149,13.162994], "title":"blue"},
        {"loc":[41.507149,13.172994], "title":"chocolate"},
        {"loc":[41.847149,14.132994], "title":"coral"},
        {"loc":[41.219190,13.062145], "title":"cyan"},
        {"loc":[41.344190,13.242145], "title":"darkblue"},
        {"loc":[41.679190,13.122145], "title":"Darkred"},
        {"loc":[41.329190,13.192145], "title":"Darkgray"},
        {"loc":[41.379290,13.122545], "title":"dodgerblue"},
        {"loc":[41.409190,13.362145], "title":"gray"},
        {"loc":[41.794008,12.583884], "title":"green"},
        {"loc":[41.805008,12.982884], "title":"greenyellow"},
        {"loc":[41.536175,13.273590], "title":"red"},
        {"loc":[41.516175,13.373590], "title":"rosybrown"},
        {"loc":[41.506175,13.273590], "title":"royalblue"},
        {"loc":[41.836175,13.673590], "title":"salmon"},
        {"loc":[41.796175,13.570590], "title":"seagreen"},
        {"loc":[41.436175,13.573590], "title":"seashell"},
        {"loc":[41.336175,13.973590], "title":"silver"},
        {"loc":[41.236175,13.273590], "title":"skyblue"},
        {"loc":[41.546175,13.473590], "title":"yellow"},
        {"loc":[41.239190,13.032145], "title":"white"}
      ];

      //var map = new L.Map('map', {zoom: 9, center: new L.latLng(data[0].loc) });	//set center from first location




      /*var controlSearch = new L.control.search({
        position:'topright',
        layer: markersLayer,
        initial: false,
        zoom: 12,
        marker: false
      });

      map.addControl( controlSearch );*/
      //console.log(L.geoJSON(this.geo_json_data),);


      //markersLayer.addLayer(overlayMaps);

      ////////////populate map with markers from sample data
      for(let i in data1) {
        let title = data1[i].title,	//value searched
           loc = data1[i].loc,		//position found
          marker = new L.Marker( L.latLng(41.436175,13.573590), {title: title} );//se property searched
        marker.bindPopup('title: '+ title );
        markersLayer.addLayer(marker);
      }
        //L.control.search().addTo(this.map);


      /*  var controlSearch = new L.Control.Search({
          position:'topright',
          layer: markersLayer,
          initial: false,
          zoom: 12,
          marker: false
        });

        map.addControl( controlSearch );
        */

        /*(L.Control as any).search({
          position:'topleft',
          layer: this.geoJSon,
          initial: false,
          zoom: 12,
          marker: false
        }).addTo(this.map);*/

        //L.control.minimap(osm2, { toggleDisplay: true }).addTo(map);

        //legend.addTo(this.map);


        //var searchLayer = L.geoJSON().addTo(map);
        //... adding data in searchLayer ...
        //L.map('map', { leafletSearch: {layer: searchLayer} });


       /* var searchControl = new L.Control.Search({
          layer: this.geoJSon,
          propertyName: 'name',
          marker: false,

        });*/
         //L.control.search().setLayer(this.geoJSon);




         // Add a Search bar to the map
   /*var searchControl = new L.control.search({
		layer: this.geoJSon,
		propertyName: 'lad16nm',
		marker: false,
		moveToLocation: function(latlng, title, map) {
			var zoom = map.getBoundsZoom(latlng.layer.getBounds()) - 1;
           	map.setView(latlng, zoom); // access the zoom
		}
	});

    searchControl.on('search:locationfound', function(e) {
		e.layer.setStyle({weight: "2", opacity: "1"});
	});

	map.addControl( searchControl );  //inizialize search control
  */





        /*var controlSearch =  L.control.search({
          layer: new L.LayerGroup()
      }).on('search_expanded', function () {
          console.log('search_expanded!')
      }).addTo(map);/







        /*searchControl.on('search:locationfound', function(e) {

          //console.log('search:locationfound', );

          //map.removeLayer(this._markerSearch)

          e.layer.setStyle({fillColor: '#3f0', color: '#0f0'});
          if(e.layer._popup)
            e.layer.openPopup();

        }).on('search:collapsed', function(e) {

          this.geoJSon.eachLayer(function(layer) {	//restore feature color
            this.geoJSon.resetStyle(layer);
          });
        });

        map.addControl( searchControl );  //inizialize search control*/

        //minimap(osm2, { toggleDisplay: true }).addTo(map);


        //////////////
        //M.StreetLabels.default({
        /*this.streetLabelsRenderer =  SL.StreetLabels({
          collisionFlg : true,
          propertyName : 'nombre',
          showLabelIf: function(layer) {
            return true; //layer.properties.type == "primary";
          },
          fontStyle: {
            dynamicFontSize: false,
            fontSize: 10,
            fontSizeUnit: "px",
            lineWidth: 4.0,
            fillStyle: "black",
            strokeStyle: "white",
          },
        })*/

        // Create a new map and attach the renderer created above:
        /* map = new M.Map('map', {
          renderer : this.streetLabelsRenderer, //Custom Canvas Renderer
        });*/



        //L.multiControl(overlayMaps1, {position:'topright', label: 'Mapas'}).addTo(map);

      });

      let overlayMaps1 = [
        //{name:"general" , layer: this.streetMaps},
        {name:"Satélite" , layer: this.esri},
        {name:"Uv" , layer:this.wms_uv_yina},
        {name:"Manzanas" , layer:this.wms_manzanas_yina},
        {name:"Lotes" , layer:this.wms_lotes},
        {name:"Vías" , layer:this.wmsLayers},
        //{name:"Manzanas_yina" , layer:this.geoJSon},
      ];
      let mc=L.multiControl(overlayMaps1, {position:'topright', label: 'Mapas'}).addTo(map);

      let ebSearch=L.easyButton('fa-search', function(btn, map){
        //helloPopup.setLatLng(map.getCenter()).openOn(map);
        let b = document.getElementById("sidebar");
            b.click();
      }).addTo(map );

      let ebmc=L.easyButton('fa-globe', function(btn, map){
        //helloPopup.setLatLng(map.getCenter()).openOn(map);
            mc.toggle();
      }).addTo(map );













      setTimeout(() => {
        this.map = map;
        map.invalidateSize();
      }, 0);



      /*this.ourCustomControl = L.Control.extend({
        options: {
          position: 'bottomright',
        },
        onAdd: function (map) {
          var container = L.DomUtil.create(
            'div',
            'leaflet-bar leaflet-control leaflet-control-custom leaflet-control-layers-toggle'
          );
          container.style.backgroundColor = 'white';
          container.style.width = '30px';
          container.style.height = '30px';
          container.onclick = function () {
            console.log('buttonClicked');
            let b = document.getElementById("sidebar");
            b.click();
          };
          return container;
        },
      });
      map.addControl(new this.ourCustomControl());*/
}

onMapReady1(map) {

  this.map_export =map;// Map.map('map_export').setView([-17.7876515, -63.1825049], 16);

      /*L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
          attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
      }).addTo(this.map_export);*/

      /*this.map_export.attributionControl= false;
      this.map_export.zoomControl=  false;
      this.map_export.fadeAnimation=  false;
      this.map_export.zoomAnimation= false;*/

      const tileLayer = Map.tileLayer(
        'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
        maxZoom: 19,
        minZoom: 1,
        attribution: 'Tiles © Esri',
      }).addTo(this.map_export);

      setTimeout(() => {
        this.map_export = map;
        map.invalidateSize();
      }, 0);



      //let mapElement:any = document.getElementById('map_export');

      //document.getElementById("map_export").style.display = "none";


}






    /*
     initStatesLayer() {
      const stateLayer = L.geoJSON(this.geo_json_data, {
        style: (feature) => ({
          weight: 3,
          opacity: 0.5,
          color: '#008f68',
          fillOpacity: 0.8,
          fillColor: '#6DB65B'
        }),
        onEachFeature: (feature, layer) => (
          layer.on({
            mouseover: (e) => (this.highlightFeature(e)),
            mouseout: (e) => (this.resetFeature(e)),
          })
        )
      });
    console.log(this.geo_json_data);
      //this.map.addLayer(stateLayer);
    }




    highlightFeature(e: L.LeafletMouseEvent) {
      const layer = e.target;

      layer.setStyle({
        weight: 10,
        opacity: 1.0,
        color: '#DFA612',
        fillOpacity: 1.0,
        fillColor: '#FAE042'
      });
    }

    resetFeature(e: L.LeafletMouseEvent) {
      const layer = e.target;

      layer.setStyle({
        weight: 3,
        opacity: 0.5,
        color: '#008f68',
        fillOpacity: 0.8,
        fillColor: '#6DB65B'
      });
    }
  */









  agregarMarcadorCalle(evt){
    this.ProcesoCompleto=0;


    this.miFormulario_Construccion.controls['clasificacion'].setValue('');
    this.miFormulario_Construccion.controls['subcategoria'].setValue('');
    this.miFormulario_Construccion.controls['actividad'].setValue('');
    this.clasificaciones=[];
    this.subcategorias=[];
    this.actividades=[];
    this.FormGroupBusqueda.controls['firstCtrl'].setValue('');

    var coordenada =  evt.latlng;
    var latitud = coordenada.lat; // lat  es una propiedad de latlng
    var longitud = coordenada.lng; // lng también es una propiedad de latlng



    this.GeogToUTM(latitud,longitud);

    this.imagen(latitud , longitud);


    this.calleService.obtenerDonde(this.lat1,this.lng1).subscribe(calle =>{ this.calle = calle[0]

      console.log("Calle"+this.calle);
      console.log("Calle"+this.lat1 +"-"+this.lng1);
      console.log("Calle Encontrada"+this.calle.length);


      if (calle[0].idgrupolin>0){

        //this.FormGroupBusqueda.controls['firstCtrl'].setValue('1');

        console.log("nombre Calle"+calle[0].idgrupolin);
        this.marcadores.pop();//  =[];//.splice(0,this.marcadores.length);
        if (this.i>0){
          this.map.removeLayer(this.marker);
          this.borrarMarcador(this.i );          
          this.map.removeLayer(this.marker_circle);
        }

        console.log("nombre Calle"+calle[0].nombrevia);
        let nuevoMarcador = new Marcador(latitud , longitud);
        nuevoMarcador.nombrevia=calle[0].nombrevia;
        nuevoMarcador.ancho_via=calle[0].ancho_via;
        nuevoMarcador.grupolineamiento=calle[0].idgrupolin;
        this.idgrupoLineamiento=calle[0].idgrupolin;
        console.log("grupolineamiento "+this.idgrupoLineamiento);
        //this.idgrupoLineamiento=calle[0].idgrupolin;
        //this.idgrupoLineamiento=24;

        this.marcadores.push(nuevoMarcador);
        this.guardarStorage();
        for(let marcador of this.marcadores){
          //alert(marcador.lat);
          this.marker  = L.marker([marcador.lat, marcador.lng]).bindPopup('Punto');
          //this.marker.push(LamMarker);
          //LamMarker.addTo(this.map);
          this.map.addLayer(this.marker);

          this.i=1;

          this.marker_circle= L.circle([latitud, longitud], {
            color: "red",
            fillColor: "#f03",
            fillOpacity: 0.1,
            radius: 50
          });

          /*Map.circle([latitud, longitud], {
            color: "red",
            fillColor: "#f03",
            fillOpacity: 0.1,
            radius: 50
          }).addTo(this.map);*/          
          this.map.addLayer(this.marker_circle);
          this.map.setView([latitud, longitud], 18);

        }
      }else{
        //alert('Acerquesé mas a su acera');
        Swal.fire({
          position:'center',
          icon:'warning',
          title: "Acérquese mas a su acera",
          showConfirmButton: false,
          timer: 2000
        })
      }



        this.lcnService.getGrupoCategorias(this.idgrupoLineamiento).subscribe(grupoClasificacion=>{
          this.clasificaciones=grupoClasificacion;

          console.log("Clasificaciones: "+this.clasificaciones.length);

          if (this.clasificaciones.length>0){
            this.FormGroupBusqueda.controls['firstCtrl'].setValue('1');
          }


        });







    });

    //this.generateImage();



      /*var punto = L.marker([this.lat1, this.lng1]).bindPopup('Soy un puntazo');
      punto.addTo();*/


/*      let Position = L.Control.extend({
        _container: null,
        options: {
          position: 'bottomleft'
        },

        onAdd: function (map) {
          var latlng = L.DomUtil.create('div', 'mouseposition');
          this._latlng = latlng;
          return latlng;
        },

        updateHTML: function(lat, lng) {
          var latlng = lat + " " + lng;
          //this._latlng.innerHTML = "Latitude: " + lat + "   Longitiude: " + lng;
          this._latlng.innerHTML = "LatLng: " + latlng;
        }
      });

      this.position = new Position();
      this.map.addControl(this.position);
        */
    }





  GeogToUTM(latd0: number,lngd0 : number){
    //Convert Latitude and Longitude to UTM
    //Declarations();
    let k0:number;
    let b:number;
    let a:number;
    let e:number;
    let f:number;
    let k:number;
    //let latd0:number;
    //let lngd0 :number;
    let lngd :number;
    let latd :number;
    let xd :number;
    let yd :number;
    let phi :number;
    let drad :number;
    let utmz :number;
    let lng :number;
    let latz :number;
    let zcm :number;
    let e0 :number;
    let esq :number;
    let e0sq :number;
    let N :number;
    let T :number;
    let C :number;
    let A :number;
    let M0 :number;
    let x :number;
    let M :number;
    let y :number;
    let yg :number;
    let g :number;
    let usft=1;
    let DatumEqRad:number[];
    let DatumFlat:number[];
    let Item;

    let lng0 :number;




    DatumEqRad = [6378137.0, 6378137.0, 6378137.0, 6378135.0, 6378160.0, 6378245.0, 6378206.4,
      6378388.0, 6378388.0, 6378249.1, 6378206.4, 6377563.4, 6377397.2, 6377276.3, 6378137.0];
      DatumFlat = [298.2572236, 298.2572236, 298.2572215,	298.2597208, 298.2497323, 298.2997381, 294.9786982,
      296.9993621, 296.9993621, 293.4660167, 294.9786982, 299.3247788, 299.1527052, 300.8021499, 298.2572236];
      Item = 1;//Default



    //drad = Math.PI/180;//Convert degrees to radians)
    k0 = 0.9996;//scale on central meridian
    a = DatumEqRad[Item];//equatorial radius, meters.
          //alert(a);
    f = 1/DatumFlat[Item];//polar flattening.
    b = a*(1-f);//polar axis.
    e = Math.sqrt(1 - b*b/a*a);//eccentricity
    drad = Math.PI/180;//Convert degrees to radians)
    latd = 0;//latitude in degrees
    phi = 0;//latitude (north +, south -), but uses phi in reference
    e0 = e/Math.sqrt(1 - e*e);//e prime in reference
    N = a/Math.sqrt(1-Math.pow(e*Math.sin(phi),2));


    T = Math.pow(Math.tan(phi),2);
    C = Math.pow(e*Math.cos(phi),2);
    lng = 0;//Longitude (e = +, w = -) - can't use long - reserved word
    lng0 = 0;//longitude of central meridian
    lngd = 0;//longitude in degrees
    M = 0;//M requires calculation
    x = 0;//x coordinate
    y = 0;//y coordinate
    k = 1;//local scale
    utmz = 30;//utm zone
    zcm = 0;//zone central meridian

    k0 = 0.9996;//scale on central meridian
    b = a*(1-f);//polar axis.
    //alert(a+"   "+b);
    //alert(1-(b/a)*(b/a));
    e = Math.sqrt(1 - (b/a)*(b/a));//eccentricity
    //alert(e);
    //Input Geographic Coordinates
    //Decimal Degree Option

    //latd0 = parseFloat(document.getElementById("DDLatBox0").value);
    //lngd0 = parseFloat(document.getElementById("DDLonBox0").value);

    //latd1 = Math.abs(parseFloat(document.getElementById("DLatBox0").value));
    //latd1 = latd1 + parseFloat(document.getElementById("MLatBox0").value)/60;
    //latd1 = latd1 + parseFloat(document.getElementById("SLatBox0").value)/3600;

    lngd=lngd0;
    latd=latd0;



    xd = lngd;
    yd = latd;
  //        alert(Item);
  //        alert(usft);

    phi = latd*drad;//Convert latitude to radians
    lng = lngd*drad;//Convert longitude to radians
    utmz = 1 + Math.floor((lngd+180)/6);//calculate utm zone
    latz = 0;//Latitude zone: A-B S of -80, C-W -80 to +72, X 72-84, Y,Z N of 84
    if (latd > -80 && latd < 72){latz = Math.floor((latd + 80)/8)+2;}
    if (latd > 72 && latd < 84){latz = 21;}
    if (latd > 84){latz = 23;}

    zcm = 3 + 6*(utmz-1) - 180;//Central meridian of zone
    //alert(utmz + "   " + zcm);
    //Calculate Intermediate Terms
    e0 = e/Math.sqrt(1 - e*e);//Called e prime in reference
    esq = (1 - (b/a)*(b/a));//e squared for use in expansions
    e0sq = e*e/(1-e*e);// e0 squared - always even powers
    //alert(esq+"   "+e0sq)
    N = a/Math.sqrt(1-Math.pow(e*Math.sin(phi),2));
    //alert(1-Math.pow(e*Math.sin(phi),2));
    //alert("N=  "+N);
    T = Math.pow(Math.tan(phi),2);
    //alert("T=  "+T);
    C = e0sq*Math.pow(Math.cos(phi),2);
    //alert("C=  "+C);
    A = (lngd-zcm)*drad*Math.cos(phi);
    //alert("A=  "+A);
    //Calculate M
    M = phi*(1 - esq*(1/4 + esq*(3/64 + 5*esq/256)));
    M = M - Math.sin(2*phi)*(esq*(3/8 + esq*(3/32 + 45*esq/1024)));
    M = M + Math.sin(4*phi)*(esq*esq*(15/256 + esq*45/1024));
    M = M - Math.sin(6*phi)*(esq*esq*esq*(35/3072));
    M = M*a;//Arc length along standard meridian
    //alert(a*(1 - esq*(1/4 + esq*(3/64 + 5*esq/256))));
    //alert(a*(esq*(3/8 + esq*(3/32 + 45*esq/1024))));
    //alert(a*(esq*esq*(15/256 + esq*45/1024)));
    //alert(a*esq*esq*esq*(35/3072));
    //alert(M);
    M0 = 0;//M0 is M for some origin latitude other than zero. Not needed for standard UTM
    //alert("M    ="+M);
    //Calculate UTM Values
    x = k0*N*A*(1 + A*A*((1-T+C)/6 + A*A*(5 - 18*T + T*T + 72*C -58*e0sq)/120));//Easting relative to CM
    x=x+500000;//Easting standard
    y = k0*(M - M0 + N*Math.tan(phi)*(A*A*(1/2 + A*A*((5 - T + 9*C + 4*C*C)/24 + A*A*(61 - 58*T + T*T + 600*C - 330*e0sq)/720))));//Northing from equator
    yg = y + 10000000;//yg = y global, from S. Pole
    if (y < 0){y = 10000000+y;}
    //Output into UTM Boxes
          //alert(utmz);
    //console.log(utmz)  ;
    //console.log(Math.round(10*(x))/10 / usft)  ;
    //console.log(Math.round(10*y)/10 /usft)  ;
    this.lat1=Math.round(10*(x))/10 / usft  ;
    this.lng1=Math.round(10*y)/10 /usft;

    //document.getElementById("UTMeBox1").value = Math.round(10*(x))/10 / usft;
    //document.getElementById("UTMnBox1").value = Math.round(10*y)/10 /usft;
    //if (phi<0){document.getElementById("SHemBox").checked=true;}

    //ajaxmagic();
  }//close Geog to UTM

  /*refresh(map){
    //alert('entro');

  }*/


  guardarStorage(){
    localStorage.setItem('marcadores', JSON.stringify(this.marcadores) );
  }

  borrarMarcador(i : number){
    //console.log(i);
    this.marcadores.splice(i,1);
    this.guardarStorage();
    //this.snackBar.open('Marcador borrado', 'Cerrar',{duration : 3000});
  }



  toDataURL = async (url) => {
    console.log("Downloading image...");
    var res = await fetch(url);
    var blob = await res.blob();

    const result = await new Promise((resolve, reject) => {
      var reader = new FileReader();
      reader.addEventListener("load", function () {
        resolve(reader.result);
      }, false);

      reader.onerror = () => {
        return reject(this);
      };
      reader.readAsDataURL(blob);
    })

    return result
  };



   CargarMap1(lat:number,lng:number) {
    const width = 200;
    const height = 200;
    this.ContadorMapa++;

    if (this.marker_export !=null){
      if(this.map_export.hasLayer(this.marker_export) ) {
      //console.log(this.geoJSon);
        this.map_export.removeLayer(this.marker_export);
        this.map_export.removeLayer(this.marker_circleExport);
        
      //this.map.removeLayer(marker);
      }
    }

    this.map_export.setView([lat, lng], 18);
    var markerLin;
    this.marker_export=Map.marker([lat, lng]).addTo( this.map_export);

    this.marker_circleExport= L.circle([lat, lng], {
      color: "red",
      fillColor: "#f03",
      fillOpacity: 0.1,
      radius: 50
    });

    /*Map.circle([latitud, longitud], {
      color: "red",
      fillColor: "#f03",
      fillOpacity: 0.1,
      radius: 50
    }).addTo(this.map);*/          
    this.map_export.addLayer(this.marker_circleExport);


    /*Map.circle([lat, lng], {
      color: "red",
      fillColor: "#f03",
      fillOpacity: 0.1,
      radius: 50
    }).addTo(this.map_export);*/


      var x = document.getElementById("map_export");
      this.getValueWithPromise(x);
  }

  resolveAfter2Seconds(image64,mapElement) {
    return new Promise(resolve => {
      setTimeout(() => {
        //resolve(x);
        //console.log("resolveAfter2Seconds: "+ mapElement);
        htmlToImage.toPng(mapElement, { quality: 0.95 })
      .then((dataUrl) => {
         console.log("htmlToImage: --" +dataUrl);
         image64=dataUrl;// this prints only data:,
         //console.log("#idMap"+this.ContadorMapa); // this prints only data:,
         //console.log("Imagen64: "+dataUrl);

         localStorage.setItem('imagenLcn', JSON.stringify(dataUrl) );

         //mapElement.hidden=true;


         /*var img = new Image();
          img.src = dataUrl;
          document.body.appendChild(img);*/
          return image64;
      });
      }, 2000);
    });
  }

  async getValueWithAsync(mapElement) {
    const value = <any>await this.resolveAfter2Seconds(20,mapElement);
    console.log(`async result: ${value}`);
  }

  getValueWithPromise(mapElement) {
    this.resolveAfter2Seconds(2,mapElement).then(value => {
      console.log(`promise result: ${value}`);


    }).catch(function (error) {
      console.error('oops, something went wrong!', error);
    });
    ;
    console.log('I will not wait until promise is resolved');
  }

   generarImagen(){
    let ele:any;
    //let variable="#idMap"+this.ContadorMapa;
    let variable="div"+ this.ContadorMapa;
    console.log(variable);
    ele=document.querySelectorAll(variable);
    console.log(ele);
    //map.once
    //map.

     //htmlToImage.toPng(document.getElementById("idMap"+this.ContadorMapa)  , { quality: 0.95 })
     htmlToImage.toPng(ele  , { quality: 0.95 })

      .then((dataUrl) => {
        //var img = new Image();
        //img.src = dataUrl;
        //document.body.appendChild(document.getElementById("idMap"));
        //download(dataUrl, 'my-node.png');
         //console.log(dataUrl); // this prints only data:,
         //localStorage.removeItem("imagenPlano");
         //localStorage.
         localStorage.removeItem('imagenPlano');
          localStorage.setItem('imagenPlano', JSON.stringify(dataUrl) );
          this.recuperar();
        //alert(dataUrl);

        /*const imgElement = document.createElement("img");
        imgElement.src = dataUrl;
        document.body.appendChild(imgElement);*/
    });
  }


  //(click)="irSiguiente($event)"
  irSiguienteMapa(event){
    //alert("");
    let v=this.FormGroupBusqueda.get('firstCtrl')?.value;


    //alert(v);
    if(v===""){
      //alert("Debe seleccion ubicación en el mapa");

      Swal.fire({
        position:'center',
        icon:'warning',
        title: "Debe seleccion ubicación en el mapa",
        showConfirmButton: false,
        timer: 2000
      });      
      return;
    }else{
      //alert(v);
      this.stepper.next();
    }

  }

  irSiguiente(event){
    console.log("Proceso completo: "+this.ProcesoCompleto);



    if (this.ProcesoCompleto==1){


        //alert("Click");
        this.planoSuelo.numerplano='2';
        this.planoSuelo.superficietitulo=this.miFormulario_Catastro.get('superficietitulo')?.value;
        //console.log(object);
        this.planoSuelo.superficiemensura=this.miFormulario_Catastro.get('superficiemensura')?.value;
        this.planoSuelo.zona=this.miFormulario_Catastro.get('zona')?.value;
        this.planoSuelo.manzana=this.miFormulario_Catastro.get('manzana')?.value;
        this.planoSuelo.distrito=this.miFormulario_Catastro.get('distrito')?.value;
        this.planoSuelo.lote=this.miFormulario_Catastro.get('lote')?.value;
        this.planoSuelo.tipousosuelo=this.miFormulario_Catastro.get('tipousosuelo')?.value;
        //this.idtipoLineamiento= parseInt( this.miFormulario_Catastro.get('ubicacionlote')?.value);
        this.planoSuelo.uv="0";
        this.planoSuelo.estadopago =0;



        //this.planoSuelo.ubicacionlote=this.miFormulario.get('ubicacionlote')?.value

        this.planoSuelo.tipovia="COLECTORA";//this.miFormulario.get('tipovia')?.value;

        if(Math.floor(Math.random() * (10 + 1)) >5){
          this.planoSuelo.tipovia="COLECTORA";//this.miFormulario.get('tipovia')?.value;
        }else{
          this.planoSuelo.tipovia="NORMAL"

        }

        this.planoSuelo.idpersona=1;//this.miFormulario.get('idpersona')?.value




        //console.warn(this.idplanoSuelo);
            /*
            public numerotramite: string,
            public oficiodale: string,
            public fechatramite: string, //01/04/2022"
            public fotolote: string,
            public idgrupolineamiento:number,
            public idactividad:number,
            public idpersona:number,
            public idtipolineamiento:number,
            public idplanosuelo:number,
            public idexcepcion:number,
            public id_clasi_activi_regla:number,
            public estadopago:number*/

            /*this.lcnService.createPlanoSuelo(
          {
            "numerplano": "59",
            "superficietitulo": "100",
            "superficiemensura": "101",
            "zona": "F1",
            "manzana": "120",
            "distrito": "9",
            "lote": "8",
            "tipousosuelo": "NORMAL",
            "ubicacionlote": "ESQUINA",
            "tipovia": "1",
            "idpersona": 1
          }
        ).subscribe((result)=>{
          console.warn(result);
        });  */
        //let datePipe;
         this.lcnService.createPlanoSuelo(this.planoSuelo).subscribe(resp=>{
          this.respuestaPlanoSuelo=resp;
          this.idplanoSuelo=parseInt(this.respuestaPlanoSuelo.idplanosuelo);
            console.log(this.idplanoSuelo);
          this.lineamiento.numerotramite="1";
          this.lineamiento.oficiodale="002/2021";
          this.lineamiento.fechatramite=this.datePipe.transform( new Date(),"dd/MM/yyyy");

          //this.datePipe.transform(this.form.value.fecha_nacimiento,"dd/MM/yyyy"),
          //fecha_nacimiento: this.datePipe.transform( new Date(),"dd/MM/yyyy"),
          this.lineamiento.fotolote="";
          this.lineamiento.idgrupolineamiento=this.idgrupoLineamiento;
          this.lineamiento.idactividad=this.idActividad,
          this.lineamiento.idpersona=1;
          this.lineamiento.idtipolineamiento=this.idtipoLineamiento;
          this.lineamiento.idplanosuelo=this.idplanoSuelo;
          this.lineamiento.idrestriccion =11;
          this.lineamiento.id_clasi_activi_regla=this.idClasifi_Activi_Regla;
          this.lineamiento.estadopago=0;
          console.log(this.lineamiento);
          this.lcnService.registrarLineamiento(this.lineamiento).subscribe(resp=>{


            console.warn(resp);

            localStorage.setItem('resultadoLineamiento', JSON.stringify(resp));



          });





          });






        //alert('LINEAMIENTO GENERADO CORRECTAMENTE, SE PROCEDE A REALIZAR EL PAGO');
       // this.router.navigate(['pages/pago'])

       //this.router.navigate(['pages/pago']);;;

       //this.miFormulario_Catastro.markAllAsTouched;


       
       
       //this.GenerarQR();

       this.ProcesoCompleto=0;

    this.stepper.next();
   }else{
    alert("Faltan datos");
   }


  }


  buscarRegla(event: any){



    /*console.log("ubicacionlote: "+this.miFormulario_Catastro.get('ubicacionlote')?.value);
    switch (this.miFormulario_Catastro.get('ubicacionlote')?.value){
      case "1":
        this.planoSuelo.ubicacionlote="INTERMEDIO";
        break;
      case "2":
        this.planoSuelo.ubicacionlote="ESQUINA";
        break;

      case "3":
        this.planoSuelo.ubicacionlote="CALLE A CALLE";
        break;
      default:
         console.log("No such day exists!");
        break;
    }*/


    this.ProcesoCompleto=0;
    this.idtipoLineamiento=0;
    
    var valortipolineamiento=event.value;
    console.log("ubicacion:" + valortipolineamiento);
    //event
    //switch (this.miFormulario_Catastro.get('ubicacionlote')?.value){

      this.idtipoLineamiento=0;         
      this.ProcesoCompleto=0;

      if (valortipolineamiento==1){
        this.planoSuelo.ubicacionlote="INTERMEDIO";
        this.idtipoLineamiento=1;
        this.ProcesoCompleto=1;
        console.log("ubicacion: INTERMEDIO" );
      }

      if (valortipolineamiento==2){
        this.planoSuelo.ubicacionlote="ESQUINA";
        this.idtipoLineamiento=2;
        this.ProcesoCompleto=1;
        console.log("ubicacion: ESQUINA" );
      }

      if (valortipolineamiento==3){
        this.planoSuelo.ubicacionlote="CALLE A CALLE";
        this.idtipoLineamiento=3;
        this.ProcesoCompleto=1;
        console.log("ubicacion: CALLE A CALLE" );

      }



      /*switch (valortipolineamiento){
      case 1:{
        this.planoSuelo.ubicacionlote="INTERMEDIO";
        this.idtipoLineamiento=1;
        this.ProcesoCompleto=1;
        console.log("ubicacion: INTERMEDIO" );
        break;}
      case 2:{
        this.planoSuelo.ubicacionlote="ESQUINA";
        this.idtipoLineamiento=2;
        this.ProcesoCompleto=1;
        console.log("ubicacion: ESQUINA" );
        break;}

      case 3:{
        this.planoSuelo.ubicacionlote="CALLE A CALLE";
        this.idtipoLineamiento=3;
        this.ProcesoCompleto=1;
        console.log("ubicacion: CALLE A CALLE" );
        break;}
      default:{
        this.idtipoLineamiento=1;         
         this.ProcesoCompleto=0;
        break;}
    }*/


    this.tipoEdificacion.idcategoria    = parseInt(this.miFormulario_Construccion .get('categoria')?.value);
    this.tipoEdificacion.idsubcategoria = parseInt(this.miFormulario_Construccion.get('subcategoria')?.value);
    this.tipoEdificacion.idactividad    = parseInt(this.miFormulario_Construccion.get('actividad')?.value);

    this.idActividad=parseInt(this.miFormulario_Construccion.get('actividad')?.value);
    //console.log("ID_ACTIVIDAD: "+this.idActividad);
    //alert(this.idActividad);
    console.log("tipo lineamiento"+ valortipolineamiento);
    this.lcnService.getClasifi_Activi_Regla(this.idActividad,this.idgrupoLineamiento  ,this.idtipoLineamiento );

      this.Clasifi_Activi_Regla =JSON.parse(localStorage.getItem('clasifi_activi_regla')!);
      this.idClasifi_Activi_Regla=this.Clasifi_Activi_Regla.id_clasi_activi_regla;






    //this.idClasifi_Activi_Regla=6508//this.Clasifi_Activi_Regla.id_clasi_activi_regla;

    console.log("Primer id_clasi_activi_regla: "+this.idClasifi_Activi_Regla);
  }



  imagen(lat:number,lng:number){
      
    this.CargarMap1(lat,lng);    

  }

  startTimer() {
    let timeLeft: number = 3;
    let interval;
    interval = setInterval(() => {
      if(timeLeft > 0) {
        timeLeft--;
      } else {
        timeLeft = 60;
      }
    },1000)
  }
  recuperar(){
    console.log(JSON.parse(localStorage.getItem('imagenPlano')));

  }

  refreshMap() {
    if (this.map) {
       //this.streetMaps.redraw();
       //alert("ingreso");
      this.map.invalidateSize();
    }
  }



/////////////////////////////catastro


/////////////////////////////////////////////////////////



buscarGeografico(){

  //this.map.removeLayer( this.groupBuscador );
  //buscarGeografico
  let dm;
  let uv;
  let mz;
  let lt;
  let tipobus;

  if (this.FormGroupBusqueda.get('busdm')?.value!=0){
    dm=this.FormGroupBusqueda.get('busdm')?.value;
  }else{
    dm=0;
  }

  if (this.FormGroupBusqueda.get('busuv')?.value!=0){
    uv=this.FormGroupBusqueda.get('busuv')?.value;
  }else{
    uv=0;
  }

  if (this.FormGroupBusqueda.get('busmz')?.value!=0){
    mz=this.FormGroupBusqueda.get('busmz')?.value;
  }else{
    mz=0;
  }

  if (this.FormGroupBusqueda.get('buslt')?.value!=0){
    lt=this.FormGroupBusqueda.get('buslt')?.value;
  }else{
    lt=0;
  }


  tipobus=0;
  if (dm!=0 && uv==0 && mz==0 && lt==0){
    tipobus='1'
  }

  if (dm==0 && uv!=0 && mz==0 && lt==0){
    tipobus='2'
  }

  if (dm==0 && uv!=0 && mz!=0 && lt==0){
    tipobus='3'
  }

  if (dm==0 && uv!=0 && mz!=0 && lt!=0){
    tipobus='4'
  }

  if (tipobus!=0){
    //tipobus='4';//this.FormGroupBusqueda.get('tipobus').value;
    //alert(dm);
    this.mapaService.buscarGeografico(dm,uv,mz,lt,tipobus).subscribe((data)=>{
      this.geo_json_data = data;

      if (this.geoJSon!=null){
        if(this.map.hasLayer(this.geoJSon) ) {

        //console.log(this.geoJSon);
          this.map.removeLayer(this.geoJSon);
        //this.map.removeLayer(marker);
        }
      }

      this.geoJSon  = L.geoJSON(this.geo_json_data, {
        style: this.style,
        onEachFeature: this.popup,
      }).addTo(this.map);

      this.map.flyToBounds(this.geoJSon.getBounds());
      this.groupBuscador = L.layerGroup().addTo(this.map);

    });
  }else{
    alert('Su búsqueda podría generar muchos resultados, debe ser más específico');
  }
}

buscarCoordenada(){

  if (this.resBusquedaCoordenada !=null){
    if(this.map.hasLayer(this.resBusquedaCoordenada) ) {

    //console.log(this.geoJSon);
      this.map.removeLayer(this.resBusquedaCoordenada);
    //this.map.removeLayer(marker);
    }
  }

let busx;
let busy;
  if (this.FormGroupBusqueda.get('busx')?.value!=0){
    busx=this.FormGroupBusqueda.get('busx')?.value;
  }else{
    busx=0;
  }

  if (this.FormGroupBusqueda.get('busy')?.value!=0){
    busy=this.FormGroupBusqueda.get('busy')?.value;
  }else{
    busy=0;
  }

  //var utm = L.utm(479304.6, 8034569.2, 20, 'K', true);
  //https://jjimenezshaw.github.io/Leaflet.UTM/examples/input.html
  var utm = L.utm(busx, busy, 20, 'K', true);
  var ll = utm.latLng();
  if (ll) {
      //marker.setLatLng(ll).bindPopup(utm + '<br>' + ll).openPopup()
      //el1.lat.value = ll.lat.toFixed(6);
      //el1.lng.value = ll.lng.toFixed(6);
      //document.getElementById('result2').innerHTML = '' + ll;

      //alert(ll.lat.toFixed(6));
      //alert(ll.lng.toFixed(6));


        this.resBusquedaCoordenada = L.marker([ll.lat.toFixed(6),ll.lng.toFixed(6)], {
        title: "Resultado de búsqueda",
        draggable:false,
        opacity: 1
        }).bindPopup("<b>Coordenada</b>")
        .addTo(this.map);
        //this.map.flyToBounds(this.resBusquedaCoordenada.getBounds());



        /*let marker;
        marker = L.marker([-17.795595,-63.196819], {
            //icon: customIcon,
        })
        .bindPopup("<p>Coordenada: </p><p>Latitud: " +"-17.795595" + "</p><p>Longitud: " +"-63.196819"+ "</p>")
        .addTo(this.map);

        var coordenada = marker.latlng;*/

        /*let animatedMarker = L.animatedMarker(marker.getLatLngs(), {
          autoStart: false,
          icon
        });

        this.map.addLayer(animatedMarker);*/
  }
}
limpiarCoordenada(){


  this.FormGroupBusqueda.controls['busx'].setValue('');
  this.FormGroupBusqueda.controls['buxy'].setValue('');

    if (this.resBusquedaCoordenada !=null){
      if(this.map.hasLayer(this.resBusquedaCoordenada) ) {

      //console.log(this.geoJSon);
        this.map.removeLayer(this.resBusquedaCoordenada);
      //this.map.removeLayer(marker);
      }
    }

  }

  limpiarGeografico(){
  this.FormGroupBusqueda.controls['busdm'].setValue('');
  this.FormGroupBusqueda.controls['busuv'].setValue('');
  this.FormGroupBusqueda.controls['busmz'].setValue('');
  this.FormGroupBusqueda.controls['buslt'].setValue('');
  if (this.geoJSon!=null){
    if(this.map.hasLayer(this.geoJSon) ) {

    //console.log(this.geoJSon);
      this.map.removeLayer(this.geoJSon);
    //this.map.removeLayer(marker);
    }
  }
  //
  //alert("");

  }

  generateImage(mapElement){



      //var mapElement:any = document.getElementById('div');

      console.log("generando imagen");
    htmlToImage.toPng(mapElement)
      .then(function (dataUrl) {
        var img = new Image();
        img.src = dataUrl;

        console.log(dataUrl);
        document.body.appendChild(img);
      })
      .catch(function (error) {
        console.error('oops, something went wrong!', error);
      });




  }

  Pagar(){





    this.actualizarEstado= new ActualizarEstado(0,'',0);
    this.ActualizarEstadoPagoLineamiento= new ClsActualizarEstadoPago(0);

    //localStorage.setItem('resultadoLineamiento', JSON.stringify(resp));

    if(localStorage.getItem('resultadoLineamiento')){
      this.actualizarEstado =JSON.parse(localStorage.getItem('resultadoLineamiento')!);
      console.log("actualizarEstado.idlineamiento: "+this.actualizarEstado.idlineamiento);

      //this.idLineamiento=this.resultadolineamiento.idlineamiento;

      //this.ActualizarEstadoPagoLineamiento.idlineamiento=this.actualizarEstado.idlineamiento;

      //this.ActualizarEstadoPagoLineamiento.idlineamiento=this.actualizarEstado.idlineamiento;
      //this.lcnService.ActualizaEstadoLineamiento("{'idlineamiento':" +this.idLineamiento +"}");
      //this.lcnService.ActualizaEstadoLineamiento( this.ActualizarEstadoPagoLineamiento);

    }

    this.lcnService.ActualizaEstadoLineamiento(
      {
        "idlineamiento": this.actualizarEstado.idlineamiento
      }
    ).subscribe((result)=>{
      console.warn(result);
    });


    //ObtenerReportePdfLineamiento
    //cadena.substr(inicio[, longitud])
    let cadena,cadena1:string;

    //data:image/png;base64,


    //cadena="/9j/4AAQSkZJRgABAQAAAQABAAD/4gIoSUNDX1BST0ZJTEUAAQEAAAIYAAAAAAQwAABtbnRyUkdCIFhZWiAAAAAAAAAAAAAAAABhY3NwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAA9tYAAQAAAADTLQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAlkZXNjAAAA8AAAAHRyWFlaAAABZAAAABRnWFlaAAABeAAAABRiWFlaAAABjAAAABRyVFJDAAABoAAAAChnVFJDAAABoAAAAChiVFJDAAABoAAAACh3dHB0AAAByAAAABRjcHJ0AAAB3AAAADxtbHVjAAAAAAAAAAEAAAAMZW5VUwAAAFgAAAAcAHMAUgBHAEIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFhZWiAAAAAAAABvogAAOPUAAAOQWFlaIAAAAAAAAGKZAAC3hQAAGNpYWVogAAAAAAAAJKAAAA+EAAC2z3BhcmEAAAAAAAQAAAACZmYAAPKnAAANWQAAE9AAAApbAAAAAAAAAABYWVogAAAAAAAA9tYAAQAAAADTLW1sdWMAAAAAAAAAAQAAAAxlblVTAAAAIAAAABwARwBvAG8AZwBsAGUAIABJAG4AYwAuACAAMgAwADEANv/bAEMAAwICAgICAwICAgMDAwMEBgQEBAQECAYGBQYJCAoKCQgJCQoMDwwKCw4LCQkNEQ0ODxAQERAKDBITEhATDxAQEP/bAEMBAwMDBAMECAQECBALCQsQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEP/AABEIANsAxwMBIgACEQEDEQH/xAAdAAACAgMBAQEAAAAAAAAAAAAGBwUIAAMECQIB/8QARBAAAgEDAwMDAgQEAwYEBQUBAQIDBAURBhIhAAcxEyJBUWEIFDJxFSOBkUJSoRYzYrHB8CRy0fE0Q1OC4QkXJSZj0v/EABsBAAIDAQEBAAAAAAAAAAAAAAQFAgMGBwEA/8QAQxEAAgEDAgIHAwgIAwkAAAAAAQIDAAQRBSESMQYTIkFRYYFxkdEHFCNCUpKhwTIzQ2JygqKxFVPSFhckNUVUsuHx/9oADAMBAAIRAxEAPwCtPeLvJd9X3OorU1RfnmSPb+cr7jNUVM2MAbmOMfQADjHPQDS6t1PSLFDU6juYZhyZKuR2Bz9c4PXzULTXGkkpppC5bKoxLbt2frn9uo6NGpkMMtOXkhOG93j9sdUR6pe26hI5mAHdxH41jjDBc5doxnv2FGdu1XfppF23esYITu9Sd8MeMcZ+erX9kb3PcbW9LcdB2W6Rq4/8XUUETEMQMrllLED9/nqounTa45oqyvjf2kOoLH3HPxz5GfHjq6PauutuptIWW108VXE9MqwmaCPPrFnLKX2kHI3hT+3Un1W9Zc9e/wB4/GjNOsbVjgRL7hRNcrRYiFNwtWm6PdxspLbC7/sVCgj989Q1RBoVHSlorHbaqqVMmJyiFyWYDChASeDxuHjprUfa+3l0kuG/3EkHB3eSASCTjx1G6l7A6Pu0hqzDNFVtEI0qIHYSABmP1wcZOQfjJGMEEU6nqH1bh/vt8a1SaZZEgPCv3R8KEKHQV6vFUltXQdooZmQylWpKfKxjAJxlm4JGPrz1O3HsbPpq2LU3S2WucyNh9lGuUOfGWUYz9uPIzkgHXpaC79qbnUUZuFfd1kEaxyVUoZkhTO0ITwV9xyPJIJ8bQHTaNWUV+sjTyiJ1Ee5oY0J3EDIx9cYxz9z568/xbUlXtTv95vjRMukaZEwMcS/dHwpd6d7L2CrhWqu2nrTSh19sJpkaTOPnI4/byPnBz193fsVoB4PSFhCyckMpEXPz+nGQPoOmJbZpquCKqqYJaVZl9UKy5bB8BgPBHz0P6qvuj6eaVK+8QQTUiB5czktEpJw2P8Pg8nPXw1fUT+3f7zfGqZbLT0/ZJj+EfClZX9urLa7ha7LbKGwxeu2+SouEY3OC6RosZEUhLFm8MEHBJkXrXdu8+pKNa+22jX2qKM28pQxFbo1LT0hJUBjT7FEewOMBpGXIAKsPMjTXq51XcairNJpFqE11pq6CKOGaEb4ZADI675I13Kq85xncv0I6X98r5dO6JvHc7VNQlTUrWVFNUWeHAamnhnkiKtIG/wALI3uOFPBzjBJBu5rpB18jNjxJOPeaDjSCIkwIq+wAf2p6as7p0vaHT9dURd3r7c7PQj06CvrbxLXTV5dDKNswYiVt0jKDgALGBgAZFY9J/iR72DWdVrrUWutUfk3bZRWie7V0VCFdcLOzbvSyp9wSTajNlWZAAOp7TFX/ALd0VNijcUctOld7UDOCH/UDwSeAoKnwePGejXRlsslykrXp7dTVNTHI9MvoVAZoVL4LMg4B3KFA+pH16D61ojkUdHGXGTyo9ou4131JW/wLRevNW+pHRUq3MVl0qlqaeUgM8hSV8qso2lXiJTG4q3uVepKifutR3K2XGfujqBzQUslKYamrqJIahWRUEkwDRKZAE3bhk72z/iI6jLXu0pQVdbHaZLg52xilpIVEk8ruVVAXIGGZudxA3Z589bv4h3hqZWlpu0UwpBAi0y1F2oFkDDglmWZ8JgLtRF4xzngCsfOLo8SbVahitD2yM1D1lx1jo63Xeqpu5mua1mQysK3UlZMYmUe1U3SYXI5IX5Pk/Hn7r/8AER+ISw6julNZu/8A3Qqqb1mMZbVVcmxfkACXAGTgft16D1v/AO5EbOa7QOjVVoWEUdy1ZOgSTIO+QxUMg454zg56rBrj8KustTaiqdSXHuJ2rglrpDOaOG9MkajcDgZpw2Avt8DLY+vR1tbXCg8e9K9RuY5WzGQKr9bPxP8A4mEjJl79dzJGGP16orm/c/73pjWX8Rv4idlt1JN3f7gy0UddCkgfUFayOM52lfVwxIB4JxgHqGpOxl1qr1NDDftKtFSVDU5nmklSMHk7gwjO9M58KWOCBg46tr2B7Vacmsb2SKy1Tx2+X1Dc3iiWKpn27d0aLIzhWw5UOgYLjIViw6+uI5LYB376DsYjcy5zsKsXB+MXQYp6RaOHUFxeqDCNlakhRmTG8CSeoRSVyAUyGz4621H4uYKlquCw6KuFfV0m4PQQ18VTVuyoG2rHRioC/qTl2Vfdy3BIX4tF7o7/AE1FAtGtpgZnk/kjJJBw30VskHPHjnz0uNRfiUtmktdJZLa1EtrjdEnnkKkoWOTtQsMnO3k55HS8yuBkGnc5ihxk03Lv+MnXFPbp6hu31ttUlNSS1c6V9xr98AXJ2SAUKoJMchRI24fpL+eq+ax76dwO69NQa/pO4mr9OxVNMCtDZL9XUdOuGYZKpKMtyQc/QeCODPv7PV3bS1VLWVdDFQ6ls1NW26GQZniSVOPWXAO4FccZwWbnk9VV7d3+e0aLNju9M8k0c80e31sKVC4PPzzzn5/v0PLcS8GUNPdHtrWaZetO1Fvdjvn3701oiKayd5daLGssSyt/tBVfmHcZGfUL7wuG5AOCQMg4HWdLvV2rbdrWzR2RbYiUrBSrGZt3B3Dkft1nUY3k4e0aJvhZrORGNqRVTXVtLbJauWaJmimC7U8ecZP26+VrmlC3KQlHlG19pG04+3/Xos7j9v62kq2g0vNDW26R1C+idzrk+0uCOB4/oc9dPbvstrO/XWCljt38Seo3KKWkjeWTx5CgHPGTgHxnjpqxRedcz6pQOFedDlruNJW1y0j1qhdv6S2BjPPu+PvjBwM54wbidgu9mkdJWI2IGCqFMCXqIxh3ZmzlG8lfGD9jwPHUVpz8EesrRC18pe3tZuqgDKGnTeMeQFYqwXI4DDJ+c8dZafw0XHTl+SOa1y29J2MjIcA5LcjOSDyc4HPVMhHIVfbpJCwKrVmqfu9U3K3LXUFFEscnuVnB3H98dEdl7g0V1hhoqlESpdc7cex/cR8/PHSvvVoi0bpimq6md4pJE9lOQRtAH/fnpa2bu9DLqOipY4VV9wMau3D/AMxh+r48dU53xT17tYiFkO9We1xZ7fV0SCYqKwESoh/UCM5x/fz0DaT1ILDcJIq2o9OFjiQlipUlsjA8f+56Op70KoR3GuihiYwKgL7WxkcqD/bqmf4ktWX5tWRWnTu8Rwv6s2ZgFPkAHndwM458sD8de5QKWdgAOeTiiEtrrUJlgsI2kkbYKoLE+wDJo47v/i/lkqJ6DSjz0lPGoUylgDIfO4sOQDkdVjvXdjUV2rpalbrVO87h5CQSOfkHPI64xZqVJN93uJmYAZiGP9fr/p1orLvQWgf/AMdZ4w3xIy/9n/XoH5+JRi1jL+f6K+8/kDWj/wB3Mtn2uk1/FafuZM0w9sceeH+Z1PjT2/DH3QGmu41k1BqW5vQ08VQfXq5GZNsbKUY+CRkP8c8dZ3KveiLter7Zqaue42a4XGtraeVUytNI9TLKjRkqGGd+fnz46RkGqHYCWrlEaYz/AC1BOR8YOeuKk1u1W808Zb2sVWNgFwo+f36+D6i3IIvvPwqaW3QGzXqy95OR3gQxA+wHrTj271Yvtf3A0ro6nFNX1dbJ6qeixlMkiRLnJK5ycnHkAdSnbDWmhtGapvM9FfBRUFdO1RB+YM8+TjBy4jSQEk7gCWCsoO7nhL6P7n0lippKu8UsEtOgYmNoxIHJAxuP0yOuCbXi3W4RXKegp1R3D+nHCERk5xgAAY8dVP8A4gpzxKfQ/Gmaah0EKBGt7pR4iWIn3GIUztW94tXv3tjFi1PcmsP8QgEDQPKICuFDEMpBQ53HJI5P1HVirdHT3SBP4i71jAAq9S5kbwOcsSf69UV1Hqr1NdRw2pEhtlQ8BMEajgEYcDPI56sFb9D3K0gVGjNcV9umHuWnqATG2TxuK8H+qHp7ZXGoW0eXtw48Vbf3MBn30svujnQ/WGzY6nJbv3CeLKk/xxM2B5lKc1Tpyxy7itroW3Da/wD4dCSPvx1H1GkqKV1lhgSJozuHpR44HPJHnkDz9B0F2ruvr3RUn/8Ad9HQ3ahHElZQHY+P8xAyv9Cqfv0c6b7w6N1G5a0zESNE26nlwkoP/l+eD5GR9+nNrrNpct1OSkn2WHC348/TNZHW/k71zRIPn5VZ7YftoWEsfqV3T+cKaFdV6RpIYpamnLowDMm0Mu34OMfUZz1YHsXRLNpW33uWsqo2WiSljpDKFp1KkqWWPIDFsfqOTz0r7lcBVxyJJRsyFGICs3IKnI4B/wAw6qn3hortLeKymt99uyLRw+r6JgkmiSUAbTkA+krbmwSqrlDznPV+qR/OIAW7qz2kXJWVlWnb37/FNerXfrpYNMPQCjppnp3kEQLtGTtYEOD/AIty8fAHVUdT6tWevFwg2xyM5YpgEY8c/Xj69C0t0lqqSKSJZJipG5pCuSM5JPtGfnxkffOehy7VFZJVuVcsFbHjHOOekFxahEHDU79XlfnTNoO5dbU1Y3VchV02hs4wcYP/AL9dNHdq2BXpYK4BJWO31FLKufphgR/y6UdNMyTpECF4GRnkffpq6VsdwrkWqpKepkwAqlEJ3fuc9ArCAcGj9OnaIhia/ZbddWyjXucJgYWONEUD7Yz/AKnrOpLUSLpyaGC811LDUSpuWBpQrgc+Rz/frOrjCRtTYnrDxZprdj+3dl79094uFFc5rRc6ZUegduUIB2usg/xqTtHjPHtZPPVqewtbojQMUXby8adptM6nqNxNYX9SK7upyrRy4ywC5/8ADkhlw2N/uJqF2z0P3C7Z0z3iys9VQK6zU8tDMyzoHXcSI1GcruwyKzYOdu79PTqTu7bNeWKSxajFLPUtH6kc0o2xyyquU3OMmLDhcsoOAPAwWDFrdXwRWfhuF4s99XUn7i6ctrvQ188VLUbd6RF8yTg/p2452nIGSTx1k2lbBqa2y3aogLVdSoWFi/qLGoJ2lR4XknnG76ngYoR2sk1lqiOquevLjfKWs0/E0kbG2y1E0u3LRIxjLs6cO3qJu3ooPLZ3Ww7Ydx6LT+lKZ57ymomngBpmohEv5iVmwUUBsMzSMqheMM4QgbXcCdT26cxyhl3qv34pblU0F8msMklUZqVChkJ4Jx8DwR9+qu2uWOnutrE1SEBxl3yoT+c+SR84BJ4+mPkdXU/F3HZ75oSLWBeKCqjqvRjAZd0yEHB3fOcHAHODyAc9UdrqtKVoTKrSMqhFZ/JQucn6Z5Pj6dKtQuhBKIoxxOeQ/M+Ao3Quh02uO+qajMLexiIDykZy3dHGu3HIfAbKO0xAxl3t3O1hqCmNg0WpWONQKivmPsTj/CCOP35JxwB0ou5LVOkL+lJU1k1bNNE0lTUSoC5kznKhslhgEZPyR0ztES23U1bRaN0/MHrqkrBHTQTKpeQuuE9zKoYttG4k8E56me8vbSlksddLLEI7zb5VjVWRGkZcSbkJHHkHx8qD19a6aqv194eNu7wHsH5netzddMha2jab0ZjNrb8iQfppPOSQb7/YXCDOMGqo0ZZ5JJYgxVB75G4Eh8Aj+mOue5JUVKKEmw3Ht+MdTN+pnM0WynalhEce9GbPxnGF8ec/161elRVEyCFXGzIwfpjpjI68W1cyeYLJg99CdzrRRhEhnR9rgNj5PW1YoZJWlEQy4DZB6gLosZvckCo8arIcbvA+/wB+pClrJopCmx5HzgOq8H746t4ABkb17KoBylTE1Z6lI8MfCp7TEvBb746+7dWzPGsGDmLAVnfCj/0/5dRlS4mb1G/lT59uVPu/sR102xZ5auJgVdRhXURHaTnweeePr0M2eeKoKOd6/b0Zqe9UFc8oQB1XcrbvDA8Y8j3deg2haSy3TStvvIq6tZJVQyBqbblim72BuCv6QDvzljxx1576yqkkuNNEY0X0ASoQDYMkYGMYHgdXh7b3eSt7T2KthikSMQxRepEoBUxopIznOMfA6O+cyQICKcWFqLtMSUxks9trbrtpY6uSEjdwEhLLnG7DOfv446GNW9jdNX6Waot9DUWqsD7oqqBkA3A+WXdhv3GD9+ibRVTFJWFmkeRZN231JBtyX3H4yOM8Ho4kTfE7oxxg5G/I+vHVU0sWoR9XcKGHn+Xh6U30y6v+jN2LvS52ifxU7EeDDkw8mBFV7or7qbthWfkO6dkN4tEymCnvFNIWaEnhS+P1eP0thvJBbGOpTXPa3uL3O0+tRoC822bScyKEoaKpkcVBzy7AL7iCRuDnIGRgEdOB7XQVtNLRXCiiqoJ1MUkUq7ldfkEHgjoDOi9f9gppe53aYzXPTAm3XvTsrl1EYHLqOSVAP6wCyeTuXdhbLdXWmrwljJF57sv+ofiPOtXBpWidOZC0caWmoHlw9mCY+GOUUh7sdhj3KTVOu5nbO6aduz2eWnInpF9N4zEYgj/qYANyEG4j6Zz0qb3bp2rZKeGnkkkZz/MDADHnOP69ep1m0B2m/ELWTd1qCseWKZQlRbwm2WCpIPE+DxjPxw3nkHoP7mfg6srNUXjSa0u2pSQyCdeKYbPaUIxlcrjactk58dMotQjmQE7g+FcuvtEu7C4ktrpCkiEhlbIII7jXmnQWqWWZ546KpkWAH3rGWAP7jj+/HTM0hqKupaWJYJZYomRXxwfIBHTz7M9oqu9dq9U1lrsc0k1PKwmf1UCiMxq+0KzhiB/MxhWbAGcnpC0dtloYlWOMiNAI4wCcBV4HB5HA+RnqD8Jag7aDtcLVO6meLVtOlLcaeGaRcF5fQRHOPA3KAf8AXrOiHTtHZZqUtdKsQMcEu5AUf/nOB/XrOvjIe4UyKpH2c1y6c7vdxu3F0YyNDe7LPNiSEKygeSGGQecHAbOCQ2fI6bVJZNPd1KdtS6QP8JvDESPA6Eeq/wDlkjGOf/8ARcH5GeB0iKCmoNYVclkhoXl/OVeymf8AlsrRqMurZ/SAOduMZA6NJu1HcrRhivehbnM70srSCAS4EgUsCg/wrtAA58gDq2K6YMFIrMCElsJTi7e93tYdpmn0nfaAxLLGy/lpoxsC/pMtPIfa4GOcY5/UFY8TloZjbL93X0ffYtOXGxSLU1NNUyN6NVvVkJkjPPqlWYLKhEgMhXLjHS30j3z0d3BiTRHdu3fwq8x4VKqfMMcknAJWQ8o/uABBCngEjgHTT2T/AGh10vbyl1FNV2OmqcVVT/u3nEalirEHYWARlDYC+3ODgZ+1W5W0QdWMu2yjxPwHM+VbboZ0efX79kun6u1hXjmk+yg8PF2PZQbkseWAai+5HczUncqpS4X6aeOkgDT0lGSWDu3DyDACnlccAABQMZ3EqrUlzpqCmpGrpE4i5O4nj1W5GfPkD+p6uTr+k07Q6V/L01ppgyQCGjRYMsR+kBeAQo+P2yfnNN9a6Fu99ip6p7dLFIKcmPejKgJkY4HgZA+PPS6G0S2HHI3E53Y+J+A7hR/S3Xm124jgt4+qtIRwxRDki55nxdubsd2PkBU/ovVUlk1Q18sElPTEMrGopuHZhklQxARQeeSxJABxkA9NPvp3pstkoKe5UEs9ylukCzq1QgjeT4MpC8HLqeBjyfjpTaP7R19LYR6tWonqsSRvGQHAU54wSF+nwQPsetn4kNNpTUOnq6IVksjU4iqJKidZ3M4zufegC4bllAAwrLn3Ano9XjdgtKYUKoSKVdNqiuvEktVVqY3lcy4VcAZPjjnjkY6m4HYiOWNYxu9xbJBz0I22RYkWmgaVyAWkViF+fP1PGOpyirdsIBhYjJP+YjoS6jXi2pJdJ28miVNNWS5V9JW3SjkqI1kjaVKchHkQMCyBiDjIyM44JB58HVU9q7xXXGprbektJbJ9wt7VC/zJADjDAHhseT+nOccdEmnJK1dM3LUNthLfkad3Evpe0NtyAT9iASPHRtoi61uoNDUlyr23TCZldo1C5G4jGFAAABxjqUEzhcGmlhAHTLVVi/Wqvob/AD22qkY/lZTEjFc7iBnGf9enP267XzUppr1elUQVCetEqsN2DjYxA+x6YujO3dmuF9vOq7pZLc9ttUqQVLsRJM0zQqy7YmcZU8KWCkKSM4z1MVV0s1DWyK9J6cYCijRKdEaOMLjB2synBBHz+nq+R+IcON6YCJFXNV/772qkoNVxyUAxDLTofkjILAk55GCB4+vVvezEdJP2L05XflkhIbCyPCpxtiKlhuByCwccfTqvHfnS1wvlTb7tYaCtrA8LQNHSUErOgBJJJAZSDu/f29DVg7sd0dP22n0pVXKejoLW5VaRo2V425zw4z5YnI45+3XuFlj4TXol+bbjlV5tNVaLd2lNQpjMjMiBowEzHz7QoJ5OeTno2a5QBGWSRVIJA+Dx9s+OvOyXufrusqf5OppKfd53RKcj78Zz11U+oO79dBVyyXmpjhgQyLL6yRxYPJYtjOcbjt/4fv1KKCNRjNQe8689mr8pf7XBIxqblTQBP8TtwAP+x0xu3d6tdfaJhbrrS1y+rudo+FQED4JyeVP189eTlDr6rudJHLebndZpnznFSUUD9gRk5Geenf2x7utVNZ7JQhoI7dMh3FyXZnIySTznjqu5RQwCneoW98YpOA1Y7uJpC+di9Xt3r7S0Qe0SkHUNjjXbC0JOTJGBnCDzkDKNyBsLAOTT9xvHd+jodc9u9UUMlhq6P0JKKqpneSmmB/mbwoP8wDCYzxnIyGBOnSVwhulhhaokjcTIwl9Qq2/LEHOTzn79Ju0XOq/Cl3ULBJD211rKFljcFkttR5B+2z6+THnyyDpA4OnS8S/qyd/3Se8eR7/fXVuFen+n/NZP+YQr9G3fPGoyY28ZFG6HmwBU9xqQ7T3Gk0hYu7enpKmktklEBTQQVNQFEwSOaEOASCTJ6SSHjP8AN6pVTxwVNmmrYQsiRTzZ2ceXbjx8Dn+nVufxo2nt1WaKuGp7HT2QahmrqWUz00kTTzgsFJfaQSAmcEg/p6pFqt7tZtNtHY7vWRiWpdo6dKuJtrMxLbwYgyjwcZxzgdOpQWwBXKLW2jOWc8qB9eavrYp3sdPLGI2AMq+TkHOM44wfp1nQJcTd6mvYXQSCrOS/qKVOM+eeecdZ0ySI8I2pJO3G5Ir2q0Z2p7XWWgp5LRoWwxN6QdJxQpI+DzkO4Lf1znr77gQ2Om09XAWmFl9Pc6rCqbhz+kKACef7kdbLv3U7a6SoxSXLV1LF6cSqilHyqjgfH26UWqe//bq9YtFov35qasYU0TiNtgdmAGTnK+fIBAx9x0vmc81o4GADPfVF7/SX3UPcSvrblUs1FHVTNMkowSqOREm0cYwBg+cA/U5sf2pp7XLpuShNH6xlO6q3ReqjFhwHIztXG0Dggnzjz0KU/wDB67XFbcNRmnW326V0lenXfHNMSwXBwNwLBmGR4UDpmWKt0/V6JutpsKztW0dY9VSO8SCkp6U+kgAAOfULtISWGCrbdwChelVrnUZmuJDsvZX8z6nb2Cum6rGOiui2+gR/rZgs9x45YZhjPkiHiI+0+e6pK4adt18SCKauenni90QjqSrvuTaSdpy+QxGT8EcDrXQ9sbdVQz3H+G/n4rVCZ6qpDeutLH/Mb1Hc5Kqdp5HJ5+nUfcNHxR3nUqCjvl/raW3JJQXeiAanocq7SSytvjSJCo4KxuoZT+ryZ3tpV6s0Tp+4JBaqOnob3YZoq4Vt3jIp4lf/AH8WyNlnlOWK0x9MrvYMfGGHzRO9qxckiucha2Wyz2H0ARKGWXMagIrKfOCxDAkAfb4HznoU7rdsjftPhrpR1MKxI7w5VkZH55CtjLefIIzn6ddtrvlgNsrKe4VFSJljl9COkiYF58n0xuZdjKXxubIBBIznpqd2aiprLLa4a+ezO8VJHC5tVY1TCdoYZMjIjbzjJBUEFsZPk/RRBDxA71NXJXBWvMCWmalr6iKSJDJll5c4wpwu0+c/UeM9dtPdJkqVCoUGNpxjzjop7lWGa36xuAgoXZJpCUYwDbyoPtAxwMgHHJyT0K00T002Km3TSSR5LhFySftjhvPx9Prnq64Gd81nruNy+QKdXb7VivZKjTtdSt/DamB0nSPjcSMbj/38dT2kbtZqjRTmzUb0dHSVMvt3mQyKrckYycnjjHz0V9nOzlHc9Ff7R3I1ETVtPIsURjP8rjAZlznnnjr67d6QpdPWI22oho3gilBAp1ZVmkCqCx3EkH6gY+OidF06bVbgW8PtJ7gPGq7y/XRLI3Fzy7h3k+FBtj1Tqyr1TX2K10FXT6cuao89RJSvGrSLFwdzoDjKoCBwdq+T0Vaz7I3G4XGPWOn74amKKijhdIva84AILKfjDMeDycdNK3W3UFdSTVNotVfPS0gzO9HRSyRQDGf5jRoVTj/MR1FyVdUsUooKhoJZBu/lFds3zyCCpP0bHW0vOhjdWWs5eKReYPf7uVZqw6cjrQl7Dwxt3ju89+fpSiqGj0J2+vF1krpK2VmJi/OKY2UuOB4zwcL5/wAXVbaj8/WH847v6k8nvkZ8DcTgEnyRwBx09daWnuprGCutc97NRbTN/K9aojVnC4w5VIgM/b7dcujuzlNZ6OZrnUw1Ek8QjcyoZfRHyYzuRVYgYyynHkc9YUKqfrNjW+uI1nQdWdqTFMtSm3bNJuMmDhiT8c/XHzz9em/pLSms71p+qp7dZBVQzKYWmkqkjHuGD4YHPI5xx/Xrg1t23s1LdILnT3OG20dHAsAjhhcyzFSSzM7ykM/I5xt5GBx1s/iFhuMlLS1dzrIfyiCnKwTIo2jJBdU9pJOCSfPA8DqlmUHI3oWFUt2w3OhOm7WVtuM0N7sF1ow07RxVT07RJO5PAjLqBIxzkYyWUp9emVpfsnqGqoZbjTOtCLZB6lTOZmRYlHBkZsEKMYYZBOQRk4PT60nruWjtNra23yasitLRR0QkhgYQMqiJH3Pg71jYqGGWAI+gxzPp2eO4VV2tGoLhbBcJlmrYBPH6M4XAAK7vcMDHI55yOefnmU4Kiio7JGfj51Gdv+1PcCW70K1epfWjjnjleKneZ12Z5G5j9z5A8dNn8RNjqNTadeS8Vtup7Hb1apeRVKz00ixMFY5Y+oOTwqjwcn6R1HraC2fnKo122Sqb/wARJJIpMrfLeTnzz1zVOr7Heaae11c8NTDUApJGwLRyKfK4PGP/AM9CE9YGWUbHatLYNcWMqXVuSroQykcwQcg1XLRS0mpKmnsFQWkMqMkcYpDJGynncSFwrZIGXI8+c9Sl+7AwUBrtWX3UtZLRemxkhjhjUIWJwpywVvceCCcY6INSwaV0FryyalsFrpYrSG/L1dNHCojjOCC6qBgHacjHyh+vTSuGurRgQU2z5TBkO05PgKOg7C4l3gO5Q49o7j7v7UZ8oWnRLPFrlmvDFeqXwOSyg4lQeQftD91hiqia0/DzTX/TdLrXS90mrK9ykNZalLNNABlTIZDw2SF4GQN3WdWynviRRJNNbq1UQ4ZoqBuDj6s2COfp1nWgW/IGK5qkGBvWqO01kpWqp9F0MczYKzNaUjmK+32g7VIHDff3+fGFp3+g1bFZbEauOWnSG5BxJJIrOvsbAUjLKPnHAyo+nVsI9A6sqJxGaBwOdx/zHcuTzxyRn9uk7+KLt/X2LTOnbZJRFa653oRUytICXOx14/rIo/r1i3v7yJWZu4f/ACujdFuiNjq2tWlk5yruvF/CN3/pBpC6etl9FFHcaChjq3E6zrFIUcO2QeYw4ZuATzjxxnJHVne3umHqNNUlfdrTTU9dVQD85FHCVG4kkgoQCuQR7SP9Om7287Q0Fl0lZqKpiolq6ahigmdEyDIqgNz9SR/Xx0WLpq3QYWeo244C+kQf22+cdPLZBBAsQ7gK+1m9k1rV7nU5T+sdiPIZ7I9FwPSlFS6Uipa+WupVngaWL0ZUilaNJkznbIoIEi55wwI616p0k1405UWqMinadG95GVGSc5X5HJz+5z02Limm7YoM1TI5LKhUSwFuSBuIyMADk/boJ15ftCR0MQpNd0lAPVCyejcKXcYy+1wC4bBAzkDyRg56vXtHApe6KBSboO28bJ+RnuUE0bZcSxZw/OQcngjx/YfTrsvFKIrU6vUyTuuMtIST5P1/6dM7RVLpC92CC4WR2rKFyyQyTurMEDEYZY8KpyCRgDKlSeSSRrVdqYwVIgiwqnbhseAfjHxjrxgeLLd1UBewc0mW0Vp/Vt4o7ZfaFZImqI9pB2yAFgDhhhhkE/PX13C/DdovTNyob7bqw26gaVT/AD5dsVOwTIACgkl2OM/Xon0bQXR9R3EyWiWrio5KRhVrNEi03L5VkYb5d5AxsPtIBb2kgsPu9rGn0JpcTq9E1VUn0oFqY/VUsFLA7AwL4CnGCPPUznah2WLhLnurRoqgFTpCmqKWGSSF4y0cklNsLRgYyOcnP16SdBMKaEwPG0Rgq545FYYO4Ock/wBMdOztbr+n1fp8Crak/O0qn1hEvpAjOP0EttAyo854PS71tpkQ6Zs/cGlVlh1HU10M8W4MsNRDUyomCPh0TPPyB1sugd0lvqJjfk4x691Y35QLRr3Slnh+oc48uR91W5/D73j7RWPtLQWq66ts1krbYsorqWtqUhllcuzeoqsQZQwIwVz/AJfjHVQ9cXu03jWl2uVghMFvq7hPPSxldpETOSDtP6c8tj43Y6B0uc6LtSZwB4Abx1Aa41S+m9K111iRpKl0MNOgPLSNxn9uRz8Z66VDo0OiNNfu5IIJ37u/1rls+rTa2kFgqAEEAY7+6lncNV3q6ayuVPYrjVQ0yVTxJiEOo5xxyPHP+vU9dK7WtqsVTdZaqqnEUe8RNQxDA55OX4GMc4PQX2Nu1FcNdWuy3eDe9YJpKlahB+vaxAGfOT89Xbt3a7SN0k9SpsVJggbVUYC/tt8H9uuKXH08zSH6xJ95rs9kksMKxLuAAPcKoRFLrXXtsrbmaG61kUMyItRBTsyLuyUUlAQTlWO3glQx8KSNtLp/VompZP8AZ2uUsgSfcuFKAcOpByec/Tj4HXo1bu2GnLQGNr07RwPKu2V46RQzr9GYruI+xOOsrO2NLMplFkZznhkp9xPB4/QSf6ZPHVHAvLFFNbdawLVQ+NdSU9bbLNT3Gb8tWyLHWqgxsjJwCS2cjOfHPHT3vOrtBWDS1Ta7fGsV0paEtApmlcbwAq4yxGckHH0B6FO/unKnQ9bLcqm0PbI5KlY6OF5Yy7KwGJFRSdo5PDAMPoOOlRBX1VwphU19Qss9T72aM5UnwT+/HVDwsTmnlsBGoFS9u1XqG725JrlVkSuzFmc4AO74wp4xjHPjHU9QXY08iSTaigDFduEaQ84J8cY8fT56G4Kf1KIsGHBwB8+B18RtDSKJtoLL+rPBI8n/AEB/v1XMnGeEU5S4Ea70zdaaotE2j7XZqFoKg1cTSSuJA0kUqSkDIHIyB8/foHhqbjVSwzG5VpendZEAlZgWB8YPGOOlvomsqrhfK6Zap6SmSp9R0U8yZBwp+cA548dMK3bjIqNI4yy5C8Z6oSAWl7Gx+uCp9o3H50fcTnWuiN7b/WtZI5l8eFz1Ug9mTGfTNMSp7s6znCQSNEVRQGBpsnP3YED48Y6zoVmh2xFigc5BOeCfPz1nTLqlzXKUuCBXrPKxklKA8sf0g5/0+n9eqq/imvlBV97+1VkqKkVENpqpLpUxwkMf97G20qD5xTnz9T46+u4X4iNUW+UWO5t/DaSINHUNC2+d8AjczIFBb2lyFAHOMdJuC+WHUnejRV7tUpmoaq2TVJbB9Qt/4lSG5zuyg/06SajEOoUeLKPxFdZ+Ts9Xqs10DvFb3DD2iFh+dWDuPcLUl0rBJZjLa4I8iOb1Vd+fghgYz9uCR8H6arbpe63Ctju9fdrqtTTtueV6uY+QRyN/IO7z48dTqW4PbIZLUo9aVeGkALqPv8Z/bqQoK+PSdEaq/VaBVyr44L5/1OCMgfb7YJ0eTtisyzYG1Q09ss9SkljmWmkil2h4jJlsjH1yR+kZ5+o8dC2tO3Ogqi2vp+ot0NHJUxCJJ6VAXiBJy2Tx/wC/TMittBJVmtjhCzli+5Vx555+v1z85z9uvq4UVFXMpraaJ2Hgn/v7dXqCDmqixIxSl7QWuLtVU3jREVe1xp4nirIJps72jkTCqT9vSI/p0TS3W0XnStRdqGppmqZZ5IZ6ZJAXpyryoBID43+m7Jjyoz1BVkcsPcq/mIM+aGiYDIC//P4XP7+MdBGnNS/mbre9H09PQw2+gqZD6Ykj/Mhx7y5ZVDuu2SQZckLkBcE9QfIY5qAcquKmYoqJauOrVYWlwdsyqCwLYBKMeVyOMjGRx0GfivueoZq63U9NaGNniQPHWPKNrTFWYoE8cKq/Hzjr61FforDbqi41ckcS0iNIQW4Hzgf6YH3HVcNbd9NQ9y6sUl1RYqahRTRxJjMW1VBwR+oHAPuz+rqcMZYcR5UDqRVYSAd6fn4Y6uWht12jq5wHJCsxYBVHuyPceccdfmr9Q6nt2kK/S9TXQ1lptlTLV2+IQBfSLTvIzCQcniQn3EgfA6Avw23OnuV+MFfR745ISsnq+/DEnj6eSem/etP0r3W4aeqaVViniKvCjYAjwB98HwBnA5POeD7bzPbyB05g7VVbKtzZ9XJuOR9lLK6U9VQ3B6KpjKTrIY5E+kgOCB9vkfYjoE7kX1KO2S1i05rIaZTGkakZB/z+7g88/Xx0ZV1F6dvnfTcP5qNZ5aQvDhlWoXG8DHg4cZB+WH7Droe31ken/KaoWOcqqs0WDkHAycfHOR/TrovSTpUNQ02C3jOC27+m2PU71gejnRJrDUpppBlVOE9d8+g29tKX8OsP8d7s6Xramkp5mFxigMoZSqM4YhShOSDg+OOOvV3TtHb6OBRFSQIF5GFjX+vHXlwLDftCdyKKXR9NUPS1FfHNHJTxMjw7doJDL48lvPjPV+6Gu1fcbNQC33qrmrxTpPWxQTyMzDGXBVGUryThh9s+esL1ik4FdHit5IF3G1OaSvgBO5o1OMeRyOuOqraYReqzR7RxvJyAfgD7k4H9R1VTXvc2ssNFUV1beblvt0cEdbJDf6uCFZJSAgjiaT1CSSMgnjI56BKT8TmmKCsjrUrbhLWw5WKasq6qdVfaRnZJIw884OR4PX3CQc1ONxIcAZrh/HhWpUVscTK6SUlSiYJO9cqCQfoc8f06rJZD6ttglAclF2gZJ+T0edwdW6i7n3kUjxBY5qr/AOJqJgQVJIEjEjIAPOTnPz0L2qSntizUSFJVhnli9VSGV9jlCQfkEqT1TI7YwKujV0ftDArfQyypUBXQgEHAfz9evqrjVkcSMDkMMD+3XxUTfzhOS2VfjAzxxjrAXZHZDlyMAN/f/p1WpJwWouQsRkUC6BcRXi7xFv8AGSAecHe3/r0zLfIvroyzFMYw3nkY+vSz0qyx6su0fsGWkOOc59T/AND0cmtFFRmrELSmMg7BwWPHVepAfQMOYkX8cj86ddEj1seqQHk1pN/RwuPcVo5qJdgWXcCMDGQOs6D4tZxVKLDNbqmIjjLDKj5+Oes6MIwa5OKs5rSjo9e6Pj1PbrcstcxH5sJIBtVfleP1E4Gfn2n/ABdKnsUKfTveGxUlzhkpqSkqKlIxI3/yXhl2D7fqxj+/Oenx3TsI7Ma7/OU9PPDp69iWrSFpV2IoHMQAGF2q2ec8BCMeiekx3Egi0pqq0aijl/PYqPWkb1NrzphGBYHAjZlY8A4Bz9D0m1TsW4b7LKf6hXVPkzPWa69n/mwzp6mF8f2q1U+vJaxVpbLSqiMu2KQoVY/QkjOB9to/fqDv+iNSalpKBG1IstQK2nqqlmhIgWOKQPsUkljnH2B+nU1pFrfcGt1xttGtbQVcMdUtQQMPG6hlYcDGQRx8HI+OjW4TR8SxyRUkKMG5wqADxnwMfYZ/botGYClDCNuVcEt2o7HBAtwnZ5HHnLHcfk8fU56647rRtCJ8oARkblb/AJ9QN9vtFHOryQmqyoZWVdoAzjcMjBH3+3XFVa7tcNFGWp9zVBIEXqplF+pGf3/t1cqyEZoZmReVQmqlePX4rqVwTcLQWOBt2tFIFByD9Js8DPt6X1rgs1s1XeqemucdVcapJJZjsBljUxqv+8ADFeVOwn9QU/Trst2r6fWfdmvpo1YQ2m3R05Crui3tKrEeovggNCWAOcHrtsfb661WvdRaiFPWyUKrBiRPQMC1DrszK24SqSjnaFUrkgtgopEZOMnevARjJoO1bZ4r/Yaq2zxM5nAycZIIGVxn4yM4+vVR9UaFrNIXJo9QpNSOp3IkQDesmWC5IYbScYHGPb+3V2ayn90mGXETZBDE/HJY+H/px1r7kHt9a9D2286m07RV9dUK9FSrJBuMzFnOT/wrtBJbOBnGCevIZyv0bUNe2vGOLupWfhUsE1Rd/wCJU7g00UbZdo2j9UD9J2tyPpn5Kk9WBNfbpKvXlnud3qFqWpoGtdCJ321UjRSGQFAQpI2pgtnHxjnpB9nu7Gn9KVdTQUGmlgFVMoAD5ZVXgMw+AcHHzxz0fa5uEP8AHpbhgI7Q7gzcEAjnJH068DFW2qu16vgwnrX73U1VY7rpTTVssVHNFNYLRFQVavCFaWeONASvkMfa+WPyR0h9X6g19W6RmutlWelnglQVcA2sY4vSd2Zsglsbfefjb1E0Gv7hDZIqQ1amVlfa+0ZQEk5IxzznqOtN+SirZZbzqkmee3VdOiJDlJfUTYFbLAZIZgcKT7s9Es4ZcsKtMkMUyiM0U2e7vU2+2TOJVZoAGyMEMq4yfvkefp0y9K9zNcdtqw3DStthrFmppoZTNC0u0uytkopGR7fH/oOk/o+4SVVro6OU7pYIzG5LAk88c/sfnB6dFptUVVQwyTxoxkRHXwxAIznjkeRz0FESr8QrRalOrW4RRzoJ7f6noLr3Dutb3VqoI7fdlnllpqqmllatmYlo1UZLIVcqwLH/AA4+3URPQWUzSx0lIkELs22NlGVByMeB/px1Ldy9APPdrFebXQtV1K14iqDSRB5Y4ApYs74BChvA55P9vy3aFulQQTS1MefA9F8DnyOPjA48cjqV7PI4xGKhoAsoTwTHeomuNBDEqmWCEEEKWKqAfgjJznpY06CSsrlhl3oayQAhhglju8jj56aerdO3Ckt6plvUikGInfbhxxsOVHOQQf26/dF2mk/2Tq4dV223T22oqvVR3mZJHKgA5ZCGAU7QApHkk56qtX7HbO9F64g60CIdmgWKmgjpWmlq48MQAxYbRj4B+vWz1Lbu/wDj6dnzlV9QbjxjkYGc5+Ombc9K9p5qSd7XoSmmdtn5eSsiLMuc4Db3yQcg5Vh/TobodK2q3MKmngtUMpkDQyJbFkEOGyCoM/OORznx46k3Ee0DWfacr2U3pQabFng7mTwXe4CioZpCk1SWwqLgEkDBOdwxg/Xo61nPpingMmiLlFcaIIg9aZ5MNJu9w4XORwOBjj9+mPYbVFY665utmsldUSuspqpxCpw55YJK6HdyDtQP4HUEKe7VWpK8fxhYqiD2NPHBEgYZHAXbtHj4APHnz0Jf3e8K/vqfdk1uehumlbfU7pv+1kX1kKoP/KlRFcb7FDG1NQ0xkkAAxDUSHGMknhecg8fTP26zp22zVOpdC3Spq1rFvdPJTgRpVxGRFk3jOwKu0Ng/POAes6PN1xnIrnS6OsYwy716Q93e3dm7g6HrdLLsFSGFVRVk0eQlQuSp/wCGM8qwXGUZx89UQ7i0i3TtzNa6+nkpbxpOoWNqaXmVYAxjMbY4JTcORhRhm59Qdej9fOzhW2ExFd0WPAyf/wAdU0/Fxpy1ae1FR6lpCT/HoJaW40xTbGwVAvq7vAYDbwOfYreEYGu4g+dQND3kEetEaBqv+z2s2mrDlFIrN5rntD1XI9ag/wAP2tdQU/bmkscdtrK2qpZpIKeFIiz+icOrk+MEu2OfH08dOaz2bVFzjje6zR2pI8kiJszkn43Yyv7A9KX8J2t4amwVvb+4yBaqzzNLACMloHbLYHklZM/0cfTqwG+RwshJJxnGcgft9upWU3XwJKOZG/tGx/GtB0m0caJrFzZKeyrHh80btIR7VINbLVZ7Nbacq9to6ipLbnqZYUeZz93I3f69SEt+q6ZCKX0kzk42/P2x46iQ7eQAPjr8mkcqBuxx8cHz0XxsKRdWpO9JWrtsene7kNElTWmmulBXXGRJX3bqlpoMt9cKF4HxuPRLp9aWlqdSU0l9MazvDWwW5KLK1s8ZYRkTH9DRuqkjjOR5yeh/uk/5LuLpOtVmQvSXGnLL85RDj/Tpc6n7q1un9c2yzNTQtTXWbFRNMDkICPA8D7EDO7HPJ6oeRwc18sSlc5o0ujrLNOAAQjBR9fkAk/XHz/fnqU1Z23s3cPthFQXpZS1HvlhkilaN1cZPlCCQd3IPB+Rx1EyxiOeb1D/u2wTnIz9B/wB+c9MjS5jm0lNHtBwHQBjhRwDnJ6FBZnya9c8ULA1S/Q2ltNad7i0V2vNTNBSiRIXWZiUmds+mMBSzHec+cc89d/4j9Z3KO/SaW03FJLVPS5nWIF3CgnK4AUDK593UXeb9YU1lTvNRS1K0NYtQsSyYcOhGCCPj2+PnoQ7wanuV51xU6lpI0SlrqQxwxRPtckYzu+rAnOBwQPp0fbgO+KzcEvArIDvS8muyyhIXqH9SMKrsgAG7HwcePH9c9Rl1NQ0rVAcmMDa2fPWqmq4FiERqNjxDYAx3qwUkbl+gJB4+Dn46+K2sggdXarjeN1yTk53fTHjq90KncVWkbh+Km12vjr6emhuUmyeBnDMrkk7fJwOr+dmNGWW76Jtt3rbbBJLh4yUXAwkjIBj7BV589eVtn1TWGqp6WCvniiB9yiQrhs8Bfjn78dev/ZqOnh0JbpaaX1I5IVqdxG3a0qrIy4+zOwx/fnPVXUcB3760IuutQI3dXDZuyF+uGp5btdNW1q2+mqlqEtsFQqQPSg7ijqYy5yQAcN4Jxg46a9gtXa/UsNT/AAGw2iT0VUSOtvVCN4OMMVB3cZI8+Pp0he6/fvXGhNYTaT0/RW54ZaSPE0lNJJKvqsQWQiVQT7Rj24GOc8dNnsXos6Pa/U8l4nrTXPDMY6gg+m4LlyOSeSwOfv0V1IRc+NDqVJyBiqmd+LDVQahkRfykFuQgtHLAwDlSdxMiupycfA+pJ6GdK6a09cYaOpqa6nqILc0plVJ0G5/btDncAqn3Zx9OgL8dV5p6nu9U0YLCOji2HawL59aYgIefgjIPx56Tds1RW2TQNro6GV91xr6nJRmJCoI+PBxlnXkEeT0sksxE3Gxp9DqDXirbY35VdOm0tpqgNMZ6G2TpISsbxx+AefcQcY4ADccn7dRurNFW2+1SC0CGkgihzmOIrvz4+Qc4+nPPPUaLhVzUMdQ7lmVVAO48ggZyPnrh13ZbjqHT9LJbLbNW3FKuJUEMMckixkn1CGkBA4J/rjpet51jlafXXRtNPt+uHPnW+5W6026BnFTtNPGu6eI5EpCgEnI84Hx0u9JQ/mXqbrMzA1EznlsZ+f8AmT0Q69krrPppLdcLa9HVyN+XEQKFU+WX2nA+eFGOehmkrqC3UEdMVmdoVBYKmRuPJGQT856XXKtNdhPsjPqeX4ZrY2k0Wm9E3mOzXUiqP4Iu0x++VHpU25CxCONSGU8tnOes646a6x1cRkpqWVyuB6IG1sn/ABHI/ThSOD5I6zo5YWxWKluLfi516PaV7lWW/VIsUqTUxmd4aaUggNMrEMuGGfCjB8HOB7ioMX3k7ZR9xtIVthn9KKVR+YpamYeyGoUZTA8sDkjj4O0/q6rVfu6tx0tdhQyLbkekkSWb0KlJtjk5KmRWYBgy8gEHxx46mbr+LLUWo2zTvToikRqVhBA+vDA/TyOr4p3Rh1grESWi3IITlVYbLf752t7jCsRZoa22VBhnil9pZQdkkZIxnGCM+DhX8MOr4aXv1t1PZKS/WuoL0tbEJYiRhgPGCPrxz1UnvNpeq1rp6Hu9a4zNVx1Ho3pioBUMUWFwB4GHSNuOf5bHiPog/Cb3Famucug7jOxhrA1RQ+pn2SgZdOfG4Dx9R9SczjdLe5MY/Qk7S+TfWHrzHrW/mz0h6OxXw3uLICKUd5iyepk/l3jbmf0SdqtWMY5Xr8lA2A4+P+vX0pwDvRl+eef9R1h/nIFjXe5GAqHcQST5HHTHntWQBBpK991liumj62GURMlwlhLsONrwPx/dR/bpNa/ls51NYq6tjjrKhZW9CQSFUUBScY5DEkL8ecdPXvOlJWTaWo3nhScXiGb05gVUxFGy3POCG9v9Oh+m0LYL1qOlirq1bNFRI9TR1zSCIRzBlATcyspyjuArAFsLg56gpCydqhHkI2Xxrlqqt4jUOxTadzMF5A92c58H55+nSl7r98Z/9l6rQen7h+XeWQfmKhSck4GUGOV8efv11fiR1LctNW+msFNVRUpq3EM8sRKE4XBKA87PIz9fnz1VCsr7hXVUkklSCQ3DKx/mLnySPP3z8g9Qkj4Tmhb25MS8K0TNUTGqLidGfChnLDJKjA/9x56LtGU1l1ZeLfZ9VUQqqCaceqqlo2GThtrKQVyD5BBzjpZwTbtymMMwGOBgf389Emmby9HX0oRm3xuroM5HB5XJ6r4mB7NZmOQq+TTO1x+HDSlo1Tdqamq61YoJ5BDHmNAsQLbF4GSQgUEnkkEnJyelfrft1ZrBo6evpI2klnmjjTMm5ixyTwR9ATx9OrL/AJ6t1FRVeoxQTVUiJG9X6EUzmFZWIR2ZYmjUeThnVjj2g89Kfu/HG/buZYm3RwzBwQf14JUDyceSfrxz89GRceQWNaReFkGKrZRU7QVIE0LCX9QVwoLA4xgZz4x4Hnr2M/D5XUd17cWU00/qGGjgjnO1h/M9Nd36gCcjac/Oc5znrxrp2b1fWV8AqRjOD9uf2A/v16ofh01/YrJ27tq3G6U0J/I0ki7iSzFYVTG0eeEXo2XLkACvY1w2Safd/wC0WiO4bwSX2hIrlbdHW0u2OoRUyQm/bnbk+M4P06nbD2e07YqiSonr7tdlMe307nVLNHkEYIAAweq86s7z61g1RHJobULG2ERsY5YhGpdSGcAFST4HO7nd446b9L+JDSdJbYTc4bh6kMKrLMojw7gYzgPnBPUSGzg1evC2ymqGf/qCaPtFg7lU9VQ05U1wlWRQxdVZWTaVB4XAkPjH36HPw5mjqO1tTSVlKk35e7TyKJBnbmKLx9Oiv8dmvrfq662fU9rjdYpamoWKOVAGyFhzuIJ/ynn/ANOlX2G1nQ27S1wpq6hu1W9ZWvMgttE1QFJVQVO3xnA+Pj79L9VyYuzWj6NGJLwNLyFP22GOWlEaxqqY4AHx1Da5uNfbrOHoiPZKrMPf4APPtOT+2MdcVHrujpYUo00XrN3XCl/4FUf/APP3z1xa3vVPf9K+jNp660X5ipWOmjrqf0ZJnGCcRMN+0Bh7vBJA6yikWv00nKuoSwN0gddPsRl3IA8s958gNye4Ur9cdwqsUNFLVuRL6jx0yIu07mYn1Np5zgLkH5wOmrFTpVxQz08PppUKJAPT2ef+f79JXv3o6o0ZetP2i5KBXrF6lVGScRM5U4GODtVlBx9Sfp1d7td2qtd90TY7r/EKKQVNDGd4qkRSyqFJC4zwQRz9Om9rYmaHrWOHY5Pl4D0G1YPpR0mtf8RFhZ5a2tVEUZ+1jd5PbI5LezFJmO3VVNG3+9ZPJA/SOR8f9/HWdWil7PWWhhNRLUUkyLhXSCqDyc4wduQcZ+3WdEpYFRgvWXn16NnysdVK19ZW0x3I1PpqNzKkVX6yHGVYEFgecj/GvP26jrRbbjNSyYb2OSof1+QfkAf1+Pr08e+mg3p9caf1w9JHLar+tNb6tQXDb8kHJB4JOzbj/KeojvPoOxWO2Wm62eh/KQbDGqxk7mK4OW584z/2B1YY1cZpZa3TQN1Z76muy88NTSXrRl7SOSjvFE8M/qOF3IylCOfghtpA5Oft1WW7RX3tp3HqLM0sq1tsqzJTz4IZip3K5+zrtf8A+4j46Y1rr62WkpapW3cAI/yGBIy30wG/59MjvPpq2ay7NLqwWuI6gtcaVJrUQbmgUh3BI9xxtG36EkeHPQjQJOvUOSPA+B7jWg0nV5+jV+upWwDjBV0P6MkbbOjeTD3HB5iuaz98+6mtKGKpt01rt8SnZI1NGokLD/AvqHDSEY2xrzjPwM9FlLT6guC0r3q+1l04zIk8jek5LEn+UTgeMeMjOMnya49rNY6g0tXtqnTkclTRGEtdaWI8+mjqJTgeNpZTn4EmORu6ud28tmktc2Sl1PaNQm4U864fYoQrLncyOCS6MCeRn6EZBySbW4JJguNpV5+BH2h5H8ORpj0h0OKKNNY0UmSxl/RP1o274pPBl7jydcMCd6X960TQ3aldfy0UHqMZlSnQIUOc5VRj9OR9sDA8dcUWpaTQk/8AF9YWimrpaFAscdZTh43cuArhWBG8cMjYyDkDHViaWwWegJamooxIP8TZZh54BbJA5PSO/FLpc3fT0VbDOsSs0dJM7MTsYuDBJj6LLsDHIwrMfjo08JINZVIuAcRqk/4itQ0d/wBapWmr9aOWL1SG8oTkj7lvcCc/9OlHR1MU0LSwExtu9oP+XzyPGejrvTpCpsl8gaYyzTzRJJU5YENIR7gm0AAYx7c5A5JOelzDUQyNLTyo0QbBjCDc2fucgH+3VjoGGRSu6Uuc0Qh3IVkhIJbOQx56mbXGZqhN8LqAwJYcY+vQtTzzU5Cb9yR//LkBVv8An0T6WrGqqqOPaY3eRVUF8+Tg/wDMdBNldwKThBxgGrPdpr7X6asF7tklvjmjv1LTK1ST/MjFOr4/uX6UP+0M2uNK6voJKBIY7apkjyxYuPdgefPj+/TmtCxPQU/qlFLRbTyFOcnIB+fJ4x8dJmroLHpWpuWn6VqqpN3jdpFWblsKpVEKr7WYMRz/AOnR9nC9zsTWhHBGgIpOaO7e601nJN/slpyruaUrJ6skaLhWbJXILL5Ct48Y6uboTQmsrHpS10FfY5Ypo6aNZEeeM+mQoyp92ODxn7dA/wCDCjv0CX+qqqOY2qZVEbO38sTKRvGCMggMMnx9On9ee83bKhD2ybUm6qpJXgkp6ekmkZGDEY4Xb5BPH16Ille2OMVYsJuB2RQDf5bjpiP8xc9kKuSFLSBucHgEE9R0mor5XxO9utdTVo2SGipWfH7Mo48eOpHUstu7wxLbdKy1vrwyGRVqIPy+5cFSRvU5G4oCceWX79TvZy0i109+s8kUn/hbmYgZ8M+Ai/2ycnjHnoZb1pmq9rLqFBO1Ux72V1+uOo1mu9kr7fTr7acVaMjMobllVvAJY+OrB/gc3S6R1NTnD+nWQkF8tgFG8DwPA6DPxj0oW+2hhGVAhmYDz/iT6/8ALos/A/erTadI62rrzVx0dFRvSSSSyHAGRKPjkk4AAHJPA6o1UAwFs4pn0bhZ7wRqCxJwB3kmntqautek7fPcbjIkUcaZZsYJ+AAPknwOgvtXYazuBqo90tTqsdLTSbLTTyH2rsJIf7hT4+rknjHQ/Wvde8mpFr3iqINM0spFNFKdrVJHknB/fJB4HAOST05aGaGhoaOioxD6UMexFijCBABjH/TrBNx3JDZ7A5eZ8fhXbL64h6JWkljER89lGJCP2SH6gP22+v8AZG3PJrZU9udK6q1lXV9507T3d4oo3Rp5CgjLqApA2kncsTeMfpHnrqGnNXWpPyWmLjabZblyYKeOn3sqsc4LNtyfuAOu2yXCpo7rXUtbNSgVdJTNApx+ZbaZM5f5VfZgHwWb6nr6qbukyOKSty5YMcn3Mfkk/J+M/brV2r/Rjh51wG/tz84yOVao7RriUr+Z123jmP8AKRMgP7Z6zrUbjVRys7b8H9PuO3/T56zowCQ1QyxA4JqX7t0C3rszWVMdQ6VGnq5KuLJICEH/AHhA/UBvc4zxg9Aet6ujv3bOG4XSKQKAlRFuUxrITldi4OSSTtPPGCeiy9alks9/oLVe6ikGmbzT1NHcIap40ilJQlSxdlzgjbtBPtkc7cgHpVa+ulv1VFQ6feFKK0W12ejpaFjHEpIbkEHPO8nzjkfQdVmVYFDVbFp0moPwxUtaDUP8poYI1WND6gTAYhjhWVsfsR/T69OfQevIZbFPQXenikpMGOMgbU2nIYHHIHIyfoCBychdw2Sz0Ct6FBGruOT6algfr1y6UsEel1mipa+qkilkeRBIf92WznB8/J/v0LJeowyBitBD0UuBtK21BCLW9uu501u0eJq6iM6PAppi0StgBqedCcbNjtFklQyvE5YZwHPfJ6TsxdKXW3aLWlJWNW0sUt4sT07xwzgj/fIoG1FblgoYFN2FJU7Qpe7VHUPYaya1sWKyQ1tR6ZYYSLcrE7fAxKGJ+dgz46gK/VT19DaJKmPKJT78Bjuibe2UB/4TlTj5B6Plt49QhVjsw5EcwfL8xS+w1u96EXzpBiSGQYeNxlHHgw8RzVhhlPI1au2fi0pNRejS2qwCCtdMypVVOFjI8kMFIKjn/iPGASQvR9qbtnqvuRoiOoqdXl6G7xRutNS0AWEhwMBwWZnyG8KwGOD1R6BKC7wxz0UNTHUb2Y1S7jlvbhT+3Jz556tF2q/Eb3C7eaPpIu5ui7hetKRZpYNQW9N0kAGBslzhTgFQNxQ4/wA3HQYupbM8F6Oz9sDb1H1f7Vqxomm9Kx1/RmTEh520hAkB8InOFlXwGz45qTSgm0vdL0Lno3Vluhqau3sIRUxQgrOrA7JViOSjHB3KfDK2MDA6Sfdb8O95s1bGdK2mpqkMY9YBN8glx+o5IIBwT54OB1bKu13pG496IdaaF1Ba1t1WKaqqZKzMMULmR1b1lO11VWaNmI+Cccdd+pTSV2qKiVqyjqYaiRs1EbD8vIXAP8r5cZYgfPt8dHm8jUAxnIPeNxWRu9LuLWRoLqMo45qwII9CM15uxSNQVEkFfGX9D2bcYw44If5BB4PPTD7fadq79cEqaOnQwx7WmlwNoGCQF5+oHVkO4P4fNKahmDw0v5KoXHp7FCj6srj5Y7skEA/Ofgi2oO3dFpetsWnbXRyRxuzFyscir6gKry+CoP6vaxwM8/atbuK4PCtKJdLCN1hovsVrs1NrSI3m21M609mb8mEhZ0jqmlXk4YbcgnliR9ugy8WejtlgvVTPp2Oe52wVk8FSSWaRWdWQsuOdqqSrZxmQg446b89LFR0UtRIlOiUpWIgzwM+CpO9olkMioOPeUCkg4PGOlRqfW2lb3TXejpEneelRnInUGGbaMYVgTxxkD6ryBgjotGeLsg1N0Thop/BpldE3WnwfUSvbeCpRl3RoMcgEjH9/I8g9RfempvNJc6impwViFTvLLwx5Jxnz89aPwd6jo0tuphca+khxVRvuZ9mQfO0eMDGPsAB0Ud2EivV2drUY6uKoQyCVGypA/wApHx/r1Tfy4TJO9O9BjVnoY0a+odK2emv1trqo1FYjK59QYRC2HTPkEggjPBIX6DqH7f8AdTuE9wv1Vpun0/P+brC7S3OplErsqImSka4LDYc4OMk8cddUN1oNPW2SC4hpKqQFRGzezbt4PHyCf9OhPTDVtvP8N03H6clTMzB2O0bmHJH38n7/AE6QDUkt+Zy3gNzXRrXoVca0BKF4YRzkbsoP5jt6DJ8qDu6+p9Z9z9ai13mjokNoLRSvR7xAd2D+phn6eOeOjr8OWitLX65XOy1V3lntlnSOuucUcwjSokBKou4nAA3NnGSAWAIJJCY7sakv1sv9fpmOZEjiIE7pkSSlkBOT8Dn+vz9Ol/ar5cLfDLDSV09KtQfckbMu/wA+QOCOfHTMWdzrEebk8Kdyj+7H8ht45rP3XSbSegszW/R7MtydmuGGOEHYiFTy85G7XPhC869Ftcd2e1mlbTNFBX2OKppYdtPTxgsFZVG1MqfaP246TVq/FmtDSCsq7G8kk0bZgjZPTV8+3aMbiP69VVq7jPJG0btJuY87xgnrYtVP+QhUH2bicYPTC20OBFCuc4rn970hmncyA7n31dPtl+IFu4utp6y/0kNBRUltkijSJCCSzx5zyeeP+H9+jOXvJoS0TNHbIqm6VQYKSzOVJ/xe2mScj9m2/wBOqr9h6ijprhWVEtHawrUgBNeZEp5JFlRvcFwXx/lXn6jo5re6VntM1d+Y1fFSrOQiQWKnjgEm0gFV3kSqAT9BxnoxLSGHas5NqdzO23KnRVd4dbz0K3O26ZNvoWOFqKqKKmHnja8ssm7PjBjU/YdZ1WO96mut0Z7XZdF3arrIHZXq6n1WPDHONxwfBHAx1nVwji8apLTNuauP+JO0PW9uK6rhUmWikiqgB8qjZYf1UsP69KXTF1TUFgoK5CrhFMXLhQpQkfP2A6YdFpmr0fpq3WrujryumnuUgWdK28ioyxb3RSEyMIzygVGAY7zxwenNpDtNo6z0woV0pboqVJMJmkT9Q9pIbbg52+R1nrqLEYVga2uiauYZjJGtVyMNfNE88O70lOJJREdqj/zH6f8AXqHq6v05PT9bcPH6zz1cRtHWiuSawzUsP5OZhHIjRqy7Cyg4GMffn4B6on3TsFXo3uDe7GLhUfk6smvoRDUEiFWLN6SEHhUb1EI/ypHjyMjxaelzsNqez9MXtRmVc1JrqGKy6ptVPVUUdbS3j1rXNTzMypKJomUKSCM5J8dKiOkuFsqX0tUQyyS0BkRZNy7gDNKQzj43YB4/+p0Y2TQ+tNX1cEdDbLxXIjCohqF3lY3DAExyN7VYYOMHPnHVx+zfYbROjtM1Oru59jtmoNSXyL2Q11JFWSwxDBVcur+7aQSc8NszxkdN4E+ZrwNWL1C/XWJutQYqitt1dWWGllooc7XlVsqT7cDBIH3OOmJpf8SWuLNpyp0rQagNLR1kmZ4P5YZ88Y/QW2kAZ2sM/OerRx9vO3j3GVm7U6ZT0MwotRbabADKmJCojDuNyPgkg+4nnOF7Kztz27orC7L280fHXCVZIo4bNT8n2eGdWx5cYwMbfnI68knThJIqhUK9nNVT0Tpi069vNabnLPQOKSWr9SijVNrryf5eMYxn2jBJ6YOl9DfiMoaGO6Wu3vWUrqTT0l5CLJVQAZEq7m90ZyQp384yB4PR5PHpu01TxWjS1qpqhmbfIlJGjfq/T/LVMjGM53efjnOys1tfYVig/irrFSU6U1LDE2xKaNBhEiGPaoHAHAAUAdJzplvNl0yhPepx/wCvwrb2Hyi65ZItpeOt1EOSzqJAPYx7a+jCgms7k9wLP/L1p2nuUZUDdUUof08Yzge1l8H/ADcDqPqO+ej5QVa1XOjmSL0tphQbP+Ecn28Dg/AHnyWTR3q41tPmW6VLH0jsBlYnLBjyDkeQG+OR1peC1VNbLcaiioql33bpmh9TK4AwWOSftngeehY9Lu4H4kl94z/bFN5+mXRm7X/jNK4Se+KdlHorrIPxpZVvfTTt10PUaEuFqlkpjcDcI55PeSSgTZtI4G1F+/npVNcLKGuVHHTRC31835pYYaUK/q7Aq5II9o54GMhjnPHVorBRX3TNouc0Nq0LURy1Mtca652CCpaGLJG0NMpGxURc4ON25h+rJ5dVQ6wns1PLq2x6UtlJX7aq11VqsEFFLKij3OrQqHeMpKP1e05G3ODhkINQ/wA5R/Kf9VL5OkfQtf8Apkze24AHvEOapno+xfwK4VVKlUK+eQqUgGUZQM53IGycg856YkX+1dZSpSirhoKaKMlI4yF9v0GM8/uei8WDTK3M3KbTtJLXM2DUflIvXcYHuLjLe45PPOAOpN7QtW38u0rD6rYVWiw7H5xxyT0LJpk10fppifIYH4D400tPlJ0/TY+HSdKijPixaVvaC54f6aDrVomlf056yKad3Ab+ccAn54Hx48noitVktsdW7Sw0ayUv8yNnRR6ZBOME/v46I37d327GBIbrHQiLIeP8p62FdTH7wXTG0uGGWA3KoIPjob1F2o7d6AWO66tvM8xKPDGlazMHKgZABYiTA4JYeQThc5atdJhtjw5waB1H5RdS1pg14S+OWTsPYBsPQCkP3j7faKe8V98XXNNHcaht5ow6vk7QMDYCRwPnpKVcVHR0ixLVNJIkoZZI4n9y/OdwBznHI456uFXXDRUMy09L2x0sKdXy0D2iN1fAxljkOScf/UC/bqBagtVzE60ejrDSwlgzxU9tp40U549oyMAD6knptBeparwk5rJ3mk3OqydcFAzVaqWw32704uVPaZXgY+2olkCxLzyHYnjwQB0ZaH7S3zVit69xoKS30wZ3l9fAGMnbuPC+3JyQc4GOm3qbtjeEs73Kjttlkp2ImNNTUgUBMcOFIxnznAz0kO40OqNCXo0lmv8AcKenlij9f8tVSJCHGcqVBHjOPp9OmEF+lztGd6S32kzacuZ1NMWh7T6PhstbXW29y3+JisLpGyq8rYU7Y18yDJ+QuScDx1uotK6zrJ46Swdv5bfp2mmcQ1FXAsXrFhwyNOrZXgM23hchc5bpGUN/1Jb5Y7ratQ3CnnWYOrw1Tq6P/mBByD9+jSfu93a1BNSyah7pawuklP8Ayqf87e6mf04yylkXe52qSEJHAOAfjr2aXgGTSZJeBguOdWzoqKGzWWhjrquFaxIUjlKFXXKjHtYjJHnz5JPWdSX4epLTfLSlTqSKnuUrwh3esQTMoJ4OXB8nrOlZuwTyNPDYSnfIFPfVOhr3V6Q0tSU9po1rNGVM9ND6gZlnpXaTB9NWG5ctBnDKSHl8sEHRpTXOChkiiqK5qiVUCn0lLNuUY85KjJDMFzwGHJzxJUC+rHSvJJIxlrEgYeo2PTPJUDOAM/Ax0I0cjXKpqKmsw8sbRlWCheWznO3Gf69e3JaQDJ2oqHERIWtMvcS1XmoqbbaKuigq3AQtcC485H6FHu/owx8+R0iNddsdRa6oNL3SytG1VAVqJXkp1pYUjkSPKqwLScsgPuz/AE56cGqYaejWqhpqWnRWRwR6Kk/pz5Iz0M2+sqpFakaok9CArFHGGIVFG3AAHjqMDmInFQucTLwtRPZrpQ0B9Gant9CyowSBGMzZAICEjAA4XkDPUpedRyXhY4SaiVYFAUMNsQwAFAA5KjAJycnoFmijp6mKWGNVc7vdjn9R6m6d2SLKsRuxnnqMkjytkmqIIlj/AEa7JZ6hG9aapC+iowgPtQHwB9gAB5+Ooa56ppIm9Yzq7RZxnPuJ6571PMFbEh5IB+46CLrLIlTKFOB7T4+eerQudzXzHhFfF91FRUUdXf6qVmgp1Mj7DkjPwMeTx4HJ4x0FXvuXa6TSM2sdjzUzDbFGTgyMWCjOcYx+48j6jqI7poqaZuMCFljmkpg6hiAd0yZ/bwOpbQtltM+grRQVFvhlpmqXhMUi7lKCSUgEH/yj+w6IhwNiKCfL71BaM7x3rUt+noJbXUUVuiiMySUVvqJp9ykBVIM233e5AxGAW3eASGNdLlqcU9qlWSnh9KSGWuE9WkSTKkis8THBchlypUYwMe74MVqyqnsWhrVVWiT8rLPUSK7RgZK+nJwPoOBwPp0ntQ3e5CGWb825cnJJweT56skOSMVJYi6gE09NWa1tN0nW4VNRQWmFZBJ+SstMxpv0bHVNxQAP7nJwQZJZH2qWI6EKjuDZKRdtFTzSlUCE1E2TIBwvPngYH9PHSmpJpaiON55Xct5yxx/bx1PSxRLbw4jXdxzgZ6rdyGxTCO3UjtVO1XcW8Vnsp2NPESAFj9pA/cjLdGHarWd2tN5eohqPfOPT9RgjMBkEg70fzjGQVI+/wpQArkj5xn/Xoi0azC5QkMeHJ89F6Zw/OgHGRQ98OGL6PanzatC9xu4VfXW2xagWnWKjqa7KLhwAwBUZx6hPq4Ax4BPkL0q/xP6T1VT9ktHT6hpUa5WsmC4oXDshaFkXDg4ILKBuzzvUk/PT37YVtbT1P5qmraiGbZw8UrIwzNEhwQRj2SOv7Mw+epr8XGnLHbO22q7Jb7ZBT0NqtE8tFAgwsDU7N6JX/wAvppj/AMoB6L1tYjOOBcUHpTu0ZLnODVOI4KmotdFc2gzHNTRyGTJHLDIyD9ST/r9OuqyVzg+hFGVhkUtyB8HBwfnk9TvbNVqu2dLWVA3zRTKiOfKqCAAPtgDoOQmW+1MknuZY5WBPwQWAP+p6x80PNQa6FbX+yHHKnNpisSe20gTLMIpEbcQwKjcpGP3B/t9x1VDXbrN3AvdsuFPIaVKyeJUkB/mKrNkr9eQw/cHq4nb2ipl01WERn+XUTxp7jhRhSMc8EEkg+Rk4I6iO71mtM8ly9W3U5aNXKuIwHysjxglhyfZGg5PwT5JJlpds0Lk551DpJdx3kKkLgiqV3jQdpelar0rcKp9pHqUMgHqox5GPt0RaZ0PcJrM9ZLZZZFpjEcoQWwzgSlsH/Cm5v/tOOt+oKWCgqqWejVopJJcOyuckDx89OntE7S6SatfBnqKmSGV8frRUYqCPHB6ZzuEQGs1ZWMF5MocbmiDsvJJLTfl7ZGFikUhMeMAA4P8AbP8AX7jrOtugWNsvVZSUH8mGMllRfALKrH/Vj1nQiHi3FRu4JIJmjDbCv//Z";
    cadena=JSON.parse(localStorage.getItem('imagenLcn'));


    cadena1=cadena.substring(22, cadena.length-1);
    console.log("Cadena: "+cadena1);

    //cadena1=""

    let ruta="http://207.244.229.255:5007/pdfreportes/Lineamiento822.pdf";
    this.lcnService.ObtenerReportePdfLineamiento({
        "idlineamiento": this.actualizarEstado.idlineamiento,
        "imagen64": cadena1}).subscribe((resp)=>{
      this.urlReporte=resp;

      console.log(resp);
      //this.urlReporte=resp;

      if (this.urlReporte.codigo==0){
      console.log("RutaReporte"+this.urlReporte.urldocumento);
      ruta=this.urlReporte.urldocumento
      this.goToLink(ruta);
      this.reset();
      }
      else{
        //alert("Error");

        this.reset();
        this.goToLink(ruta);
        //this.router.navigateByUrl("home");
        
      }
    });




    this.borrarAlerta();
    //alert("El pago ha sido realizado");
    Swal.fire({
      position:'center',
      icon:'success',
      title: "El pago ha sido realizado",
      showConfirmButton: false,
      timer: 2000
    })

  }
  goToLink(url: string){ window.open(url, "_blank"); }

reset(){
  //alert("aaa");
   //this.router.navigate(['dashboard/jurisdiccion/jurisdiccion-ubicacion']);
   location.reload();
};


GenerarQR()
  {

    this.qr.qrGenerar({
      "importe": 1}).subscribe((resp)=>{
    //this.urlReporte=resp;
    //let respuesta=resp;
    //console.log(resp);//
    this.respuestaQr=resp;
    //console.log("RutaReporte"+this.urlReporte.urldocumento);

    this.base64=this.respuestaQr.imagen64;
    this.idqr=this.respuestaQr.idqr

    });
    this.temporizadorDeRetraso();
  }

  /*GenerarQR()
  {
     //alert("Generando QR");
    this.qr.qrCrearSesion(
     {
      "user":"admin",
      "password":"admin123",
      "companyId":"1"
     }
   ).subscribe((result)=>{
     console.warn(result);
     this.objtoken=result;
     console.warn(this.objtoken['transactionIdentifier']);
     this.token=this.objtoken['transactionIdentifier'];
   })

   const header = [{
    attribute: "currency",
    value: "BOB"
    },
    {
      attribute: "gloss",
      value: "PRUEBA GMSCZ"
    },
    {
      attribute: "amount",
      value: "0.5"
    },
    {
      attribute: "singleUse",
      value: "true"
    },
    {
      attribute: "expirationDate",
      value: "2022-12-31"
    },
    {
      attribute: "additionalData",
      value: "Datos adicionales"
    },
    {
      attribute: "destinationAccountId",
      value: "1"
    },
    {
      attribute: "bank",
      value: "BNB"
    },
    {
      attribute: "user",
      value: "admin"
    },
    {
      attribute: "company",
      value: "1"
    }
];

const body = {
  'operation': 'VTO041',
  'header': header

};

console.warn("Datos enviados:" + body);

   this.qr.qrGenerar(body).subscribe((result)=>{
    console.warn(result);
    this.objqr=result;
    console.warn(this.objqr['message']);
    console.warn(this.objqr['status']);
    console.warn(this.objqr['responseList']);
    console.warn(this.objqr['responseList'][0]);
    console.warn(this.objqr['responseList'][0]['response']);
    console.warn(this.objqr['responseList'][0]['response'][0]);
    console.warn(this.objqr['responseList'][0]['response'][0]['identificator']);
    console.warn(this.objqr['responseList'][0]['response'][1]['identificator']);

    //this.idqr.setValue("100");
    this.idqr=this.objqr['responseList'][0]['response'][0]['identificator'];
    this.base64=this.objqr['responseList'][0]['response'][1]['identificator'];

    this.temporizadorDeRetraso()


  })
  }*/

  /*ConsultarQR()
  {

    console.warn("Consultar QR:" + this.idqr);
    const body = { 'operation': this.idqr};
    this.qr.qrConsultar(body).subscribe((result)=>{
     console.warn(result);
     this.objqr=result;
     console.warn(this.objqr['responseList']);
     console.warn(this.objqr['responseList'][0]);
     console.warn(this.objqr['responseList'][0]['response']);
     console.warn(this.objqr['responseList'][0]['response'][0]);
     console.warn(this.objqr['responseList'][0]['response'][0]['identificator']);
     //this.estadoqr=this.objqr['responseList'][0]['response'][0]['identificator'];
     this.estadoqr.setValue(this.objqr['responseList'][0]['response'][0]['identificator']);

     //alert(this.estadoqr.value);

     if (this.estadoqr.value=="PAG"){

      this.borrarAlerta();
      this.Pagar();

     }

   })


  }*/

  ConsultarQR()
  {


    this.qr.qrConsultar({
      "operation": this.idqr}).subscribe((resp)=>{
    //this.urlReporte=resp;
    //let respuesta=resp;
    console.log("IDQR: "+this.idqr);//
    this.respuestaConsultaQr =resp;
    //console.log("RutaReporte"+this.urlReporte.urldocumento);
    this.estadoqr.setValue(this.respuestaConsultaQr.estado);

    if (this.estadoqr.value=="PAG"){

      this.borrarAlerta();
      this.Pagar();

     }

    });



     //alert(this.estadoqr.value);






  }
  temporizadorDeRetraso() {
    //alert("Three seconds have elapsed." +this.identificadorDeTemporizador_tiempo) ;
    this.identificadorDeTemporizador_tiempo=0;
    //this.identificadorDeTemporizador = setInterval(this.funcionConRetraso,this.identificadorDeTemporizador_tiempo, 3000);
    this.identificadorDeTemporizador = setInterval(()=>{
      this.identificadorDeTemporizador_tiempo=this.identificadorDeTemporizador_tiempo+1;
      //alert(this.identificadorDeTemporizador_tiempo);

      this.ConsultarQR()
      if (this.identificadorDeTemporizador_tiempo==100 ){

        this.borrarAlerta();
      }

    }, 5000);

  }

   funcionConRetraso() {
    ////let valor=0;


    this.identificadorDeTemporizador_tiempo=this.identificadorDeTemporizador_tiempo+1;
    alert("Three seconds have elapsed." +this.identificadorDeTemporizador_tiempo) ;
    if (this.identificadorDeTemporizador_tiempo==5 ){      //alert("Three seconds have elapsed.");
      this.borrarAlerta();
    }
  }

   borrarAlerta() {
    clearInterval(this.identificadorDeTemporizador);
  }

  selectionChange(event){


    //this.ProcesoCompleto
    if(event.selectedIndex==3 && this.ProcesoCompleto==1 ){
      this.irSiguiente(0);
      //alert(event.selectedIndex);
      //alert(event.selectedStep.);
    }
  }

}
