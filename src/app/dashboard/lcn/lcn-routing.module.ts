import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LcnComponent } from './lcn/lcn.component';


const routes: Routes = [
  {
    path: '',
    component: LcnComponent
  },  
  {
    path: 'lcn',
    component: LcnComponent
  },   
];



@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LcnRoutingModule { 
  static components = [LcnComponent];
}
