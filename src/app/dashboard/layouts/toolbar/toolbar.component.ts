import { DatePipe } from '@angular/common';
import { Component, EventEmitter, OnInit, Output,ViewChild, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/services/auth.service';
import { DataService } from '../../../services/data.service';
import { PublicidadService } from '../../publicidad/services/publicidad.service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit,AfterViewInit {
  url: string = '';
  idpersona: number=0;
  nombreUsuario: string = "";
  ufv: number=0;
  
  @Output() sidenavToggle = new EventEmitter<boolean>();
  message:string;

  constructor(
    private authService: AuthService,
    private router: Router,
    private publicidadService: PublicidadService,
    public dataService:DataService,
    public datepipe: DatePipe
  ) { }

  ngOnInit(): void {
    const fecha=new Date();
    let fecha_actual =this.datepipe.transform(fecha, 'dd/MM/yyyy');

    this.publicidadService
      .getUFV(fecha_actual)      
      .subscribe((data) => {
        if (data.codigo==0){
          this.ufv = parseFloat(data.mensaje);
        }   
        
      });

    
    this.nombreUsuario = this.authService.getUsuario();
    this.idpersona = this.authService.getidPersona();

    this.url = "";

    this.authService
      .getPersona(this.idpersona, false)
      .subscribe((data) => {       
        if (data.codigo==0){
          if (data.persona.foto.length>0){
            this.url = "data:image/png;base64," + data.persona.foto;
          }
        } else {
          
        }
        
      });
  }

  onToggeleSidenav() {
    // this.appSidenavComponent.toggle();
    this.sidenavToggle.emit();
  }

  onLogout() {
    this.authService.logout();
    this.router.navigate(['auth/login']);
  }

  onFullscreenToggle() {
    const elem = <any>document.querySelector('.dashboard');

    if (elem.requestFullscreen) {
      elem.requestFullscreen();
    } else if (elem.webkitRequestFullScreen) {
      elem.webkitRequestFullScreen();
    } else if (elem.mozRequestFullScreen) {
      elem.mozRequestFullScreen();
    } else if (elem.msRequestFullScreen) {
      elem.msRequestFullScreen();
    }
  }
  ngAfterViewInit(): void {
    //this.message = this.child.message
  }

  perfilFotos(){
    this.router.navigate(['dashboard/perfil_fotos'])
  }
 
}
