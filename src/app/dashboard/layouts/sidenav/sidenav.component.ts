import { BreakpointObserver } from '@angular/cdk/layout';
import { Component, ViewChild, ElementRef, HostListener, AfterContentInit, OnInit } from '@angular/core';
import { finalize } from 'rxjs';
import { AreaMunicipio } from 'src/app/classes/areaMunicipio';
import { AuthService } from 'src/app/core/services/auth.service';
import { Recurso } from 'src/app/interfaces/lcn.interfaces';
import { DataService } from 'src/app/services/data.service';
import { MapasService } from 'src/app/services/mapas.service';
import Swal from 'sweetalert2';

const WIDTH_FOR_RESPONSIVE = 599;

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
})
export class SidenavComponent implements OnInit, AfterContentInit   {
  isFixed = false;
  isOpen = true;

  recursos: Recurso[] = [];
  id;
  cargado : boolean = false;

  @ViewChild('sidenav', { static: true }) sidenavElement!: ElementRef;
 
  public areamunicipio= new AreaMunicipio("","","","","","","");

  @HostListener('window:resize', ['$event']) onResize(e: any) {
    this.changeToResponsiveViewIfNeed(e.target.innerWidth);
  }

  constructor(
    private authService: AuthService,
    private dataService: DataService,
    private mapaService:MapasService,
    private breakpointObserver: BreakpointObserver,
  ) { 
       
  }
  
  ngOnInit(): void {
    this.cargarMenu();
    
    this.id = setInterval(() => {         
      //replaced function() by ()=>
      if (this.recursos.length>0) {
        clearInterval(this.id);
        this.setMenusExperienceScripts();
        this.cargado=true;
      }
      
      // just testing if it is working
    }, 1*1*1000);
  }

  ngOnDestroy() {
    if (this.id) {
      console.log('destruyendo menu');
      clearInterval(this.id);
    }
  }

  ngAfterContentInit(): void {    
    this.changeToResponsiveViewIfNeed(window.innerWidth);
  } 


  setMenusExperienceScripts(): void {
    if (this.cargado) return;
    const submenuArray = this.sidenavElement.nativeElement.querySelectorAll('.submenu');    
    const itemArray = this.sidenavElement.nativeElement.querySelectorAll('dl:not(.submenu) dt');    

    itemArray.forEach((item: Element) => {
      item.addEventListener('click', (e) => {
        submenuArray.forEach((submenu: any) => {          
          submenu.classList.remove('open');
          submenu.querySelector('dd').style.height = '0';
        });
      });
    });

    if (submenuArray.length > 0) {
      submenuArray.forEach((submenu: any) => {
        submenu.setAttribute('default-height', `${submenu.querySelector('dd')?.clientHeight}`);
        submenu.querySelector('dd').style.height = 0;

        submenu.querySelector('dt').addEventListener('click', () => {
          submenu.classList.toggle('open');
          const smCollapsingSection = submenu.querySelector('dd');
          if (smCollapsingSection.style.height === '0px') {
            smCollapsingSection.style.height = smCollapsingSection.parentElement.getAttribute('default-height') + 'px';
          } else {
            smCollapsingSection.style.height = 0;
          }

          [].slice
            .call(submenuArray)
            .filter( sm => sm !== submenu )
            .map((sibiling: any) => {
              sibiling.querySelector('dd').style.height = 0;
              sibiling.classList.remove('open');
            });
        });
      });
    }
  }

  toggle(): void {
    this.isOpen = !this.isOpen;
  }

  private changeToResponsiveViewIfNeed(windowsWidth: number): void {
    if (windowsWidth <= WIDTH_FOR_RESPONSIVE) {
      this.isOpen = false;
      this.isFixed = true;
    } else {
      this.isOpen = true;
      this.isFixed = false;
    }
  }

  cargarMenu(){    
    const idusuario= this.authService.getidUsuario();    
    this.dataService
      .getMenu(idusuario)
      .pipe(finalize(() => {
        console.log('Menú cargado');
      }))
      .subscribe((data) => {
        if (data.codigo == 0) {
          this.recursos = data.recursos;
          localStorage.setItem('idrol', data.idrol.toString());
          this.distritosGeo(this.authService.getIdEmpresa(), parseInt(data.idrol.toString()));
          this.AreaMunicipioLCN(this.authService.getIdEmpresa());
        } else {
          Swal.fire({
            position: 'center',
            icon: 'error',
            title: data.mensaje,
            showConfirmButton: false,
            timer: 3000
          })
        }
      });
  }
  

  public distritosGeo(idempresa:number,idrol:number){
    console.log('valores '+ idempresa +'-' + idrol )
    this.mapaService.obtenerDatosDistritosGeo(idempresa,idrol);
  }

  AreaMunicipioLCN(idempresa: number){

    this.mapaService
     .obtenerDatosAreaMunicipio_php(idempresa) 
     //.pipe(finalize(() => console.log("terminado")))    
     .subscribe(
      async(data)  => {                               
            //console.log( this.areamunicipio[0].xcentro ); 

            
              //Promise.resolve(data);
              this.areamunicipio=  (data) ;
              
                localStorage.setItem('areaMunicipio', JSON.stringify(await this.areamunicipio));
                //alert("entro");
            
    });

   }

   toggleSideNav() {
    const isSmallScreen = this.breakpointObserver.isMatched(
      "(max-width: 599px)"
    );
    if (isSmallScreen) {
      this.isOpen = !this.isOpen;
    }
  }

}
