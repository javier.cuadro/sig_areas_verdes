import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { JurisdiccionRoutingModule } from './jurisdiccion-routing.module';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';




@NgModule({  
  
  declarations: [    
    ...JurisdiccionRoutingModule.components
  ],
  imports: [
    CommonModule,
    JurisdiccionRoutingModule,
    SharedModule,
    LeafletModule,
    NgbModule 
  ]
})
export class JurisdiccionModule { 


}
