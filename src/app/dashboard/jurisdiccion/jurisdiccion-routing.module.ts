import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { JurisdiccionUbicacionComponent } from './jurisdiccion-ubicacion/jurisdiccion-ubicacion.component';

const routes: Routes = [

  {
    path: '',
    component: JurisdiccionUbicacionComponent
  },    
  {
    path: 'jurisdiccion-ubicacion',
    component: JurisdiccionUbicacionComponent
  },   
  
];  

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JurisdiccionRoutingModule {

  static components = [JurisdiccionUbicacionComponent ];

 }
