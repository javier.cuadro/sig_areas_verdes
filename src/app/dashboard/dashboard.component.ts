import { Component, OnInit, ViewChild } from '@angular/core';


import { SidenavComponent } from './layouts/sidenav/sidenav.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  static path = () => ['dashboard'];  

  @ViewChild('appSideNav', { static: false }) appSidenavComponent!: SidenavComponent;  
  //areamunicipio: import("c:/laragon/www/sig_areas_verdes/src/app/classes/areaMunicipio").AreaMunicipio;  

  constructor(    
    
  ) {     
  }

  ngOnInit() {
    
  }

  onSidenavToggle() {
    this.appSidenavComponent.toggle();
  }  
}
