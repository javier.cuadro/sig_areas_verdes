import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/core/services/auth.service';
import Swal from 'sweetalert2';
import { ParametroCambio } from '../../auth/interfaces/auth.interface';

@Component({
  selector: 'app-cambiar-password',
  templateUrl: './cambiar-password.component.html',
  styles: [
  ]
})
export class CambiarPasswordComponent implements OnInit {

  form1 = this.fb.group({     
    clave: ['', [
      Validators.required,
      this.regexValidator(new RegExp('(?=.*?[0-9])'), { 'at-least-one-digit': true }),
      //this.regexValidator(new RegExp('(?=.*[a-z])'), { 'at-least-one-lowercase': true }),
      //this.regexValidator(new RegExp('(?=.*[A-Z])'), { 'at-least-one-uppercase': true }),
      this.regexValidator(new RegExp('(?=.*[.!@#$%^&*])'), { 'at-least-one-special-character': true }),
      this.regexValidator(new RegExp('(^.{8,}$)'), { 'at-least-eight-characters': true }),
    ]],
    clavenueva: ['', [
      Validators.required,
      this.regexValidator(new RegExp('(?=.*?[0-9])'), { 'at-least-one-digit': true }),
      //this.regexValidator(new RegExp('(?=.*[a-z])'), { 'at-least-one-lowercase': true }),
      //this.regexValidator(new RegExp('(?=.*[A-Z])'), { 'at-least-one-uppercase': true }),
      this.regexValidator(new RegExp('(?=.*[.!@#$%^&*])'), { 'at-least-one-special-character': true }),
      this.regexValidator(new RegExp('(^.{8,}$)'), { 'at-least-eight-characters': true }),
    ]],
  })


  constructor(
    public fb: FormBuilder,
    private authService: AuthService,
    private route: ActivatedRoute,
    private router: Router, 
  ) { }

  ngOnInit(): void {
  }

  
  private regexValidator(regex: RegExp, error: ValidationErrors): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (!control.value) {
        return null;
      }
      const valid = regex.test(control.value);
      return valid ? null : error;
    };
  } 

  cambiarPassword(){
    let parametroCambio: ParametroCambio ={
      idusuario: this.authService.getidUsuario(),
      correo_electronico : this.authService.getLogin(),
      clave: this.form1.value.clave,
      clavenueva: this.form1.value.clavenueva
    }

    this.authService
      .Cambiar_Password(parametroCambio)     
      .subscribe(
        data => {         
          
          if (data.codigo == 0) {
            this.router.navigate(['..'], { relativeTo: this.route });   
            Swal.fire({
              position:'center',
              icon:'success',
              title: data.mensaje,
              showConfirmButton: false,
              timer: 2000
            })
          }
          else  {           
            Swal.fire({
              position:'center',
              icon:'warning',
              title: data.mensaje,
              showConfirmButton: false,
              timer: 2000
            })
             
           
          }
        },
        error => {
          Swal.fire({
            position:'center',
            icon:'error',
            title: 'El Servicio se encuentra fuera de línea',
            showConfirmButton: false,
            timer: 2500
          })
        }
    );
  }
}
