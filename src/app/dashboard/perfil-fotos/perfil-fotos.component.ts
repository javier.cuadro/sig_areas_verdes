import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { ParametroPersonaDatos, ParametroPersonaFotos, ParametroPersonaUbicacion } from 'src/app/auth/interfaces/auth.interface';
import { AuthService } from '../../core/services/auth.service';
import { LoadingBackdropService } from '../../core/services/loading-backdrop.service';
import { finalize } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { DatePipe } from '@angular/common';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';

import "leaflet";
declare let L;

export const MY_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'YYYY-MM-DD HH:mm:ss',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};


@Component({
  selector: 'app-perfil-fotos',
  templateUrl: './perfil-fotos.component.html',
  styles: [ `
    .button {
        width: 100%;
        margin: 16px auto;
        display: block;
    }
  
` ],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE],
    },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
    DatePipe,
  ],
})
export class PerfilFotosComponent implements OnInit {
  map;

  i=0;
  lat1=0;
   lng1=0;
   marker;

  latitud: number;
  longitud: number;
  X: number;
  Y: number;


  files: File[] = [];
  files2: File[] = [];
  files3: File[] = [];

  foto_64: string='';
  ci_anverso64: string='';
  ci_reverso64: string='';

  foto; ci_anverso; ci_reverso;

  form1 = this.fb.group({
    documento_identidad: ['', [ Validators.required, ]],
    complemento: [''],
    expedicion: ['', [ Validators.required, ]],
    expiracion_documento: ['', [ Validators.required, ]],
    nombre: ['', [Validators.required,]],
    apellido_paterno:['', [ Validators.required, ]],
    apellido_materno:['', [ Validators.required, ]],    
    fecha_nacimiento:['', [ Validators.required ]],
    genero: ['', [ Validators.required, ]],
    estado_civil: ['', [ Validators.required, ]],
    correo_electronico: ['', [
      Validators.required,
      Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$'),], 
      
  ],
    telefono_fijo: [''],
    telefono_movil:[''],
    nit: [],  
   
  }, );

  form2 = this.fb.group({
    residencia: ['',],
    tipo_lugar: ['', [ Validators.required, ]],
    nombre_lugar: ['', [ Validators.required, ]],
    numero_casa: ['', [ Validators.required, ]],
    direccion_descriptiva: ['', [ Validators.required, ]],
  },
    { updateOn: "blur" }
  );

  form3 = this.fb.group({
    
  },
    { updateOn: "blur" }
  );

  streetMaps =L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
   detectRetina: true,
   attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
   maxZoom: 19,
 });
 

  esri = L.tileLayer(  
    'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
    maxZoom: 19,
    minZoom: 1,
    attribution: 'Tiles © Esri',
  });
  
  
  wmsLayers = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
      layers: "lotes_cartografia_yina",
      format: 'image/png',
      maxZoom: 19,
      transparent: true,
      version: '1.3.0',
      attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
    }
  );
  
  wms_lim_jur = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
      layers: "limite_jurisdiccion",
      format: 'image/png',
      maxZoom: 19,
      transparent: true,
      version: '1.3.0',
      attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
    }
  );
  
  wms_lim_mun = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
      layers: "limite_municipio",
      format: 'image/png',
      maxZoom: 19,
      transparent: true,
      version: '1.3.0',
      attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
    }
  );
 
  //wms_area_mun_uso = L.tileLayer.wms("http://localhost:8080/geoserver/gamsc/wms?",{
   wms_area_mun_uso = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
      layers: "areas_municipales_uso",
      format: 'image/png',
      maxZoom: 19,
      transparent: true,
      version: '1.3.0',
      attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
    }
  );
 
  
  
  wms_lotes = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
      layers: "lotes_cartografia_yina",
      format: 'image/png',
      maxZoom: 19,
      transparent: true,
      version: '1.3.0',
      attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
    }
  );
  wms_Vias = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
         layers: "vias",
         format: 'image/png',
         transparent: true,
         version: '1.3.0',
         attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
       }
     );
  
  
  wms_uv_yina = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
      layers: "unidades_vecinales_yina",
      format: 'image/png',
      maxZoom: 19,
      transparent: true,
      version: '1.3.0',
      attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
    }
  );
  wms_manzanas_yina = L.tileLayer.wms("http://207.244.229.255:9080/geoserver/gamsc/wms?",{
      layers: "manzanas_yina",
      format: 'image/png',
      maxZoom: 19,
      transparent: true,
      version: '1.3.0',
      attribution: '<a href="http://dati.mit.gov.it/catalog/dataset/grafo-stradale-anas">MIT</a>'
    }
  );
 
  
  
  
  options = {
   layers: [ this.streetMaps ],
   zoom: 15,
   center: L.latLng([ -17.7876515, -63.1825049 ]),
   //maxBounds: [[-17.622216426146586, -63.28947607997527],[-17.902058376130743, -63.00586013670787]],
 
   //lat = -17.7876515;
   //lng = -63.1825049;
 };

  constructor(
    public fb: FormBuilder,
    public authService: AuthService,
    private loadingBackdropService: LoadingBackdropService,
    private route: ActivatedRoute,
    private router: Router,
    private datePipe: DatePipe
  ) { }

  ngOnInit(): void {
    this.cargarFotos();
  }

  private regexValidator(regex: RegExp, error: ValidationErrors): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (!control.value) {
        return null;
      }
      const valid = regex.test(control.value);
      return valid ? null : error;
    };
  } 

  private async cargarFotos(){
    this.loadingBackdropService.show();
    this.authService
      .getPersona(this.authService.getidPersona(), true)
      .pipe(finalize(() => this.loadingBackdropService.hide()))      
      .subscribe((data) => {       
        if (data.codigo==0){

          let [dia, mes, anho] = data.persona.fecha_nacimiento.split('/');

          const fecha_nacimiento = new Date(+anho, +mes-1, +dia);

          let fecha_expiracion;
          if (data.persona.expiracion_documento!=null){
            [dia, mes, anho] = data.persona.expiracion_documento.split('/');
            fecha_expiracion = new Date(+anho, +mes-1, +dia);
          } else {
            fecha_expiracion = '';
          }

          this.form1.patchValue({
            documento_identidad: data.persona.documento_identidad ?? '',
            complemento: data.persona.complemento ?? '',
            expedicion: data.persona.expedicion ?? '',
            expiracion_documento: fecha_expiracion,           
            nombre: data.persona.nombre ,
            apellido_paterno: data.persona.apellido_paterno,
            apellido_materno: data.persona.apellido_materno,
            fecha_nacimiento: fecha_nacimiento,
            correo_electronico: data.persona.correo_electronico,
            genero: data.persona.genero || '',
            estado_civil: data.persona.estado_civil || '',
            telefono_fijo: data.persona.telefono_fijo,
            telefono_movil: data.persona.telefono_movil,
            nit: data.persona.nit || ''
          });
         
          this.agregarMarcador(data.persona.latitud, data.persona.longitud);

          this.form2.patchValue({
            residencia: data.persona.residencia ||'',
            tipo_lugar: data.persona.tipo_lugar ||'',
            nombre_lugar: data.persona.nombre_lugar ||'',
            numero_casa: data.persona.numero_casa ||'',
            direccion_descriptiva: data.persona.direccion ||''

          });

         
          if (data.persona.foto != null){
            this.files = [];          
           
            const base64 = data.persona.foto;
            const imageName = 'name.png';
            const imageBlob = this.dataURItoBlob(base64);
            const imageFile = new File([imageBlob], imageName, { type: 'image/png' });

            this.foto = data.persona.foto;
            this.foto_64 = data.persona.foto.replace('data:', '')
                .replace(/^.+,/, ''); // To remove data url part
            
            this.files.push(imageFile);
          }

          if (data.persona.ci_anverso != null){
            this.files2 = [];          
           
            const base64 = data.persona.ci_anverso;
            const imageName = 'name.png';
            const imageBlob = this.dataURItoBlob(base64);
            const imageFile = new File([imageBlob], imageName, { type: 'image/png' });

            this.ci_anverso = data.persona.ci_anverso;
            this.ci_anverso64 = data.persona.ci_anverso.replace('data:', '')
                .replace(/^.+,/, ''); // To remove data url part
            
            this.files2.push(imageFile);
          }

          if (data.persona.ci_reverso != null){
            this.files3 = [];          
           
            const base64 = data.persona.ci_reverso;
            const imageName = 'name.png';
            const imageBlob = this.dataURItoBlob(base64);
            const imageFile = new File([imageBlob], imageName, { type: 'image/png' });

            this.ci_reverso = data.persona.ci_reverso;
            this.ci_reverso64 = data.persona.ci_reverso.replace('data:', '')
                .replace(/^.+,/, ''); // To remove data url part

            this.files3.push(imageFile);
          }

        } else {
          
        }
        
      });
  }

  dataURItoBlob(dataURI) {
    const byteString = window.atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], { type: 'image/png' });    
    return blob;
 }

 

  onSelect(event) {
    this.files = [];
		this.files.push(...event.addedFiles);
    if (this.files && this.files[0]) {
      for (let i = 0; i < this.files.length; i++) {
       this.fileToBase64(this.files[i])
        .then(result=>{
          this.foto = result;
          this.foto_64 = result.replace('data:', '')
          .replace(/^.+,/, ''); // To remove data url part
        });         
      }
    }
	}

  fileToBase64 = (file:File):Promise<string> => {
    return new Promise<string> ((resolve,reject)=> {
         const reader = new FileReader();
         reader.readAsDataURL(file);
         reader.onload = () => resolve(reader.result.toString());
         reader.onerror = error => reject(error);
     })
    }

	onRemove(event) {
		this.files.splice(this.files.indexOf(event), 1);
    this.foto_64='';
    this.foto='';
	}
 
  onSelect2(event) {
    this.files2 = [];
		this.files2.push(...event.addedFiles);
    if (this.files2 && this.files2[0]) {
      for (let i = 0; i < this.files2.length; i++) {
       this.fileToBase64(this.files2[i])
        .then(result=>{
          this.ci_anverso=result;
          this.ci_anverso64= result.replace('data:', '')
          .replace(/^.+,/, ''); // To remove data url part
        });         
      }
    }
	}

	onRemove2(event) {
		this.files2.splice(this.files.indexOf(event), 1);
    this.ci_anverso64 = '';
    this.ci_anverso='';
	}

  onSelect3(event) {
    this.files3 = [];
		this.files3.push(...event.addedFiles);
    if (this.files3 && this.files3[0]) {
      for (let i = 0; i < this.files3.length; i++) {
       this.fileToBase64(this.files3[i])
        .then(result=>{
          this.ci_reverso=result;
          this.ci_reverso64= result.replace('data:', '')          
          .replace(/^.+,/, ''); // To remove data url part
        });         
      }
    }
	}

	onRemove3(event) {
		this.files3.splice(this.files.indexOf(event), 1);
    this.ci_reverso64 = '';
    this.ci_reverso='';
	}
  
  onMapReady(map) {     
    var fitBoundsSouthWest = new L.LatLng(-17.606754259255016, -63.394996841180316);
    var fitBoundsNorthEast = new L.LatLng(-17.9500307832854, -62.98270820363417);
    var fitBoundsArea = new L.LatLngBounds(fitBoundsSouthWest, fitBoundsNorthEast);


    L.Icon.Default.imagePath = "assets/leaflet/";

    let geojson;
    
    this.map = map;
    this.map.setMaxBounds(fitBoundsArea);
  
    var baseMaps = {      
      
      "Satelital":this.esri,
      "General": this.streetMaps
    };       

      var overlayMaps = {
        "Limite Municipio": this.wms_lim_mun,
        "Limite Jurisdicción": this.wms_lim_jur,
        "Uv": this.wms_uv_yina,
        "Manzanas": this.wms_manzanas_yina,                 
        "Lotes": this.wms_lotes, 
                       
      };
      
      L.control.scale().addTo(this.map);

      var markersLayer = new L.LayerGroup();	//layer contain searched elements    
      map.addLayer(markersLayer);  


      setTimeout(() => {
        this.map = map;
        map.invalidateSize();        
      }, 0);

  }

  agregarMarcador(latitud: number, longitud:number){ 
    
    let i = 0;    
    
    this.GeogToUTM(latitud,longitud);

    console.log("Coordenadas en Grados " + latitud+"-" +longitud );
    console.log("Coordenadas en UTM (X Y) "+ this.lat1 +"-" +  this.lng1);
  
    this.latitud = latitud;
    this.longitud = longitud;

   this.X = this.lat1;
    this.Y = this.lng1;

    //this.map.removeLayer(this.marker);
      if (this.i>0){
        this.map.removeLayer(this.marker);
        
      }
      this.marker  = L.marker([this.latitud, this.longitud]).bindPopup('Punto');
      //this.marker.push(LamMarker);
      //LamMarker.addTo(this.map);
      //this.map.fitBounds(this.marker);
      //this.map.flyToBounds(this.marker);
      this.map.addLayer(this.marker);
      this.i=1;

      
    

    var center = L.latLng(this.latitud, this.longitud);
    var bounds = center.toBounds(500);
    this.map.flyToBounds(bounds);
      
  }

  agregarMarcadorJurisdiccion(evt){ 
    
    let i = 0;
    
    var coordenada =  evt.latlng;
    var latitud = coordenada.lat; // lat  es una propiedad de latlng
    var longitud = coordenada.lng; 

    this.GeogToUTM(latitud,longitud);

  console.log("Coordenadas en Grados " + latitud+"-" +longitud );
  console.log("Coordenadas en UTM (X Y) "+ this.lat1 +"-" +  this.lng1);
  
  this.latitud = latitud;
  this.longitud = longitud;

  this.X = this.lat1;
  this.Y = this.lng1;

  //this.map.removeLayer(this.marker);
      if (this.i>0){
        this.map.removeLayer(this.marker);
        
      }
    this.marker  = L.marker([coordenada.lat, coordenada.lng]).bindPopup('Punto');
        //this.marker.push(LamMarker);
        //LamMarker.addTo(this.map);
        //this.map.fitBounds(this.marker);
        this.map.addLayer(this.marker);
        this.i=1;
  }

  GeogToUTM(latd0: number,lngd0 : number){
    //Convert Latitude and Longitude to UTM
    //Declarations();
    let k0:number;
    let b:number;
    let a:number;
    let e:number;
    let f:number;
    let k:number;
    //let latd0:number;
    //let lngd0 :number;
    let lngd :number;
    let latd :number;
    let xd :number;
    let yd :number;
    let phi :number;
    let drad :number;
    let utmz :number;
    let lng :number;
    let latz :number;
    let zcm :number;
    let e0 :number;
    let esq :number;
    let e0sq :number;
    let N :number;
    let T :number;
    let C :number;
    let A :number;
    let M0 :number;
    let x :number;
    let M :number;
    let y :number;
    let yg :number;
    let g :number;
    let usft=1;
    let DatumEqRad:number[];
    let DatumFlat:number[];
    let Item;
  
    let lng0 :number;  
  
    DatumEqRad = [6378137.0, 6378137.0, 6378137.0, 6378135.0, 6378160.0, 6378245.0, 6378206.4,
      6378388.0, 6378388.0, 6378249.1, 6378206.4, 6377563.4, 6377397.2, 6377276.3, 6378137.0];	
      DatumFlat = [298.2572236, 298.2572236, 298.2572215,	298.2597208, 298.2497323, 298.2997381, 294.9786982,
      296.9993621, 296.9993621, 293.4660167, 294.9786982, 299.3247788, 299.1527052, 300.8021499, 298.2572236]; 
      Item = 1;//Default
  
    //drad = Math.PI/180;//Convert degrees to radians)
    k0 = 0.9996;//scale on central meridian
    a = DatumEqRad[Item];//equatorial radius, meters. 
          //alert(a);
    f = 1/DatumFlat[Item];//polar flattening.
    b = a*(1-f);//polar axis.
    e = Math.sqrt(1 - b*b/a*a);//eccentricity
    drad = Math.PI/180;//Convert degrees to radians)
    latd = 0;//latitude in degrees
    phi = 0;//latitude (north +, south -), but uses phi in reference
    e0 = e/Math.sqrt(1 - e*e);//e prime in reference
    N = a/Math.sqrt(1-Math.pow(e*Math.sin(phi),2));
    
    
    T = Math.pow(Math.tan(phi),2);
    C = Math.pow(e*Math.cos(phi),2);
    lng = 0;//Longitude (e = +, w = -) - can't use long - reserved word
    lng0 = 0;//longitude of central meridian
    lngd = 0;//longitude in degrees
    M = 0;//M requires calculation
    x = 0;//x coordinate
    y = 0;//y coordinate
    k = 1;//local scale
    utmz = 30;//utm zone
    zcm = 0;//zone central meridian
  
    k0 = 0.9996;//scale on central meridian
    b = a*(1-f);//polar axis.
    
    e = Math.sqrt(1 - (b/a)*(b/a));//eccentricity
    
  
    lngd=lngd0;
    latd=latd0;
    
    
    
    xd = lngd;
    yd = latd;
  
  
    phi = latd*drad;//Convert latitude to radians
    lng = lngd*drad;//Convert longitude to radians
    utmz = 1 + Math.floor((lngd+180)/6);//calculate utm zone
    latz = 0;//Latitude zone: A-B S of -80, C-W -80 to +72, X 72-84, Y,Z N of 84
    if (latd > -80 && latd < 72){latz = Math.floor((latd + 80)/8)+2;}
    if (latd > 72 && latd < 84){latz = 21;}
    if (latd > 84){latz = 23;}
      
    zcm = 3 + 6*(utmz-1) - 180;//Central meridian of zone
    //alert(utmz + "   " + zcm);
    //Calculate Intermediate Terms
    e0 = e/Math.sqrt(1 - e*e);//Called e prime in reference
    esq = (1 - (b/a)*(b/a));//e squared for use in expansions
    e0sq = e*e/(1-e*e);// e0 squared - always even powers
    //alert(esq+"   "+e0sq)
    N = a/Math.sqrt(1-Math.pow(e*Math.sin(phi),2));
    //alert(1-Math.pow(e*Math.sin(phi),2));
    //alert("N=  "+N);
    T = Math.pow(Math.tan(phi),2);
    //alert("T=  "+T);
    C = e0sq*Math.pow(Math.cos(phi),2);
    //alert("C=  "+C);
    A = (lngd-zcm)*drad*Math.cos(phi);
    //alert("A=  "+A);
    //Calculate M
    M = phi*(1 - esq*(1/4 + esq*(3/64 + 5*esq/256)));
    M = M - Math.sin(2*phi)*(esq*(3/8 + esq*(3/32 + 45*esq/1024)));
    M = M + Math.sin(4*phi)*(esq*esq*(15/256 + esq*45/1024));
    M = M - Math.sin(6*phi)*(esq*esq*esq*(35/3072));
    M = M*a;//Arc length along standard meridian
    
    M0 = 0;//M0 is M for some origin latitude other than zero. Not needed for standard UTM
    
    //Calculate UTM Values
    x = k0*N*A*(1 + A*A*((1-T+C)/6 + A*A*(5 - 18*T + T*T + 72*C -58*e0sq)/120));//Easting relative to CM
    x=x+500000;//Easting standard 
    y = k0*(M - M0 + N*Math.tan(phi)*(A*A*(1/2 + A*A*((5 - T + 9*C + 4*C*C)/24 + A*A*(61 - 58*T + T*T + 600*C - 330*e0sq)/720))));//Northing from equator
    yg = y + 10000000;//yg = y global, from S. Pole
    if (y < 0){y = 10000000+y;}
    //Output into UTM Boxes
          //alert(utmz);
    //console.log(utmz)  ;
    //console.log(Math.round(10*(x))/10 / usft)  ;
    //console.log(Math.round(10*y)/10 /usft)  ;
    this.lat1=Math.round(10*(x))/10 / usft  ;
    this.lng1=Math.round(10*y)/10 /usft;  
  }//close Geog to UTM
  

  guardarDatos(){
    Swal.fire({
      title: 'Está seguro?',
      text: "De Actualizar los Datos Personales!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, quiero actualizar!',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.isConfirmed) {
        let estado_civil: string;
        estado_civil =  this.form1.value.estado_civil;
        estado_civil = estado_civil.substring(0,1);

        let genero: string;
        genero = this.form1.value.genero;
        genero = genero.substring(0,1);
        let parametroPersonaDatos: ParametroPersonaDatos = {
          idpersona: this.authService.getidPersona(),
          nombre: this.form1.value.nombre,      
          apellido_paterno: this.form1.value.apellido_paterno || '',
          apellido_materno: this.form1.value.apellido_materno || '',
          fecha_nacimiento: this.datePipe.transform(this.form1.value.fecha_nacimiento,"dd/MM/yyyy"),
          telefono_fijo: this.form1.value.telefono_fijo  || '',
          telefono_movil: this.form1.value.telefono_movil || '',          
          documento_identidad: this.form1.value.documento_identidad,
          complemento: this.form1.value.complemento,
          expedicion: this.form1.value.expedicion,
          expiracion_documento: this.datePipe.transform(this.form1.value.expiracion_documento,"dd/MM/yyyy"),
          nit: this.form1.value.nit,
          genero: genero,
          estado_civil: estado_civil,
        }

        this.loadingBackdropService.show();
    
        this.authService
          .actualizaDatos(parametroPersonaDatos)
          .pipe(finalize(() => this.loadingBackdropService.hide()))
          .subscribe(
            data => {
              if (data.codigo == 0) {
                
                this.router.navigate(['dashboard']) .then(() => { window.location.reload(); });       
                Swal.fire({
                  position:'center',
                  icon:'success',
                  title: 'Se ha actualizado correctamente',
                  showConfirmButton: false,
                  timer: 2000
                })
              } else {
                Swal.fire({
                  position:'center',
                  icon:'warning',
                  title: data.mensaje,
                  showConfirmButton: false,
                  timer: 2000
                })
              }
             
            },
            error => {
              console.log('error en el componente', error);
              Swal.fire({
                position:'center',
                icon:'error',
                title:'El servicio se encuentra fuera de línea',
                showConfirmButton: false,
                timer: 2000
              });
             
            }
          );
      }
    });
    
  }

  guardarUbicacion(){
    Swal.fire({
      title: 'Está seguro?',
      text: "De Actualizar los Datos de Ubicación!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, quiero actualizar!',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.isConfirmed) {
        
        let parametroPersonaUbicacion: ParametroPersonaUbicacion = {
          idpersona: this.authService.getidPersona(),
          residencia: this.form2.value.residencia,
          tipo_lugar: this.form2.value.tipo_lugar,
          nombre_lugar: this.form2.value.nombre_lugar,
          numero_casa: this.form2.value.numero_casa,
          direccion: this.form2.value.direccion,
          latitud: this.latitud,
          longitud: this.longitud,
          x: this.X,
          y: this.Y,
        }

        this.loadingBackdropService.show();
    
        this.authService
          .actualizaUbicacion(parametroPersonaUbicacion)
          .pipe(finalize(() => this.loadingBackdropService.hide()))
          .subscribe(
            data => {
              if (data.codigo == 0) {
                
                this.router.navigate(['dashboard']) .then(() => { window.location.reload(); });       
                Swal.fire({
                  position:'center',
                  icon:'success',
                  title: 'Se ha actualizado correctamente',
                  showConfirmButton: false,
                  timer: 2000
                })
              } else {
                Swal.fire({
                  position:'center',
                  icon:'warning',
                  title: data.mensaje,
                  showConfirmButton: false,
                  timer: 2000
                })
              }
             
            },
            error => {
              console.log('error en el componente', error);
              Swal.fire({
                position:'center',
                icon:'error',
                title:'El servicio se encuentra fuera de línea',
                showConfirmButton: false,
                timer: 2000
              });
             
            }
          );
      }
    });
    
  }

  guardarFotos(){
    Swal.fire({
      title: 'Está seguro?',
      text: "De Actualizar las fotos de perfil!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, quiero actualizar!',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.isConfirmed) {
        let parametroPersonaFotos: ParametroPersonaFotos = {
          idpersona: this.authService.getidPersona(),
          foto: this.foto_64 ,
          ci_anverso: this.ci_anverso64,
          ci_reverso: this.ci_reverso64,
        }

        this.loadingBackdropService.show();
    
        this.authService
          .actualizaFoto(parametroPersonaFotos)
          .pipe(finalize(() => this.loadingBackdropService.hide()))
          .subscribe(
            data => {
              if (data.codigo == 0) {
                
                this.router.navigate(['dashboard']) .then(() => { window.location.reload(); });       
                Swal.fire({
                  position:'center',
                  icon:'success',
                  title: 'Se ha actualizado correctamente',
                  showConfirmButton: false,
                  timer: 2000
                })
              } else {
                Swal.fire({
                  position:'center',
                  icon:'warning',
                  title: data.mensaje,
                  showConfirmButton: false,
                  timer: 2000
                })
              }
             
            },
            error => {
              console.log('error en el componente', error);
              Swal.fire({
                position:'center',
                icon:'error',
                title:'El servicio se encuentra fuera de línea',
                showConfirmButton: false,
                timer: 2000
              });
             
            }
          );
      }
    });
    
  }
  
}
