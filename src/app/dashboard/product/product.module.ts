import { NgModule } from '@angular/core';

import { ProductRoutingModule } from './product-routing.module';


import {
  NbLayoutModule,
  NbAccordionModule,
  NbButtonModule,
  NbCardModule,
  NbListModule,
  NbRouteTabsetModule,
  NbStepperModule,
  NbTabsetModule, 
  NbUserModule,
} from '@nebular/theme';

//import { ThemeModule } from '@theme/theme.module';

@NgModule({
  declarations: [
    ...ProductRoutingModule.components
  ],
  imports: [
    ProductRoutingModule, 
    NbLayoutModule,   
    NbAccordionModule,
    NbButtonModule,
    NbCardModule,
    NbListModule,
    NbRouteTabsetModule,
    NbStepperModule,
    NbTabsetModule, 
    NbUserModule,
  ]
})
export class ProductModule { }
