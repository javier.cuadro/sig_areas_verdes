import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { finalize } from 'rxjs/operators';

import { ActivatedRoute, Router } from '@angular/router';
import { LcnLineamiento } from '../interfaces/dashboard.interfaces';
import { DashboardService } from '../dashboard.service';
import { LoadingBackdropService } from '../../core/services/loading-backdrop.service';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-lineamientos',
  templateUrl: './lineamientos.component.html',
  styles: [
    `
    table {
      width: 100%;
    }
    `
  ]
})
export class LineamientosComponent implements OnInit {

  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator!: MatPaginator;

  idpersona: number;

  dataSource = new MatTableDataSource<LcnLineamiento>();
  displayedColumns: string[] = [
    'idlineamiento',
    'fechatramite',
    'estadopago',
    'rutapdf',
  ];

  constructor(
    private authService: AuthService,
    private loadingBackdropService: LoadingBackdropService,
    private dashboardService: DashboardService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.idpersona =this.authService.getidPersona();
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.loadLineamientos();
  }

  loadLineamientos(){
  
    this.loadingBackdropService.show();

    this.dashboardService
      .getLineamientos(this.idpersona)
      .pipe(finalize(() => this.loadingBackdropService.hide()))
      .subscribe((data) => {       
        this.dataSource.data = data.lineamientos;
      });
  }

  onLineamientoDetailNavigate(lineamiento: LcnLineamiento) {
    //this.router.navigate([lineamiento.idlineamiento], { relativeTo: this.route });
  }

  goToLink(id: number): string{
    return 'http://207.244.229.255:5007/pdfreportes/lineamiento' + (id-1) + '.pdf';
  }
}
