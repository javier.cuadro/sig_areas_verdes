export class PlanoSuelo{

    /*numerplano: string;
    superficietitulo: string;
    superficiemensura: string;
    zona: string;
    manzana: string;
    distrito: string;
    lote: string;
    tipousosuelo: string;
    ubicacionlote: string;
    tipovia: string;
    idpersona:number;*/

    constructor(
        public numerplano: string,
        public superficietitulo: string,
        public superficiemensura: string,
        public zona: string,
        public manzana: string,
        public distrito: string,
        public lote: string,
        public tipousosuelo: string,
        public ubicacionlote: string,
        public tipovia: string,
        public idpersona:number,
        public uv:string,
        public estadopago:number
    ){}

}