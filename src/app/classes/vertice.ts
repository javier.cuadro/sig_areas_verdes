export class Vertice{
    
    public coordlat: string;
    public coordlng:string;    

    constructor(coordlat:string,coordlng:string){
        this.coordlat =coordlat;
        this.coordlng = coordlng;
    }
}