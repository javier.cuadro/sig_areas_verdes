export class AreaMunicipal{


    /*'id' => $row['id'],
    'nombre' => $row['nombre'],
    'jerarquia' => $row['jerarquia'],
    'categoria' => $row['categoria'],
    'anchovia' => $row['anchovia']  */

    /*'distrito' => $row['distrito'],                            
        'uv' => $row['uv'],
        'sit_legal' => $row['sit_legal'],
        'nom' => $row['nom'],
        'uso' => $row['uso'],
        'uso_especi' => $row['uso_especi'],
        'ley_mcpal' => $row['ley_mcpal'],
        'instrument' => $row['instrument'],
        'matricula' => $row['matricula'],
        'sup_titulo' => $row['sup_titulo'],
        'sup_men' => $row['sup_men'],
        'sup_cc' => $row['sup_cc'],
        'n_plano' => $row['n_plano'],
        'f_emision' => $row['f_emision'],
        'obs' => $row['obs'],
        'n_plano' => $row['n_plano'],
    */

    

    constructor(
    public idareaverde: number,
    public distrito: string,
    public uv: string,
    public sit_legal: string,
    public nom: string,
    public uso: string,
    public uso_especi: string,
    public ley_mcpal: string,
    public instrument: string,
    public matricula: string,
    public sup_titulo: string,
    public sup_men: string,
    public sup_cc: string,
    public n_plano: string,
    public f_emision: string,
    public obs: string,    
    ){
        
    }
}