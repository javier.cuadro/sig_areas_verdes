export class Marcador{
    
    public lat: number;
    public lng:number;
    public grupolineamiento: number;
    public ancho_via :number;
    public esquina :number;// 1 o 0
    public nombrevia :string="";// 1 o 0

    constructor(lat:number,lng:number){
        this.lat =lat;
        this.lng = lng;
    }
}