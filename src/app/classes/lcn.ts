export class LCN{
    /* numerotramite: string;
     oficiodale: string;  
     fechatramite: string; //01/04/2022"
     fotolote: string;
     idgrupolineamiento:number;
     idactividad:number;
     idpersona:number;
     idtipolineamiento:number;
     idplanosuelo:number;
     idexcepcion:number;
     */
 
 
     constructor(
         public numerotramite: string,
         public oficiodale: string,  
         public fechatramite: string, //01/04/2022"
         public fotolote: string,
         public idgrupolineamiento:number,
         public idactividad:number,
         public idpersona:number,
         public idtipolineamiento:number,
         public idplanosuelo:number,
         public idrestriccion:number,
         public id_clasi_activi_regla:number,
         public estadopago:number
     ){}
 }