export class MarcadorLote{
    
    public lat: number;
    public lng:number;
    //public referenc_1: string;
    public dm:string;
    public uv:string;
    public mz:string;
    public lt:string;
    public shape_leng:string;
    /*public ancho_via :number;
    public esquina :number;// 1 o 0
    public nombrevia :string="";// 1 o 0
    */

    constructor(lat:number,lng:number){
        this.lat =lat;
        this.lng = lng;
    }
}