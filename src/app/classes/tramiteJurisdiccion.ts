import { Vertice } from './vertice';
export class TramiteJurisdiccion{      

    /*"superficiemensura": "2805",
    "uv": "5",
    "manzana": "8",
    "lote": "",
    "coordlat": "100",
    "coordlng": "200",
    "estadopago": 0,
    "idpersona": 1,
    "poligono": -1    */
    constructor(
        public superficiemensura: string,
        public uv: string,  
        public manzana: string, //01/04/2022"
        public lote: string,
        public coordlat:string,
        public coordlng:string,
        public estadopago:number,
        public idpersona:number,        
        public poligono:number, 
        public tabla:string,
        public puntos=[]
        
    ){}
}