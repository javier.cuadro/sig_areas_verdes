export class Properties_Lotes{
     
     constructor(
        public id:         number,
        public objectid:   number,
        public dm:         string,
        public uv:         string,
        public mz:         string,
        public lt:         string,
        public codigomuni: string,
        public usodesuelo: number,
        public codigolote: string,
        public shape_leng: string,
        public shape_area: string
     ){}
 }
