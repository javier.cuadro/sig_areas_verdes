import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CoreModule } from './core/core.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { DatePipe } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatSelectModule } from '@angular/material/select';

import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    AppComponent,
  ],  
  imports: [
    BrowserModule,
    AppRoutingModule, // Main routes for application
    CoreModule,
    LeafletModule,
    MatSelectModule,
    NgbModule,
    ReactiveFormsModule,
    
  ],
  providers: [   
    DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
